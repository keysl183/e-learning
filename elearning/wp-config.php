<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'elearning_dev_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kp.YQ&M 8eF2ubaGvP7XxEHW1^w>Iv1eRJJD=a5P]]@<= ]j G^)}r };qRA8cIM');
define('SECURE_AUTH_KEY',  '((Y/^PA EWPeu@9U0kkBjR,*`7e8mjmGY;U;M?9VLB89[A)9J8>O0+OS=Q{6h_Q!');
define('LOGGED_IN_KEY',    'Y&friO*=*0}j+.c6hk$Sl%Y.}oIHy^2`Q-%#x)g+xV-(N7<J-x<sE)l<qfO90WHT');
define('NONCE_KEY',        'B4px_?IR`pV%bb/fK[ |LV,_+bz7Tf1UH6b6CrnuIu&xdC(eLdDs@aK{h_:IJrY(');
define('AUTH_SALT',        '-QF Sg?sT&w.3,?E+AOSCd3-NphB9ZT9D;!@FdWQa/Xq ;^6dLa-zLE$iIeuM&Ap');
define('SECURE_AUTH_SALT', '1=oN4^w&Q5_HE7h*EMphF<A3,qeSF=_:)pt>-RNlk:->OZY>Z0BFm5c_t*y<gYR:');
define('LOGGED_IN_SALT',   '7*DoxpsVw,!M&+vFO~.8Yk]Jt,+(blwgqBG=,{l =.al`I<iV;u]fhg0X[-a=_E6');
define('NONCE_SALT',       'rLb%;e2b!8M4#.{w<^1LGgv?Az+]9liIvQP~uDQF6AQMnCl|u&IQfJ+zaI#uhxG6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

set_time_limit(300);
