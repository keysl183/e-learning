<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$post_id = get_the_ID();
$check_mail  = get_post_meta($post_id, '_lp_learnpress_announcements_send_mail');
if (!empty($check_mail)) {
    $check_mail = $check_mail[0];
    if ($check_mail === 'on') {
        $check_mail = ' checked="checked"';
    }
    else {
        $check_mail = '';
    }
}
else {
    $check_mail = ' checked="checked"';
}
$check_discussion = get_post_meta($post_id, '_lp_learnpress_announcements_display_discussion');

if (!empty($check_discussion)) {

    $check_discussion = $check_discussion[0];

    if ($check_discussion === 'on') {
        $check_discussion = ' checked="checked"';
    }
    else {
        $check_discussion = '';
    }
}
else {
    $check_discussion = ' checked="checked"';
}

?>
<div id="lp-course-<?php the_ID();?>" class="lp-wrap-list-announcements" data-id="<?php the_ID(); ?>">

    <!-- Form Add Announcement -->
    <div class="lp-form-add-announcement">

        <!-- Title -->
        <div class="lp-field-title">
            <input type="text" class="lp-title" placeholder="<?php _e('Title Announcement', 'learnpress-announcements'); ?>">
        </div>
        <!-- End Title -->

        <!-- Content -->
        <div class="lp-field-content">
            <textarea class="lp-content" placeholder="<?php _e('Content Announcement', 'learnpress-announcements'); ?>"></textarea>
        </div>
        <!-- End Content -->

        <!-- List Course -->
        <div class="lp-list-course-select">
            <div class="lp-course-item-select lp-hidden">
                <a href="#" title="<?php _e('Edit Course', 'learnpress-announcements'); ?>"><?php _e('Course Title', 'learnpress-announcements'); ?></a>
                <span class="lp-remove-course">×</span>
            </div>
        </div>
        <button class="button lp-select-courses" type="button" data-action="add-lp_courses" data-type="lp_courses">
            <?php _e('Multi-course', 'learnpress-announcements'); ?>
        </button>
        <!-- End List Course -->

        <!-- Checkbox Send Mail -->
        <div class="lp-field-send-mail">
            <input id="lp-send-mail" type="checkbox" class="lp-send-mail" name="_lp_learnpress_announcements_send_mail" <?php echo $check_mail; ?>>
            <label for="lp-send-mail"><?php _e('Send email for student is enrolled when create new an announcement', 'learnpress-announcements'); ?></label>
        </div>
        <!-- End Checkbox Send Mail -->

        <!-- Checkbox Display Discussion -->
        <div class="lp-field-display-discusson">
            <input id="lp-display-comment" type="checkbox" class="lp-display-discussion" name="_lp_learnpress_announcements_display_discussion" <?php echo $check_discussion; ?>>
            <label for="lp-display-comment"><?php _e('Display the comments on new announcement post', 'learnpress-announcements'); ?></label>
        </div>
        <!-- End Checkbox Display Discussion -->

        <!-- Alert -->
        <div class="lp-alert hide">
            <span class="lp-closebtn">&times;</span>
            <?php _e('Everything is empty. Please enter "Title" and "Content"', 'learnpress-announcements'); ?>
        </div>
        <!-- End Alert -->

        <!-- Button Add Announcement -->
        <div class="lp-field-btn clearfix">
            <button class="lp-add-announcement lp-button button" data-nonce="<?php echo wp_create_nonce('lp-create-announcement') ?>" data-no-title="<?php _e('No Title', 'learnpress-announcements'); ?>"><?php _e('Post', 'learnpress-announcement'); ?></button>
        </div>
        <!-- End Button Add Announcement -->

    </div>
    <!-- End Form Add Announcement -->

    <div class="item-bulk-actions">
        <div class="lp-button">

        </div>
        <span class="lp-check-items">
            <input class="lp-check-all-items" data-action="check-all" type="checkbox">
        </span>
        <button class="button remove-items-announcements" data-title="<?php _e('Remove', 'learnpress-announcements'); ?>" data-confirm="<?php _e('Are you sure you want to remove these items from announcement?', 'learnpress-announcements'); ?>">
            <?php _e('Remove', 'learnpress-announcements'); ?>
        </button>
    </div>
    <table class="list-announcements">
        <tbody>
            <?php

            RWMB_List_Announcements_Field::html_section_item();

            $arg_query = array(
                'post_type' => 'lp_announcements',
                'type' => 'publish',
                'posts_per_page' => '-1',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_lp_course_announcement',
                        'value' => get_the_ID(),
                        'compare' => 'LIKE'
                    ),
                )
            );
            $query = new WP_Query($arg_query);

            if ($query->have_posts()) {

                foreach ($query->posts as $current_post) {

                    RWMB_List_Announcements_Field::html_section_item($current_post->ID);
                }
            }
            ?>
        </tbody>
    </table>
    <?php
        require_once( LP_ADDON_ANNOUNCEMENTS_PATH . '/templates/popup.php' );
    ?>
</div>
