
<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<div class="lp-announcement-not-found">

    <?php
        $not_found = __('Announcements is empty', 'learnpress-announcements');

        apply_filters('learnpress-announcements-not-found', $not_found);

        echo $not_found;

    ?>
</div>
