<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$announcement_id = $announcement->ID;
$title           = $announcement->post_title;
$before_title    = '<h2 class="title">';
$after_title     = '</h2>';
$before_content  = '<div class="lp-announcement-content">';
$content         = $announcement->post_content;
$after_content   = '</div>';

if ( empty( $title ) ) {
	$title = __( 'No Title', 'learnpress-announcements' );
}

if ( empty( $content ) ) {
	$content = __( 'No Content', 'learnpress-announcements' );
}
?>
<div id="lp-announcement-<?php echo esc_attr( $announcement_id ); ?>" class="lp-announcement-item">

	<?php

	apply_filters( 'learnpress-announcements-before-title-item', $before_title );
	apply_filters( 'learnpress-announcements-title-item', $title );
	apply_filters( 'learnpress-announcements-after-title-item', $after_title );

	echo $before_title;
	echo wp_kses_post( $title );
	echo $after_title;

	apply_filters( 'learnpress-announcements-before-content-item', $before_content );
	apply_filters( 'learnpress-announcements-content-item', $content );
	apply_filters( 'learnpress-announcements-after-content-item', $after_content );

	echo $before_content;
	?>
    <div class="lp-announcement-wrap-content">

		<?php

		/* Check role admin or author */
		$user = get_current_user_id();

		if ( current_user_can( 'administrator' ) || $user === (int) $announcement->post_author ) {
			echo apply_filters( 'lp_announcements_edit_post', ' <p class="lp-edit-post"><a href="' . get_edit_post_link( $announcement_id ) . '">' . __( 'Edit', 'learnpress-announcement' ) . '</a></p>' );
		}
		?>

		<?php
		echo wp_kses_post( $content );
		?>
    </div>

	<?php

	$comment_open = comments_open( $announcement_id );

	if ( $comment_open ) {
		learn_press_announcements_template( 'frontend-comments.php', array( 'announcement_id' => $announcement_id ) );
	} else {
		echo apply_filters( 'lp_announcement_hide_comments', __( '<p> Comments is closed </p>', 'learnpress-announcements' ) );
	}

	echo $after_content;

	?>

</div>