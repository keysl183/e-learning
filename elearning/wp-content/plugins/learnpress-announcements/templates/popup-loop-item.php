<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$title = get_the_title();

if (empty($title)) {
    $title = __('No Title', 'learnpress-announcements');
}

?>
<li id="course-<?php the_ID(); ?>" class="lp-course-item" data-text="<?php echo esc_attr($title); ?>" data-type="lp_course" data-id="<?php the_ID(); ?>">
    <label>
        <input type="checkbox" value="<?php the_ID(); ?>">
        <span class="lp-item-text">
            <?php

            echo $title;
            ?>
        </span>
    </label>
</li>
