<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<li class="lp-not-found">
    <?php _e('Course is empty', 'learnpress-announcements')?>
</li>