<?php
global $post;
$post = get_post($announcement_id);
setup_postdata( $post );
if ( comments_open() || get_comments_number() ) {
    comments_template();
}
wp_reset_postdata();