<?php
/*
    Plugin Name: LearnPress - Announcements
    Plugin URI: http://thimpress.com/learnpress
    Description: Announcements add-on for LearnPress
    Author: ThimPress
    Version: 1.1
    Author URI: http://thimpress.com
    Tags: learnpress, lms
    Text Domain: learnpress-announcements
    Domain Path: /languages/
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

define( 'LP_ADDON_ANNOUNCEMENTS_FILE', __FILE__ );
define( 'LP_ADDON_ANNOUNCEMENTS_PATH', dirname( __FILE__ ) );
define( 'LP_ADDON_ANNOUNCEMENTS_URI', plugins_url( '/', LP_ADDON_ANNOUNCEMENTS_FILE ) );
define( 'LP_ADDON_ANNOUNCEMENTS_VERSION', '1.1' );

require_once( LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/announcements-functions.php' );
require_once( LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/lp-addon-announcements.php' );

add_action ( 'plugins_loaded', array( 'LP_Addon_Announcements', 'instance' ));
