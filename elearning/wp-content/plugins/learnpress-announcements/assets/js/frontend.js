/**
 * Created on 23/11/2016.
 */

'use strict';

(function ($){
    
    $(document).ready( function () {

        /* Find comment active */
        var hash = window.location.hash,
            $anchor = $(hash),
            index = 0;


        if ($anchor.length) {
            index = $anchor.closest('.lp-announcement-item').index();
        }

        $('#lp-announcements').accordion({
            header: '.title',
            heightStyle: 'content',
            icons: {
                collapsible: true,
                header: 'lp-announcement-plus',
                activeHeader: 'lp-announcement-minus'
            },
            collapsible: true,
            active: index
        });
    });

})(jQuery);