<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'LP_Announcement_Post_Type' ) ) {

    final class LP_Announcement_Post_Type extends LP_Abstract_Post_Type {

        protected static $_instance = null;

        public function __construct( $post_type ) {
            add_action( 'init', array( __CLASS__, 'load_text_domain' ), 100 );
            parent::__construct( $post_type );
        }

        public function admin_scripts() {
            $screen = get_current_screen();

            if (!empty($screen) && $screen->id === 'edit-lp_announcements') {
                wp_enqueue_style( 'lp_announcements', LP_ADDON_ANNOUNCEMENTS_URI . 'assets/css/admin.css', array(), LP_ADDON_ANNOUNCEMENTS_VERSION );
                wp_enqueue_script( 'lp_announcements', LP_ADDON_ANNOUNCEMENTS_URI . 'assets/js/admin.js', array('jquery'), LP_ADDON_ANNOUNCEMENTS_VERSION, true);
            }
        }

        public static function load_text_domain() {

            if ( function_exists( 'learn_press_load_plugin_text_domain' ) ) {
                learn_press_load_plugin_text_domain( LP_ADDON_ANNOUNCEMENTS_PATH, true );
            }

        }

        public function register() {
            $labels = array(
                'name' => __( 'Announcements', 'learnpress-announcements' ),
                'add_new_item' => __( 'Add Item', 'learnpress-announcements' ),
                'edit_item' => __( 'Edit Item', 'learnpress-announcements' ),
                'new_item' => __( 'New Item', 'learnpress-announcements' ),
                'view_item' => __( 'View Item', 'learnpress-announcements' ),
                'search_items' => __( 'Search Items', 'learnpress-announcements' ),
                'not_found' => __( 'No items found', 'learnpress-announcements' ),
                'not_found_in_trash' => __( 'No items found in Trash', 'learnpress-announcements' ),
                'all_items' => __( 'Announcements', 'learnpress-announcements' ),
                'archives' => __( 'Item Archives', 'learnpress-announcements' ),
                'insert_into_item' => __( 'Insert item', 'learnpress-announcements' ),
                'uploaded_to_this_item' => __( 'Uploaded to this item', 'learnpress-announcements' )
            );

            $args = array(
                'label' => __('Announcements', 'learnpress-announcements'),
                'description' => __('Announcement', 'learnpress-announcements'),
                'labels' => $labels,
                'public' => false,
                'exclude_from_search' => false,
                'show_ui' => true,
                'show_in_menu' => 'learn_press',
                'hierarchical' => false,
                'capability_type' => 'post',
                'supports' => array( 'title', 'editor', 'comments' )
            );

            return $args;

        }

        public function add_meta_boxes() {

            if (class_exists('RW_Meta_Box')) {

                require_once( LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/lp-field.php' );

                $prefix     = '_lp_announcements_';
                $meta_box   = array(
                    'title'      => __( 'Announcements', 'learnpress-announcements' ),
                    'post_types' => 'lp_course',
                    'context'    => 'normal',
                    'priority'   => 'high',
                    'autosave'   => true,
                    'fields' => array(
                        array(
                            'name' => __( 'Announcements', 'learnpress-announcements' ),
                            'id'   => "{$prefix}list_announcements",
                            'desc' => __( 'Click the button "Send Mail" to send the new announcement for all students who were enrolled this course', 'learnpress-announcements' ),
                            'type' => 'list_announcements',
                            'std' => ''
                        ),
                        array(
                            'name'  => __('Display Comments', 'learnpress-announcements'),
                            'id'    => "{$prefix}display_comments",
                            'desc'  => __('Allow the users who is enrolled comment for the all announcements', 'learnpress-announcements'),
                            'type'  => 'checkbox',
                            'std'   => 'true'
                        )
                    )
                );

                apply_filters( 'learn_press_announcements_meta_box_args', $meta_box );

                new RW_Meta_Box($meta_box);
            }
        }

        public function columns_head($columns) {

            $keys   = array_keys( $columns );
            $values = array_values( $columns );
            $pos    = array_search( 'title', $keys );

            if ( $pos !== false ) {
                array_splice( $keys, $pos + 1, 0, array( 'course', 'author') );
                array_splice( $values, $pos + 1, 0, array(
                    __( 'Course', 'learnpress-announcements' ),
                    __( 'Author', 'learnpress-announcements' ) ) );
                $columns = array_combine( $keys, $values );
            }
            else {
                $columns['course'] = __( 'Course', 'learnpress-announcements' );
                $columns['author'] = __( 'Author', 'learnpress-announcements' );
            }

            return $columns;

        }

        public function sortable_columns($columns) {
            $columns['author'] = 'author';

            return $columns;
        }

        public function columns_content($column, $post_id = 0) {

            global $post;

            switch( $column ) {

                case 'course':
                    $courses_id = get_post_meta($post_id, '_lp_course_announcement');
                    $flag = false;

                    foreach ($courses_id as $course_id) {

                        $course = get_post($course_id);

                        if (!empty($course)) {
                            echo '<div class="lp-announcement-column-course"><a href="' . esc_url( add_query_arg( array( 'filter_course' => $course->ID ) ) ) . '">' . get_the_title( $course->ID ) . '</a>';
                            echo '<div class="row-actions">';
                            printf( '<a href="%s">%s</a>', admin_url( sprintf( 'post.php?post=%d&action=edit', $course->ID ) ), __( 'Edit', 'learnpress-announcements' ) );
                            echo "&nbsp;|&nbsp;";
                            printf( '<a href="%s">%s</a>', get_the_permalink( $course->ID ), __( 'View', 'learnpress-announcements' ) );
                            echo "&nbsp;|&nbsp;";
                            if ( $course_id = learn_press_get_request( 'filter_course' ) ) {
                                printf( '<a href="%s">%s</a>', remove_query_arg( 'filter_course' ), __( 'Remove Filter', 'learnpress-announcements' ) );
                            } else {
                                printf( '<a href="%s">%s</a>', add_query_arg( 'filter_course', $course->ID ), __( 'Filter', 'learnpress-announcements' ) );
                            }
                            echo '</div></div>';

                            $flag = true;
                        }

                    }

                    if (!$flag) {
                        _e('Not assigned yet', 'learnpress-announcements');
                    }

                    break;
            }

        }

        private function _get_orderby() {
            return isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : '';
        }

        private function _get_search() {
            return isset( $_REQUEST['s'] ) ? $_REQUEST['s'] : false;
        }

        private function _is_archive() {
            global $pagenow, $post_type;
            if ( !is_admin() || ( $pagenow != 'edit.php' ) || ( 'lp_announcements' != $post_type ) ) {
                return false;
            }
            return true;
        }

        private function _filter_course() {
            return !empty( $_REQUEST['filter_course'] ) ? absint( $_REQUEST['filter_course'] ) : false;
        }

        function posts_fields( $fields ) {
            if ( !$this->_is_archive() ) {
                return $fields;
            }

            $fields = " DISTINCT " . $fields;
            return $fields;
        }

        public function posts_join_paged( $join ) {
            if ( !$this->_is_archive() ) {
                return $join;
            }
            global $wpdb;
            if ( $this->_filter_course() || ( $this->_get_orderby() == 'course-name' ) || ( $this->_get_search() ) ) {
                $join .= " LEFT JOIN {$wpdb->prefix}postmeta mt ON (mt.post_id = {$wpdb->posts}.ID)";
            }
            return $join;
        }

        public function posts_where_paged($where) {

            if ( !$this->_is_archive() ) {
				return $where;
			}
			global $wpdb;
			if ( $course_id = $this->_filter_course() ) {
                $where .= $wpdb->prepare( " AND mt.meta_key = '_lp_course_announcement' AND mt.meta_value = %d", $course_id );
            }
            if ( isset( $_GET['s'] ) ) {
                $s     = $_GET['s'];
                $where = preg_replace(
                    "/\.post_content\s+LIKE\s*(\'[^\']+\')\s*\)/",
                    " .post_content LIKE '%$s%' ) OR ({$wpdb->posts}.post_title LIKE '%$s%' )", $where
                );
            }

			return $where;
        }

        public static function instance() {
            if ( !self::$_instance ) {

                self::$_instance = new self( 'lp_announcements' );
            }
            return self::$_instance;
        }
    }
    $announcement_post_type = LP_Announcement_Post_Type::instance();

}
