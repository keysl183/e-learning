<?php
/**
 * Announcements functions.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function learn_press_announcements_template( $name, $args = null ) {
	return learn_press_get_template( $name, $args, learn_press_template_path() . '/addons/announcements/', LP_ADDON_ANNOUNCEMENTS_PATH . '/templates/' );
}