<?php

/**
 * Class LP_Email_Announcement
 */

defined( 'ABSPATH' ) || exit();

if ( !class_exists( 'LP_Email_Announcement' ) ) {

	class LP_Email_Announcement extends LP_Email {

		public function __construct() {

			add_filter( 'learn_press_section_callback_emails_announcements', array( $this, 'learn_press_section_callback_announcements' ) );
			add_action( 'wp_ajax_rwmb_send_mail_announcements', array( __CLASS__, 'ajax_send_mail_announcements' ) );

			$this->id    = 'announcements';
			$this->title = __( 'Announcement', 'learnpress-announcements' );

			$this->template_base = LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/email/';

			$this->template_html  = 'email-text-settings.php';
			$this->template_plain = 'email-plain-settings.php';

			$this->default_subject = __( '[{{site_title}}] You have a new announcement ({{announcement_name}})', 'learnpress-announcements' );
			$this->default_heading = __( 'New Announcement', 'learnpress-announcements' );

			$this->support_variables = array(
				'{{site_url}}',
				'{{site_title}}',
				'{{site_admin_email}}',
				'{{site_admin_name}}',
				'{{login_url}}',
				'{{header}}',
				'{{footer}}',
				'{{email_heading}}',
				'{{footer_text}}',
				'{{announcement_id}}',
				'{{announcement_name}}',
				'{{announcement_excerpt}}',
				'{{announcement_content}}',
				'{{course_id}}',
				'{{course_name}}',
				'{{course_url}}',
				'{{user_id}}',
				'{{user_name}}',
				'{{user_email}}',
				'{{user_profile_url}}'
			);

			parent::__construct();

		}

		public function ajax_send_mail_announcements() {

			$error              = __( 'Error! Please try again!', 'learnpress-announcements' );
			$error_course       = __( 'Error! The course isn\'t created or is removed.', 'learnpress-announcements' );
			$error_announcement = __( 'Error! The announcement does not exist or is removed.', 'learnpress-announcements' );

			if ( isset( $_POST['action'] ) && $_POST['action'] === 'rwmb_send_mail_announcements'
				&& isset( $_POST['announcement_id'] ) && !empty( $_POST['announcement_id'] )
				&& isset( $_POST['course_id'] ) && !empty( $_POST['course_id'] )
			) {

				/* All user is enrolled the course */
				$course   = LP_Course::get_course( $_POST['course_id'] );
				$all_user = $course->get_students_list( true );

				$status_announcement = get_post_status( $_POST['announcement_id'] );
				$status_course       = get_post_status( $_POST['course_id'] );

				if ( empty( $status_course ) || $status_course == 'trash' ) {

					echo $error_course;

					wp_die();
				}

				if ( empty( $status_announcement ) || $status_announcement == 'trash' ) {

					echo $error_announcement;

					wp_die();
				}

				if ( count( $all_user ) ) {

					foreach ( $all_user as $user ) {

						$new_class = new LP_Email_Announcement();
						$new_class->trigger( $_POST['announcement_id'], $_POST['course_id'], $user->ID );
					}
				}

				echo 'Success';

				wp_die();
			}

			if ( !isset( $_POST['course_id'] ) ) {

				echo $error_course;

				wp_die();
			}

			if ( !isset( $_POST['announcement_id'] ) ) {

				echo $error_announcement;

				wp_die();
			}

			echo $error;

			wp_die();
		}

		public function admin_options( $settings_class ) {
			include( LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/email/email-settings.php' );
		}

		public function learn_press_section_callback_announcements() {
			return array( $this, 'output_section_announcements' );
		}

		public function output_section_announcements() {
			if ( $email = $this->get_email_class( 'announcements' ) ) {
				$email->admin_options( $email );
			}
		}

		public function get_email_class( $id ) {
			$emails = LP_Emails::instance()->emails;
			if ( $emails )
				foreach ( $emails as $email ) {
					if ( $email->id == $id ) {
						return $email;
					}
				}
			return false;
		}

		public function trigger( $announcement_id, $course_id, $user_id ) {

			if ( !( $user = learn_press_get_user( $user_id ) ) ) {
				return;
			}

			$format          = $this->email_format == 'plain_text' ? 'plain' : 'html';
			$announcement    = get_post( $announcement_id );
			$course          = get_post( $course_id );
			$this->object    = $this->get_common_template_data(
				$format,
				array(
					'announcement_id'      => $announcement_id,
					'announcement_name'    => $announcement->post_title,
					'announcement_excerpt' => $announcement->post_excerpt,
					'announcement_content' => $announcement->announcement_content,
					'course_id'            => $course_id,
					'course_name'          => $course->post_title,
					'course_url'           => get_the_permalink( $course_id ),
					'user_id'              => $user_id,
					'user_name'            => learn_press_get_profile_display_name( $user ),
					'user_email'           => $user->user_email,
					'user_profile_url'     => learn_press_user_profile_link( $user->id )
				)
			);
			$this->variables = $this->data_to_variables( $this->object );

			$this->object['user'] = $user;
			$this->recipient      = $user->user_email;
			$return               = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			return $return;
		}

		public function get_recipient() {
			if ( !empty( $this->object['user'] ) ) {
				$this->recipient = $this->object['user']->user_email;
			}
			return parent::get_recipient();
		}
	}
}

return new LP_Email_Announcement();