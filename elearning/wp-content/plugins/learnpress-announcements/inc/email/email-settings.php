<?php
/**
 * Display general settings for emails
 *
 * @author  ThimPress
 * @package LearnPress/Admin/Views/Emails
 * @version 1.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$settings = LP()->settings;
?>
<h3><?php _e( 'Announcements', 'learnpress' ); ?></h3>
<p class="description">
    <?php _e( 'Send this email to user when have new announcement.', 'learnpress-announcements' ); ?>
</p>
<table class="form-table">
    <tbody>
    <?php do_action( 'learn_press_before_' . $this->id . '_' . $this->section['id'] . '_settings_fields', $settings ); ?>
    <tr>
        <th scope="row">
            <label for="learn-press-emails-announcements-course-subject"><?php _e( 'Subject', 'learnpress-announcements' ); ?></label></th>
        <td>
            <input id="learn-press-emails-announcements-course-subject" class="regular-text" type="text" name="<?php echo LP_Settings_Emails::get_field_name( 'emails_announcements[subject]' ); ?>" value="<?php echo $settings->get( 'emails_announcements.subject', $this->default_subject ); ?>" />

            <p class="description">
                <?php printf( __( 'Email subject, default: <code>%s</code>', 'learnpress' ), $this->default_subject ); ?>
            </p>
        </td>
    </tr>
    <tr>
        <th scope="row">
            <label for="learn-press-emails-announcements-course-heading"><?php _e( 'Heading', 'learnpress-announcements' ); ?></label></th>
        <td>
            <input id="learn-press-emails-announcements-course-heading" class="regular-text" type="text" name="<?php echo LP_Settings_Emails::get_field_name( 'emails_announcements[heading]' ); ?>" value="<?php echo $settings->get( 'emails_announcements.heading', $this->default_heading ); ?>" />

            <p class="description">
                <?php printf( __( 'Email heading, default: <code>%s</code>', 'learnpress-announcements' ), $this->default_heading ); ?>
            </p>
        </td>
    </tr>
    <?php
    $view = LP_ADDON_ANNOUNCEMENTS_PATH . '/inc/email/email-template.php';
    include_once $view;
    ?>
    <?php do_action( 'learn_press_after_' . $this->id . '_' . $this->section['id'] . '_settings_fields', $settings ); ?>
    </tbody>
</table>