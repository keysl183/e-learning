<?php

/**
 * Class LP_Addon_Announcements
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class LP_Addon_Announcements {

	protected static $_instance = null;

	private function __construct() {

		/* Actions hook */

		$this->lp_create_announcement_post_type();
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ), 100 );
		add_action( 'learn_press_emails_init', array( $this, 'learnpress_email_announcement' ) );

		// Loading list announcements
		add_action( 'wp_ajax_rwmb_lists_course', array( __CLASS__, 'ajax_lists_course' ) );
		add_action( 'wp_ajax_rwmb_lp_create_announcement', array( __CLASS__, 'ajax_lp_create_announcement' ) );
		add_action( 'wp_ajax_rwmb_lp_remove_announcement', array( __CLASS__, 'ajax_lp_remove_announcement' ) );


		/* Render Frontend */
		add_filter( 'learn_press_course_tabs', array( $this, 'learn_press_course_tab_announcement' ) );
		add_filter( 'learn_press_course_tabs', array( $this, 'learn_press_active_tab_announcements' ), 200 );
		add_action( 'comment_form', array( $this, 'announcement_comment_form' ) );
		add_filter( 'comment_post_redirect', array( $this, 'announcement_comment_post_redirect' ), 100, 2 );

	}

	public function lp_create_announcement_post_type() {
		include_once( 'lp-announcement-post-type.php' );
	}

	public function wp_enqueue_scripts() {

		wp_enqueue_style( 'jquery-ui-accordion', LP_ADDON_ANNOUNCEMENTS_URI . 'assets/css/jquery-ui-accordion.css', array(), LP_ADDON_ANNOUNCEMENTS_VERSION );
		wp_enqueue_style( 'lp_announcements', LP_ADDON_ANNOUNCEMENTS_URI . 'assets/css/frontend.css', array(), LP_ADDON_ANNOUNCEMENTS_VERSION );
		wp_enqueue_script( 'lp_announcements', LP_ADDON_ANNOUNCEMENTS_URI . 'assets/js/frontend.js', array(
			'jquery',
			'jquery-ui-accordion'
		), LP_ADDON_ANNOUNCEMENTS_VERSION, true );

	}

	static function ajax_lists_course() {

		if ( ( isset( $_POST['action'] ) && $_POST['action'] === 'rwmb_lists_course' )
		     && ( isset( $_POST['post_id'] ) && ! empty( $_POST['post_id'] ) )
		) {

			$post = get_post( $_POST['post_id'] );

			$user = $post->post_author;

			if ( empty( $user ) ) {
				wp_die();
			}
			$lp_args = array(
				'post_type'      => 'lp_course',
				'post_status '   => 'publish',
				'posts_per_page' => '-1',
				'author'         => $user
			);

			if ( isset( $_POST['post__not_in'] ) && ! empty( $_POST['post__not_in'] ) ) {
				$lp_args['post__not_in'] = explode( ',', $_POST['post__not_in'] );
			}

			$query = new WP_Query( $lp_args );

			ob_start();

			if ( $query->have_posts() ) {

				while ( $query->have_posts() ) {
					$query->the_post();
					global $post;
					setup_postdata( $post );
					require( LP_ADDON_ANNOUNCEMENTS_PATH . '/templates/popup-loop-item.php' );
				}
				wp_reset_postdata();

			} else {
				require( LP_ADDON_ANNOUNCEMENTS_PATH . '/templates/popup-not-found.php' );
			}

			$result = ob_get_contents();
			ob_clean();
			echo $result;
		}
		wp_die();
	}

	static function ajax_lp_create_announcement() {

		if ( isset( $_POST['action'] ) && $_POST['action'] === 'rwmb_lp_create_announcement'
		     && isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'lp-create-announcement' )
		     && ( ( isset( $_POST['content'] ) && ! empty( $_POST['content'] ) ) || ( isset( $_POST['title'] ) && ! empty( $_POST['title'] ) ) )
		     && isset( $_POST['post_id'] ) && ! empty( $_POST['post_id'] ) ) {

			$title     = urldecode( $_POST['title'] );
			$content   = urldecode( $_POST['content'] );
			$send_mail = false;

			if ( isset( $_POST['send_mail'] ) ) {
				$send_mail = $_POST['send_mail'];
			}


			$args = array(
				'post_status'  => 'publish',
				'post_type'    => 'lp_announcements',
				'post_title'   => $title,
				'post_content' => $content
			);

			$post = get_post( $_POST['post_id'] );

			if ( ! empty( $post ) ) {
				$args['post_author'] = $post->post_author;
			}

			if ( isset( $_POST['display_comment'] ) ) {
				if ( $_POST['display_comment'] === 'true' ) {
					$args['comment_status'] = 'open';
				} else {
					$args['comment_status'] = 'close';
				}
			}

			$current_post = wp_insert_post( $args );

			/* Set multiple metadata for current announcement */

			if ( isset( $_POST['posts_id'] ) && ! empty( $_POST['posts_id'] ) ) {
				$posts_id = explode( ',', $_POST['posts_id'] );

				foreach ( $posts_id as $key => $post_id ) {
					add_post_meta( $current_post, '_lp_course_announcement', $post_id, false );
				}

			}

			$current_time = current_time( 'timestamp' );
			$post_time    = get_the_time( 'U', $current_post );

			if ( ( $current_time - $post_time ) < DAY_IN_SECONDS ) {
				$date = human_time_diff( $post_time, $current_time ) . __( ' ago', 'learnpress-announcement' );
			} else {
				$date = get_the_date( '', $current_post );
			}

			echo json_encode( array(
				'id'        => $current_post,
				'send_mail' => $send_mail,
				'title'     => get_the_title( $current_post ),
				'date'      => $date
			) );

			wp_die();
		}

		echo 'error';

		wp_die();
	}

	static function ajax_lp_remove_announcement() {

		if ( isset( $_POST['action'] ) && $_POST['action'] === 'rwmb_lp_remove_announcement'
		     && isset( $_POST['course_id'] ) && ! empty( $_POST['course_id'] )
		     && isset( $_POST['post_id'] ) && ! empty( $_POST['post_id'] ) ) {

			$course_id = $_POST['course_id'];
			$post_id   = $_POST['post_id'];

			delete_post_meta( $post_id, '_lp_course_announcement', $course_id );

		}

		wp_die();

	}

	public static function learnpress_is_active() {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		return is_plugin_active( 'learnpress/learnpress.php' );
	}

	public function learn_press_course_tab_announcement( $tabs ) {

		$user_id = get_current_user_id();

		if ( ! $user_id ) {
			return $tabs;
		}

		$course_id = get_the_ID();

		$user = new LP_User( $user_id );

		/* Check permission of user is admin or enrolled */
		$roles = $user->user->roles[0];

		if ( $user->has_enrolled_course( $course_id ) || $roles === 'lp_teacher' || $roles === 'administrator' ) {
			$tabs['announcements'] = array(
				'title'    => __( 'Announcements', 'learnpres-announcements' ),
				'priority' => 30,
				'callback' => array( $this, 'learn_press_course_tab_announcement_callback' )
			);
		}

		return $tabs;

	}

	public function learn_press_active_tab_announcements( $tabs ) {

		if ( isset( $_REQUEST['tab'] ) && $_REQUEST['tab'] === 'announcements' ) {
			if ( ! empty( $tabs['announcements'] ) && is_array( $tabs['announcements'] ) ) {
				$tabs['announcements']['active'] = true;
			}
		}

		return $tabs;
	}

	public function learn_press_course_tab_announcement_callback( $key, $tab ) {
		learn_press_announcements_template( 'frontend-announcements.php', array( $key, $tab ) );
	}

	public function announcement_comment_form() {

		global $post;

		$current_post = get_post();

		if ( $post->post_type === 'lp_announcements' ) {

			wp_reset_postdata();

			$parent_post = get_post();

			if ( $parent_post->post_type === 'lp_course' ) {
				?>
                <input type="hidden" name="lp_comment_announcement_from_course_url" value="<?php the_permalink(); ?>"/>
                <input type="hidden" name="lp_comment_announcement_from_course" value="yes"/>
				<?php
			}

			$post = $current_post;
			setup_postdata( $post );

		}

	}

	public function announcement_comment_post_redirect( $location, $comment ) {

		if ( isset( $_REQUEST['lp_comment_announcement_from_course'] ) && ! empty( $_REQUEST['lp_comment_announcement_from_course'] ) ) {

			if ( isset( $_REQUEST['lp_comment_announcement_from_course_url'] ) && ! empty( $_REQUEST['lp_comment_announcement_from_course_url'] ) ) {

				return $_REQUEST['lp_comment_announcement_from_course_url'] . '?tab=announcements#comment-' . $comment->comment_ID;

			}

		}

		return $location;
	}

	public function learnpress_email_announcement( $that ) {

		$that->emails['LP_Email_Announcement'] = include_once( 'lp-email-announcements.php' );

		return $that;
	}

	public static function notifications() {
		?>
        <div class="error">
            <p><?php _e( '<strong>LearnPress Announcements</strong> notice. Please install and active <strong>Learnpress Plugin</strong> before you can using this addon', 'learnpress-announcements' ) ?></p>
        </div>
		<?php
	}

	public static function instance() {
		if ( self::learnpress_is_active() ) {
			if ( ! self::$_instance ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		} else {
			add_action( 'admin_notices', array( __CLASS__, 'notifications' ) );
		}
	}

}