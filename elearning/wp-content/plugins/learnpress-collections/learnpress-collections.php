<?php
/*
Plugin Name: LearnPress - Collections
Plugin URI: http://thimpress.com/learnpress
Description: Collecting related courses into one collection by administrator
Author: ThimPress
Version: 2.1.6
Author URI: http://thimpress.com
Tags: learnpress
Text Domain: learnpress
Domain Path: /languages/
*/

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function learn_press_addon_collections_notice() {
	?>
    <div class="error">
        <p><?php _e( '<strong>Collections for LearnPress</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress_collections' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_collections() {
	if ( defined( 'LEARNPRESS_VERSION' ) && version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__.DIRECTORY_SEPARATOR.'backward.php';
		LP_Addon_Collections::instance();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_collections_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_collections' );
