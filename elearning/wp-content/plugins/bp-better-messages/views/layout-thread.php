<?php
defined( 'ABSPATH' ) || exit;
global $wpdb;
$message_id = false;
if(isset($_GET['message_id'])) $message_id = intval($_GET['message_id']);
$participants = BP_Better_Messages()->functions->get_participants( $thread_id );
if($message_id){
	$stacks = BP_Better_Messages()->functions->get_stacks( $thread_id, $message_id, 'to_message');
} else {
	$stacks = BP_Better_Messages()->functions->get_stacks( $thread_id );
}
?>
<div class="bp-messages-wrap bp-messages-wrap-main">
    <div class="chat-header">
        <?php do_action('bp_better_messages_thread_pre_header'); ?>
        <a href="<?php echo BP_Better_Messages()->functions->get_link(); ?>" class="back ajax"><i class="fas fa-chevron-left" aria-hidden="true"></i></a>
        <?php if(count($participants['links']) < 2) {
            echo implode( ', ', $participants[ 'links' ] );
        } else {
            echo '<strong>' . BP_Better_Messages()->functions->get_thread_subject($thread_id) . '</strong>';
            echo ' ('. count($participants['links']) . ' participants)';
        }?>

        <a href="#" class="mobileClose"><i class="fas fa-window-close"></i></a>
    </div>

    <div class="scroller scrollbar-inner thread"
         data-users="<?php echo implode( ',', array_keys( $participants[ 'users' ] ) ); ?>"
         data-users-json="<?php esc_attr_e(json_encode( $participants[ 'users' ] )); ?>"
         data-id="<?php echo $thread_id; ?>">
        <div class="loading-messages">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <div class="list">
            <?php
            if(count($stacks) == 1 && $stacks[0]['user_id'] == 0){ ?>
            <div class="empty-thread">
                <i class="fas fa-comments"></i>
                <span><?php esc_attr_e(' Write the message to start conversation', 'bp-better-messages'); ?></span>
            </div>
            <?php } else {
                foreach ( $stacks as $stack ) {
                    echo BP_Better_Messages()->functions->render_stack( $stack );
                }
            } ?>
        </div>
    </div>

    <span class="writing" style="display: none"></span>

    <?php if( apply_filters('bp_better_messages_can_send_message', true, get_current_user_id(), $thread_id ) ) { ?>
    <div class="reply">
        <form action="" method="POST">
            <div class="message">
                <textarea placeholder="<?php esc_attr_e( "Write your message", 'bp-better-messages' ); ?>" name="message" autocomplete="off"></textarea>
            </div>
            <div class="send">
                <button type="submit"><i class="fas fa-paper-plane" aria-hidden="true"></i></button>
            </div>
            <input type="hidden" name="action" value="bp_messages_send_message">
            <input type="hidden" name="thread_id" value="<?php echo $thread_id; ?>">
            <?php wp_nonce_field( 'sendMessage_' . $thread_id ); ?>
        </form>

        <span class="clearfix"></span>

        <?php do_action( 'bp_messages_after_reply_form', $thread_id ); ?>
    </div>
    <?php } ?>

    <div class="preloader"></div>
</div>