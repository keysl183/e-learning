<?php

defined( 'ABSPATH' ) || exit();

class LP_WC_Emails {

	public function __construct() {
		add_action( 'woocommerce_email_order_details', array( $this, 'order_courses' ), 10, 4 );
	}

	/**
	 * Show order downloads in a table.
	 *
	 * @since 3.2.0
	 * @param WC_Order $order
	 * @param bool $sent_to_admin
	 * @param bool $plain_text
	 * @param string $email
	 */
	public function order_downloads( $order, $sent_to_admin = false, $plain_text = false, $email = '' ) {
		
		$items = $order->get_items();
		var_dump($items);
		exit();
		$show_downloads = $order->has_downloadable_item() && $order->is_download_permitted();

		if ( ! $show_downloads ) {
			return;
		}

		$downloads = $order->get_downloadable_items();
		$columns   = apply_filters( 'woocommerce_email_downloads_columns', array(
			'download-product' => __( 'Product', 'woocommerce' ),
			'download-expires' => __( 'Expires', 'woocommerce' ),
			'download-file'    => __( 'Download', 'woocommerce' ),
		) );

		if ( $plain_text ) {
			wc_get_template( 'emails/plain/email-downloads.php', array( 'order' => $order, 'sent_to_admin' => $sent_to_admin, 'plain_text' => $plain_text, 'email' => $email, 'downloads' => $downloads, 'columns' => $columns ) );
		} else {
			wc_get_template( 'emails/email-downloads.php', array( 'order' => $order, 'sent_to_admin' => $sent_to_admin, 'plain_text' => $plain_text, 'email' => $email, 'downloads' => $downloads, 'columns' => $columns ) );
		}
	}

}

new LP_WC_Emails();
