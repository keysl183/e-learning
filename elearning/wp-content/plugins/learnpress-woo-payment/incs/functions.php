<?php
/**
 * WooCommerce Payments Addon functions
 *
 * @author  ThimPress
 * @version 2.2.1
 * @package LearnPress/Functions
 */

defined( 'ABSPATH' ) || exit();
add_action( 'template_include', 'learn_press_wc_defines' );
function learn_press_wc_defines( $template ) {
	define( 'LP_WC_TEMPLATE', learn_press_template_path() . '/addons/woo-payment/' );

	return $template;
}

/**
 * Get template file for addon
 *
 * @param      $name
 * @param null $args
 */
function learn_press_wc_get_template( $name, $args = null ) {
	learn_press_get_template( $name, $args, LP_WC_TEMPLATE, LP_ADDON_WOOCOMMERCE_PAYMENT_PATH . '/templates/' );
}

function learn_press_wc_locate_template( $name ) {
	// Look in folder learnpress-woo-payment in the theme first
	$file = learn_press_locate_template( $name, 'learnpress', LP_WC_TEMPLATE );

	// If template does not exists then look in learnpress/addons/woo-payment in the theme
	if ( ! file_exists( $file ) ) {
		$file = learn_press_locate_template( $name, LP_WC_TEMPLATE, LP_ADDON_WOOCOMMERCE_PAYMENT_PATH . '/templates/' );
	}

	return $file;
}


add_filter( 'woocommerce_json_search_found_products', 'learn_press_woocommerce_json_search_found_products_callback' );
function learn_press_woocommerce_json_search_found_products_callback( $products ) {
	global $wpdb;
	$term = wc_clean( empty( $term ) ? stripslashes( $_GET['term'] ) : $term );
	$sql  = $wpdb->prepare( "SELECT 
				ID, post_title
			FROM
				{$wpdb->posts}
			WHERE
				post_title LIKE %s
					AND post_type = 'lp_course'
					AND post_status = 'publish'", '%' . $wpdb->esc_like( $term ) . '%' );
	$rows = $wpdb->get_results( $sql );
	foreach ( $rows as $row ) {
		$products[ $row->ID ] = $row->post_title . ' (' . __( 'Course', 'learnpress-woo-payment' ) . ' #' . $row->ID . ')';
	}

	return $products;
}