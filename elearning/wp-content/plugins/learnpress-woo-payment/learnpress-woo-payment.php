<?php
/*
Plugin Name: LearnPress - WooCommerce Payment Methods Integration
Plugin URI: http://thimpress.com/learnpress
Description: Using the payment system provided by WooCommerce
Author: ThimPress
Version: 2.4.8.5
Author URI: http://thimpress.com
Tags: learnpress, woocommerce
Text Domain: learnpress-woo-payment
Domain Path: /languages/
*/

function learn_press_addon_woo_payment_notice() {
    ?>
    <div class="error">
        <p><?php _e( '<strong>LearnPress - WooCommerce Payment Methods Integration</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress-woo-payment' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_woo_payment() {
	if ( defined( 'LEARNPRESS_VERSION' ) && version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__.DIRECTORY_SEPARATOR.'backward.php';
		LP_Woo_Payment_Init::instance();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_woo_payment_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_woo_payment' );

