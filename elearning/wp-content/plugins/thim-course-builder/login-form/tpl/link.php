<div class="thim-sc-login <?php echo esc_attr($params['el_class'])?>">
	<?php
	$html = '';
	if ( is_user_logged_in() ) {
		$html .= '<a href="' . wp_logout_url() . '" title="' . esc_attr( $params['logout_text'] ) . '">' . esc_html( $params['logout_text'] ) . '</a>';
	} else {
		$login_url = thim_get_login_page_url();
		if ( $params['login_url'] ) {
			$login_url = $params['login_url'];
		}
		$html .= '<i class="ion-android-person"></i>
               <a class="register-link" href="' . esc_url( $login_url . '/?action=register' ) . '" title="' . esc_attr( $params['register_text'] ) . '">' . esc_html( $params['register_text'] ) . '</a>' . '/' .
		         '<a href = "' . esc_url( $login_url ) . '" title = "' . esc_attr( $params['login_text'] ) . '" > ' . esc_html( $params['login_text'] ) . ' </a > ';

	}

	echo ent2ncr( $html );
	?>
</div>