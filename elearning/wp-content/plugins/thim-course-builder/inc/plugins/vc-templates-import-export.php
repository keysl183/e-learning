<?php
/**
 * Description: Import & Export Visual Composer templates. Ever wanted to take your designed templates to another site? Now you can.
 * Version: 0.1.0
 * Author: Khoapq
 * Requires at least: 4.4
 * Tested up to: 4.5
 */

if ( ! class_exists( 'Thim_VC_Templates_Import_Export' ) ) {
	class Thim_VC_Templates_Import_Export {

		protected $option_name = 'wpb_js_templates';

		function __construct() {
			add_action( 'admin_menu', array( $this, 'menu_page' ) );
			$this->export_handle();
		}

		public function menu_page() {
			add_submenu_page( 'vc-general', esc_attr__( 'Templates Import & Export', 'course-builder' ), esc_attr__( 'Templates Import & Export', 'course-builder' ), 'manage_options', 'vc-templates-import-export', array(
				$this,
				'page_callback'
			) );
		}


		/**
		 * Render submenu
		 * @return void
		 */
		public function page_callback() { ?>

			<div class="wrap">
				<h2><?php esc_html_e( 'Templates Manager', 'course-builder' ); ?></h2>
				<?php
				if ( isset( $_GET['tab'] ) ) {
					$active_tab = $_GET['tab'];
				} else {
					$active_tab = 'export';
				}
				?>

				<h2 class="nav-tab-wrapper">
					<a href="?page=vc-templates-import-export&tab=export" class="nav-tab <?php echo $active_tab == 'export' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Export', 'course-builder' ); ?></a>
					<a href="?page=vc-templates-import-export&tab=import" class="nav-tab <?php echo $active_tab == 'import' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Import', 'course-builder' ); ?></a>
				</h2>

				<?php if ( $active_tab == 'export' ) : ?>
					<p>
						<a href="?page=vc-templates-import-export&tab=export&action=export" class="button-bottom button-primary"><?php esc_html_e( 'Download Export File', 'course-builder' ); ?></a>
					</p>
				<?php else: ?>
					<?php if ( isset( $_FILES['file-upload'] ) ):
						$uploaded = $this->upload_file();
						$this->import_handle( $uploaded['file'] );
						?>
					<?php else: ?>
						<form action="" id="uploadtemplates" method="post" enctype="multipart/form-data">
							<p>
								<input type="file" name="file-upload" id="file-upload" />
							</p>
							<button class="button-bottom button-primary"><?php esc_html_e( 'Import Templates', 'course-builder' ); ?></button>
						</form>
					<?php endif; ?>
				<?php endif; ?>
			</div><!-- /.wrap -->
			<?php
		}

		/**
		 * Upload JSON file
		 * @return boolean
		 */
		public function upload_file() {
			if ( isset( $_FILES['file-upload'] ) ) {
				add_filter( 'upload_mimes', array( __CLASS__, 'json_upload_mimes' ) );
				$upload = wp_handle_upload( $_FILES['file-upload'], array( 'test_form' => false ) );
				remove_filter( 'upload_mimes', array( __CLASS__, 'json_upload_mimes' ) );

				return $upload;
			}

			return false;
		}

		/**
		 * Add mime type for JSON
		 *
		 * @param array $existing_mimes
		 *
		 * @return string
		 */
		public static function json_upload_mimes( $existing_mimes = array() ) {
			$existing_mimes['json'] = 'application/json';

			return $existing_mimes;
		}


		/**
		 * @param $file
		 */
		public function import_handle( $file ) {
			if ( file_exists( $file ) ) {
				$contents   = file_get_contents( $file );
				$contents   = json_decode( $contents, true );
				$theme_mods = get_option( $this->option_name );

				// Mergers new options and clean
				if ( $theme_mods ) {
					$theme_mods = array_merge( $theme_mods, $contents );
				} else {
					$theme_mods = $contents;
				}

				// Update theme mods
				$update = update_option( $this->option_name, $theme_mods );
				if ( $update ) {
					echo '<div class="notice notice-success is-dismissible"><p><span class="dashicons dashicons-smiley"></span> ' . esc_html__( 'Import successfully!!!', 'course-builder' ) . '</p></div>';
				} else {
					echo '<div class="notice notice-error is-dismissible"><p>' . esc_html__( 'Error: Can\'t import, templates exist.', 'course-builder' ) . '</p></div>';
				}
				wp_delete_file( $file );
			}

		}

		public function export_handle() {
			if ( ( isset( $_GET['page'] ) == 'vc-templates-import-export' ) && ( isset( $_GET['action'] ) == 'export' ) ) {
				$saved_templates = get_option( $this->option_name );
				$text            = json_encode( $saved_templates );
				self::download_file( $text, 'vc_templates.json' );
			}
		}

		public function download_file( $content, $file_name = 'vc_templates.json', $type = 'text/plain' ) {
			if ( headers_sent() ) {
				wp_die( __( 'Something went wrong', 'course-builder' ) );
			}

			header( "Content-type: $type" );
			header( "Content-Disposition: attachment; filename=$file_name" );

			echo $content;
			die();
		}
	}

	$Thim_VC_Templates_Import_Export = new Thim_VC_Templates_Import_Export();
}