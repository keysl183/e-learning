<?php
/*
Plugin Name: LearnPress - Paid Membership Pro Integration
Plugin URI: http://thimpress.com/learnpress
Description: Paid Membership Pro add-on for LearnPress
Author: ThimPress
Version: 2.3.11
Author URI: http://thimpress.com
Tags: learnpress, lms
Text Domain: learnpress-paid-membership-pro
Domain Path: /languages/

*/

function learn_press_addon_paid_membership_pro_notice() {
    ?>
    <div class="error">
        <p><?php _e( '<strong>Paid Memberships Pro for LearnPress</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress-paid-membership-pro' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_paid_membership_pro() {

	// Requires LP installed and activated
	if ( ! defined( 'LEARNPRESS_VERSION' ) ) {
		return;
	}

	if ( version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__.DIRECTORY_SEPARATOR.'backward.php';
		LP_Addon_PMPRO::instance();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_paid_membership_pro_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_paid_membership_pro' );