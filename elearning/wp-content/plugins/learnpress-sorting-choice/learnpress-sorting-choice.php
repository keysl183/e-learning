<?php
/*
Plugin Name: LearnPress - Sorting Choice Question
Plugin URI: http://thimpress.com/learnpress
Description: Sorting Choice provide ability to sorting the options of a question to the right order
Author: ThimPress
Version: 2.1.2
Author URI: http://thimpress.com
Tags: learnpress
Text Domain: learnpress
Domain Path: /languages/
*/
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function learn_press_addon_sorting_choice_notice() {
    ?>
    <div class="error">
        <p><?php _e( '<strong>LearnPress - Sorting Choice Question</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress-random-quiz' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_sorting_choice() {
	if ( defined( 'LEARNPRESS_VERSION' ) && version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__.DIRECTORY_SEPARATOR.'backward.php';
		LP_Addon_Question_Sorting_Choice::load_text_domain();
		LP_Addon_Question_Sorting_Choice::init();
		
	} else {
		add_action( 'admin_notices', 'learn_press_addon_sorting_choice_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_sorting_choice' );