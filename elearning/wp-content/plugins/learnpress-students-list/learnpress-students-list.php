<?php
/*
Plugin Name: LearnPress - Students List
Plugin URI: http://thimpress.com/learnpress
Description: Get students list by filters.
Author: ThimPress
Version: 2.0.2
Author URI: http://thimpress.com
Tags: learnpress
Text Domain: learnpress
Domain Path: /languages/
*/

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function learn_press_addon_students_list_notice() {
    ?>
    <div class="error">
        <p><?php _e( '<strong>LearnPress - Students List</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_students_list() {

	// Requires LP installed and activated
	if ( ! defined( 'LEARNPRESS_VERSION' ) ) {
		return;
	}

	if ( version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__.DIRECTORY_SEPARATOR.'backward.php';
		LP_Addon_Students_List::instance();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_students_list_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_students_list' );

