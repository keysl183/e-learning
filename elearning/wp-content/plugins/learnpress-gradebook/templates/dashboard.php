<?php
/**
 * Template for displaying content of gradebook inuser profile.
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.x.x
 */
defined( 'ABSPATH' ) or exit();

?>
<div id="gradbook-dashboard">

    <h3><?php _e('Gradebook', 'learnpress-gradebook');?></h3>
	<?php
    /**
     *
     */

    learn_press_debug($_REQUEST);
	do_action( 'learn-press/gradbook/dashboard' );
	?>

</div>
