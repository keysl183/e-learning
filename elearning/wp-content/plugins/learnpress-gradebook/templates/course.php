<?php
/**
 * Template for displaying gradebook of a course
 *
 * @author  ThimPress
 * @package LearnPress/Templates/Gradebook
 * @version 3.0.0
 */

defined( 'ABSPATH' ) or die();

if ( ! isset( $course ) ) {
	return;
}

if ( ! is_a( $course, 'LP_Gradebook_Course' ) ) {
	return;
}

?>

    <table>
        <tr>
            <th><?php _e( 'Course', 'learnpress-gradebook' ); ?></th>
            <td>
                <a href="<?php echo $course->get_permalink(); ?>"><?php echo $course->get_title(); ?></a>
            </td>
        </tr>
        <tr>
            <th><?php _e( 'Instructor', 'learnpress-gradebook' ); ?></th>
            <td><?php echo $course->get_instructor_name(); ?></td>
        </tr>
    </table>
<?php if ( $items = $course->get_items() ) { ?>

    <div class="gradebook-top-nav">
        <form method="get">
            <input type="text" name="search" placeholder="<?php _e( 'Student name, email', 'learnpress-gradebook' ); ?>"
                   value="<?php echo esc_html( LP_Request::get( 'search' ) ); ?>">
            <button><?php _e( 'Filter', 'learnpress-gradebook' ); ?></button>
        </form>
    </div>

    <table class="gradebook-list">
        <thead>
        <tr>
            <th class="course-item-user fixed-column">
				<?php _e( 'Student', 'learnpress-gradebook' ); ?>
            </th>
            <th class="user-grade fixed-column">
				<?php _e( 'Completed', 'learnpress-gradebook' ); ?>
            </th>
            <th class="user-grade fixed-column">
				<?php _e( 'Grade', 'learnpress-gradebook' ); ?>
            </th>
			<?php foreach ( $items as $item ) {
				$item = $course->get_item( $item );

				?>
                <th class="course-item-header" title="<?php echo esc_attr( $item->get_title() ); ?>">
                    <div>
						<?php
						echo $item->get_title();
						?>
                    </div>
                </th>
			<?php } ?>
        </tr>
        </thead>
        <tbody>
		<?php
		$query_user = $course->get_users();
		if ( $query_user['items'] ) {
			foreach ( $query_user['items'] as $user_id ) {
				$user        = learn_press_get_user( $user_id );
				$course_data = $user->get_course_data( $course->get_id() );
				?>
                <tr>
                    <th class="course-item-user fixed-column">
						<?php echo $user->get_display_name(); ?>
                        (<a href="mailto:<?php echo $user->get_email(); ?>"><?php echo $user->get_email(); ?></a>)
                    </th>
                    <th class="user-grade fixed-column">
						<?php
						echo sprintf( '%d/%d', $course_data->get_completed_items(), sizeof( $course_data->get_items() ) );
						?>
                    </th>
                    <th class="user-grade fixed-column">
						<?php
						echo $course_data->get_percent_result();
						learn_press_label_html( $course_data->get_status_label() );
						?>
                    </th>
					<?php
					foreach ( $course_data->get_items() as $item ) {
						switch ( $item->get_result( 'grade' ) ) {
							case 'passed':
								$grade = 'passed';
								break;
							case 'failed':
								$grade = 'failed';
								break;
							default:
								$grade = '';
						}

						?>
                        <td class="course-item-data <?php echo $grade; ?>">
							<?php
							switch ( $item->get_post_type() ) {
								case LP_LESSON_CPT:
									echo $item->get_status() ? $item->get_percent_result() : '-';
									break;
								case LP_QUIZ_CPT:
									echo $item->get_status() ? $item->get_percent_result() : '-';
									break;
							}
							?>
                        </td>
					<?php } ?>
                </tr>
				<?php
			}
		}
		?>
        </tbody>
    </table>
    <ul class="list-table-nav">
        <li class="nav-text">
			<?php echo $query_user->get_offset_text(); ?>
        </li>
        <li class="nav-pages">
			<?php $query_user->get_nav_numbers( true ); ?>
        </li>
    </ul>
    <p>
        <a href="<?php echo learn_press_gradebook_export_url( $course->get_id() ); ?>"><?php _e( 'Export', 'learnpress-gradebook' ); ?></a>
    </p>

<?php } else {
	learn_press_display_message( __( 'No data.', 'learnpress-gradebook' ), 'error' );
}
