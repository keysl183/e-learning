<?php
/**
 * @param string $template_name
 * @param array  $args
 */
function learn_press_gradebook_get_template( $template_name, $args = array() ) {
	learn_press_get_template( $template_name, $args, learn_press_template_path() . '/addons/gradebook/', LP_ADDON_GRADEBOOK_PLUGIN_PATH . '/templates/' );
}

/**
 * @param $course_id
 *
 * @return mixed|string
 */
function learn_press_gradebook_export_url( $course_id ) {
	return add_query_arg(
		array(
			'export-gradebook' => $course_id,
			'export-nonce' => wp_create_nonce( 'gradebook-export-course-' . $course_id )
		),
		get_site_url()
	);
}