<?php
/*
Plugin Name: LearnPress - Co-Instructors
Plugin URI: http://thimpress.com/learnpress
Description: Building courses with other instructors
Author: ThimPress
Version: 2.0.6
Author URI: http://thimpress.com
Tags: learnpress, lms, add-on, co-instructor
Text Domain: learnpress-co-instructor
Domain Path: /languages/
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function learn_press_addon_co_instructor_notice() {
	?>
    <div class="error">
        <p><?php _e( '<strong>LearnPress - Co-Instructors</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress-co-instructor' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_co_instructor() {

	// Requires LP installed and activated
	if ( ! defined( 'LEARNPRESS_VERSION' ) ) {
		return;
	}

	if ( version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once __DIR__ . DIRECTORY_SEPARATOR . 'backward.php';
		LP_Addon_Co_Instructor::instance();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_co_instructor_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_co_instructor' );

function learn_press_addon_co_instructor_install() {
	$teacher = get_role( 'lp_teacher' );
	if ( $teacher ) {
		$teacher->add_cap( 'edit_others_lp_lessons' );
		$teacher->add_cap( 'edit_others_lp_courses' );
	}
}

register_activation_hook( __FILE__, 'learn_press_addon_co_instructor_install' );


function learn_press_addon_co_instructor_uninstall() {
	$teacher = get_role( 'lp_teacher' );
	if ( $teacher ) {
		$teacher->remove_cap( 'edit_others_lp_lessons' );
		$teacher->remove_cap( 'edit_others_lp_courses' );
	}
}

register_deactivation_hook( __FILE__, 'learn_press_addon_co_instructor_uninstall' );