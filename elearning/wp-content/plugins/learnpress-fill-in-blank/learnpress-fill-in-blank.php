<?php
/*
Plugin Name: LearnPress - Fill In Blank Question
Plugin URI: http://thimpress.com/learnpress
Description: Supports type of question Fill In Blank lets user fill out the text into one ( or more than one ) space
Author: ThimPress
Version: 2.1.1
Author URI: http://thimpress.com
Tags: learnpress
Text Domain: learnpress-fill-in-blank
Domain Path: /languages/
*/
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function learn_press_addon_fill_in_blank_notice() {
    ?>
    <div class="error">
        <p><?php _e( '<strong>LearnPress - Fill In Blank Question</strong> addon requires upgrading to works with <strong>LearnPress</strong> version 3.0 or higher', 'learnpress-fill-in-blank' ); ?></p>
    </div>
	<?php
}

function learn_press_load_addon_fill_in_blank() {

	// Requires LP installed and activated
	if ( ! defined( 'LEARNPRESS_VERSION' ) ) {
		return;
	}

	if ( version_compare( LEARNPRESS_VERSION, '3.0', '<' ) ) {
		include_once "backward.php";
		LP_Addon_Question_Fill_In_Blank::init();
	} else {
		add_action( 'admin_notices', 'learn_press_addon_fill_in_blank_notice' );
	}
}

add_action( 'plugins_loaded', 'learn_press_load_addon_fill_in_blank' );