<?php
/**
 * Form for display additional info user in profile page
 *
 * @author  ThimPress
 * @version 2.1.1
 */

defined( 'ABSPATH' ) || exit;

?>
<h4 class="title"><?php esc_html_e( 'Personal Information', 'course-builder' ) ?></h4>
<ul class="lp-form-field-wrap additional-information">
	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Email Address', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo $user->user_email ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-at" aria-hidden="true"></i>
			</label>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Age', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo esc_attr( $user_meta['age'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-birthday-cake" aria-hidden="true"></i>
			</label>
		</div>
	</li>
	
	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Country', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo esc_attr( $user_meta['country'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-flag" aria-hidden="true"></i>
			</label>
		</div>
	</li>	

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Zip', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo esc_attr( $user_meta['zip'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-compass" aria-hidden="true"></i>
			</label>
		</div>
	</li>
	
	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Education', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo esc_attr( $user_meta['education'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-graduation-cap" aria-hidden="true"></i>
			</label>
		</div>
	</li>	

	<h4 class="title"><?php esc_html_e( 'Account Information', 'course-builder' ) ?></h4>
	<li class="lp-form-field">
		<label class="lp-form-field-label"><?php esc_html_e( 'Username', 'course-builder' ); ?></label>
		<div class="lp-form-field-input">
			<input type="text" name="display_name" id="display_name" value="<?php echo esc_attr( $user_info->display_name ) ?>" class="regular-text tgb_normal_padd_left" />
		</div>
	</li>


	<!-- <li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Phone Number', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_phone" id="lp_info_phone" value="<?php echo esc_attr( $user_meta['lp_info_phone'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_phone">
				<i class="fa fa-phone" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Email', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="email" id="email" value="<?php echo esc_attr( $user_info->user_email ); ?>" class="regular-text" disabled>
			<label class="icon" for="email">
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
			</label>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Skype', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_skype" id="lp_info_skype" value="<?php echo esc_attr( $user_meta['lp_info_skype'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_skype">
				<i class="fa fa-skype" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Google Plus URL', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_google_plus" id="lp_info_google_plus" value="<?php echo esc_attr( $user_meta['lp_info_google_plus'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_google_plus">
				<i class="fa fa-google-plus" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Facebook URL', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_facebook" id="lp_info_facebook" value="<?php echo esc_attr( $user_meta['lp_info_facebook'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_facebook">
				<i class="fa fa-facebook" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Twitter URL', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_twitter" id="lp_info_twitter" value="<?php echo esc_attr( $user_meta['lp_info_twitter'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_twitter">
				<i class="fa fa-twitter" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li>

	<li class="lp-form-field">
		<h5 class="lp-form-field-label"><?php esc_html_e( 'Pinterest URL', 'course-builder' ); ?></h5>
		<div class="lp-form-field-input">
			<input type="text" name="lp_info_pinterest" id="lp_info_pinterest" value="<?php echo esc_attr( $user_meta['lp_info_pinterest'] ); ?>" class="regular-text">
			<label class="icon" for="lp_info_pinterest">
				<i class="fa fa-pinterest" aria-hidden="true"></i>
			</label>
			<span class="clear-field"><?php esc_html_e( 'Remove', 'course-builder' ) ?></span>
		</div>
	</li> -->
</ul>
<div id="lp-profile-edit-password-form" >
	<label class="lp-form-field-label"><?php esc_attr_e( 'Change Password', 'course-builder' ); ?></label>
	<ul class="lp-form-field-wrap">
		<?php do_action( 'learn_press_before_' . $section . '_edit_fields' ); ?>

		<li class="lp-form-field">
			<div class="lp-form-field-input">
				<input type="password" id="pass0" name="pass0" autocomplete="off" class="regular-text" placeholder="<?php esc_attr_e('Old Password','course-builder') ?>" />
			</div>
		</li>

		<li class="lp-form-field">
			<div class="lp-form-field-input">
				<input type="password" name="pass1" id="pass1" class="regular-text" value="" placeholder="<?php esc_attr_e('New Password','course-builder') ?>"/>
			</div>
		</li>

		<li class="lp-form-field">
			<div class="lp-form-field-input">
				<input name="pass2" type="password" id="pass2" class="regular-text" value="" placeholder="<?php esc_attr_e('Confirm New Password','course-builder') ?>"/>
				<p id="lp-password-not-match" class="description lp-field-error-message hide-if-js"><?php esc_html_e( 'New password does not match!', 'course-builder' ); ?></p>
			</div>
		</li>
		<?php do_action( 'learn_press_after_' . $section . '_edit_fields' ); ?>
	</ul>
</div>

