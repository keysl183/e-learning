<?php
/**
 * User Courses tab
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post;
$args = array(
	'user' => $user
);

$paged     = 1;
$userid    = $user->ID;
$limit     = LP()->settings->get( 'profile_courses_limit', 10 );
$limit     = apply_filters( 'learn_press_profile_tab_courses_all_limit', $limit );
$count     = count( $user->get( 'courses' ) );
$courses   = $user->get( 'courses', array( 'limit' => $limit, 'paged' => $paged ) );
$num_pages = learn_press_get_num_pages( $user->_get_found_rows(), $limit );

if ( $courses ) {
	?>
	<div class="learn-press-subtab-content">
		<div class="profile-courses archive-courses courses-list row" data-count='<?php echo esc_attr( $count ); ?>' data-limit='<?php echo esc_attr( $limit ); ?>' data-page='<?php echo esc_attr( $paged ); ?>' data-user="<?php echo esc_attr( $userid ) ?>">
			
		<?php if($user->user->roles[0] == 'lp_teacher' || $user->user->roles[0] == 'administrator' ){  ?>
		<!--LINK TO EXAM MANAGER-->
		<div class="course col-md-4">
		<div class="content">
			<div class="thumbnail">
					<a href="<?php echo get_site_url().'/exam-manager' ?>" class="img_thumbnail"> 
					<img src="<?php echo get_site_url() ?>/wp-content/uploads/2018/04/add_plus_new_user_green_page_file_up_text-512.png" width="365" height="405"/>
					</a>
			</div>
			<div class="thim-course-content">
				<div class="sub-content">
					<div class="title">
						<a href="<?php echo get_site_url().'/exam-manager' ?>"><?php echo 'Create New Course Exam'?></a>
					</div>
					<div class="date-comment">
						Go to Teacher Dashboard to get started
					</div>
				</div>
			</div>
		</div>			
		</div>	
		<?php } ?>	
			
			
			<?php foreach ( $courses as $post ) {
				setup_postdata( $post );
				?>

				<?php
				learn_press_get_template( 'profile/tabs/courses/loop.php', array(
					'user'      => $user,
					'course_id' => $post->ID
				) );
				wp_reset_postdata();
				?>

			<?php } ?>
		</div>
		<?php if ( $count > $limit ) { ?>
			<div id="load-more-button">
				<div class="btn btn-primary btn-lg loadmore">
<!--					<i class="fa fa-spin fa-spinner"></i>-->
					<div class="sk-three-bounce">
						<div class="sk-child sk-bounce1"></div>
						<div class="sk-child sk-bounce2"></div>
						<div class="sk-child sk-bounce3"></div>
					</div>
					<span><?php echo esc_html__( 'Load more', 'course-builder' ); ?></span>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php
} else {
	learn_press_display_message( esc_html__( 'You haven\'t got any courses yet!', 'course-builder' ) );
}
?>
