<?php
/**
 * User Information
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_meta = get_user_meta( $user->ID );
$user_meta = array_map( function ( $a ) {
	return $a[0];
}, $user_meta );

if ( is_user_logged_in() ) :
	?>
	<!-- <div class="list-contact">
		<?php if ( ! empty( $user_meta['lp_info_phone'] ) ): ?>
			<div class="item">
				<a class="contact-icon" href="tel:<?php echo esc_attr( $user_meta['lp_info_phone'] ); ?>" title="<?php esc_attr_e( 'Phone', 'course-builder' ) ?>"><i class="fa fa-phone"></i></a>
				<div class="contact-content">
					<div class="title"><?php esc_html_e( 'Phone Number', 'course-builder' ) ?></div>
					<a href="tel:<?php echo esc_attr( $user_meta['lp_info_phone'] ); ?>"><?php echo esc_html( $user_meta['lp_info_phone'] ); ?></a>
				</div>
			</div>
		<?php endif; ?>

		<div class="item">
			<a class="contact-icon" href="mailto:<?php echo esc_attr( $user->user_email ); ?>" title="<?php esc_attr_e( 'Email', 'course-builder' ) ?>"><i class="fa fa-envelope-o"></i></a>
			<div class="contact-content">
				<div class="title"><?php esc_html_e( 'Email', 'course-builder' ) ?></div>
				<a href="mailto:<?php echo esc_attr( $user->user_email ); ?>"><?php echo esc_html( $user->user_email ); ?></a>
			</div>
		</div>

		<?php if ( ! empty( $user_meta['lp_info_skype'] ) ): ?>
			<div class="item">
				<a class="contact-icon" href="<?php echo esc_url( $user_meta['lp_info_skype'] ); ?>" title="<?php esc_attr_e( 'Skype', 'course-builder' ) ?>"><i class="fa fa-skype"></i></a>
				<div class="contact-content">
					<div class="title"><?php esc_html_e( 'Skype', 'course-builder' ) ?></div>
					<a href="tel:<?php echo esc_attr( $user_meta['lp_info_skype'] ); ?>"><?php echo esc_html( $user_meta['lp_info_skype'] ); ?></a>
				</div>
			</div>
		<?php endif; ?>
	</div> -->

<?php endif; ?>

<div class="info-general" style="display:none">
	<div class="avatar">
		<?php
		echo( $user->get_profile_picture( null, '250' ) ); ?>
	</div>

	<div class="biographical" >
		<h6 class="title"><?php esc_html_e( 'Personal Information', 'course-builder' ) ?></h6>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Email : &nbsp</div><div class="content" ><?php echo($user->user_email); ?></div>
		<div style= "clear:both"></div>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Age : &nbsp</div><div class="content"><?php echo($user_meta['age']); ?></div>
		<div style= "clear:both"></div>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Country : &nbsp</div><div class="content"><?php echo($user_meta['country']); ?></div>
		<div style= "clear:both"></div>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Zip Code : &nbsp</div><div class="content"><?php echo($user_meta['zip']); ?></div>
		<div style= "clear:both"></div>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Education : &nbsp</div><div class="content"><?php echo($user_meta['education']); ?></div>
		<div style= "clear:both"></div>
		<br>
		<h6 class="title"><?php esc_html_e( 'Account Information', 'course-builder' ) ?></h6>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Username : &nbsp</div>
		<div class="content"><?php echo($user->user_login); ?></div>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Password : &nbsp</div>
		<div class="content"><?php echo "********" ?></div>
		<h6 class="title"><?php esc_html_e( 'Payment Information', 'course-builder' ) ?></h6>
		<div class="lp-form-field-label"style="color:black; font-weight:bold;float:left;" >Payment Method : &nbsp</div>
		<div class="content"><?php 
		$a = pmpro_getMembershipLevelForUser($user->ID);
		echo $a->name;
		//var_dump($a);
		?>   &nbsp <a href="<?php  echo get_site_url(). '/membership-cancel/?levelstocancel=4' ?>" style="color:cyan;"> Cancellation</a>  </div> 
		
		<!-- <?php if ( ! empty( $user_meta['description'] ) ) : ?>
			<div class="content"><?php echo( $user_meta['description'] ); ?></div>
		<?php endif; ?> -->
	</div>
</div>
