<?php
/**
 * Group Utilities
 *
 * @package Thim_Starter_Theme
 */

thim_customizer()->add_section(
	array(
		'id'       => 'utilities',
		'panel'    => 'general',
		'title'    => esc_html__( 'Utilities', 'course-builder' ),
		'priority' => 80,
	)
);

// Register Redirect
thim_customizer()->add_field(
	array(
		'type'     => 'text',
		'id'       => 'thim_register_redirect',
		'section'  => 'utilities',
		'label'    => esc_html__( 'Register Redirect', 'course-builder' ),
		'tooltip'  => esc_html__( 'Allows register redirect url. Blank will redirect to current page.', 'course-builder' ),
		'priority' => 20,
	)
);

// Login Redirect
thim_customizer()->add_field(
	array(
		'type'     => 'text',
		'id'       => 'thim_login_redirect',
		'section'  => 'utilities',
		'label'    => esc_html__( 'Login Redirect', 'course-builder' ),
		'tooltip'  => esc_html__( 'Allows login redirect url. Blank will redirect to current page.', 'course-builder' ),
		'priority' => 30,
	)
);