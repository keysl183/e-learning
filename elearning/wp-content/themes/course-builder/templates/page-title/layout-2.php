<?php
/**
 * Template for displaying page title layout 2.
 *
 * @author  ThimPress
 * @package CourseBuilder/Templates
 * @version 1.1
 */

// page title settings
$page_title = thim_page_title();

/**
 * Extract $page_title to parameters
 *
 * @var $show_title
 * @var $show_sub_title
 * @var $show_text
 * @var $title
 * @var $description
 * @var $main_css
 * @var $overlay_css
 * @var $show_breadcrumb
 */
extract( $page_title );
$courses_layout = '';

$top_widgetarea = isset( $_GET['top_widgetarea'] ) ? $_GET['top_widgetarea'] : get_theme_mod( 'learnpress_top_sidebar_archive_display', true );
if ( ( $top_widgetarea === '1' || $top_widgetarea === 'true' ) && ! is_single() ) {
	if ( get_post_type() == 'lp_course' || get_post_type() == 'lp_quiz' || thim_check_is_course() || thim_check_is_course_taxonomy() ) {
		$courses_layout = 'breadcrumb-plus';
	}
}

// Display page title for single forum and single topic
if ( class_exists( 'bbPress' ) ) {
	if ( function_exists( 'bbp_is_single_forum' ) && bbp_is_single_forum() ) {
		$title = '<h1>' . esc_html_x( 'Forum', 'Page title', 'course-builder' ) . '</h1>';
	}

	if ( function_exists( 'bbp_is_single_topic' ) && bbp_is_single_topic() ) {
		$title = '<h1>' . esc_html_x( 'Topic', 'Page title', 'course-builder' ) . '</h1>';
	}
} ?>

<?php if ( $show_text ) { ?>
    <div class="main-top" <?php echo ent2ncr( $main_css ); ?>>
        <span class="overlay-top-header" <?php echo ent2ncr( $overlay_css ); ?>></span>

        <div class="content container">

			<?php if ( $show_title ) { ?>
                <div class="text-title">
					<?php
						global $post;
						if($post->ID== 9) //My Profile
						{
							global $current_user;
							get_currentuserinfo();
							echo '<h1>'. $current_user->user_firstname.' '.$current_user->user_lastname.'</h1>';									
						}else
						{
							echo ent2ncr( $title );
						}
					?>
                </div>
			<?php }; ?>

			<?php if ( $show_sub_title ) { ?>
                <div class="text-description">
					<?php echo ent2ncr( $description ); ?>
                </div>
			<?php } ?>

        </div>
    </div><!-- .main-top -->

	<?php if ( $show_breadcrumb ) { ?>
		<?php if ( ! is_front_page() || ! is_home() ) { ?>
            <div class="breadcrumb-content <?php echo esc_attr( $courses_layout ); ?>">
                <div class="breadcrumbs-wrapper container">
					<?php
					if ( is_singular( 'lp_course' ) ) {
						$layout = isset( $_GET['layout'] ) ? $_GET['layout'] : get_theme_mod( 'learnpress_single_course_style', 1 );
						if ( $layout == 1 && get_post_meta( get_the_ID(), '_lp_coming_soon', true ) != 'yes' ) {
							learn_press_get_template( 'single-course/buttons.php' );
						}
					} else {
						if ( get_post_type() == 'lp_course' || get_post_type() == 'lp_quiz' || thim_check_is_course() || thim_check_is_course_taxonomy() ) {
							thim_learnpress_breadcrumb();
						} elseif ( get_post_type() == 'product' ) {
							woocommerce_breadcrumb();
						}else if($post->ID == 9 ||$post->ID == 4191)//profile
						{ $user = learn_press_get_current_user();
							?>

						<ul class="tabs-title  breadcrumbs">
							<li class="tab active" data-tab="courses_tab">
							<a href="#" data-slug="#tab-courses" class="color_blck">Courses</a>
							</li>
        
							<li class="tab go-other-page" data-tab="new_course_tab">
							<a href="<?php echo get_site_url().'/messages' ?>" data-slug="#tab-messages" class="color_blck">Messages</a>
							</li>
							<?php  if($user->user->roles[0] == 'lp_teacher' || $user->user->roles[0] == 'administrator' ){ //show this to teachers only?>
							<li class="tab go-other-page" data-tab="discussion_tab">
							<a href="<?php echo get_site_url().'/discussions' ?>" data-slug="#tab-discussion" class="color_blck">Discussions</a>
							</li>

							<li class="tab go-other-page" data-tab="assignment_tab">
							<a href="<?php echo get_site_url().'/assignments' ?>" data-slug="#tab-assignments" class="color_blck">Assignments</a>
							</li>

							<li class="tab go-other-page" data-tab="grades_tab">
							<a href="<?php echo get_site_url().'/grades' ?>" data-slug="#tab-grades" class="color_blck">Grades</a>
							</li>
							<?php }else{ //show this to teachers only?>
							<!--For student only-->
							<li class="tab go-other-page" data-tab="activity_tab">
							<a href="<?php echo get_site_url().'/activity' ?>" data-slug="#tab-activity" class="color_blck">Activity</a>
							</li>
							
							<li class="tab go-other-page" data-tab="reportcard_tab">
							<a href="<?php echo get_site_url().'/reportcard' ?>" data-slug="#tab-reportcard" class="color_blck">Card</a>
							</li>

							<li class="tab go-other-page" data-tab="help_tab">
							<a href="<?php echo get_site_url().'/help' ?>" data-slug="#tab-reportcard" class="color_blck">Help</a>
							</li>							
							<!--END For student only-->
							<?php } ?>
							<li class="tab" data-tab="settings_tab">
							<a href="#" data-slug="#tab-settings"  class="color_blck">Profile</a>
							</li>
							
						</ul>
						<div class="tabs-content">
							<!--Question Bank List-->
							<div class="content active">
								<div class="entry-tab-inner" id="tab-course_exams">

								</div>
							</div>

							<!--Create Question Bank-->
							<div class="content">
								<div class="entry-tab-inner" id="tab-new_course">
								</div>
							</div>
						</div>

							<!-- <h3 style="font-weight:bold">ABOUT</h3> -->
						<?php }
						
						else {
							thim_breadcrumbs();
						}
					} ?>
                </div><!-- .breadcrumbs-wrapper -->
            </div><!-- .breadcrumb-content -->
		<?php } ?>
	<?php } ?>
<?php } ?>
