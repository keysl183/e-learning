<?php 
//so outside users will not be able to access this remotely
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



//Helper Methods
//return empty if not set, return the value otherwise
function issetReturnValue($key, $user_meta)
{
    if(! empty( $user_meta[$key] ) )
    {
        return $user_meta[$key];
    }
}

function fieldAgeError($field, $errorFieldName)
{
    if(!isset( $field ))
    {
        return $errorFieldName . ' is a required field';
    }
    else if($field == '' || $field === 0)
    {
        return $errorFieldName . ' is a required field';
    }
    else if(is_int($field))
    {
        return $errorFieldName . ' has invalid values';
    }
    else if($field <= 0)
    {
        return $errorFieldName . ' has invalid values';
    }
    else if($field >= 100)
    {
        return "You gottabe be kidding me. Are you immortal?";
    }
}

function fieldCountry($field, $errorFieldName)
{
    if(!isset( $field ))
    {
        return $errorFieldName . ' is a required field';
    }
    else if($field == '')
    {
        return $errorFieldName . ' is a required field';
    }
    else if(strlen($field) > 2)
    {
        return $errorFieldName . ' has invalid values';
    }
    //will do country validation later
}


//Logical Starts here
$user = wp_get_current_user();
$user_meta = get_user_meta( $user->ID );

if(is_user_logged_in())
{
    $user_meta = array_map( function ( $a ) {
        return $a[0];
    }, $user_meta ); //to retain key value pair values
}
//var_dump($user_meta); //for debug purpose

if(isset($_POST["cmpltProfSubmit"]))
{
    //Valiation Starts here
    $hasError = false;
    $profileUpdated = false;

    $first_name = '';
    $first_name_err = fieldRequiredError($_POST['first_name'], "First Name");
    if($first_name_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $first_name =  sanitizeNormalText($_POST['first_name']);
    }
    
    $last_name = '';
    $last_name_err = fieldRequiredError($_POST['last_name'], "Last Name");
    if($last_name_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $last_name =  sanitizeNormalText($_POST['last_name']);
    }


    $age = 0;
    $age_err = fieldAgeError($_POST['age'], "Age");
    if($age_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $age = $_POST['age'];
    }

    $country = '';
    $country_err = fieldCountry($_POST['country'], "Country");
    if($country_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $country = sanitizeNormalText($_POST['country']); 
    }

    $zip = '';
    $zip_err = fieldRequiredError($_POST['zip'], "Zip Code");
    if($last_name_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $zip =  sanitizeNormalText($_POST['zip']);
    }

    $education = '';
    $education_err = fieldCountry($_POST['education'], "Education");
    if($education_err != '')
    {
        $hasError = true;
    }else
    {
        //sanitize
        $education = sanitizeNormalText($_POST['education']); 
    }

    //Do the database update
    if(!$hasError)
    {
        //wp_user
        $user_fields = array(
            'ID'           => $user->ID,
            'first_name'   => esc_attr($first_name),
            'last_name'    => esc_attr($last_name)
           );
        wp_update_user($user_fields);

        //wp_user_meta
        update_user_meta( $user->ID , "age", $age);
        update_user_meta( $user->ID , "country", $country);
        update_user_meta( $user->ID , "zip", $zip);
        update_user_meta( $user->ID , "education", $education);
        update_user_meta( $user->ID , "student_level", ''); //none no specs 03182018
        update_user_meta( $user->ID , "is_profile_complete", 1); //set this to 1 completed

        $profileUpdated =true;
    }
    //DEBUG
    // print_r($_POST) ."<br>";

    // echo $age ."<br>";
    // echo $country ."<br>";
    // echo $zip ."<br>";
    // echo $education ."<br>";
}


// if($profileUpdated)
// {
//     echo "<h1>Profile was successfully updated</h1>";
//     echo "Please Choose your Membership Type";
//     echo "LINK TO BUY MEMBERSHIP HERE";
// }
?>

<?php if ($profileUpdated ): ?>
    <h1>Profile was successfully Completed</h1>
    <div>You may now access your Profile <a href=<?php echo get_site_url().'/lp-profile' ?>> here </a></div>
    <br>
    <div>
        Or you can now choose a membership plans to access our wide selections of courses
        <form action='<?php echo get_site_url(). '/membership' ?>' method="get">
            <input type="submit" value="Get Membership" name="Submit"/>
        </form>
    </div>

<?php endif; ?>




<?php if (! is_user_logged_in()): ?>


<h4>Sorry Currently you are not logged in<h4>
<div>Click this link to logged in <?php echo get_site_url() . '/account'?> </div>
<?php else: ?>
<?php if (!$profileUpdated): ?>
<h4>Complete your Profile</h4>
<form action="<?php the_permalink(); ?>" id="completeProfileForm" method='POST' >
	<label class="tgb_input_label">FIRST NAME :</label>
     <input type='text' name='first_name' class='tgb_input_text' value= "<?php echo issetReturnValue('first_name', $user_meta); ?>">
    <?php
        echo showIfFieldHasError($first_name_err);
    ?>
    <br>
    <label class="tgb_input_label">LAST NAME :</label>
      <input type='text' name='last_name' class='tgb_input_text' value= "<?php echo issetReturnValue('last_name', $user_meta); ?>">
    <?php
        echo showIfFieldHasError($last_name_err);
    ?>
    <br>
    <label class="tgb_input_label">EMAIL ADDRESS :</label>
      <input type='text' name='email_address' class='tgb_input_text' disabled value = "<?php echo  $user->user_email; ?>">
    <?php
        echo showIfFieldHasError($last_name_err);
    ?>
    <br>
	<label class="tgb_input_label">AGE :</label>
    <input type='text' name='age' class='tgb_input_text' value= "<?php echo issetReturnValue('age', $user_meta); ?>">
    <?php
        echo showIfFieldHasError($age_err);
    ?>       
    <br>
	<!--WILL DO THIS THE RIGHT WAY LATER I PROMISE :( -->
	<label class="tgb_input_label">COUNTRY :</label>
	  <select name='country' class='tgb_input_text'>
	<option value="AF">Afghanistan</option>
	<option value="AX">Åland Islands</option>
	<option value="AL">Albania</option>
	<option value="DZ">Algeria</option>
	<option value="AS">American Samoa</option>
	<option value="AD">Andorra</option>
	<option value="AO">Angola</option>
	<option value="AI">Anguilla</option>
	<option value="AQ">Antarctica</option>
	<option value="AG">Antigua and Barbuda</option>
	<option value="AR">Argentina</option>
	<option value="AM">Armenia</option>
	<option value="AW">Aruba</option>
	<option value="AU">Australia</option>
	<option value="AT">Austria</option>
	<option value="AZ">Azerbaijan</option>
	<option value="BS">Bahamas</option>
	<option value="BH">Bahrain</option>
	<option value="BD">Bangladesh</option>
	<option value="BB">Barbados</option>
	<option value="BY">Belarus</option>
	<option value="BE">Belgium</option>
	<option value="BZ">Belize</option>
	<option value="BJ">Benin</option>
	<option value="BM">Bermuda</option>
	<option value="BT">Bhutan</option>
	<option value="BO">Bolivia, Plurinational State of</option>
	<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
	<option value="BA">Bosnia and Herzegovina</option>
	<option value="BW">Botswana</option>
	<option value="BV">Bouvet Island</option>
	<option value="BR">Brazil</option>
	<option value="IO">British Indian Ocean Territory</option>
	<option value="BN">Brunei Darussalam</option>
	<option value="BG">Bulgaria</option>
	<option value="BF">Burkina Faso</option>
	<option value="BI">Burundi</option>
	<option value="KH">Cambodia</option>
	<option value="CM">Cameroon</option>
	<option value="CA">Canada</option>
	<option value="CV">Cape Verde</option>
	<option value="KY">Cayman Islands</option>
	<option value="CF">Central African Republic</option>
	<option value="TD">Chad</option>
	<option value="CL">Chile</option>
	<option value="CN">China</option>
	<option value="CX">Christmas Island</option>
	<option value="CC">Cocos (Keeling) Islands</option>
	<option value="CO">Colombia</option>
	<option value="KM">Comoros</option>
	<option value="CG">Congo</option>
	<option value="CD">Congo, the Democratic Republic of the</option>
	<option value="CK">Cook Islands</option>
	<option value="CR">Costa Rica</option>
	<option value="CI">Côte d'Ivoire</option>
	<option value="HR">Croatia</option>
	<option value="CU">Cuba</option>
	<option value="CW">Curaçao</option>
	<option value="CY">Cyprus</option>
	<option value="CZ">Czech Republic</option>
	<option value="DK">Denmark</option>
	<option value="DJ">Djibouti</option>
	<option value="DM">Dominica</option>
	<option value="DO">Dominican Republic</option>
	<option value="EC">Ecuador</option>
	<option value="EG">Egypt</option>
	<option value="SV">El Salvador</option>
	<option value="GQ">Equatorial Guinea</option>
	<option value="ER">Eritrea</option>
	<option value="EE">Estonia</option>
	<option value="ET">Ethiopia</option>
	<option value="FK">Falkland Islands (Malvinas)</option>
	<option value="FO">Faroe Islands</option>
	<option value="FJ">Fiji</option>
	<option value="FI">Finland</option>
	<option value="FR">France</option>
	<option value="GF">French Guiana</option>
	<option value="PF">French Polynesia</option>
	<option value="TF">French Southern Territories</option>
	<option value="GA">Gabon</option>
	<option value="GM">Gambia</option>
	<option value="GE">Georgia</option>
	<option value="DE">Germany</option>
	<option value="GH">Ghana</option>
	<option value="GI">Gibraltar</option>
	<option value="GR">Greece</option>
	<option value="GL">Greenland</option>
	<option value="GD">Grenada</option>
	<option value="GP">Guadeloupe</option>
	<option value="GU">Guam</option>
	<option value="GT">Guatemala</option>
	<option value="GG">Guernsey</option>
	<option value="GN">Guinea</option>
	<option value="GW">Guinea-Bissau</option>
	<option value="GY">Guyana</option>
	<option value="HT">Haiti</option>
	<option value="HM">Heard Island and McDonald Islands</option>
	<option value="VA">Holy See (Vatican City State)</option>
	<option value="HN">Honduras</option>
	<option value="HK">Hong Kong</option>
	<option value="HU">Hungary</option>
	<option value="IS">Iceland</option>
	<option value="IN">India</option>
	<option value="ID">Indonesia</option>
	<option value="IR">Iran, Islamic Republic of</option>
	<option value="IQ">Iraq</option>
	<option value="IE">Ireland</option>
	<option value="IM">Isle of Man</option>
	<option value="IL">Israel</option>
	<option value="IT">Italy</option>
	<option value="JM">Jamaica</option>
	<option value="JP">Japan</option>
	<option value="JE">Jersey</option>
	<option value="JO">Jordan</option>
	<option value="KZ">Kazakhstan</option>
	<option value="KE">Kenya</option>
	<option value="KI">Kiribati</option>
	<option value="KP">Korea, Democratic People's Republic of</option>
	<option value="KR">Korea, Republic of</option>
	<option value="KW">Kuwait</option>
	<option value="KG">Kyrgyzstan</option>
	<option value="LA">Lao People's Democratic Republic</option>
	<option value="LV">Latvia</option>
	<option value="LB">Lebanon</option>
	<option value="LS">Lesotho</option>
	<option value="LR">Liberia</option>
	<option value="LY">Libya</option>
	<option value="LI">Liechtenstein</option>
	<option value="LT">Lithuania</option>
	<option value="LU">Luxembourg</option>
	<option value="MO">Macao</option>
	<option value="MK">Macedonia, the former Yugoslav Republic of</option>
	<option value="MG">Madagascar</option>
	<option value="MW">Malawi</option>
	<option value="MY">Malaysia</option>
	<option value="MV">Maldives</option>
	<option value="ML">Mali</option>
	<option value="MT">Malta</option>
	<option value="MH">Marshall Islands</option>
	<option value="MQ">Martinique</option>
	<option value="MR">Mauritania</option>
	<option value="MU">Mauritius</option>
	<option value="YT">Mayotte</option>
	<option value="MX">Mexico</option>
	<option value="FM">Micronesia, Federated States of</option>
	<option value="MD">Moldova, Republic of</option>
	<option value="MC">Monaco</option>
	<option value="MN">Mongolia</option>
	<option value="ME">Montenegro</option>
	<option value="MS">Montserrat</option>
	<option value="MA">Morocco</option>
	<option value="MZ">Mozambique</option>
	<option value="MM">Myanmar</option>
	<option value="NA">Namibia</option>
	<option value="NR">Nauru</option>
	<option value="NP">Nepal</option>
	<option value="NL">Netherlands</option>
	<option value="NC">New Caledonia</option>
	<option value="NZ">New Zealand</option>
	<option value="NI">Nicaragua</option>
	<option value="NE">Niger</option>
	<option value="NG">Nigeria</option>
	<option value="NU">Niue</option>
	<option value="NF">Norfolk Island</option>
	<option value="MP">Northern Mariana Islands</option>
	<option value="NO">Norway</option>
	<option value="OM">Oman</option>
	<option value="PK">Pakistan</option>
	<option value="PW">Palau</option>
	<option value="PS">Palestinian Territory, Occupied</option>
	<option value="PA">Panama</option>
	<option value="PG">Papua New Guinea</option>
	<option value="PY">Paraguay</option>
	<option value="PE">Peru</option>
	<option value="PH">Philippines</option>
	<option value="PN">Pitcairn</option>
	<option value="PL">Poland</option>
	<option value="PT">Portugal</option>
	<option value="PR">Puerto Rico</option>
	<option value="QA">Qatar</option>
	<option value="RE">Réunion</option>
	<option value="RO">Romania</option>
	<option value="RU">Russian Federation</option>
	<option value="RW">Rwanda</option>
	<option value="BL">Saint Barthélemy</option>
	<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
	<option value="KN">Saint Kitts and Nevis</option>
	<option value="LC">Saint Lucia</option>
	<option value="MF">Saint Martin (French part)</option>
	<option value="PM">Saint Pierre and Miquelon</option>
	<option value="VC">Saint Vincent and the Grenadines</option>
	<option value="WS">Samoa</option>
	<option value="SM">San Marino</option>
	<option value="ST">Sao Tome and Principe</option>
	<option value="SA">Saudi Arabia</option>
	<option value="SN">Senegal</option>
	<option value="RS">Serbia</option>
	<option value="SC">Seychelles</option>
	<option value="SL">Sierra Leone</option>
	<option value="SG">Singapore</option>
	<option value="SX">Sint Maarten (Dutch part)</option>
	<option value="SK">Slovakia</option>
	<option value="SI">Slovenia</option>
	<option value="SB">Solomon Islands</option>
	<option value="SO">Somalia</option>
	<option value="ZA">South Africa</option>
	<option value="GS">South Georgia and the South Sandwich Islands</option>
	<option value="SS">South Sudan</option>
	<option value="ES">Spain</option>
	<option value="LK">Sri Lanka</option>
	<option value="SD">Sudan</option>
	<option value="SR">Suriname</option>
	<option value="SJ">Svalbard and Jan Mayen</option>
	<option value="SZ">Swaziland</option>
	<option value="SE">Sweden</option>
	<option value="CH">Switzerland</option>
	<option value="SY">Syrian Arab Republic</option>
	<option value="TW">Taiwan, Province of China</option>
	<option value="TJ">Tajikistan</option>
	<option value="TZ">Tanzania, United Republic of</option>
	<option value="TH">Thailand</option>
	<option value="TL">Timor-Leste</option>
	<option value="TG">Togo</option>
	<option value="TK">Tokelau</option>
	<option value="TO">Tonga</option>
	<option value="TT">Trinidad and Tobago</option>
	<option value="TN">Tunisia</option>
	<option value="TR">Turkey</option>
	<option value="TM">Turkmenistan</option>
	<option value="TC">Turks and Caicos Islands</option>
	<option value="TV">Tuvalu</option>
	<option value="UG">Uganda</option>
	<option value="UA">Ukraine</option>
	<option value="AE">United Arab Emirates</option>
	<option value="GB">United Kingdom</option>
	<option value="US">United States</option>
	<option value="UM">United States Minor Outlying Islands</option>
	<option value="UY">Uruguay</option>
	<option value="UZ">Uzbekistan</option>
	<option value="VU">Vanuatu</option>
	<option value="VE">Venezuela, Bolivarian Republic of</option>
	<option value="VN">Viet Nam</option>
	<option value="VG">Virgin Islands, British</option>
	<option value="VI">Virgin Islands, U.S.</option>
	<option value="WF">Wallis and Futuna</option>
	<option value="EH">Western Sahara</option>
	<option value="YE">Yemen</option>
	<option value="ZM">Zambia</option>
	<option value="ZW">Zimbabwe</option>
</select>
<?php
    echo showIfFieldHasError($country_err);
?>       
<br>

ZIP CODE : <input type='text' name='zip' class='tgb_input_text' value= "<?php echo issetReturnValue('zipcode', $user_meta); ?>">
<?php
    echo showIfFieldHasError($zip_err);
?>       
<br>
<label class="tgb_input_label">Education :</label>
 <select name='education' class='tgb_input_text'>
    <option value='PG'>Post Graduate</option>
    <option value='BH'>Bachelors</option>
    <option value='HS'>High School</option>
    <option value='GS'>Grade School</option>
</select>
<?php
    echo showIfFieldHasError($education_err);
?>       
<br>


<input type="submit" value="SUBMIT" name="cmpltProfSubmit">

</form>
<?php endif; ?>
<?php endif; ?>