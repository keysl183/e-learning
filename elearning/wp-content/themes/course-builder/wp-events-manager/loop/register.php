<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( wpems_get_option( 'allow_register_event' ) == 'no' ) {
	return;
}

$event    = new WPEMS_Event( get_the_ID() );
$user_reg = $event->booked_quantity( get_current_user_id() );

if ( absint( $event->qty ) == 0 || $event->post->post_status === 'tp-event-expired' ) {
	return;
}
?>

<div class="entry-register">

    <ul class="event-info">
        <li class="total">
            <span class="label"><?php _e( 'Total Slot:', 'course-builder' ) ?></span>
            <span class="detail"><?php echo esc_html( absint( $event->qty ) ) ?></span>
        </li>
        <li class="booking_slot">
            <span class="label"><?php _e( 'Booked Slot:', 'course-builder' ) ?></span>
            <span class="detail"><?php echo esc_html( absint( $event->booked_quantity() ) ) ?></span>
        </li>
        <li class="price">
            <span class="label"><?php _e( 'Cost:', 'course-builder' ) ?></span>
            <span class="detail"><?php printf( '%s', $event->is_free() ? __( 'Free', 'course-builder' ) : wpems_format_price( $event->get_price() ) ) ?></span>
        </li>
    </ul>

    <div class="register-inner">
		<?php if ( !is_user_logged_in() ) { ?>
			<a href="<?php echo esc_url( wpems_login_url() ) ?>" class="event_register_submit event_auth_button" target="_blank"><?php esc_html_e( 'Login Now', 'course-builder' ); ?></a>
			<p></p>
			<p class="login-notice">
				<?php echo esc_html__('You must login to our site to book this event!', 'course-builder'); ?>
			</p>
		<?php } else { ?>
			<a class="event_register_submit event_auth_button event-load-booking-form" data-event="<?php echo esc_attr( get_the_ID() ) ?>"><?php _e( 'Register Now', 'course-builder' ); ?></a>
		<?php } ?>
	</div>

</div>
