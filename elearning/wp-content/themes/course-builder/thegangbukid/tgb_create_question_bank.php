<?php 
//so outside users will not be able to access this remotely
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
// include("lib/tgb_validation.php"); now loaded globally in functions.php
//Logical Starts here
$user = wp_get_current_user();
$user_meta = get_user_meta( $user->ID );

if(is_user_logged_in())
{
    $user_meta = array_map( function ( $a ) {
        return $a[0];
    }, $user_meta ); //to retain key value pair values
}

function create_new_question_bank($question_bank_title, $question_bank_desc)
{
    global $wpdb;

    $value = $wpdb->insert("wp_tgb_quest_bank",
        array(
            'question_bank_title' => $question_bank_title,
            'question_bank_desc' => $question_bank_desc
        )
    );

    return $wpdb->insert_id;
}

function delete_question_bank($question_bank_id)
{
    global $wpdb;

    $value = $wpdb->delete("wp_tgb_quest_bank",
        array(
            'quest_bank_id' => $question_bank_id
        )
    );
    return $value;
}

function get_all_quest_bank()
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_tgb_quest_bank");
    return $result;
}

if(isset($_POST["newQuestionBankSubmit"]))
{
//will add validation later
//do a redirect after save to avoid confirm resubmission later

    $notif_msg = '';
    $hasError = false;

    $quest_bank_title = '';
    $quest_bank_title_err = fieldRequiredError($_POST['quest_bank_title'], "Question Bank Title");
    if($quest_bank_title_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_bank_title =  sanitizeNormalText($_POST['quest_bank_title']);
    }


    $quest_bank_desc = '';
    $quest_bank_desc_err = fieldRequiredError($_POST['quest_bank_desc'], "Question Bank Description");
    if($quest_bank_desc_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_bank_desc =  sanitizeNormalText($_POST['quest_bank_desc']);
    }

    if(!$hasError)
    {
        $created = create_new_question_bank($quest_bank_title, $quest_bank_desc);
        $notif_msg = 'Successfully Created a New Question Bank';
    }else
    {
        $notif_msg = 'Error Creating a New Question Bank';
    }

    $showNotif = true;
}//end of post button


if(isset($_POST["delete_question_bank"]))
{
    $notif_msg = '';
    $hasError = false;

    $quest_bank_count = check_quest_bank_exists($_POST["question_bank_id"]);

    if($quest_bank_count == 0)
    {
        $hasError = true;
        $quest_bank_err = 'Target Question Bank does not exists!.';
    }
    else
    {
        $quest_bank_id =  $_POST["question_bank_id"];
    }

    if(!$hasError)
    {
        delete_question_bank($quest_bank_id);
        $notif_msg = 'Successfully Deleted Question Bank';
    }
    else
    {
        $notif_msg = 'Error Deleting a Question Bank';
    }
    $showNotif = true;
}



if(!isset($showNotif))
{
    $showNotif = false;
}
?>

<?php if ($showNotif) : ?>
<div class="learn-press-message success noti-success">
<p><?php echo $notif_msg ?></p>		
</div>
<?php endif;?>
<h2>Question Banks</h2>

<div class="tgb-exam-main">
<div class="tgb-exam-main-inner tgb-new-exam">

<div class="group-settings">
    <ul class="tabs-title">
        <li class="tab active" data-tab="questionbank_tab">
        <a href="#" data-slug="#tab-questionbank">Question Banks</a>
        </li>
        
        <li class="tab" data-tab="createbank_tab">
        <a href="#" data-slug="#tab-orders">New Question Bank</a>
        </li>
        <li class="tab" data-tab="settings_tab">
        <a href="#" data-slug="#tab-settings">Settings</a>
        </li>
    </ul>

    <div class="tabs-content">
        <!--Question Bank List-->
        <div class="content active">
            <div class="entry-tab-inner" id="tab-questionbank">
                <table  class="tgb_main_table" id="tgb_table_question_bank_lst">
                    <tr>
                        <th>Bank Name</th>
                        <th>Bank Description</th>
                        <!-- <th>Status</th> -->
                        <th>Actions</th>
                    </tr>
                    <?php 
                    $quest_banks = get_all_quest_bank();
                    foreach($quest_banks as $bank){?> 
                    <tr>
                        <td><?php echo $bank->question_bank_title ?></td>

                        <td><?php echo $bank->question_bank_desc ?></td>

                        <!-- <td>Inactive (Not used) </td> Will add later. Tells uf bank was used in a course -->
                        <td>
                            <a class="btn" href="<?php echo get_site_url().'/exam-manager'.'/manage-question-bank/?quest_bank='. $bank->quest_bank_id ?>">Manage</a>
                            <button class="delete_question_bank_btn" data-questionbankid = "<?php echo $bank->quest_bank_id ?>">Delete</button>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>

        <!--Create Question Bank-->
        <div class="content">
            <div class="entry-tab-inner" id="tab-createbank">
                <form  action="<?php the_permalink(); ?>" method="POST">
                    <label class="tgb_input_label">Question Bank Title</label>
                    <input type="text" name="quest_bank_title" placeholder="Question Bank Title" class="tgb_input_text">
                    <label class="tgb_input_label">Question Bank Description</label>
                    <textarea name="quest_bank_desc" id="" cols="30" rows="3" class="tgb_input_text marg_bot_5" placeholder="Question Bank Description"></textarea>
                    <input type="hidden" name="dummy_submit" value="yes"/>
                    <input type="submit" name="newQuestionBankSubmit">
                </form>
            </div>
        </div>
    </div>
</div>

</div>
</div>

<!--Delete Question Bank Modal-->
<div class="tgb_modal" id="delete_question_bank_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">Delete Question</div>
        <div>
            <h4>Are you sure you want to delete the Question?</h4>
            <form action="<?php the_permalink(); ?>"  method="POST" >
                <input type="hidden" name="question_bank_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="delete_question_bank" value="Ok"/>
            </form>
        </div>
    </div>
</div>