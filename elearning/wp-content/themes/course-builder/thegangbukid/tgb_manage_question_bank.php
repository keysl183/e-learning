<?php 
//so outside users will not be able to access this remotely
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// include("lib/tgb_validation.php"); already loaded in the background
include("lib/tgb_common.php");
if(!isset($_REQUEST['quest_bank']))
{
    //additional layer of defense
    //do something here
}

//essential funtions
function get_quest_basic_details($quest_bank_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_tgb_quest_bank WHERE quest_bank_id = ". $quest_bank_id);
    return $result[0];
}

function get_quest_bank_questions($quest_bank_id)
{
    global $wpdb;
    $result =  $wpdb->get_results
    ("SELECT  wpm.post_id,w.quest_bank_id, wm.quest_bank_meta_id, wp.post_title, wp.post_content, wp.post_type, wpm.meta_key, wpm.meta_value 
    FROM wp_tgb_quest_bank as w JOIN wp_tgb_quest_bank_meta as wm ON w.quest_bank_id = wm.question_bank_id 
    JOIN wp_posts as wp ON wm.question_id = wp.ID JOIN wp_postmeta as wpm ON wpm.post_id = wp.ID
    WHERE wpm.meta_key = '_lp_type' AND w.quest_bank_id = ". $quest_bank_id);
    return $result;
}

//updates the details for question_banks
function update_new_question_bank($question_bank_id, $question_bank_title, $question_bank_desc)
{
    global $wpdb;

    $value = $wpdb->update("wp_tgb_quest_bank",
        array(
            'question_bank_title' => $question_bank_title,
            'question_bank_desc' => $question_bank_desc
        ),
        array(
            'quest_bank_id' => $question_bank_id
        )
    );

    return $wpdb->insert_id;
}

//Logical Starts here
$user = wp_get_current_user();
$user_meta = get_user_meta( $user->ID );

if(is_user_logged_in())
{
    $user_meta = array_map( function ( $a ) {
        return $a[0];
    }, $user_meta ); //to retain key value pair values
}

//update question bank
if(isset($_POST["update_quest_bank"]))
{
    $notif_msg = '';

    $hasError = false;
    $quest_bank_err = '';
    $quest_bank_count = check_quest_bank_exists($_POST["quest_bank"]);
    if($quest_bank_count == 0)
    {
        $hasError = true;
        $quest_bank_err = 'Target Question Bank does not exists!.';
    }
    else
    {
        $quest_bank_id =  $_POST["quest_bank"];
    }

    $quest_bank_title = '';
    $quest_bank_title_err = fieldRequiredError($_POST['quest_bank_title'], "Question Bank Title");
    if($quest_bank_title_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_bank_title =  sanitizeNormalText($_POST['quest_bank_title']);
    }

    $quest_bank_desc = '';
    $quest_bank_desc_err = fieldRequiredError($_POST['quest_bank_desc'], "Question Bank Description");
    if($quest_bank_desc_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_bank_desc =  sanitizeNormalText($_POST['quest_bank_desc']);
    }
    
    if(!$hasError)
    {
        $created = update_new_question_bank($quest_bank_id, $quest_bank_title, $quest_bank_desc);
        $showNotif = true;
        $notif_msg = 'Successfully Updated the Question Bank';
    }
}



//add new quetion
if(isset($_POST["new_quest_add"]))
{
    $hasError = false;
    $quest_bank_err = '';
    $quest_bank_count = check_quest_bank_exists($_POST["quest_bank"]);
    if($quest_bank_count == 0)
    {
        $hasErrors = true;
        $quest_bank_err = 'Target Question Bank does not exists!.';
    }
    else
    {
        $quest_bank_id =  $_POST["quest_bank"];
    }

    $quest_title = '';
    $quest_title_err = fieldRequiredError($_POST['quest_title'], "Question Title");
    if($quest_title_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_title =  sanitizeNormalText($_POST['quest_title']);
    }

    $quest_desc = '';
    $quest_desc_err = fieldRequiredError($_POST['quest_desc'], "Question Description");
    if($quest_desc_err != '')
    {
        $hasError = true;
    }else
    {
        $quest_desc =  sanitizeNormalText($_POST['quest_desc']);
    }
    
    $quest_type='';
    $quest_type_choices = array('multi_choice','multi_choice_img','essay', 'audio');
    $quest_type_err = '';
    if(!isset($_POST["quest_type"]) || $_POST["quest_type"] == '')
    {
        $quest_type_err= "Target Question Type is not existing";
        $hasError = true;
    }
    else if(!in_array($_POST["quest_type"], $quest_type_choices))
    {
        $quest_type_err= "Target Question Type is not existing";
        $hasError = true;
    }else
    {
        $quest_type = $_POST["quest_type"];
    }

    $quest_ans_err = fieldRequiredError($_POST['quest_correct_ans'], "Question Answer");
    if($quest_ans_err != '')
    {
        $hasError = true;
    }

    $errors;
    if($quest_type=='multi_choice') //validate the answers for multi_choice
    {
        $quest_option_one_err = fieldRequiredError($_POST['quest_option_one'], "Question Answer One");
        if($quest_option_one_err != '')
        {
            $hasError = true;
            $errors["quest_option_one_err"] = $quest_option_one_err;
        }

        $quest_option_two_err = fieldRequiredError($_POST['quest_option_two'], "Question Answer Two");
        if($quest_option_two_err != '')
        {
            $hasError = true;
            $errors["quest_option_two_err"] = $quest_option_two_err;
        }

        $quest_option_three_err = fieldRequiredError($_POST['quest_option_three'], "Question Answer Three");
        if($quest_option_three_err != '')
        {
            $hasError = true;
            $errors["quest_option_three_err"] = $quest_option_three_err;
        }

        $quest_option_four_err = fieldRequiredError($_POST['quest_option_four'], "Question Answer Four");
        if($quest_option_four_err != '')
        {
            $hasError = true;
            $errors["quest_option_four_err"] = $quest_option_four_err;
        }
    }
    else if($quest_type== 'multi_choice_img')
    {
        if(!isset($_FILES["quest_option_one_file"]) || !isset($_FILES["quest_option_two_file"]) ||
           !isset($_FILES["quest_option_three_file"]) || !isset($_FILES["quest_option_four_file"]))
           {
               //do a validation later for image only
                $hasError = true;
                $errors["quest_option_file"] = 'Image are required';
           }else
           {
                $answer_one_attachment_id = tgb_upload_image_answer($_FILES["quest_option_one_file"] ,$user->ID);
                $answer_two_attachment_id = tgb_upload_image_answer($_FILES["quest_option_two_file"] ,$user->ID);
                $answer_three_attachment_id = tgb_upload_image_answer($_FILES["quest_option_three_file"] ,$user->ID);
                $answer_four_attachment_id = tgb_upload_image_answer($_FILES["quest_option_four_file"] ,$user->ID);
           }

    }

    if(!$hasError)
    {
        $is_answer = ($_POST["quest_correct_ans"] == "ans_first" ? 'yes' : 'no');
        $choiceOne = array(
                'text' =>  ($quest_type == 'multi_choice_img') ?  wp_get_attachment_image_src($answer_one_attachment_id) : $_POST["quest_option_one"],
                'value' => 'option_first',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_seconds"? 'yes' : 'no');
        $choiceTwo = array(
                'text' => ($quest_type == 'multi_choice_img') ?  wp_get_attachment_image_src($answer_two_attachment_id) : $_POST["quest_option_two"],
                'value' => 'option_seconds',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_third" ? 'yes' : 'no');
        $choiceThree = array(
                'text' => ($quest_type == 'multi_choice_img') ?  wp_get_attachment_image_src($answer_three_attachment_id) :  $_POST["quest_option_three"],
                'value' => 'option_third',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_fourth" ? 'yes' : 'no');
        $choiceFour = array(
                'text' => ($quest_type == 'multi_choice_img') ?  wp_get_attachment_image_src($answer_four_attachment_id) :  $_POST["quest_option_four"],
                'value' => 'option_fourth',
                'is_true' =>  $is_answer
            );               
        
        $all_choices = array( $choiceOne, $choiceTwo, $choiceThree, $choiceFour);   
    
       $new_quest_id = insert_questions($quest_type, $quest_bank_id, $quest_title,  $quest_desc,$all_choices, $user->ID  );
       $showNotif = true;
       $notif_msg = "Successfully Added new Question";
    }else
    {
        echo $quest_bank_err;
        echo $quest_title_err;
        echo $quest_desc_err;
        echo $quest_type_err;
        var_dump($errors);

    }
}

$quest_bank_count = check_quest_bank_exists($_REQUEST["quest_bank"]);
if($quest_bank_count  == 1)
{
    $quest_bank_basic_info = get_quest_basic_details(sanitizeNormalText($_REQUEST["quest_bank"]));
    $quest_bank_quest_list = get_quest_bank_questions(sanitizeNormalText($_REQUEST["quest_bank"]));
}else
{
    //do something here later 
}

if(!isset($showNotif))
{
    $showNotif = false;
}

?>

<?php if ($showNotif) : ?>
<div class="learn-press-message success noti-success">
<p><?php echo $notif_msg; ?> </p>		
</div>
<?php endif;?>
<h2>Edit Question Bank</h2>


<div class="tgb-exam-main">
<div class="tgb-exam-main-inner tgb-new-exam">

<div class="group-settings">
    <ul class="tabs-title">
        <li class="tab active" data-tab="basic_info_tab">
        <a href="#" data-slug="#tab-questionbank">Basic Info</a>
        </li>
        
        <li class="tab" data-tab="question_list_tab">
        <a href="#" data-slug="#tab-orders">Question Lists</a>
        </li>

        <li class="tab" data-tab="settings_tab">
        <a href="#" data-slug="#tab-settings">Settings</a>
        </li>
    </ul>

    <div class="tabs-content">
        <!--Bank Basic Info-->
        <div class="content active">
            <div class="entry-tab-inner" id="tab-basic_info">
                <!--Will convert this to ajax later-->
                <form  action="<?php the_permalink(); ?>" method="POST">
                    <label class="tgb_input_label">Question Bank Title</label>
                    <input type="text" name="quest_bank_title" placeholder="Question Bank Title" class="tgb_input_text" value="<?php echo $quest_bank_basic_info->question_bank_title;?>">
                    <label class="tgb_input_label">Question Bank Description</label>
                    <textarea name="quest_bank_desc" id="" cols="30" rows="5" class="tgb_input_text" placeholder="Question Bank Description"><?php echo $quest_bank_basic_info->question_bank_desc;?></textarea>
                    <input type="hidden" name="quest_bank" value="<?php echo $_REQUEST['quest_bank'];?>"/>
                    <input type="hidden" name="dummy_submit" value="yes"/>
                    <input type="submit" name="update_quest_bank" value="Update">
                </form>
            </div>
        </div>

        <!--Question List -->
        <div class="content">
            <div class="entry-tab-inner" id="tab-question_list">
                <input type="button" value="Add New Question" id="open_modal_btn"/>
                <table style="width:100%" id="tgb_table_question_lst">
                    <tr>
                        <th>Question Title</th>
                        <th>Question Description</th>
                        <th>Question Type</th>
                        <th>Actions</th>
                    </tr>
                    <?php  
                    
                    foreach($quest_bank_quest_list as $questions) {?>
                    <tr>
                        <td><?php echo $questions->post_title  ?> </td>
                        <td><?php echo $questions->post_content  ?> </td>
                        <td><?php echo 'Multiple Choice' //echo $questions->meta_value  ?> </td>
                        <td>
                            <!-- <button class="edit_question_btn">Edit</button> -->
                            <button class="delete_question_btn" data-questionid = "<?php echo $questions->post_id ?>">Delete</button>
                        </td>
                    </tr>
                    <?php }?>
                </table>
            </div>
        </div>

        <div class="content">
            <div class="entry-tab-inner" id="tab-settings">
                <p>Will Add Passing rate here</p>
            </div>
        </div>
    </div>
</div>


</div>
</div>

<div class="tgb_modal" id="new_question_modal">
    <div class="thim-loading-icon" style="display:none">
    <div class="sk-spinner sk-spinner-pulse"></div></div>
    <div class="tgb_modal_inner">
        <div class="widget-title">
        <h4>New Question</h4>
        </div>
        <div>
            <form action="<?php the_permalink(); ?>" method="POST" id="frm_new_question" enctype="multipart/form-data" >
            <input type="text" name="quest_title" class="tgb_input_text marg_bot_5" placeholder="Question Title"/>
            <textarea name="quest_desc" id="" cols="5" rows="2" class="tgb_input_text marg_bot_5" placeholder="Question Description"></textarea>
            <select name="quest_type" class="tgb_input_label marg_bot_5">
                <option value="multi_choice">Multiple Choice</option>
                <option value="multi_choice_img">Multiple Choice (Images)</option>
                <option value="essay" disabled>Essay</option>
                <option value="audio" disabled>Audio</option>
            </select>
            <div class="answer-multiple" style="margin-top:5px">
                <table  class="tgb_table">
                    <tr>
                        <th>Choices</th>
                        <th>Is Correct</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="quest_option_one" placeholder="Option One" class="tgb_input_text tgb_answer_input" />
                            <span class="answer_image_file">
                                <label>Option One</label>
                                <input type="file" name="quest_option_one_file" accept="image/x-png,image/gif,image/jpeg"/>
                            </span>
                        </td>
                        <td><input type="radio" name="quest_correct_ans" value="ans_first"/></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="quest_option_two" placeholder="Option Two" class="tgb_input_text tgb_answer_input" />
                            <span class="answer_image_file">
                                <label>Option Two</label>
                                <input type="file"  name="quest_option_two_file" accept="image/x-png,image/gif,image/jpeg"/>
                            </span>
                        </td>
                        <td><input type="radio" name="quest_correct_ans" value="ans_seconds"/></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="quest_option_three" placeholder="Option Three" class="tgb_input_text tgb_answer_input" />
                            <span class="answer_image_file">
                                <label>Option Three</label>
                                <input type="file"  name="quest_option_three_file" accept="image/x-png,image/gif,image/jpeg"/>
                            </span>
                        </td>
                        <td><input type="radio" name="quest_correct_ans" value="ans_third"/></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="quest_option_four" placeholder="Option Four" class="tgb_input_text tgb_answer_input" />
                            <span class="answer_image_file">
                                <label>Option Four</label>
                                <input type="file" class="answer_image_file" name="quest_option_four_file" accept="image/x-png,image/gif,image/jpeg"/>
                            </span>
                            
                        </td>
                        <td><input type="radio" name="quest_correct_ans" value="ans_fourth"/></td>
                    </tr>
                </table>
            </div>
            <!--For security-->
            <input type="hidden" name="dummy_submit" value="yes"/>
            <input type="hidden" name="quest_bank" value="<?php echo $_REQUEST['quest_bank'];?>"/>
            <input type="hidden" name="action" value="add_new_quesion_ajax"/>
            <input type="hidden" name="security" value="<?php  echo wp_create_nonce("add_new_question");?>"/>
            <!--For security-->
            <input type="submit" name="new_quest_add" value="Add"/>
            <input type="hidden" name="ajax_new_quest_add" value="AJAX" id="ajax_new_quest_add" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
            <input type="button" value="Cancel" class="tgb_modal_close"/>
        </form>
        </div>
    </div>
</div>

<div class="tgb_modal" id="delete_question_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">Delete Question</div>
        <div>
            <h4>Are you sure you want to delete the Question Bank?</h4>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_delete_question">
                <!--For security-->
                <input type="hidden" name="action" value="delete_question_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("delete_question");?>"/>
                <input type="hidden" name="ajax_delete_quest" value="AJAX" id="ajax_delete_quest" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="question_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="new_quest_add" value="Ok"/>
            </form>
        </div>
    </div>
</div>

<!--Use this as loader next time-->
<div class="thim-loading-icon" style="display:none">
<div class="sk-spinner sk-spinner-pulse"></div></div>










