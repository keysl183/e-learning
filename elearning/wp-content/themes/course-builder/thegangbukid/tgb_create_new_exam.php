<?php 
//so outside users will not be able to access this remotely
if ( ! defined( 'ABSPATH' ) ) {
	exit; 
}
// include("lib/tgb_validation.php"); it was now loaeded globally
include("lib/tgb_common.php");

//Logical Starts here
$user = wp_get_current_user();
$user_meta = get_user_meta( $user->ID );

if(is_user_logged_in())
{
    $user_meta = array_map( function ( $a ) {
        return $a[0];
    }, $user_meta ); //to retain key value pair values
}

function create_new_course_exam($post_title, $post_name, $post_excerpt, $post_content, $author_id)
{
	$post_id = -1; //not created
	if(get_page_by_title($post_title) == null)
	{
		$post_id = wp_insert_post(
			array(
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_author' => $author_id,
				'post_name' => sanitize_title($post_title),
				'post_title' => $post_title,
				'post_content' => $post_content,
				'post_excerpt' => $post_excerpt,
				'post_status' => 'publish',
				'post_type' => 'lp_course'
			)
		);
		update_post_meta( $post_id, "_thumbnail_id", '0'); 
		update_post_meta( $post_id, "_lp_course_result", 'evaluate_passed_quizzes');
	}else
	{
		$post_id = -2; //exist
	}
	return $post_id;
}


if(isset($_POST["newExamSubmit"]))
{
	//will add validation later
	$exam_id =   create_new_course_exam($_POST['post_title'], $_POST['post_name'], $_POST['post_excerpt']
									, $_POST['post_content'],$user->ID );
									
	if($exam_id != -1 && $exam_id != -2)
	{
		if(isset($_FILES["exam_feature_image"]))
		{
			$attachment_id = tgb_upload_image($_FILES["exam_feature_image"] ,$user->ID, $exam_id);
		}
		$notif_msg = 'Successfully Added New Exam';
	}else
	{
		$notif_msg = 'Error Adding new Exam';
	}

	$showNotif = true;
}

if(isset($_POST["delete_exam_course"]))
{
	//add validation later
	if(isset($_POST["exam_id"]) && is_numeric($_POST['exam_id']))
	{
		$course_exam = get_post($_POST['exam_id']);
		$post_attachments = get_children(array('post_parent' => $_POST['exam_id']));
		foreach ($post_attachments as $attachment) {
			wp_delete_attachment($attachment->ID, true);
  		}
		wp_delete_post($_POST["exam_id"], true);
		$notif_msg = 'Successfully Deleted Exam';
	}else
	{
		$notif_msg = 'Error Deleting Exam';
	}
	$showNotif = true;
}

$exam_course_list = get_all_courses_exams($user->ID);
if(!isset($showNotif))
{
    $showNotif = false;
}

?>


<h1>Exam Manager</h1>
<?php if ($showNotif) : ?>
<div class="learn-press-message success noti-success">
<p><?php echo $notif_msg ?></p>		
</div>
<?php endif;?>

<div class="tgb-exam-main">
    <div class="tgb-exam-main-inner tgb-new-exam">

    <ul class="tabs-title">
        <li class="tab active" data-tab="course_exams_tab">
        <a href="#" data-slug="#tab-questionbank">Course Exams</a>
        </li>
        
        <li class="tab" data-tab="new_course_tab">
        <a href="#" data-slug="#tab-orders">New Course Exams</a>
        </li>
    </ul>

    <div class="tabs-content">
        <!--Question Bank List-->
        <div class="content active">
            <div class="entry-tab-inner" id="tab-course_exams">
                <table class="tgb_main_table" id="tgb_course_exam_lst">
                    <tr>
                        <th>Exam Name</th>
                        <th>Date Created</th>
						<th>Actions</th>
						<?php 
                    foreach($exam_course_list as $course){?> 
                    <tr>
                        <td><?php echo $course->post_title; ?></td>
                        <td><?php echo  $course->post_date; ?></td>
                        <td>
							<a class="btn" href="<?php echo get_site_url().'/courses'.'/'. $course->post_name; ?>"  target="_blank">View</a>
                            <a class="btn" href="<?php echo get_site_url().'/exam-manager'.'/edit-exam-course/?exam_id='. $course->ID ?>">Manage</a>
                            <button class="delete_course_exam_btn" data-examid = "<?php echo $course->ID ?>">Delete</button>
                        </td>
                    </tr>
                    <?php } ?>
                    </tr>
                </table>
            </div>
        </div>

        <!--Create Question Bank-->
        <div class="content">
            <div class="entry-tab-inner" id="tab-new_course">
				<form  action="<?php the_permalink(); ?>" method="POST" enctype= "multipart/form-data">
				<label class="tgb_input_label">Exam Title</label>
				<input type="text" name="post_title" placeholder="Exam Title" class="tgb_input_text">
				<label class="tgb_input_label">Exam Excerpt</label>
				<input type="text" name="post_excerpt" placeholder="Exam Excerpt" class="tgb_input_text">
				<label class="tgb_input_label">Exam Description</label>
				<textarea name="post_content" id="" cols="30" rows="5" class="tgb_input_text marg_bot_5" placeholder="Exam Description"></textarea>
				<label class="tgb_input_label">Featured Image</label>
				<br>
				<image src="<?php  echo get_site_url().'/wp-content/uploads/2018/03/photo-1103595_960_720.png'?>" id ="preview_feature_image" width="300px" height="300px" class="tgb_cursor_upl marg_bot_5" ></image>
				<input type="file" name="exam_feature_image"  onchange="readURL(this);" class="tgb_hidden"/>
				<br>
				<input type="submit" name="newExamSubmit" value="CREATE" class="marg_top_15">
				<br>
        	</form>
            </div>
        </div>
    </div>
    </div>
</div>

<!--Delete Exam Confirm Modal-->
<div class="tgb_modal" id="delete_course_exam_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">Delete Exam</div>
        <div>
            <h4>Are you sure you want to delete the Exam?</h4>
            <form action="<?php the_permalink(); ?>"  method="POST" >
                <input type="hidden" name="exam_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="delete_exam_course" value="Ok"/>
            </form>
        </div>
    </div>
</div>
