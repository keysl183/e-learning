<?php 
//so outside users will not be able to access this remotely
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
include("lib/tgb_common.php");

function update_course_exam($exam_id, $post_title, $post_name, $post_excerpt, $post_content)
{
    $post_id = wp_update_post(
        array(
            'ID' => $exam_id,             
            'post_name' => sanitize_title($post_title),
            'post_title' => $post_title,
            'post_content' => $post_content,
            'post_excerpt' => $post_excerpt,
            'post_status' => 'publish',
            'post_type' => 'lp_course'
        ), true
    );
    //debug purposes
    if (is_wp_error($post_id)) {
        $errors = $post_id->get_error_messages();
        foreach ($errors as $error) {
            echo $error;
        }
    }
    //
}

if(isset($_POST["update_exam_submit"]))
{
	//will add validation later
	update_course_exam($_POST['exam_id'],$_POST['post_title'], $_POST['post_name'], $_POST['post_excerpt']
									, $_POST['post_content']);
    if(isset($_FILES["exam_feature_image"]))
    {
        $attachment_id = tgb_upload_image($_FILES["exam_feature_image"] ,$user->ID, $_POST['exam_id']);
    }
    $notif_msg = 'Successfully Updated!';

	$showNotif = true;
}

$user = wp_get_current_user();

$exam_details = get_a_course_exam($_REQUEST["exam_id"], $user->ID);

$feature_image_url = get_the_post_thumbnail_url($_REQUEST["exam_id"]);

$exam_sections = get_course_exam_sections($_REQUEST["exam_id"]);

$quest_banks = get_all_quest_bank();

if(!isset($showNotif))
{
    $showNotif = false;
}

?>
<h1>Edit Exam</h1>

<?php if ($showNotif) : ?>
<div class="learn-press-message success noti-success">
<p><?php echo $notif_msg; ?> </p>		
</div>
<?php endif;?>

<div class="tgb-exam-main">
    <div class="tgb-exam-main-inner tgb-new-exam">

    <ul class="tabs-title">
        <li class="tab active" data-tab="course_exam_info_tab">
        <a href="#" data-slug="#tab-course_exam_info">Course Exam Info</a>
        </li>
        
        <li class="tab" data-tab="exam_structure_tab">
        <a href="#" data-slug="#tab-exam_structure">Course Stucture</a>
        </li>
    </ul>

    <div class="tabs-content">
    <div class="edit_exam_top_controls">
        <a href="<?php echo get_site_url().'/exam-manager'?>" class="font_weight_700" >Back</a> &nbsp;
        <a href="<?php echo get_site_url().'/courses'.'/'. $exam_details->post_name; ?>" class="font_weight_700" target="_blank">View Exam</a>
    </div>
        <!--Edit Exam Basic Info-->
        <div class="content active">
            <div class="entry-tab-inner" id="tab-course_exam_info">
            <form  action="<?php the_permalink(); ?>" method="POST" enctype= "multipart/form-data">
				<label class="tgb_input_label">Exam Title</label>
				<input type="text" name="post_title" placeholder="Exam Title" class="tgb_input_text" value= "<?php echo $exam_details->post_title?>">
				<label class="tgb_input_label">Exam Excerpt</label>
				<input type="text" name="post_excerpt" placeholder="Exam Excerpt" class="tgb_input_text"  value= "<?php echo $exam_details->post_excerpt?>">
				<label class="tgb_input_label">Exam Description</label>
				<textarea name="post_content" id="" cols="30" rows="5" class="tgb_input_text marg_bot_5" placeholder="Exam Description"><?php echo $exam_details->post_content?></textarea>
				<label class="tgb_input_label">Featured Image</label>
				<br>
				<image src="<?php  echo ( $feature_image_url != '') ?  $feature_image_url : get_site_url().'/wp-content/uploads/2018/03/photo-1103595_960_720.png'?>" id ="preview_feature_image" width="300px" height="300px" class="tgb_cursor_upl marg_bot_5" ></image>
				<input type="file" name="exam_feature_image"  onchange="readURL(this);" class="tgb_hidden"/>
				<br>
                <input type="hidden" name="exam_id" value="<?php echo $_REQUEST['exam_id'] ?>"/>
				<input type="submit" name="update_exam_submit" value="Update" class="marg_top_15">
        	</form>
            </div>
        </div>

        <!--Course Exam Strucutre--> 
        <div class="content">
            <div class="entry-tab-inner" id="tab-exam_structure">
                <?php foreach($exam_sections as $section) {  ?>
                <div class="exam_section">
                    <div class="exam_section_title">
                        <h3 class="tgb_wrap_text_no_ov"><?php  echo $section->section_name; ?></h3>
                        <div class="controls">
                            <div class="ion-close-round control_action delete_section_btn" data-sectionid ="<?php echo  $section->section_id;?>"></div>
                            <div class="ion-edit control_action edit_sec_details_btn" data-sectionid ="<?php echo  $section->section_id;?>"></div>
                            <div class="ion-plus control_action add_new_sec_btn">
                                <!--Add New Section Element-->
                                <div class="add_new_sec_element">
                                    <ul>
                                        <li class="new_work_sheet" data-sectionid ="<?php echo  $section->section_id;?>">Work Sheet</li>
                                        <li class="new_unit_test" data-sectionid ="<?php echo  $section->section_id;?>">Unit Test</li>
                                        <li>Content Text</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="exam_section_content clearing">
                        <ul class="exam_section_items"  id="exam_section_sortable">
                            <?php $section_items = get_all_section_elements($section->section_id); 
                            foreach($section_items as $sec_items) {
                            ?>
                                <li> 
                                <?php
                                $iconclass = 'none';
                                switch($sec_items->tgb_item_type)
                                    {
                                        case 'worksheet':
                                            $iconclass = 'ion-android-list';
                                            break;
                                        case 'unittest':
                                            $iconclass = 'ion-android-folder-open';
                                            break;    
                                        default:
                                            $iconclass = 'none';    
                                    }
                                ?>                                
                                <div class="<?php echo $iconclass ?>">
                                    <span><?php echo $sec_items->post_title ?> </span>
                                </div>
                                </li>
                            <?php } ?>
                            
                          
                        </ul>
                    </div>
                </div>
                <?php } ?>

                <!--Will be used for clone-->
                <div class="exam_section disp_none">
                    <div class="exam_section_title">
                        <h3><?php  echo $section->section_name; ?></h3>
                        <div class="controls">
                            <div class="ion-close-round control_action"></div>
                            <div class="ion-edit control_action"></div>
                            <div class="ion-plus control_action add_new_sec_btn">
                                <!--Add New Section Element-->
                                <div class="add_new_sec_element">
                                    <ul>
                                        <li class="new_work_sheet">Work Sheet</li>
                                        <li>Unit Test</li>
                                        <li>Content Text</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="exam_section_content clearing">
                        <ul>
                            <li></li>
                        </ul>
                    </div>
                </div>
                <div class="add_new_element" id="add_new_section">
                    <label class="tgb_input_label tgb_add_section">+ Add Sections</label>
                </div>
            </div>
            
        </div>
    </div>
    </div>
</div>

<!--New Section-->
<div class="tgb_modal" id="new_exam_section_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">New Section</div>
        <div>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_new_exam_section">
                <label class="tgb_input_label">Section Title</label>
                <input type="text" name="section_title" placeholder="Section Title" class="tgb_input_text" value= "">
                <label class="tgb_input_label">Section Description</label>
				<textarea name="section_desc" cols="30" rows="3" class="tgb_input_text marg_bot_5" placeholder="Section Description"><?php echo ''?></textarea>
                <!--For security-->
                <input type="hidden" name="action" value="new_exam_section_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("new_exam_section");?>"/>
                <input type="hidden" name="ajax_new_section" value="AJAX" id="ajax_new_section" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="exam_id" value="<?php  echo $exam_details->ID ?>"/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="new_section_add" value="Ok"/>
            </form>
        </div>
    </div>
</div>
<!--END New Section-->

<!--Edit Section-->
<div class="tgb_modal" id="edit_exam_section_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">Edit Section</div>
        <div>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_edit_exam_section">
                <label class="tgb_input_label">Section Title</label>
                <input type="text" name="section_title" placeholder="Section Title" class="tgb_input_text" value= "" id="edit_section_title">
                <label class="tgb_input_label">Section Description</label>
				<textarea name="section_desc" cols="30" rows="3" class="tgb_input_text marg_bot_5" placeholder="Section Description" id="edit_section_desc"><?php echo ''?></textarea>
                <!--For security-->
                <input type="hidden" name="action" value="edit_exam_section_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("edit_exam_section");?>"/>
                <input type="hidden" name="ajax_edit_section" value="AJAX" id="ajax_edit_section" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="exam_id" value="<?php  echo $exam_details->ID ?>"/>
                <input type="hidden" name="section_id" value="" id="edit_section_id"/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="edit_section" value="Ok"/>
            </form>
        </div>
    </div>
</div>
<!--END Edit Section-->





<!--New Worksheet Modal-->
<!-- SELECT * FROM wp_posts as w JOIN wp_postmeta as wp ON w.ID = wp.post_id WHERE post_type ='lp_quiz' AND w.ID='4292' -->
<div class="tgb_modal" id="new_exam_worksheet_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">New Worksheet</div>
        <div>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_new_worksheet">
                <label class="tgb_input_label">Worksheet Title</label>
                <input type="text" name="worksheet_title" placeholder="Worksheet Title" class="tgb_input_text" value= "">
                <label class="tgb_input_label">Worksheet Description</label>
				<textarea name="worksheet_desc" cols="30" rows="3" class="tgb_input_text marg_bot_5" placeholder="Worksheet Description"></textarea>
                <label class="tgb_input_label">Question Bank</label>
                <select name="question_bank">
                    <?php foreach($quest_banks as $bank) {?>
                        <option value="<?php echo  $bank->quest_bank_id ?>" ><?php echo $bank->question_bank_title?></option>
                    <?php }?>
                </select>
                <label class="tgb_input_label">Question Type to Pull</label>
                <select name="quest_type">
                    <option value="multi_choice">Multiple Choice</option>
                    <option value="essay" disabled>Essay</option>
                    <option value="audio" disabled>Audio</option>
                </select>
                <label class="tgb_input_label">No of Question</label>
                <input type="text" name="question_no" placeholder="No Of Question" class="tgb_input_text" value= "10"/>
                <!--For security-->
                <input type="hidden" name="action" value="new_exam_worksheet_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("new_exam_worksheet");?>"/>
                <input type="hidden" name="ajax_new_worksheet" value="AJAX" id="ajax_new_worksheet" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="exam_id" value="<?php  echo $exam_details->ID ?>"/>
                <input type="hidden" name="section_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="new_section_add" value="Ok"/>
            </form>
        </div>
    </div>
</div>
<!--END New Worksheet Modal-->

<!--New Unit Test Modal-->
<!-- SELECT * FROM wp_posts as w JOIN wp_postmeta as wp ON w.ID = wp.post_id WHERE post_type ='lp_quiz' AND w.ID='4292' -->
<div class="tgb_modal" id="new_exam_unit_test_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">New Test</div>
        <div>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_new_unittest">
                <label class="tgb_input_label">Test Title</label>
                <input type="text" name="unit_test_title" placeholder="Test Title" class="tgb_input_text" value= "">
                <label class="tgb_input_label">Test Description</label>
				<textarea name="unit_test_desc" cols="30" rows="3" class="tgb_input_text marg_bot_5" placeholder="Test Description"></textarea>
                <label class="tgb_input_label">Target Worksheets</label> <br/>
                <div class="target_worksheets_dv">
                </div>
                <label class="tgb_input_label">No of Question</label>
                <input type="text" name="question_no" placeholder="No Of Question" class="tgb_input_text" value= "25"/>
                <label class="tgb_input_label">Time Limit (Minutes)</label>
                <input type="text" name="time_limit" placeholder="Time Limit (Minutes)" class="tgb_input_text" value= "60"/>
                <!--For security-->
                <input type="hidden" name="action" value="new_exam_unit_test_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("new_exam_unit_test");?>"/>
                <input type="hidden" id="worksheets_security" value="<?php  echo wp_create_nonce("get_section_worksheets_ajax");?>"/>
                
                <input type="hidden" name="ajax_new_unit_test" value="AJAX" id="ajax_new_unit_test" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="exam_id" value="<?php  echo $exam_details->ID ?>"/>
                <input type="hidden" name="section_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="new_section_add" value="Ok"/>
            </form>
        </div>
    </div>
</div>
<!--END New Unit Test Modal-->


<!--DELETE SECTION MODAL-->
<div class="tgb_modal" id="delete_section_modal">
    <div class="tgb_modal_inner">
        <div class="widget-title">Delete Section</div>
        <div>
            <h4>Are you sure you want to delete the Section?</h4>
            <form action="<?php the_permalink(); ?>"  method="POST" id="frm_delete_section">
                <!--For security-->
                <input type="hidden" name="action" value="delete_question_ajax"/>
                <input type="hidden" name="security" value="<?php  echo wp_create_nonce("delete_exam_section");?>"/>
                <input type="hidden" name="ajax_delete_quest" value="AJAX" id="ajax_delete_quest" data-ajaxurl = "<?php echo admin_url("admin-ajax.php"); ?>"/>
                <!--For security-->
                <input type="hidden" name="section_id" value=""/>
                <input type="button" value="Cancel" class="tgb_modal_close"/>
                <input type="submit" name="delete_section_btn" value="Ok"/>
            </form>
        </div>
    </div>
</div>
<!--DELETE SECTION MODAL-->





