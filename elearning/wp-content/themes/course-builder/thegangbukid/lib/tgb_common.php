<?php 

/*The Gang Bukid Common Helper Functions*/


/*Image Upload */

function tgb_upload_image($file_data,$uploader_id, $post_id)
{
    $uploaddir = wp_upload_dir();
    $file = $file_data;

    $uniqueName = $uploader_id.date("YmdHis").".jpg";//hack force to jpg. will change this later
    $uploadfile = $uploaddir['path'] . '/' . basename( $uniqueName );

    move_uploaded_file( $file["tmp_name"] , $uploadfile );
    $filename = basename( $uploadfile );

    $wp_filetype = wp_check_filetype(basename($filename), null );

    $attachment = array(
        'post_mime_type' => 'image/jpeg',
        'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
        'post_content' => '',
        'post_status' => 'inherit',
        'menu_order' => $_i + 1000
    );

    $attach_id = wp_insert_attachment( $attachment, $uploadfile,$post_id);


    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
 
    // Generate the metadata for the attachment, and update the database record.
    $attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
    wp_update_attachment_metadata( $attach_id, $attach_data );
    
    set_post_thumbnail($post_id, $attach_id );

    return $attach_id;
}

function tgb_upload_image_answer($file_data,$uploader_id)
{
    $uploaddir = wp_upload_dir();
    $file = $file_data;

    $uniqueName = $uploader_id.date("YmdHis").".jpg";//hack force to jpg. will change this later
    $uploadfile = $uploaddir['path'] . '/' . basename( $uniqueName );

    move_uploaded_file( $file["tmp_name"] , $uploadfile );
    $filename = basename( $uploadfile );

    $wp_filetype = wp_check_filetype(basename($filename), null );

    $attachment = array(
        'post_mime_type' => 'image/jpeg',
        'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
        'post_content' => '',
        'post_status' => 'inherit',
        'menu_order' => 1000
    );

    $attach_id = wp_insert_attachment( $attachment, $uploadfile);


    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
 
    // Generate the metadata for the attachment, and update the database record.
    $attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    return $attach_id;
}


function get_all_courses_exams($author_id)
{
    $args = array(
        'numberposts' => -1, //negative one means all posts
        'orderby' => 'desc',
        'post_type' => 'lp_course',
        'post_status' => 'publish',
        'post_author' => $author_id
   );
    return get_posts( $args );
}


function get_a_course_exam($exam_id, $author_id)
{
   $details = get_post( $exam_id ); //get only the first
   return  $details;
}

function get_course_exam_sections($exam_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_learnpress_sections WHERE section_course_id=".$exam_id);
    return $result;
}


function get_all_quest_bank()
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_tgb_quest_bank");
    return $result;
}

function get_all_section_elements($section_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id
    WHERE ws.section_id=".$section_id);
    return $result;
}

function get_all_section_worksheet_elements($section_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id
    WHERE wt.tgb_item_type = 'worksheet' AND ws.section_id=".$section_id);
    return $result;
}

?>