<?php 

//this is a helper php file that will be used in functions.php
//Will contains validation and the core saving
include("tgb_validation.php");

function validate_add_question()
{
    $user = wp_get_current_user();
    $hasError = false;

    $quest_bank_err = '';
    $quest_bank_count = check_quest_bank_exists($_POST["quest_bank"]);
    $errors;
    if($quest_bank_count == 0)
    {
        $hasError = true;
        $quest_bank_err = 'Target Question Bank does not exists!.';
        $errors["quest_bank_err"] = $quest_bank_err;
    }
    else
    {
        $quest_bank_id =  $_POST["quest_bank"];
    }

    $quest_title = '';
    $quest_title_err = fieldRequiredError($_POST['quest_title'], "Question Title");
    if($quest_title_err != '')
    {
        $hasError = true;
        $errors["quest_title_err"] = $quest_title_err;
    }else
    {
        $quest_title =  sanitizeNormalText($_POST['quest_title']);
    }

    $quest_desc = '';
    $quest_desc_err = fieldRequiredError($_POST['quest_desc'], "Question Description");
    if($quest_desc_err != '')
    {
        $hasError = true;
        $errors["quest_desc_err"] = $quest_desc_err;
    }else
    {
        $quest_desc =  sanitizeNormalText($_POST['quest_desc']);
    }
    
    $quest_type= '';
    $quest_type_choices = array('multi_choice','essay', 'audio');
    $quest_type_choice_err = '';
    if(!isset($_POST["quest_type"]) || $_POST["quest_type"] == '')
    {
        $quest_type_choice_err= "Target Question Type is not existing";
        $hasError = true;
        $errors["quest_type_choice_err"] = $quest_type_choice_err;
    }
    else if(!in_array($_POST["quest_type"], $quest_type_choices))
    {
        $quest_type_choice_err= "Target Question Type is not existing";
        $hasError = true;
        $errors["quest_type_choice_err"] = $quest_type_choice_err;
    }else
    {
        $quest_type = $_POST["quest_type"];
    }

    $quest_correct_ans_err = fieldRequiredError($_POST['quest_correct_ans'], "Question Answer");
    if($quest_correct_ans_err != '')
    {
        $hasError = true;
        $errors["quest_correct_ans_err"] = $quest_correct_ans_err;
    }


    if($quest_type=='multi_choice')
    {
        $quest_option_one_err = fieldRequiredError($_POST['quest_option_one'], "Question Answer One");
        if($quest_option_one_err != '')
        {
            $hasError = true;
            $errors["quest_option_one_err"] = $quest_option_one_err;
        }

        $quest_option_two_err = fieldRequiredError($_POST['quest_option_two'], "Question Answer Two");
        if($quest_option_two_err != '')
        {
            $hasError = true;
            $errors["quest_option_two_err"] = $quest_option_two_err;
        }

        $quest_option_three_err = fieldRequiredError($_POST['quest_option_three'], "Question Answer Three");
        if($quest_option_three_err != '')
        {
            $hasError = true;
            $errors["quest_option_three_err"] = $quest_option_three_err;
        }

        $quest_option_four_err = fieldRequiredError($_POST['quest_option_four'], "Question Answer Four");
        if($quest_option_four_err != '')
        {
            $hasError = true;
            $errors["quest_option_four_err"] = $quest_option_four_err;
        }
    }

    if($hasError)
    {
      $errors["ok"] = "NOTOK";
      return $errors;
    }
    else
    {
        $is_answer = ($_POST["quest_correct_ans"] == "ans_first" ? 'yes' : 'no');
        $choiceOne = array(
                'text' => $_POST["quest_option_one"],
                'value' => 'option_first',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_seconds"? 'yes' : 'no');
        $choiceTwo = array(
                'text' => $_POST["quest_option_two"],
                'value' => 'option_seconds',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_third" ? 'yes' : 'no');
        $choiceThree = array(
                'text' => $_POST["quest_option_three"],
                'value' => 'option_third',
                'is_true' =>  $is_answer
            );
        $is_answer = ($_POST["quest_correct_ans"] == "ans_fourth" ? 'yes' : 'no');
        $choiceFour = array(
                'text' => $_POST["quest_option_four"],
                'value' => 'option_fourth',
                'is_true' =>  $is_answer
            );               
        
        $all_choices = array( $choiceOne, $choiceTwo, $choiceThree, $choiceFour);
        $new_question_id = insert_questions($quest_type, $quest_bank_id, $quest_title,  $quest_desc,$all_choices, $user->ID  );
        
        $errors["ok"] = "OK";
        $errors["new_quest_id"] = $new_question_id;
        //wala daya lag hahahaha
        $errors["append_table"] = "<tr><td>$quest_title</td><td>$quest_desc</td><td>$quest_type</td><td><button>Edit</button> <button>Delete</button></td></tr>";    
        return  $errors;
    }
}

//insers into wp_posts as post_type = lp_question
//also in wp_post_meta _lp_type
//also inserts the answer value in wp_learnpress_question_answers
//currently accepts multiple choice only 
function insert_questions($quest_type,$quest_bank_id ,$question_title, $question_desc,$answers, $author_id)
{
	$post_id = -1; //not created
	if(get_page_by_title($question_title) == null)
	{
		$post_id = wp_insert_post(
			array(
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_author' => $author_id,
				'post_name' => sanitize_title($question_title),
				'post_title' => $question_title,
				'post_content' => $question_desc,
				'post_status' => 'publish',
				'post_type' => 'lp_question'
			)
		);
	}else
	{
		$post_id = -2; //exist
    }

    if($post_id != -1 && $post_id != -2)
    {
        update_post_meta( $post_id, "_lp_type", 'single_choice'); //wp_post_meta

        //insert the answers
        global $wpdb;
        $order = 1;
        foreach($answers as $answer)
        {
            $value = $wpdb->insert("wp_learnpress_question_answers",
                array(
                    'question_id' => $post_id,
                    'answer_data' => maybe_serialize($answer),
                    'answer_order' => $order
                )
            );
            $order = $order + 1;
        }

        //insert into bank table
        $value = $wpdb->insert("wp_tgb_quest_bank_meta",
            array(
                'question_bank_id' => $quest_bank_id,
                'question_id' => $post_id
            )
        );
    }
	return $post_id;
}

function delete_question()
{
    $user = wp_get_current_user();
    $hasError = false;
    $errors;
    $question_id = $_POST["question_id"];
    
    if(get_post_status($question_id) == FALSE)
    {
        $hasError = true;
        $errors["ok"] = "NOTOK";
    }
    else
    {
        //do the righ thing here
        //second param is to bypass trash and delete it completely. Also deletes the data in wp_post_meta
       $success = wp_delete_post($question_id, true);
       if($success == true)
       {
        $errors["ok"] = "OK";
       }else{    $errors["ok"] = "NOTOK"; }
    }
    return  $errors;
}

function validate_exam_section()
{
    $hasError = false;
    $errors;
    $exam_id = $_POST["exam_id"];
    if(get_post_status($exam_id) == FALSE)
    {
        $hasError = true;
        $errors["course_exam_err"] =  'Target Exam does not exists!.';
    }
    
    $section_title = '';
    $section_title_err = fieldRequiredError($_POST['section_title'], "Section Title");
    if($section_title_err != '')
    {
        $hasError = true;
        $errors["section_title_err"] = $section_title_err;
    }else
    {
        $section_title =  sanitizeNormalText($_POST['section_title']);
    }

    $section_desc = '';
    $section_desc_err = fieldRequiredError($_POST['section_desc'], "Section Description");
    if($section_desc_err != '')
    {
        $hasError = true;
        $errors["section_desc_err"] = $section_desc_err;
    }else
    {
        $section_desc =  sanitizeNormalText($_POST['section_desc']);
    }

    if($hasError)
    {
        $errors["ok"] = "NOTOK";
    }
    else
    {
        create_new_exam_section($section_title, $section_desc,  $exam_id,1 );
        $errors["ok"] = "OK";
    }
  
    return  $errors;
}

function create_new_exam_section($section_tile, $section_desc, $exam_id, $section_order)
{
    global $wpdb;
    $value = $wpdb->insert("wp_learnpress_sections",
        array(
            'section_name' => $section_tile,
            'section_description' => $section_desc,
            'section_course_id' => $exam_id,
            'section_order' => $section_order
        )
    );
    return $wpdb->insert_id;
}

function validate_edit_exam_section()
{
    $hasError = false;
    $errors;
    $exam_id = $_POST["exam_id"];
    if(get_post_status($exam_id) == FALSE)
    {
        $hasError = true;
        $errors["course_exam_err"] =  'Target Exam does not exists!.';
    }

    $section_id = $_POST['section_id'];
    //do a validation thiny here later
    
    $section_title = '';
    $section_title_err = fieldRequiredError($_POST['section_title'], "Section Title");
    if($section_title_err != '')
    {
        $hasError = true;
        $errors["section_title_err"] = $section_title_err;
    }else
    {
        $section_title =  sanitizeNormalText($_POST['section_title']);
    }

    $section_desc = '';
    $section_desc_err = fieldRequiredError($_POST['section_desc'], "Section Description");
    if($section_desc_err != '')
    {
        $hasError = true;
        $errors["section_desc_err"] = $section_desc_err;
    }else
    {
        $section_desc =  sanitizeNormalText($_POST['section_desc']);
    }

    if($hasError)
    {
        $errors["ok"] = "NOTOK";
    }
    else
    {
        $errors["id"] = update_exam_section($section_id, $section_title, $section_desc,  $exam_id,1 );
        
        $errors["ok"] = "OK";
    }
  
    return  $errors;
}

function update_exam_section($section_id, $section_tile, $section_desc, $exam_id, $section_order)
{
    global $wpdb;
    $value = $wpdb->update("wp_learnpress_sections",
        array(
            'section_name' => $section_tile,
            'section_description' => $section_desc,
            'section_course_id' => $exam_id,
            'section_order' => $section_order
        ),
        array(
            'section_id' => $section_id
        )
    );
    return $wpdb->insert_id;
}


function validate_add_worksheet()
{
    $hasError = false;
    $errors;
    $exam_id = $_POST["exam_id"];
    if(get_post_status($exam_id) == FALSE)
    {
        $hasError = true;
        $errors["course_exam_err"] =  'Target Exam does not exists!.';
    }

    $worksheet_title = '';
    $worksheet_title_err = fieldRequiredError($_POST['worksheet_title'], "Worksheet Title");
    if($worksheet_title_err != '')
    {
        $hasError = true;
        $errors["worksheet_title_err"] = $worksheet_title_err;
    }else
    {
        $worksheet_title =  sanitizeNormalText($_POST['worksheet_title']);
    }

    $worksheet_desc = '';
    $worksheet_desc_err = fieldRequiredError($_POST['worksheet_desc'], "Worksheet Description");
    if($worksheet_desc_err != '')
    {
        $hasError = true;
        $errors["worksheet_desc_err"] = $worksheet_desc_err;
    }else
    {
        $worksheet_desc =  sanitizeNormalText($_POST['worksheet_desc']);
    }

    $quest_bank_err = '';
    $quest_bank_count = check_quest_bank_exists($_POST["question_bank"]);
    $errors;
    if($quest_bank_count == 0)
    {
        $hasError = true;
        $quest_bank_err = 'Target Question Bank does not exists!.';
        $errors["quest_bank_err"] = $quest_bank_err;
    }
    else
    {
        $quest_bank_id =  $_POST["question_bank"];
    }
      
    $quest_type= '';
    $quest_type_choices = array('multi_choice','essay', 'audio');
    $quest_type_choice_err = '';
    if(!isset($_POST["quest_type"]) || $_POST["quest_type"] == '')
    {
        $quest_type_choice_err= "Target Question Type is not existing";
        $hasError = true;
        $errors["quest_type_choice_err"] = $quest_type_choice_err;
    }
    else if(!in_array($_POST["quest_type"], $quest_type_choices))
    {
        $quest_type_choice_err= "Target Question Type is not existing";
        $hasError = true;
        $errors["quest_type_choice_err"] = $quest_type_choice_err;
    }else
    {
        $quest_type = $_POST["quest_type"];
    }

    $question_no = $_POST['question_no'];
    if(!is_numeric($question_no))
    {
        $hasError = true;
        $errors["question_no_err"]  = "Not a Valid Number";
    }
    else
    {
        $question_no =  $_POST['question_no'];
    }

    //do a validation for section_id later
    $section_id = $_POST['section_id'];

    if($hasError)
    {
        $errors["ok"] = "NOTOK";
    }
    else
    {
        $user = wp_get_current_user();
        $status = inser_new_worksheet($quest_type, $quest_bank_id, $section_id,
         $worksheet_title,  $worksheet_desc,  $question_no, 1,  $user->ID);
        $errors["ok"] = "OK";
    }
  
    return  $errors;
}

function inser_new_worksheet($quest_type,$quest_bank_id,$section_id,$worksheet_title, $worksheet_desc ,$quest_no, $order, $author_id)
{
    $post_id = -1; //not created
	if(get_page_by_title($worksheet_title) == null)
	{
		$post_id = wp_insert_post(
			array(
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_author' => $author_id,
				'post_name' => sanitize_title($worksheet_title),
				'post_title' => $worksheet_title,
				'post_content' => $worksheet_desc,
				'post_status' => 'publish',
				'post_type' => 'lp_quiz'
			)
		);
	}else
	{
		$post_id = -2; //exist
    }

    if($post_id != -1 && $post_id != -2)
    {
        update_post_meta( $post_id, "_lp_random_mode", 'yes'); //wp_post_meta
        update_post_meta( $post_id, "_lp_show_hide_question", 'hide');
        update_post_meta( $post_id, "_lp_show_result", 'yes');
        update_post_meta( $post_id, "_lp_duration", '0 minute');  //0 means no time limit
        update_post_meta( $post_id, "_lp_retake_count", '10000'); //0 means infinite try
        update_post_meta( $post_id, "_lp_passing_grade", '70'); 
        update_post_meta( $post_id, "_lp_passing_grade_type", 'percentage');
        
        global $wpdb;
        //insert the new worksheet to section item
        $section_item_id = $wpdb->insert("wp_learnpress_section_items",
        array(
            'section_id' => $section_id,
            'item_id' => $post_id, //the id of created worksheet
            'item_order' => $order,
            'item_type' => 'lp_quiz'
        )
        );
        //insert additional information regarding the worksheet
        $tgb_value = $wpdb->insert("wp_tgb_exam_section_items_extension",
        array(
            'section_item_id' => $section_item_id,
            'quest_bank_id' => $quest_bank_id,
            'item_id' => $post_id, //the id of created worksheet
            'tgb_item_type' => 'worksheet'
        )
        );
    }
}

function validate_add_unit_test()
{
    $hasError = false;
    $errors;
    $exam_id = $_POST["exam_id"];
    if(get_post_status($exam_id) == FALSE)
    {
        $hasError = true;
        $errors["course_exam_err"] =  'Target Exam does not exists!.';
    }

    $unit_test_title = '';
    $unit_test_title_err = fieldRequiredError($_POST['unit_test_title'], "Test Title");
    if($unit_test_title_err != '')
    {
        $hasError = true;
        $errors["unit_test_title_err"] = $unit_test_title_err;
    }else
    {
        $unit_test_title =  sanitizeNormalText($_POST['unit_test_title']);
    }

    $unit_test_desc = '';
    $unit_test_desc_err = fieldRequiredError($_POST['unit_test_desc'], "Test Description");
    if($unit_test_desc_err != '')
    {
        $hasError = true;
        $errors["unit_test_desc_err"] = $unit_test_desc_err;
    }else
    {
        $unit_test_desc =  sanitizeNormalText($_POST['unit_test_desc']);
    }

    if(!isset($_POST["target_worksheets_id"]))
    {
        $hasError = true;
        $errors["target_worksheets_err"] ='Target Worksheets is Required.';
    }
    else
    {
        $worksheets = $_POST["target_worksheets_id"];
        //do a validation to check if worksheets is exsiting here later
    }


    $question_no = $_POST['question_no'];
    if(!is_numeric($question_no))
    {
        $hasError = true;
        $errors["question_no_err"]  = "Not a Valid Number";
    }
    else
    {
        $question_no =  $_POST['question_no'];
    }

    $time_limit = $_POST['time_limit']; //in minutes
    if(!is_numeric($time_limit))
    {
        $hasError = true;
        $errors["time_limit_err"]  = "Not a Valid Number";
    }
    else
    {
        $time_limit =  $_POST['time_limit'];
    }

    //do a validation for section_id later
    $section_id = $_POST['section_id'];

    if($hasError)
    {
        $errors["ok"] = "NOTOK";
    }
    else
    {
        $user = wp_get_current_user();
        $status = insert_new_unittest($quest_type
        , $section_id,
                $unit_test_title,  $unit_test_desc,  $question_no, $time_limit, $worksheets, 1,  $user->ID);//1 is hack will do this right way later
        $errors["ok"] = "OK";
    }
  
    return  $errors;
}

function insert_new_unittest($quest_type
,$section_id,$test_title, $test_desc ,$quest_no,$time_limit,$worksheets_id, $order, $author_id)
{
    $post_id = -1; //not created
	if(get_page_by_title($worksheet_title) == null)
	{
		$post_id = wp_insert_post(
			array(
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_author' => $author_id,
				'post_name' => sanitize_title($test_title),
				'post_title' => $test_title,
				'post_content' => $test_desc,
				'post_status' => 'publish',
				'post_type' => 'lp_quiz'
			)
		);
	}else
	{
		$post_id = -2; //exist
    }

    if($post_id != -1 && $post_id != -2)
    {
        update_post_meta( $post_id, "_lp_random_mode", 'yes'); //wp_post_meta
        update_post_meta( $post_id, "_lp_show_hide_question", 'hide');
        update_post_meta( $post_id, "_lp_show_result", 'yes');
        update_post_meta( $post_id, "_lp_duration", $time_limit.' minute');  //0 means no time limit
        update_post_meta( $post_id, "_lp_retake_count", '1'); //0 means infinite try
        update_post_meta( $post_id, "_lp_passing_grade", '70'); 
        update_post_meta( $post_id, "_lp_passing_grade_type", 'percentage');
        global $wpdb;
        //insert the new worksheet to section item
        $section_item_id = $wpdb->insert("wp_learnpress_section_items",
        array(
            'section_id' => $section_id,
            'item_id' => $post_id, //the id of created worksheet
            'item_order' => $order,
            'item_type' => 'lp_quiz'
        )
        );
        //for some reason this is not working
        //insert additional information regarding the worksheet
        $tgb_value = $wpdb->insert("wp_tgb_exam_section_items_extension",
        array(
            'section_item_id' => $section_item_id,
            'quest_bank_id' => 0, //set to 0 because test can have multiple banks to get from
            'item_id' => $post_id, //the id of created worksheet
            'tgb_item_type' => 'unittest',
            'question_bank_srlz' => maybe_serialize($worksheets_id)
        )
        );
    }
}


function get_worksheets_by_section_id($section_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id
    WHERE wt.tgb_item_type = 'worksheet' AND ws.section_id=".$section_id);
    return $result;
}


function get_worksheet_by_course_id($course_id){
    global $wpdb;
    $result =  $wpdb->get_results("SELECT wc.section_course_id,ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type, wt.quest_bank_id    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id JOIN wp_learnpress_sections as wc ON wc.section_id = ws.section_id
    WHERE wt.tgb_item_type = 'worksheet' AND wc.section_course_id=".$course_id);
    return $result;
}

function get_question_numbers_in_qiuzz($quiz_id,$user_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT COUNT(*) as question_count FROM `wp_learnpress_quiz_questions` WHERE quiz_id=".$quiz_id." AND user_id=".$user_id);
    return $result;
}

//GETS RANDOMIZE 10 QUESTION FOR WORKSHEET
function get_all_available_question_by_quest_bank($quest_bank_id)
{  
    global $wpdb;
    $result =  $wpdb->get_results("SELECT question_id FROM wp_tgb_quest_bank_meta WHERE question_bank_id =".$quest_bank_id. " ORDER BY RAND() LIMIT 10");
    return $result;
}

function insert_generated_question_to_worksheet($quizz_id, $question_id, $order, $user_id)
{
    global $wpdb;
    $tgb_value = $wpdb->insert("wp_learnpress_quiz_questions",
    array(
        'quiz_id' => $quizz_id,
        'question_id' => $question_id,
        'question_order' => $order,
        'user_id' => $user_id
    )
    );
}

function get_unittest_by_course_id($course_id){
    global $wpdb;
    $result =  $wpdb->get_results("SELECT wc.section_course_id,ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type, wt.quest_bank_id    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id JOIN wp_learnpress_sections as wc ON wc.section_id = ws.section_id
    WHERE wt.tgb_item_type = 'unittest' AND wc.section_course_id=".$course_id);
    return $result;
}

function get_course_all_exam_sections($exam_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_learnpress_sections WHERE section_course_id=".$exam_id);
    return $result;
}

function get_all_section_elements_api($section_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT ws.section_id,ws.item_id,wp.id,wp.post_title, wp.post_type, wt.tgb_item_type    
    FROM `wp_learnpress_section_items` as ws JOIN wp_posts as wp ON wp.ID = ws.item_id
    JOIN wp_tgb_exam_section_items_extension as wt ON ws.item_id= wt.item_id
    WHERE ws.section_id=".$section_id);
    return $result;
}


function get_course_exam_a_section($section_id)
{
    global $wpdb;
    $result =  $wpdb->get_results("SELECT * FROM wp_learnpress_sections WHERE section_id=".$section_id);
    return $result;
}

//GETS RANDOMIZE 25 QUESTION FOR Unit Test
//$quest_bank_ids is array
function get_all_available_question_using_quest_banks($quest_bank_ids)
{  
    global $wpdb;
    //$query = "SELECT question_id FROM wp_tgb_quest_bank_meta WHERE question_bank_id IN(".implode(',',$quest_bank_ids). ") ORDER BY RAND() LIMIT 25";
    $result =  $wpdb->get_results("SELECT question_id FROM wp_tgb_quest_bank_meta WHERE question_bank_id IN(".implode(',',$quest_bank_ids). ") ORDER BY RAND() LIMIT 25");
    return $result;
}

?>