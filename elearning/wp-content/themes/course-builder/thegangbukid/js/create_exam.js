/*
The Gang Bukid
*/

(function($){
    "use strict";
    
$(document).ready(function(){

    //Multitab 
    $(".tabs-title li").click(function(){
        $(this).addClass("active");
        var $tab = $(this).data("tab");
        var $contentDiv =  "tab-" + $(this).data("tab").replace("_tab",""); 
        $("#"+$contentDiv).parent().addClass("active");

        //remove the style to other tabs
        $(".tabs-title li").each(function(){
      
            if($(this).data("tab") != $tab)
            {
                $(this).removeClass("active"); 
                var $contentDiv =  "tab-" + $(this).data("tab").replace("_tab",""); 
                $("#"+$contentDiv).parent().removeClass("active"); 
            }
        });
    });

    //Modal
    $("#open_modal_btn").click(function(){
        $("#new_question_modal").show();
    });
    $(".tgb_modal_close").click(function(){
        $(this).closest(".tgb_modal").hide();
    });

    //feature image upload
    $("#preview_feature_image").click(function(){

        $('input[name=exam_feature_image]').trigger('click');
    });

    $(".answer_image_file").hide();
    $("select[name=quest_type]").change(function(){
        if($(this).val() == "multi_choice_img")
        {
            $(".answer_image_file").show();
            $(".tgb_answer_input").hide();
        }
        else
        {
            $(".answer_image_file").hide();
            $(".tgb_answer_input").show();
        }
       
    });

    //New Question AJAX
    $("#frm_new_question").submit(function(){

        if($("select[name=quest_type]").val() == "multi_choice_img")
        {
            return; //do not process via ajax. Process it using the page function
        }
        //do a client side validation later

        //only backend validation for now
        $.ajax({
            url:  $("#ajax_new_quest_add").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_new_question").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {
                    if(objects.hasOwnProperty("quest_bank_err")){
                        //target question bank does not exists
                    }
                    if(objects.hasOwnProperty("quest_title_err")){
                        $("input[name='quest_title']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='quest_title']").removeClass('tgb_input_invalid');
                    }
                    
                    if(objects.hasOwnProperty("quest_desc_err")){
                        $("textarea[name='quest_desc']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("textarea[name='quest_desc']").removeClass('tgb_input_invalid');
                    }
                    
                    if(objects.hasOwnProperty("quest_type_choice_err")){
                        $("select[name='quest_type']").addClass('tgb_input_invalid');
                    } 
                    else
                    {
                        $("select[name='quest_type']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("quest_option_one_err")){
                        $("input[name='quest_option_one']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='quest_option_one']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("quest_option_two_err")){
                        $("input[name='quest_option_two']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='quest_option_two']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("quest_option_three_err")){
                        $("input[name='quest_option_three']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='quest_option_three']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("quest_option_four_err")){
                        $("input[name='quest_option_four']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='quest_option_four']").removeClass('tgb_input_invalid');
                    }


                    if(objects.hasOwnProperty("quest_correct_ans_err")){
                        //when no answer was selected correctly
                        $("input[name='quest_option_one']").addClass('tgb_input_invalid');
                        $("input[name='quest_option_two']").addClass('tgb_input_invalid');
                        $("input[name='quest_option_three']").addClass('tgb_input_invalid');
                        $("input[name='quest_option_four']").addClass('tgb_input_invalid');
                    }

                    alert("Theres an error");
                }else
                {
                    clear_form_values_new_ques();
                    $("#tgb_table_question_lst").append(objects.append_table);
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });

    //Delete Question AJAX
    $("#frm_delete_question").submit(function(){
        //only backend validation for now
        $.ajax({
            url:  $("#ajax_delete_quest").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_delete_question").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {//do something here like modal showing error has occured later
                    alert("Theres an error");
                }else
                {
                    //remove the tr of deleted question
                    $("#tgb_table_question_lst").find("[data-questionid='"+ $("input[name=question_id]").val()+"']").closest("tr").remove();
                    $(".tgb_modal_close").click(); //close the modal
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });

    //dialog result message box
    $("#tgb_table_question_lst").on("click",".delete_question_btn",function(){
        
        $("#delete_question_modal").show();
        $("input[name=question_id]").val($(this).data("questionid"));
    });

    //dialog result delete question bank
    $("#tgb_table_question_bank_lst").on("click",".delete_question_bank_btn",function(){
        $("#delete_question_bank_modal").show();
        $("input[name=question_bank_id]").val($(this).data("questionbankid"));
    });

    //dialog result delete exam
    $("#tgb_course_exam_lst").on("click",".delete_course_exam_btn",function(){
        $("#delete_course_exam_modal").show();
        $("input[name=exam_id]").val($(this).data("examid"));
    });
    
    //Misc Functions
    function clear_form_values_new_ques()
    {
        $("input[name='quest_title']").removeClass('tgb_input_invalid');
        $("textarea[name='quest_desc']").removeClass('tgb_input_invalid');
        $("select[name='quest_type']").removeClass('tgb_input_invalid');
        $("input[name='quest_option_one']").removeClass('tgb_input_invalid');
        $("input[name='quest_option_two']").removeClass('tgb_input_invalid');
        $("input[name='quest_option_three']").removeClass('tgb_input_invalid');
        $("input[name='quest_option_four']").removeClass('tgb_input_invalid');
        $("input[name='quest_title']").val("");
        $("textarea[name='quest_desc']").val("");
        $("input[name='quest_option_one']").val("");
        $("input[name='quest_option_two']").val("");
        $("input[name='quest_option_three']").val("");
        $("input[name='quest_option_four']").val("");
        $(".tgb_modal_close").click();
    }

});


})(jQuery);

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_feature_image').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


