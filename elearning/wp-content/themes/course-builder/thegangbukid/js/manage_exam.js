(function($){
    "use strict";

$(document).ready(function(){
    
    //Add new Section Modal
    $("#add_new_section").click(function(){
        $("#new_exam_section_modal").show();
    });

    //New Exam Section Submit
    $("#frm_new_exam_section").submit(function(){
        //will add js client side validation later
        //only backend validation for now
        $.ajax({
            url:  $("#ajax_new_section").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_new_exam_section").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {//do something here like modal showing error has occured later

                    if(objects.hasOwnProperty("section_title_err")){
                        $("input[name='section_title']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='section_title']").removeClass('tgb_input_invalid');
                    }
                    
                    if(objects.hasOwnProperty("section_desc_err")){
                        $("textarea[name='section_desc']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("textarea[name='section_desc']").removeClass('tgb_input_invalid');
                    }

                }else
                {
                    $("input[name='section_title']").removeClass('tgb_input_invalid');
                    $("textarea[name='section_desc']").removeClass('tgb_input_invalid');
                    //do the drawing of section here
                    let $clone = $(".exam_section.disp_none").first().clone().removeClass("disp_none");
                    $clone.find("h3").text( $("input[name='section_title']").val());
                    $($clone).insertBefore("#add_new_section");
                    $(".tgb_modal_close").click(); //close the modal
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });

    $("#tab-exam_structure").on("click",".add_new_sec_btn",function(){
        $(this).find(".add_new_sec_element").toggle();
    });

    $("#tab-exam_structure").on('click','.edit_sec_details_btn', function(){
        $.ajax({
            url:  $("#ajax_edit_section").data("ajaxurl"),
            type: 'GET',
            dataType: 'application/json',
            data:{action: 'get_exam_section_details', section_id:$(this).data('sectionid')},
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                $("#edit_section_title").val(objects[0].section_name);
                $("#edit_section_desc").val(objects[0].section_description);
                $("#edit_section_id").val(objects[0].section_id);
                console.log(response);
            }
        });
        $("#edit_exam_section_modal").show();
    });


    //New Exam Section Submit
    $("#frm_edit_exam_section").submit(function(){
        //will add js client side validation later
        //only backend validation for now
        $.ajax({
            url:  $("#ajax_new_section").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_edit_exam_section").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {//do something here like modal showing error has occured later
                    if(objects.hasOwnProperty("section_title_err")){
                        $("input[name='section_title']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='section_title']").removeClass('tgb_input_invalid');
                    }
                    
                    if(objects.hasOwnProperty("section_desc_err")){
                        $("textarea[name='section_desc']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("textarea[name='section_desc']").removeClass('tgb_input_invalid');
                    }

                }else
                {
                    $("input[name='section_title']").removeClass('tgb_input_invalid');
                    $("textarea[name='section_desc']").removeClass('tgb_input_invalid');
                    //do the drawing of section here
                    $(".tgb_modal_close").click(); //close the modal
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });




    $("#tab-exam_structure").on("click",".new_work_sheet",function(){
        $("#new_exam_worksheet_modal").show();
        $("input[name=section_id]").val($(this).data('sectionid'));
    });

    //New Exam Worksheet Submit
    $("#frm_new_worksheet").submit(function(){
        //will add js client side validation later
        //only backend validation for now
        $.ajax({
            url:  $("#ajax_new_worksheet").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_new_worksheet").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {
                    if(objects.hasOwnProperty("worksheet_title_err")){
                        $("input[name='worksheet_title']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='worksheet_title']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("worksheet_desc_err")){
                        $("textarea[name='worksheet_desc']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("textarea[name='worksheet_title']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("question_no_err")){
                        $("input[name='question_no']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='question_no']").removeClass('tgb_input_invalid');
                    }

                }else
                {
                    var $html = "<li><div class='ion-android-list'> "+ $("input[name='unit_test_title']").val()+" </div></li>";
                    $("#exam_section_sortable").append($html);
                    $("input[name='worksheet_title']").removeClass('tgb_input_invalid');
                    $("textarea[name='worksheet_title']").removeClass('tgb_input_invalid');
                    $("input[name='question_no']").removeClass('tgb_input_invalid');
                    $("input[name='worksheet_title']").val('');
                    $("textarea[name='worksheet_title']").val('');
                    $("input[name='question_no']").val('');
                    $(".tgb_modal_close").click(); //close the modal
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });    

    //exam_section_sortable
    $( "#exam_section_sortable" ).sortable();

    $("#tab-exam_structure").on("click",".new_unit_test",function(){
        $.ajax({
            url:  $("#ajax_new_unit_test").data("ajaxurl"),
            type: 'POST',
            dataType: 'application/json',
            data:{action: 'get_section_worksheets_ajax', section_id:$(this).data('sectionid'), security:$("#worksheets_security").val()},
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                $('input[name=target_worksheets_id]').remove();
                $('.target_worksheet_cls').remove();
                $.each(objects,function(index,value)
                {
                    var $html = '<input type="checkbox" name="target_worksheets_id" value="'+value.item_id+'"/> <span class="marg_right_5 target_worksheet_cls">'+value.post_title+' <br/></span>';
                    $(".target_worksheets_dv").append($html);
                    console.log(value.section_id);
                });
                console.log(response);
            }
        });

        $("#new_exam_unit_test_modal").show();
        $("input[name=section_id]").val($(this).data('sectionid'));
    });

    //New Exam Unit Test Submit
    $("#new_exam_unit_test_modal").submit(function(){
        //will add js client side validation later
        //only backend validation for now
        $.ajax({
            url:  $("#ajax_new_unit_test").data("ajaxurl"),
            type: 'post',
            dataType: 'application/json',
            data:$("#frm_new_unittest").serialize(),
            complete: function(response) {
                var objects = JSON.parse(response.responseText);
                if(objects.ok == "NOTOK")
                {
                    if(objects.hasOwnProperty("unit_test_title_err")){
                        $("input[name='unit_test_title']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='unit_test_title']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("unit_test_desc_err")){
                        $("textarea[name='unit_test_desc']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("textarea[name='unit_test_desc']").removeClass('tgb_input_invalid');
                    }

                    if(objects.hasOwnProperty("question_no_err")){
                        $("input[name='question_no']").addClass('tgb_input_invalid');
                    }
                    else
                    {
                        $("input[name='question_no']").removeClass('tgb_input_invalid');
                    }

                }else
                {
                    var $html = "<li><div class='ion-android-folder-open'> "+ $("input[name='unit_test_title']").val()+" </div></li>";
                    $("#exam_section_sortable").append($html);
                    $("input[name='unit_test_title']").removeClass('tgb_input_invalid');
                    $("textarea[name='unit_test_desc']").removeClass('tgb_input_invalid');
                    $("input[name='question_no']").removeClass('tgb_input_invalid');
                    $("input[name='unit_test_title']").val('');
                    $("textarea[name='unit_test_desc']").val('');
                    $("textarea[name='unit_test_desc']").val('');
                    $(".tgb_modal_close").click(); //close the modal
                }
                console.log(response);
            }
        });
        return false; //so it will not redirect
    });  

    //delete section
    $("#tab-exam_structure").on('click','.delete_section_btn',function(){
        $("#delete_section_modal").show();
    });

});







})(jQuery);