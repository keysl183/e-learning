(function($){
    "use strict";

$(document).ready(function(){
    $(".go-other-page").click(function(){
        var $target_url = $(this).find('a').attr('href');
        window.location.replace($target_url);
    });
});

})(jQuery);