<?php
/**
 * Checkout Payment Section
 *
 * @author        ThimPress
 * @package       LearnPress/Templates
 * @version       3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$order_button_text            = apply_filters( 'learn_press_order_button_text', esc_attr__( 'Place order', 'course-builder' ) );
$order_button_text_processing = apply_filters( 'learn_press_order_button_text_processing', esc_attr__( 'Processing', 'course-builder' ) );
$show_button                  = true;
$count_gateways               = ! empty( $available_gateways ) ? sizeof( $available_gateways ) : 0;
?>

<div id="learn-press-payment" class="learn-press-checkout-payment">
	<?php if ( LP()->cart->needs_payment() ): ?>

		<?php if ( ! $count_gateways ): $show_button = false; ?>

			<?php if ( $message = apply_filters( 'learn_press_no_available_payment_methods_message', esc_attr__( 'No payment methods is available.', 'course-builder' ) ) ) { ?>
				<?php learn_press_display_message( $message, 'error' ); ?>
			<?php } ?>

		<?php else: ?>
			<h3 class="title"><?php esc_html_e( 'Payment Method', 'course-builder' ); ?></h3>

			<?php do_action( 'learn-press/before-payment-methods' ); ?>

			<ul class="payment-methods">

				<?php
				/**
				 * @since 3.0.0
				 */
				do_action( 'learn-press/begin-payment-methods' );
				?>

				<?php $order = 1; ?>
				<?php foreach ( $available_gateways as $gateway ) {

					if ( $order == 1 ) {
						learn_press_get_template( 'checkout/payment-method.php', array(
							'gateway'  => $gateway,
							'selected' => $gateway->id
						) );
					} else {
						learn_press_get_template( 'checkout/payment-method.php', array(
							'gateway'  => $gateway,
							'selected' => ''
						) );
					}

					$order ++;

					?>

				<?php } ?>

				<?php
				/**
				 * @since 3.0.0
				 */
				do_action( 'learn-press/end-payment-methods' );
				?>

			</ul>

			<?php do_action( 'learn-press/after-payment-methods' ); ?>

		<?php endif; ?>

	<?php endif; ?>
	<?php if ( $show_button ): ?>

		<div id="checkout-order-action" class="place-order-action">

			<?php

			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/before-checkout-submit-button' );

			?>

			<?php echo apply_filters( 'learn_press_order_button_html',
				sprintf(
					'<button type="submit" class="lp-button button alt" name="learn_press_checkout_place_order" id="learn-press-checkout-place-order" data-processing-text="%s" data-value="%s">%s</button>',
					esc_attr( $order_button_text_processing ),
					esc_attr( $order_button_text ),
					esc_attr( $order_button_text )
				)
			);
			?>

			<?php
			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/after-checkout-submit-button' );

			do_action( 'learn_press_order_after_submit' );
			?>

			<?php if ( ! is_user_logged_in() ) { ?>
				<button type="button" class="lp-button lp-button-guest-checkout"
				        id="learn-press-button-guest-checkout-back"><?php _e( 'Back', 'learnpress' ); ?></label></button>
			<?php } ?>

		</div>

	<?php endif; ?>

</div>