<?php
/**
 * Template for displaying course content within the loop.
 *
 * @author      ThimPress
 * @package     CourseBuilder/Templates
 * @version     3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}

$course = LP_Global::course();
$user   = LP_Global::user();

$layouts = get_theme_mod( 'learnpress_single_course_style', 1 );

var_dump($layouts,1111);
$layouts = isset( $_GET['layout'] ) ? $_GET['layout'] : 1;
?>

<?php
/**
 * @since 3.0.0
 */
do_action( 'learn-press/before-main-content' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/before-single-course' );
?>

    <div id="learn-press-course">
        <div class="course-summary">
			<?php if ( $user->has_course_status( $course->get_id(), array( 'enrolled', 'finished' ) )
			           || ! $course->is_required_enroll() ) {
				learn_press_get_template( 'single-course/content-learning.php', array( 'course' => $course ) );
			} else {
				learn_press_get_template( 'single-course/content-landing-' . $layouts . '.php' );
			} ?>
        </div>
    </div>

<?php
/**
 * @since 3.0.0
 */
do_action( 'learn-press/after-main-content' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/after-single-course' );
