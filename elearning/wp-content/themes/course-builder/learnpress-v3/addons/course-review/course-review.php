<?php
/**
 * Template for displaying course review.
 *
 * @author ThimPress
 * @package LearnPress/Course-Review/Templates
 * @version 3.0.0
 */

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

$course_id     = get_the_ID();
$course_review = learn_press_get_course_review( $course_id );
?>
<?php if ( $course_review['total'] ) { ?>
	<?php $reviews = $course_review['reviews']; ?>
    <div id="course-reviews">
        <ul class="course-reviews-list">
			<?php foreach ( $reviews as $review ) { ?>
				<?php learn_press_course_review_template( 'loop-review.php', array( 'review' => $review ) ); ?>
			<?php }
			if ( empty( $course_review['finish'] ) ) { ?>
                <li class="loading"><i class="fa fa-spinner fa-spin"></i></li>
			<?php }
			?>
        </ul>
		<?php if ( empty( $course_review['finish'] ) ) { ?>
            <button class="button" id="course-review-load-more"
                    data-paged="<?php echo esc_attr( $course_review['paged'] ); ?>"><?php echo esc_html__( 'Load More', 'course-builder' ); ?></button>
		<?php } ?>
    </div>
<?php }
