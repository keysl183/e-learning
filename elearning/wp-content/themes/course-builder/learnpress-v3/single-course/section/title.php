<?php
/**
 * Template for displaying title of section in single course.
 *
 * @author   ThimPress
 * @package  CourseBuilder/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$user         = LP_Global::user();
$course       = LP_Global::course();
$section_name = apply_filters( 'learn_press_curriculum_section_name', $section->get_title(), $section );
$user_course  = $user->get_course_data( get_the_ID() );
$number_items = $section->count_items( '', false );

if ( $section_name === false ) {
	return;
} ?>

<h4 class="section-header">

	<?php echo esc_attr( $section_name ); ?>&nbsp;
	<?php echo '<span class="count-lessons">' . sprintf( _n( '%d lesson', '%d lessons', $number_items, 'course-builder' ), $number_items ) . '</span>'; ?>

	<?php if ( $section_description = $section->get_description() ) { ?>
        <span class="section-description"><?php echo esc_attr( $section_description ); ?></span>
	<?php } ?>

    <span class="meta">
        <span class="step">
		<?php printf( __( '%d/%d', 'course-builder' ), $user_course->get_completed_items( '', false, $section->get_id() ), $number_items ); ?></span>
        <span class="collapse"></span>
    </span>

</h4>
