<?php
/**
 * Template for displaying curriculum tab of single course.
 *
 * @author  ThimPress
 * @package CourseBuilder/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
?>

<?php global $course;
$user = LP_Global::user();

$curriculum_heading = apply_filters( 'learn_press_curriculum_heading', esc_html__( 'Course Content', 'course-builder' ) );
?>

<div class="course-curriculum" id="learn-press-course-curriculum">
    <div class="curriculum-heading">
		<?php if ( $curriculum_heading ) { ?>
            <div class="title">
                <h2 class="course-curriculum-title"><?php echo( $curriculum_heading ); ?></h2>
            </div>
		<?php } ?>

        <div class="search">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="text" class="search-field"
                       placeholder="<?php echo esc_attr__( 'Search...', 'course-builder' ) ?>"
                       value="<?php echo get_search_query() ?>" name="s"/>
                <input type="hidden" name="post_type" value="lp_lession">
                <button type="submit" class="search-submit"><span class="ion-android-search"></span></button>
            </form>
        </div>
    </div>

	<?php
	/**
	 * @since 3.0.0
	 */
	do_action( 'learn-press/before-single-course-curriculum' ); ?>

	<?php if ( $curriculum = $course->get_curriculum() ) { ?>
        <ul class="curriculum-sections">
			<?php foreach ( $curriculum as $section ) {
				learn_press_get_template( 'single-course/loop-section.php', array( 'section' => $section ) );
			} ?>
        </ul>
	<?php } else { ?>
		<?php echo apply_filters( 'learn_press_course_curriculum_empty', esc_attr__( 'Curriculum is empty', 'course-builder' ) ); ?>
	<?php } ?>

	<?php
	/**
	 * @since 3.0.0
	 */
	do_action( 'learn-press/after-single-course-curriculum' ); ?>

	<?php if ( $user->has_course_status( $course->get_id(), array(
			'enrolled',
			'finished'
		) ) || ! $course->is_required_enroll() ) {

		do_action( 'thim_learning_end_tab_curriculum' );
	} ?>

</div>
