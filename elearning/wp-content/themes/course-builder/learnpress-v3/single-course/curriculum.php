<?php
/**
 * Template for displaying the curriculum of a course
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$course               = LP()->global['course'];
$lessons              = sizeof( $course->get_lessons() );
$course_duration_text = thim_duration_time_calculator( $course->get_id(), 'lp_course' );

?>
<div class="course-curriculum" id="learn-press-course-curriculum">

	<?php if ( $curriculum = $course->get_curriculum() ): ?>

		<div class="info-course">
			<h2 class="course-curriculum-title"><?php esc_html_e( 'Course Curriculum', 'course-builder' ); ?></h2>
			<span class="total-lessons"><?php esc_html_e( 'Total learning: ', 'course-builder' ); ?>
				<span class="text"><?php printf( _n( '%d lesson', '%d lessons', $lessons, 'course-builder' ), $lessons ); ?></span></span>
			<span class="total-time"><?php esc_html_e( 'Time: ', 'course-builder' ); ?>
				<span class="text"><?php echo( $course_duration_text ); ?></span></span>
		</div>

		<?php do_action( 'learn_press_before_single_course_curriculum' ); ?>

		<ul class="curriculum-sections">
			<?php foreach ( $curriculum as $lessons ) : ?>
				<?php learn_press_get_template( 'single-course/loop-section.php', array( 'section' => $lessons ) ); ?>
			<?php endforeach; ?>
		</ul>

	<?php else: ?>
		<p class="curriculum-empty">
			<?php echo apply_filters( 'learn_press_course_curriculum_empty', esc_attr__( 'Curriculum is empty', 'course-builder' ) ); ?>
		</p>
	<?php endif; ?>

	<?php do_action( 'learn_press_after_single_course_curriculum' ); ?>
</div>