<?php

/**
 * Theme functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

define( 'THIM_DIR', trailingslashit( get_template_directory() ) );
define( 'THIM_URI', trailingslashit( get_template_directory_uri() ) );

if ( ! function_exists( 'thim_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thim_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on this theme, use a find and replace
		 * to change 'course-builder' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'course-builder', THIM_DIR . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Add support Woocommerce
		add_theme_support( 'woocommerce' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'course-builder' ),
		) );

		if ( get_theme_mod( 'copyright_menu', true ) ) {
			register_nav_menus( array(
				'copyright_menu' => esc_html__( 'Copyright Menu', 'course-builder' ),
			) );
		}

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'audio',
			'quote',
			'link',
			'gallery',
			'chat',
		) );

		add_theme_support( 'custom-background' );

		add_theme_support( 'thim-core' );


		add_theme_support( 'thim-demo-data' );

		add_theme_support( 'thim-extend-vc-sc' );

		add_post_type_support( 'page', 'excerpt' );

	}
endif;
add_action( 'after_setup_theme', 'thim_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thim_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'thim_content_width', 640 );
}

add_action( 'after_setup_theme', 'thim_content_width', 0 );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function thim_widgets_init() {
	$thim_options = get_theme_mods();

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'course-builder' ),
		'id'            => 'sidebar',
		'description'   => esc_html__( 'Appears in the Sidebar section of the site.', 'course-builder' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	if ( get_theme_mod( 'header_topbar_display', false ) ) {
		register_sidebar( array(
			'name'          => esc_attr__( 'Top bar', 'course-builder' ),
			'id'            => 'topbar',
			'description'   => esc_attr__( 'Display in top bar.', 'course-builder' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

	if ( get_theme_mod( 'header_sidebar_right_display', true ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Header Right', 'course-builder' ),
			'id'            => 'header_right',
			'description'   => esc_html__( 'Appears in Header right.', 'course-builder' ),
			'before_widget' => '<div class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

	register_sidebar( array(
		'name'          => esc_html__( 'Below Main', 'course-builder' ),
		'id'            => 'after_main',
		'description'   => esc_html__( 'Display widgets in below main content.', 'course-builder' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Below Off-Canvas Menu', 'course-builder' ),
		'id'            => 'off_canvas_menu',
		'description'   => esc_html__( 'Display below off-canvas menu.', 'course-builder' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	$footer_columns = get_theme_mod( 'footer_columns', 6 );
	if ( $footer_columns ) {
		for ( $i = 1; $i <= $footer_columns; $i ++ ) {
			register_sidebar( array(
				'name'          => sprintf( esc_attr__( 'Footer Column %s', 'course-builder' ), $i ),
				'id'            => 'footer-sidebar-' . $i,
				'description'   => sprintf( esc_attr__( 'Display widgets in footer column %s.', 'course-builder' ), $i ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		}
	}

	/**
	 * Not remove
	 * Function create sidebar on wp-admin.
	 */
	$sidebars = apply_filters( 'thim_core_list_sidebar', array() );
	if ( count( $sidebars ) > 0 ) {
		foreach ( $sidebars as $sidebar ) {
			$new_sidebar = array(
				'name'          => $sidebar['name'],
				'id'            => $sidebar['id'],
				'description'   => '',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			);

			register_sidebar( $new_sidebar );
		}
	}


	register_sidebar( array(
		'name'          => esc_html__( 'Below Footer', 'course-builder' ),
		'id'            => 'footer_sticky',
		'description'   => esc_html__( 'Display below of footer.', 'course-builder' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	if ( get_theme_mod( 'learnpress_top_sidebar_archive_display', true ) ) {
		register_sidebar( array(
			'name'          => esc_attr__( 'Courses - Widget Area Top', 'course-builder' ),
			'id'            => 'top_sidebar_courses',
			'description'   => esc_attr__( 'Display widgets on top of course archive pages.', 'course-builder' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		) );
	}

	if ( class_exists( 'LearnPress' ) ) {

		register_sidebar( array(
			'name'          => esc_attr__( 'Courses - Sidebar', 'course-builder' ),
			'id'            => 'sidebar_courses',
			'description'   => esc_attr__( 'Sidebar of Courses', 'course-builder' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}

	if ( class_exists( 'WooCommerce' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Shop - Sidebar', 'course-builder' ),
			'id'            => 'sidebar_shop',
			'description'   => esc_html__( 'Sidebar Of Shop', 'course-builder' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<div class="sc-heading article_heading"><h3 class="widget-title heading_primary">',
			'after_title'   => '</h3></div>',
		) );
	}

	if ( class_exists( 'bbPress' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Forums - Sidebar', 'course-builder' ),
			'id'            => 'sidebar_forums',
			'description'   => esc_html__( 'Sidebar of Forums', 'course-builder' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}
}

add_action( 'widgets_init', 'thim_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function thim_scripts() {
	global $wp_query, $current_blog, $wp_locale;


	// Enqueue Styles
	wp_enqueue_style( 'fontawesome', THIM_URI . 'assets/css/libs/awesome/font-awesome.css', array() );
	wp_enqueue_style( 'bootstrap', THIM_URI . 'assets/css/libs/bootstrap/bootstrap.css', array() );
	wp_enqueue_style( 'ionicons', THIM_URI . 'assets/css/libs/ionicons/ionicons.css', array() );
	wp_enqueue_style( 'magnific-popup', THIM_URI . 'assets/css/libs/magnific-popup/main.css', array() );
	wp_enqueue_style( 'owl-carousel', THIM_URI . 'assets/css/libs/owl-carousel/owl.carousel.css', array() );
	wp_enqueue_style( 'select2', THIM_URI . 'assets/css/libs/select2/core.css', array() );
	// End: Enqueue Styles

	wp_enqueue_style( 'thim-style', get_stylesheet_uri() );

	// Style default
	if ( ! thim_plugin_active( 'thim-core' ) ) {
		wp_enqueue_style( 'thim-default', THIM_URI . 'inc/data/default.css', array() );
	}

	//	RTL
	if ( get_theme_mod( 'feature_rtl_support', false ) || is_rtl() ) {
		wp_deregister_style( 'thim-style' );
		wp_enqueue_style( 'thim-style', get_template_directory_uri() . '/style-rtl.css', array() );
	} else {
		wp_style_add_data( 'thim-style', 'rtl', 'replace' );
	}

	//	Enqueue Scripts
	wp_enqueue_script( 'tether', THIM_URI . 'assets/js/libs/1_tether.min.js', array( 'jquery' ), '', true );
//	wp_enqueue_script( 'bootstrap', THIM_URI . 'assets/js/libs/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'thim-change-layout', THIM_URI . 'assets/js/libs/change-layout.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'circle-progress', THIM_URI . 'assets/js/libs/circle-progress.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'isotope', THIM_URI . 'assets/js/libs/isotope.pkgd.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'cookie', THIM_URI . 'assets/js/libs/jquery.cookie.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'flexslider', THIM_URI . 'assets/js/libs/jquery.flexslider-min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'magnific-popup', THIM_URI . 'assets/js/libs/jquery.magnific-popup.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'thim-content-slider', THIM_URI . 'assets/js/libs/jquery.thim-content-slider.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'waypoints', THIM_URI . 'assets/js/libs/jquery.waypoints.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'owlcarousel', THIM_URI . 'assets/js/libs/owl.carousel.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'select2', THIM_URI . 'assets/js/libs/select2.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'sly', THIM_URI . 'assets/js/libs/sly.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'stellar', THIM_URI . 'assets/js/libs/stellar.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'theia-sticky-sidebar', THIM_URI . 'assets/js/libs/theia-sticky-sidebar.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'toggle-tabs', THIM_URI . 'assets/js/libs/toggle-tabs.js', array( 'jquery' ), '', true );
    // End: Enqueue Scripts


	if ( 'loadmore' == get_theme_mod( 'blog_archive_nav_style', 'pagination' ) || ( ( isset( $_GET['pagination'] ) ? $_GET['pagination'] : '' ) === 'loadmore' ) ) {
		wp_enqueue_script( 'thim-loadmore', THIM_URI . 'assets/js/libs/thim-loadmore.js', array( 'jquery' ), '', true );
		wp_localize_script( 'thim-loadmore', 'thim_loadmore_params', array(
			'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
			'posts'        => serialize( $wp_query->query_vars ), // everything about your loop is here
			'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
			'max_page'     => $wp_query->max_num_pages
		) );
	}


	if ( WP_DEBUG ) {
		$main_file = THIM_URI . 'assets/js/main.js';
	} else {
		$main_file = THIM_URI . 'assets/js/main.min.js';
	}

	wp_enqueue_script( 'thim-main', $main_file, array(
		'jquery',
		'tether',
		'thim-change-layout',
		'circle-progress',
		'imagesloaded',
		'isotope',
		'cookie',
		'flexslider',
		'magnific-popup',
		'thim-content-slider',
		'waypoints',
		'owlcarousel',
		'select2',
		'sly',
		'stellar',
		'theia-sticky-sidebar',
	), '', true );


	if ( get_theme_mod( 'feature_smoothscroll', false ) ) {
		wp_enqueue_script( 'smoothscroll', THIM_URI . 'assets/js/libs/smoothscroll.min.js', array( 'jquery' ), '', true );
	}
	// End: Enqueue Scripts

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * Dequeue Script & CSS OWL carousel of WP Events Manager plugin.
	 */
	if ( is_home() ) {
		wp_dequeue_script( 'wpems-countdown-js' );
		wp_dequeue_script( 'wpems-countdown-plugin-js' );
	}
	wp_dequeue_script( 'wpems-owl-carousel-js' );
	wp_dequeue_style( 'wpems-owl-carousel-css' );
	wp_dequeue_style( 'wpems-magnific-popup-css' );
	wp_dequeue_script( 'wpems-magnific-popup-js' );


	/**
	 * Dequeue Script & CSS miniorange-login-openid plugin.
	 */
	wp_dequeue_style( 'mo-wp-font-awesome' );


	/**
	 * learnpress-announcements
	 */
	if ( ! is_singular( 'lp_course' ) ) {
		wp_dequeue_style( 'jquery-ui-accordion' );
		wp_dequeue_style( 'lp_announcements' );
	}


	if ( is_singular( 'product' ) ) {
		wp_enqueue_script( 'prettyPhoto' );
		wp_enqueue_script( 'prettyPhoto-init' );
		wp_enqueue_style( 'woocommerce_prettyPhoto_css' );
	}

}

add_action( 'wp_enqueue_scripts', 'thim_scripts', 100 );


function thim_custom_stylesheet_for_developer( $stylesheet, $stylesheet_dir ) {
	if ( WP_DEBUG && ! is_child_theme() ) {
		$stylesheet = $stylesheet_dir . '/style.unminify.css';
	}

	return $stylesheet;
}

///add_filter( 'stylesheet_uri', 'thim_custom_stylesheet_for_developer', 10, 2 );

/**
 * Implement the theme wrapper.
 */
include_once THIM_DIR . 'inc/libs/theme-wrapper.php';

/**
 * Implement the Custom Header feature.
 */
include_once THIM_DIR . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
include_once THIM_DIR . 'inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
include_once THIM_DIR . 'inc/extras.php';

/**
 * Extra setting on plugins, export & import with demo data.
 */
include_once THIM_DIR . 'inc/data/extra-plugin-settings.php';

/**
 * Metabox
 *
 * Add Custom metabox
 */
include_once THIM_DIR . 'inc/metabox.php';

/**
 * Load Jetpack compatibility file.
 */
include_once THIM_DIR . 'inc/jetpack.php';

/**
 * Custom wrapper layout for theme
 */
include_once THIM_DIR . 'inc/wrapper-layout.php';

/**
 * Custom widgets
 */
include_once THIM_DIR . 'inc/widgets/widgets.php';


/**
 * Load shortcodes
 * thim-THEME-SLUG.php
 */
/*if ( file_exists( THIM_DIR . 'shortcodes/thim-course-builder.php' ) && ( ! class_exists( 'Thim_Course_Builder' ) ) ) {
	include_once THIM_DIR . 'shortcodes/thim-course-builder.php';
}*/


/**
 * Custom functions
 */
include_once THIM_DIR . 'inc/custom-functions.php';

/**
 * Customizer additions.
 */
include_once THIM_DIR . 'inc/customizer.php';

/**
 * Custom LearnPress functions
 * */

if ( is_admin() && current_user_can( 'manage_options' ) ) {
	include_once THIM_DIR . 'inc/admin/installer/installer.php';
	include_once THIM_DIR . 'inc/admin/plugins-require.php';
}


/**
 * LearnPress custom functions
 */
if ( class_exists( 'LearnPress' ) ) {
	$path = thim_is_new_learnpress( '3.0' ) ? 'learnpress-v3/' : 'learnpress/';
	include_once THIM_DIR . $path . 'custom-functions.php';
}

/**
 * Woocommerce custom functions
 */
if ( class_exists( 'WooCommerce' ) ) {
	include_once THIM_DIR . 'woocommerce/custom-functions.php';
}


/**
 * WP Events Manager custom functions
 */
if ( class_exists( 'WPEMS' ) ) {
	include_once THIM_DIR . 'wp-events-manager/custom-functions.php';
}


/**
 * BuddyPress custom functions
 */
if ( class_exists( 'BuddyPress' ) ) {
	include_once THIM_DIR . 'buddypress/custom-functions.php';
}

/**
 * Update user profile settings via AJAX call
 */
function thim_update_user_profile_settings() {

	if ( empty( $_REQUEST['thim-update-user-profile'] ) ) {
		return;
	}

	if ( 'yes' !== $_REQUEST['thim-update-user-profile'] ) {
		return;
	}

	// Prevent redirection
	add_filter( 'learn-press/profile-updated-redirect', '__return_false' );

	$profile  = LP_Profile::instance();
	$postdata = $_POST;
	$nonce    = '';

	// Find the nonce
	foreach ( $postdata as $prop => $val ) {
		if ( preg_match( '~^save-profile-~', $prop ) ) {
			$nonce = $val;
			break;
		}
	}

	if ( ! $nonce ) {
		return;
	}

	// Save
	$profile->save( $nonce );
	die();
}

add_action( 'init', 'thim_update_user_profile_settings' );


/*THE GANG BUKID CODES START HERE */
function logged_out_wp_nav_menu_args( $args = '' ) {
 
	if( is_user_logged_in() )
	 { 
		$args['menu'] = 'Main menu'; //logged in users
	} else { 
		$args['menu'] = 'LoggedOutMainMenu';
	} 
		return $args;
}
add_filter( 'wp_nav_menu_args', 'logged_out_wp_nav_menu_args' );


function complete_profile_short_code()
{
	 ob_start();
     get_template_part('complete_your_profile');
     return ob_get_clean();   
}
add_shortcode( 'klcompleteprofile', 'complete_profile_short_code' );



//user_registration_hook 
//additional hooks, set to none for now. It will have value on complete profile page
function additional_meta_registration_save( $user_id ) {
	add_user_meta( $user_id, "age", 0);
	add_user_meta( $user_id, "country", '');
	add_user_meta( $user_id, "zip", '');
	add_user_meta( $user_id, "education", '');
	add_user_meta( $user_id, "student_level", '');
	add_user_meta( $user_id, "is_profile_complete", 0);
	
	//so it will be easy to complete the profile
	wp_set_current_user($user_id);
	wp_set_auth_cookie($user_id);

}
add_action( 'user_register', 'additional_meta_registration_save');

function create_new_exam_shortcode()
{
	ob_start();
	get_template_part('thegangbukid/tgb_create_new_exam');
	return ob_get_clean();   
}
add_shortcode( 'createnewexam', 'create_new_exam_shortcode' );

function create_new_question_banks_shortcode()
{
	ob_start();
	get_template_part('thegangbukid/tgb_create_question_bank');
	return ob_get_clean();   
}
add_shortcode( 'createnewquestionbank', 'create_new_question_banks_shortcode' );


function manage_question_bank_shortcode()
{
	ob_start();
	get_template_part('thegangbukid/tgb_manage_question_bank');
	return ob_get_clean();   
}
add_shortcode( 'managequestionbank', 'manage_question_bank_shortcode' );

//load only the scripts to this page
function exam_manager_scripts()
{

	if(is_page(array('exam-manager','question-banks', 'manage-question-bank','edit-exam-course','complete-your-profile','messages')))
	{
		wp_enqueue_script( 'tether', THIM_URI . 'thegangbukid/js/create_exam.js', array( 'jquery' ), '', true );
		wp_enqueue_style( 'thegangbukid', THIM_URI . 'thegangbukid/css/create_exam.css', array() );
		
		//to protect from overposting attack
		if(isset($_POST["dummy_submit"]))
		{
			//do a redirect here later
		}
	}

	if(is_page('manage-question-bank'))
	{
		if(!isset($_REQUEST["quest_bank"]))
		{
			//if id of quest bank is not present, do a redirect
			wp_redirect( get_site_url().'/exam-manager' );
			exit;
		}
	}

	if(is_page('edit-exam-course'))
	{
		if(!isset($_REQUEST["exam_id"]) || $_REQUEST["exam_id"] == '')
		{
			//if id of exam id is not present, do a redirect
			wp_redirect( get_site_url().'/exam-manager' );
			exit;
		}
		wp_enqueue_script( 'manageexamjs', THIM_URI . 'thegangbukid/js/manage_exam.js', array( 'jquery' ), '', true );
		wp_enqueue_style( 'manageexamcss', THIM_URI . 'thegangbukid/css/manage_exam.css', array() );
	}

	if(is_page('lp-profile'))
	{
		wp_enqueue_script( 'tgbcustomprofile', THIM_URI . 'thegangbukid/js/custom_profile.js', array( 'jquery' ), '', true );
		wp_enqueue_style( 'thegangbukid', THIM_URI . 'thegangbukid/css/create_exam.css', array() );
		wp_enqueue_style( 'tgbcustomstyleprofile', THIM_URI . 'thegangbukid/css/custom_profile.css', array() );
	}

	if(is_page('exam-manager','question-banks', 'manage-question-bank','edit-exam-course'))
	{
		wp_enqueue_style( 'manageexamcss', THIM_URI . 'thegangbukid/css/tgb_misc.css', array() );
	}

	if(is_page('membership'))
	{
		wp_enqueue_style( 'membershiptgb.css', THIM_URI . 'thegangbukid/css/membership.css', array() );
	}


	//hack to generate questions for exams
	if ( get_post_type( get_the_ID() ) == 'lp_course' ) {
		if(is_user_logged_in())
		{
			$course = learn_press_get_the_course();
			$user   = learn_press_get_current_user();
			if ( $user->has_course_status( $course->id, array(
				'enrolled'
			) ))
			{
				//generate questions here
				$worksheets = get_worksheet_by_course_id($course->id);
				$question_banks = array();
				foreach($worksheets as $worksheet)
				{
					array_push($question_banks,$worksheet->quest_bank_id );

					$question_count = get_question_numbers_in_qiuzz($worksheet->item_id,$user->ID);
					if($question_count[0]->question_count == 0)//mdoify this later to accomodate not correct question
					{
						$available_questions = get_all_available_question_by_quest_bank($worksheet->quest_bank_id);
						$order =1 ;
						foreach($available_questions as $question)
						{
							insert_generated_question_to_worksheet($worksheet->item_id, $question->question_id, $order,$user->ID );
							$order++;
						}
					}
				}
				
				$unittests = get_unittest_by_course_id($course->id);
				foreach($unittests as $unittest)
				{
					$question_count = get_question_numbers_in_qiuzz($unittest->item_id, $user->ID);
					if($question_count[0]->question_count == 0)//mdoify this later to accomodate not correct question
					{
						$unittest_available_question = get_all_available_question_using_quest_banks($question_banks);
						$order =1 ;
						foreach($unittest_available_question as $question)
						{
							//this is also use in exam hehe
							insert_generated_question_to_worksheet($unittest->item_id, $question->question_id, $order,$user->ID );
							$order++;
						}
					}
				}
				echo var_dump($unittest_available_question);
				//echo count($available_questions);
			}
		}
	}
}
add_action('wp_enqueue_scripts','exam_manager_scripts');
 

//shortcode for teacher dashboard sidebar 
function teacher_dash_board_shortcode()
{
	ob_start();
	get_template_part('thegangbukid/tgb_teacher_dashboard');
	return ob_get_clean();   
}
add_shortcode( 'tgbteacherdashboard', 'teacher_dash_board_shortcode' );

//ajax functions
$path = 'thegangbukid/lib/';
include_once THIM_DIR . $path . 'tgb_ajax_functions.php';

function add_new_question_ajax()
{
	$hasError = false;
	$errmsg;
	if(!wp_verify_nonce($_POST["security"],'add_new_question'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
	$errors = validate_add_question(); //also perform the inserts
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_add_new_quesion_ajax','add_new_question_ajax');
add_action('wp_ajax_nopriv_add_new_quesion_ajax','add_new_question_ajax');


function delete_question_ajax()
{
	$hasError = false;
	if(!wp_verify_nonce($_POST["security"],'delete_question'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
	$errors = delete_question();
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_delete_question_ajax','delete_question_ajax');
add_action('wp_ajax_nopriv_delete_question_ajax','delete_question_ajax');

function new_exam_section_ajax()
{
	$hasError = false;
	if(!wp_verify_nonce($_POST["security"],'new_exam_section'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
	$errors = validate_exam_section();
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_new_exam_section_ajax','new_exam_section_ajax');
add_action('wp_ajax_nopriv_new_exam_section_ajax','new_exam_section_ajax');

function get_exam_section_details()
{
	global $wpdb;
	$section_id = $_GET['section_id'];
    $result =  $wpdb->get_results("SELECT * FROM `wp_learnpress_sections` WHERE section_id=".$section_id);
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_get_exam_section_details','get_exam_section_details');
add_action('wp_ajax_nopriv_get_exam_section_details','get_exam_section_details');

function edit_exam_section_ajax()
{
	$hasError = false;
	if(!wp_verify_nonce($_POST["security"],'edit_exam_section'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
	$errors = validate_edit_exam_section();
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_edit_exam_section_ajax','edit_exam_section_ajax');
add_action('wp_ajax_nopriv_edit_exam_section_ajax','edit_exam_section_ajax');


function new_exam_worksheet_ajax()
{
	$hasError = false;
	if(!wp_verify_nonce($_POST["security"],'new_exam_worksheet'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
	$errors = validate_add_worksheet();
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_new_exam_worksheet_ajax','new_exam_worksheet_ajax');
add_action('wp_ajax_nopriv_new_exam_worksheet_ajax','new_exam_worksheet_ajax');


function new_exam_unit_test_ajax()
{
	$hasError = false;
	if(!wp_verify_nonce($_POST["security"],'new_exam_unit_test'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
 	$errors = validate_add_unit_test();
	echo json_encode($errors);
	exit;
}
add_action('wp_ajax_new_exam_unit_test_ajax','new_exam_unit_test_ajax');
add_action('wp_ajax_nopriv_new_exam_unit_test_ajax','new_exam_unit_test_ajax');

function get_section_worksheets_ajax()
{
	$section_id = $_POST['section_id'];

	if(!wp_verify_nonce($_POST["security"],'get_section_worksheets_ajax'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}

	$result = get_worksheets_by_section_id($section_id);
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_get_section_worksheets_ajax','get_section_worksheets_ajax');
add_action('wp_ajax_nopriv_get_section_worksheets_ajax','get_section_worksheets_ajax');


function delete_exam_section_ajax()
{
	$section_id = $_POST['section_id'];

	if(!wp_verify_nonce($_POST["security"],'delete_exam_section'))
	{
		$hasError = true;
		$errmsg = "Invalid Nonce";
	}
}
add_action('wp_ajax_delete_exam_section_ajax','delete_exam_section_ajax');
add_action('wp_ajax_nopriv_delete_exam_section_ajax','delete_exam_section_ajax');


//shortcode for managing the exam courses
function manage_exam_courses_shortcode()
{
	ob_start();
	get_template_part('thegangbukid/tgb_manage_exam');
	return ob_get_clean();   
}

add_shortcode( 'tgbmanageexams', 'manage_exam_courses_shortcode' );



/*THE GANG BUKID EXAM API v0.01 */

function api_get_wpnonce_token()
{
	$key = $_POST['token_key'];
	$result = array("Token",wp_create_nonce($key));
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_api_get_wpnonce_token','api_get_wpnonce_token');
add_action('wp_ajax_nopriv_api_get_wpnonce_token','api_get_wpnonce_token');

function api_get_course_structure()
{
	if(!wp_verify_nonce($_POST["token_key"],'get_course_structure'))
	{
		$hasError = true;
		$result = "Invalid Nonce";
	}
	$course_id = $_POST['course_id'];
	$result = get_course_all_exam_sections($course_id);
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_api_get_course_structure','api_get_course_structure');
add_action('wp_ajax_nopriv_api_get_course_structure','api_get_course_structure');

function api_get_course_section_detail()
{
	$section_id = $_REQUEST['section_id'];
	$result = get_course_exam_a_section($section_id);
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_api_get_course_section_detail','api_get_course_section_detail');
add_action('wp_ajax_nopriv_api_get_course_section_detail','api_get_course_section_detail');


//gets all of the element of the section.
//tihs includes the worksheet and unit test
function api_get_all_section_elements()
{
	$section_id = $_REQUEST['section_id'];
	$result = get_all_section_elements_api($section_id);
	echo json_encode($result);
	exit;
}
add_action('wp_ajax_api_get_all_section_elements','api_get_all_section_elements');
add_action('wp_ajax_nopriv_api_get_all_section_elements','api_get_all_section_elements');