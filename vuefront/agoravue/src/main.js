// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import vueResource from 'vue-resource';
import  VueCookie from 'vue-cookie';
import Toasted from 'vue-toasted';
import {store} from './store/store';
import {verifySessionLogIn} from './api/member/member';
import '@/assets/css/main.css';
import '@/assets/css/style.css';
import '@/assets/css/media.css';

Vue.config.productionTip = false

//our default toast option
let toastOptions ={
  theme: "bubble", 
  iconPack:'fontawesome',
  icon : 'error_outline',
  position: "top-center", 
  duration : 5000,
  icon : {
    name : 'times-circle'
  }
};

Vue.use(vueResource)
Vue.use(VueCookie)
Vue.use(Toasted,toastOptions)

export const bus = new Vue();

var frontbase = 'https://dev.agora-school.com/';
// var apibase  =  'http://localhost/agorav2/agorapiv1/public/index.php/api/';
// var resourcebase = "http://localhost/agorav2/agorapiv1/public/resources/";

var apibase  =  'https://dev.agora-school.com/agorapiv1/public/index.php/api/';
var resourcebase = "https://dev.agora-school.com/agorapiv1/public/resources/";

Vue.prototype.$fronturl = frontbase;
Vue.prototype.$api_quest_imagesurl = resourcebase+ "images/";
Vue.prototype.$api_quest_audiourl = resourcebase+ "audios/";
Vue.prototype.$api_quest_profimageurl = resourcebase+ "profiles/";
Vue.prototype.$api_others_url = resourcebase+ "others/";
Vue.prototype.$apis = 
{
  login : apibase +'login'
};

//Helper Methods will separate this later
Vue.mixin({
  methods:{
    LogOutUser:function(){
      //TODO Log out API Session
      //For now just delete the cookie to simulate logout
      this.$cookie.delete('session_user');
      this.$cookie.delete('session_value');
      this.$router.push({ name: 'Index' });
        FB.logout((response) =>{
        });         
        this.$router.go();
    },
    IncompleteRegistration:function(){
      this.$cookie.delete('session_user');
      this.$cookie.delete('session_value');
      this.$router.push({ name: 'Index', query: { incomplete_register : true}});
      this.$router.go();
    },
    GetSessionHeaders : function()
    {
      let api_headers = 
      {
          "User-Key" : this.$cookie.get("session_user"),
          "X-Authorization" : this.$cookie.get("session_value")
      }
      return api_headers;
    },
    CheckSessionIfLoggedIn : async function()
    {
      var self = this;
      return new Promise(function(resolve, reject) {
        let isLoggedIn = false;
        if(!self.$cookie.get("session_user") || !self.$cookie.get("session_value"))
        {
          resolve(isLoggedIn);
        }
        verifySessionLogIn(self.GetSessionHeaders()).then((response) => {
          isLoggedIn = true;
          resolve(isLoggedIn);
        }).catch(function(response){
          isLoggedIn = false;
          resolve(isLoggedIn);
        });
       
      });
    },
    GetImageURL(){
      return Vue.prototype.$api_quest_imagesurl;
    },
    GetAudioURL(){
      return Vue.prototype.$api_quest_audiourl;
    },
    GetProfileImageURL(){
      return Vue.prototype.$api_quest_profimageurl;
    },      
    GetOthersURL(){
      return Vue.prototype.$api_others_url;
    },      
    HasError:function(error_arr, isInitialLoad = false){
      if(isInitialLoad){
          return;
      }

      if(error_arr.length > 0){
         return{
        'is-invalid': true
        }               
      }
      else{
         return{
        'is-valid': true
        }
      }
    },
    HasErrorBool(error_arr, isInitialLoad = false){
      if(isInitialLoad){
        return;
      }
      if(!Array.isArray(error_arr)){
        return false;
      }
      if(error_arr.length > 0){
       return true;            
      }
      else{
        return false;
      }
    },
    IsLoggedInTeacher(){
      if (this.$cookie.get("user_level") == 'TEACHER'){
        return true;
      }
      return false;
    },
    GetCategoryName:function(category_id){
      var category = this.$store.state.categories.filter((item)=>{
          return item.category_id === category_id; 
      })
      return category[0].category_title;
    },
    ToPercent(total, input){
      return ((input /total) * 100).toFixed(2);
    },
    GetClientTimeZone(){
      return Intl.DateTimeFormat().resolvedOptions().timeZone;
    },
    ToDate(i_date){
      let mydate = new Date(i_date);
      var options = { year: 'numeric', month: 'long', day: 'numeric' };
      return mydate.toLocaleDateString('en-US',options);
    },
    ToFullDisplayDate(i_date){
      let mydate = new Date(i_date);
      var options = { year: 'numeric', month: 'long', day: 'numeric', hour:'2-digit', minute:'2-digit' };
      return mydate.toLocaleDateString('en-US',options);
    },    
    ToDisplayTimeOnly(i_date){
      let mydate = new Date(i_date);
      return mydate.toLocaleTimeString('en-US');
    },
    ToClientLocalTime(utc_date){
      var date = new Date(`${utc_date} UTC`);
      var options = { year: 'numeric', month: 'long', day: 'numeric', hour:'2-digit', minute:'2-digit' };
      return date.toLocaleTimeString('en-US',options) ;
    },
    ToClientLocalTimeOnly(utc_date){
      var date = new Date(`${utc_date} UTC`);
      var options = {hour:'2-digit', minute:'2-digit' };
      return date.toLocaleTimeString('en-US',options) ;
    },    
    ToClientLocalDate(utc_date){
      var date = new Date(`${utc_date} UTC`);
      var options = { year: 'numeric', month: 'short', day: 'numeric',};
      return date.toLocaleDateString('en-US',options) ;
    },    
    ToInputDate(i_date){
      let mydate = new Date(i_date);
      var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
      return mydate.toLocaleDateString('en-US',options);     
    },
    HasValue(value){
      return (value != null && value != '' && value != 'undefined');
    },
    HasIntegerValue(value){
      return (value != null && value != '' && value != 'undefined' && value != 0);
    },
    GetFileExtension:function(filename){
      var ext = /^.+\.([^.]+)$/.exec(filename);
      return ext == null ? "" : ext[1];
    },
    Silence(){
      
    }             
  }
});

/*Directives */
Vue.directive('vpshow', {
  inViewport (el) {
    var rect = el.getBoundingClientRect()
    return !(rect.bottom < 0 || rect.right < 0 || 
             rect.left > window.innerWidth ||
             rect.top > window.innerHeight)
  },
  
  bind(el, binding) {
    el.classList.add('before-enter')
    el.$onScroll = function() {
      if (binding.def.inViewport(el)) {
        el.classList.add('enter')
        el.classList.remove('before-enter')
        binding.def.unbind(el, binding)        
      }
    }
    document.addEventListener('scroll', el.$onScroll)
  },
  
  inserted(el, binding) {
    el.$onScroll()  
  },
  
  unbind(el, binding) {    
    document.removeEventListener('scroll', el.$onScroll)
    delete el.$onScroll
  }  
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  router,
  components: { App },
  template: '<App/>'
})
