import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';


//File Upload
export const uploadFile = (api_headers, formData) => 
{
    return Vue.http.post(apiUrlBase + 'file/file_upload',formData,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const uploadProfile = (api_headers, formData) => 
{
    return Vue.http.post(apiUrlBase + 'file/profile',formData,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}