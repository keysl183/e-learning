import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
Vue.use(VueResource);


export const getCountries = () =>
{
    return Vue.http.get(apiUrlBase + 'countries');  
}


export const getCounty = (id) =>
{
    return Vue.http.get(apiUrlBase + 'countries/' + id);  
}


export const getEducation = () =>
{
    return Vue.http.get(apiUrlBase + 'education');  
}