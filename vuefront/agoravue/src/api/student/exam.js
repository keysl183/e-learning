import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
Vue.use(VueResource);


export const getEnrolledCourses = (api_headers,cate= 0 , sortby = 2, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'exam/courses?PageLimit='+page_limit+'&PageOffset='+page_offset+'&SortBy='+sortby+'&Category='+cate,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//Splitted the Enrolled Courses into two due to f client request
export const getSubEnrolledCourses = (api_headers, section_id, level_id, chapter_no, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'exam/sub_courses/'+ section_id+ '/' + level_id + '/' +chapter_no
     +'?PageLimit='+page_limit+'&PageOffset='+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const enrollCourse = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'exam/enroll', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const getStudentCourseSection = (api_headers, course_id) =>
{
    return Vue.http.get(apiUrlBase + 'student/course/'+course_id+'/section',
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const getTakenQuestionList = (api_headers, exam_id, section_id, isRetake=false, language = 0) =>
{
    return Vue.http.get(apiUrlBase + 'exam/course/'+exam_id + "/" + section_id+"?Retake=" + isRetake + "&language="+language,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getSectionItemDetail = (api_headers, exam_id, section_id) =>
{
    return Vue.http.get(apiUrlBase + 'course/'+exam_id + "/section/" + section_id,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//Exam Section Item Complete
export const updateSectionItemComplete = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'exam/section/complete', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Get Assignments for Student
export const getStudentAssignment = (api_headers) =>
{
    return Vue.http.get(apiUrlBase + 'student/assignment', {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getRecentStudentAssignment = (api_headers) =>
{
    return Vue.http.get(apiUrlBase + 'student/assignment/recent', {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const viewRecentAssignment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'student/assignment/recent/viewed', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const submitAssignment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'student/assignment/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getStudentAssignmentDetail = (api_headers, assign_id) =>
{
    return Vue.http.get(apiUrlBase + 'student/assignment/'+assign_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const getStudentAssignmentMCQAnswers = (api_headers, assign_id, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'student/question/result/'+assign_id +'?PageLimit='+ page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
