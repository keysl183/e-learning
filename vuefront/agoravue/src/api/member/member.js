import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
Vue.use(VueResource);




//verify current log in status if valid session
export const verifySessionLogIn = (api_headers,session_key) => 
{
    return Vue.http.get(apiUrlBase + 'session/verify', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//Register Step 1
export const newMemberEnroll = (inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/enroll',inputs);  
}

//Register Step 2.1
export const verifyActivationKey = (activation_key) =>
{
    return Vue.http.get(apiUrlBase + 'member/verify_activation?ActivationKey=' + activation_key);  
}

//Register Step 2.2
export const completeMemberRegistration = (inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/complete_profile', inputs);  
}

//Register Extra Step
export const resendNewActivationKey = (inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/resend_activation', inputs);  
}


//Membership Payment Step 1
export const generateClientToken = () =>
{
    return Vue.http.post(apiUrlBase + 'membership/client_token');  
}

//Membership Payment Step 2 
export const executePayment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'membership/payment_execute', inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Membership Free Trial 
export const executeFreeTrial = (api_headers) =>
{
    return Vue.http.post(apiUrlBase + 'membership/trial', '', {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//FB Register
export const newFBRegister = (inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/fb/register',inputs);  
}
//Google Register
export const newGoogleRegister = (inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/google/register',inputs);  
}



export const getMemberDetail = (api_headers,session_key) => 
{
    return Vue.http.get(apiUrlBase + 'member/' + session_key, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const updateMemberDetail = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'member/edit',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const forgotPassword = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/forgot_pass',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const changePassword = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/change_pass',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const resetPassword = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/reset_pass',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}



export const subscriptionCancellation = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'membership/cancellation',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const memberDeactivation = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/cancellation',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const getMemberStats = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'member/stats', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getMemberGradeStats = (api_headers, CategoryId = 0, CategorySectionId = 0, CategorySectionLevelId = 0,  ChapterNo = 0  ) => 
{
    return Vue.http.get(apiUrlBase + `member/stats/grades?CategoryId=${CategoryId}&CategorySectionId=${CategorySectionId}&CategorySectionLevelId=${CategorySectionLevelId}&ChapterNo=${ChapterNo}`, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const getMemberNotificationStats = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'member/stats/recent', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const getTeacherStats = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'teacher/stats', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const getTeacherCourseStats = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'teacher/stats/courses', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getTeacherStudentStats = (api_headers, page_limit = 10, page_offset = 1) => 
{
    return Vue.http.get(apiUrlBase + 'teacher/stats/students?PageLimit='+page_limit+'&PageOffset='+page_offset, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const searchMember = (api_headers,search_cri) => 
{
    return Vue.http.get(apiUrlBase + 'member/search?SearchMember=' + search_cri, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//Discussion Comments
export const postNewDiscussionComment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'discussion/new',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const updateDiscussionComment = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'discussion/edit',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


//Gets Member's Participated Discussions
export const getMemberDiscussions = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'discussion', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//Get Recent
export const getRecentMemberDiscussions = (api_headers) => 
{
    return Vue.http.get(apiUrlBase + 'discussion/recent', 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const viewRecentDiscussion = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'discussion/recent/viewed', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const deleteDiscussionComment = (api_headers, discussion_id) => 
{
    return Vue.http.delete(apiUrlBase + 'discussion/delete/'+discussion_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getDiscussionComments = (api_headers,course_id, section_item_id) => 
{
    return Vue.http.get(apiUrlBase + 'discussion/' + course_id + "/" + section_item_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


//Post New Message
export const postNewMessage = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'message/new',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getMemberMessages = (api_headers, timeZone = null) => 
{
    var url = apiUrlBase + 'message';
    if(timeZone != null){
        url += `?ClientTimeZone=${timeZone}`;
    }
    return Vue.http.get(url, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getMemberMessagesWith = (api_headers,receiver_id) => 
{
    return Vue.http.get(apiUrlBase + 'message/'+receiver_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//Viewed Message
export const postViewedMessage = (api_headers, inputs) => 
{
    return Vue.http.post(apiUrlBase + 'message/viewed',inputs, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


//Inquiry
export const getTeacherInquiry = (api_headers, page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'inquiry?PageLimit='+page_limit+'&PageOffset='+page_offset, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


//Viewed Inquiry
export const postViewedInquiry = (api_headers, inputs) => 
{
    return Vue.http.post(apiUrlBase + 'inquiry/viewed',inputs, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const postChangeProfile = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/profile_photo',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const postChangeBackdrop= (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'member/profile_backdrop',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const postNewActivity = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'activity/new',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const updateActivity = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'activity/edit',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const deleteActivity = (api_headers, post_id) => 
{
    return Vue.http.delete(apiUrlBase + 'activity/delete/'+post_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
export const getMemberActivity = (api_headers , page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'activity?PageLimit='+page_limit+'&PageOffset='+page_offset, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}





//Use in Contact US AGe
export const postNewContact = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'inquiry/new',inputs,{
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}