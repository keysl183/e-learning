import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
Vue.use(VueResource);

//Get All Teachers
export const getTeachers = (api_headers , page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'admin/teacher?PageLimit='+page_limit+'&PageOffset='+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const newTeacher = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'admin/teacher/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const grantAdminRightsTecher = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'admin/teacher/grant', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deactivateActivateTeacher = (api_headers, member_id) =>
{
    return Vue.http.put(apiUrlBase + 'admin/teacher/change/'+member_id,{}, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getMemberDetail = (api_headers, user_id ) => 
{
    return Vue.http.get(apiUrlBase + 'member_detail/' + user_id,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}




