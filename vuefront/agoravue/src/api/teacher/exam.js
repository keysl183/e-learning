import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
import { userInfo } from 'os';
Vue.use(VueResource);

//START CATEGORIES
export const getCategories = (api_headers, page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'categories?PageLimit='+page_limit+'&PageOffset='+page_offset);
}
export const getCategories2 = (api_headers, page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'categories2?PageLimit='+page_limit+'&PageOffset='+page_offset);
}

//No Auth needed, just a list of chapters
export const getCategories2LevelChapters = (api_headers, category_section_id, category_section_level_id , page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'categories/section_level/'+category_section_id + '/' + category_section_level_id + '?PageLimit='+page_limit+'&PageOffset='+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const getCategories3 = (api_headers, page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'categories3?PageLimit='+page_limit+'&PageOffset='+page_offset);
}
export const getCategories3SectionDetail = (api_headers, category_section_id) => 
{
    return Vue.http.get(apiUrlBase + 'categories3/section/'+category_section_id,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
export const getCategories3SectionLevelDetail = (api_headers, category_section_id, category_section_level_id , page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'categories3/section_level/'+category_section_id + '/' + category_section_level_id + '?PageLimit='+page_limit+'&PageOffset='+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const newCategory = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'categories/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getCategory = (api_headers,category_id) => 
{
    return Vue.http.get(apiUrlBase + 'categories/' + category_id);
}

export const updateCategory = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'categories/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteCategory = (api_headers, category_id) =>
{
    return Vue.http.delete(apiUrlBase + 'categories/'+category_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const newCategorySectionExam = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'categories/section/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const updateCategorySectionExam = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'categories/section/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteCategorySection = (api_headers, section_id) =>
{
    return Vue.http.delete(apiUrlBase + 'categories/section/delete/'+section_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const newCategorySectionLevel = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'categories/section/level/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const editCategorySectionLevel = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'categories/section/level/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteCategorySectionLevel = (api_headers, level_id) =>
{
    return Vue.http.delete(apiUrlBase + 'categories/section/level/delete/'+level_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getCategorySection = (api_headers, course_id) =>
{
    return Vue.http.get(apiUrlBase + 'categories/'+course_id+'/section');
}

//END CATEGORIES

export const newCourseExam = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'course/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//BUG : Should get only the courses by the logged in teacher, THIS WAAS FIXED
//API that gets all courses should be separated tho 
export const getTeacherCourses = (api_headers, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/courses?PageLimit='+page_limit+'&PageOffset='+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//FOR General without valid headers is OK
//cate is null
//sort by is 2 means latest
export const getCourses = (api_headers,cate='', sortby = 2, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'courses?PageLimit='+page_limit+'&PageOffset='+page_offset+'&SortBy='+sortby+'&Category='+cate,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getCourseDetail = (api_headers, course_id) =>
{
    return Vue.http.get(apiUrlBase + 'courses/'+course_id);
}

export const updateCourseExam = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'course/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteCourseExam = (api_headers, course_id) =>
{
    return Vue.http.delete(apiUrlBase + 'courses/'+course_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//TO REMOVED FOR CLIENT REQUEST
// export const newCourseSectionExam = (api_headers, inputs) =>
// {
//     return Vue.http.post(apiUrlBase + 'course/section/new', inputs, {
//         headers : {"X-Authorization" : api_headers["X-Authorization"],
//         "User-Key" : api_headers["User-Key"]}
//     });  
// }
//TO REMOVED FOR CLIENT REQUEST
export const updateCourseSectionExam = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'course/section/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
//TO REMOVED FOR CLIENT REQUEST
export const deleteCourseSection = (api_headers, section_id) =>
{
    return Vue.http.delete(apiUrlBase + 'course/section/delete/'+section_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//TO REMOVED FOR CLIENT REQUEST
export const getCourseSection = (api_headers, course_id) =>
{
    return Vue.http.get(apiUrlBase + 'course/'+course_id+'/section');
}

export const newQuizz = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'course/quiz/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const updateQuizz = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'course/quiz/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteQuizz = (api_headers, section_item_id) =>
{
    return Vue.http.delete(apiUrlBase + 'course/quiz/delete/'+section_item_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getWorksheetsBySectionID = (api_headers, section_id = null) =>
{
    return Vue.http.get(apiUrlBase + 'course/worksheets/' + section_id);
}

export const getItemsforAssignment = (api_headers, category_id = null, category_section_id = null, category_section_level_id = null) =>
{
    return Vue.http.get(apiUrlBase + 'exam/items?CategoryID=' + category_id +"&CategorySectionID="+category_section_id+
                            '&CategorySectionLevelID='+category_section_level_id);
}

export const getItemsforQuestionBanks = (api_headers, category_id = null) =>
{
    return Vue.http.get(apiUrlBase + 'exam/section_items?CategoryID=' + category_id,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getSectionLevelforQuestionBanks = (api_headers, category_section_id = null) =>
{
    return Vue.http.get(apiUrlBase + 'exam/section_levels?SectionID=' + category_section_id,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


export const newAssignment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'teacher/assignment/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const editAssignment = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'teacher/assignment/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Teacher API for checking assigment
export const approvePendingAssignment = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'teacher/student/assignment/approve', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getAssignment = (api_headers, page_limit=20, page_offset= 1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/assignment'  +'?'+ 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getRecentAssignmentSubmission = (api_headers) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/assignment/recent', {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const getAssignmentDetail = (api_headers, assignment_id) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/assignment/' + assignment_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const deleteAssignment = (api_headers, assignment_id) =>
{
    return Vue.http.delete(apiUrlBase + 'teacher/assignment/'+assignment_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


export const getAssignmentSubmissions = (api_headers, assignment_id, page_limit=20, page_offset= 1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/student/submissions/'+assignment_id +'?'+ 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Get a Student submission
export const getAssignmentSubmissionDetailed = (api_headers, assignment_id, user_id) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/student/view/'+assignment_id +'/'+ user_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getAssignmentSubmissionMCQQuestions = (api_headers, assignment_id,user_id, page_limit=20, page_offset= 1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/student/question/result/'+assignment_id +'/'+ + user_id + '?'+ 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
//Grade Schema
export const getGradeSchema = (api_headers, course_id) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/grades/'+course_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const newGradeSchema = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'teacher/grades/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const putBulkGradeSchema = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'teacher/grades/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const deleteGradeSchema = (api_headers, schema_id) =>
{
    return Vue.http.delete(apiUrlBase + 'teacher/grades/delete/'+schema_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//
export const getRecentGradeSubmission = (api_headers) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/recent/grades', {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const viewRecentGradeSumission = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'teacher/recent/grades/viewed', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Get the Main List for grade
export const getMainGradeList = (api_headers,  page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/list/grades'+'?'+ 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getLessonsBySectionLevelChapter = (api_headers, section_id, section_level_id, chapter_no,   page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/list/grades'+'?'+ 'CategorySectionID='+section_id + '&CategorySectionLevelID='+section_level_id + '&CategorySectionLevelChapterID='+chapter_no +
         '&PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
export const getGradeResultForCourse = (api_headers, course_id, page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/result/'+course_id+'?'+ 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getStudentIndepthGradeResultForCourse = (api_headers, course_id, user_id, progress_id=0) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/student/result/'+course_id + '/' + user_id + '/' + progress_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

export const getStudentQuestionResultForCourse = (api_headers, progress_id, user_id,  page_limit=20, page_offset=1) =>
{
    return Vue.http.get(apiUrlBase + 'teacher/student/questions/result/'+progress_id + '/' + user_id+ '?' + 'PageLimit='+page_limit+'&PageOffset='+page_offset, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}



export const deleteUserAttemptForGrade = (api_headers, progress_id) =>
{
    return Vue.http.delete(apiUrlBase + 'teacher/result/'+progress_id, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


//This api is for checking pending worksheet,unit test and exams

export const approvePendingExam = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'teacher/student/approve', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}