import Vue from 'vue';
import VueResource from 'vue-resource';

import {apiUrlBase} from '../_setup.js';
Vue.use(VueResource);



//Create new QuestionBank 
export const newQuestionBank = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'question_bank/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}

//Updates or deletes QuestionBank
export const updateQuestionBank = (api_headers, inputs) =>
{
    return Vue.http.put(apiUrlBase + 'question_bank/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}


//Get Question Banks associated to  the currently logged in user
export const getQuestionBanks = (api_headers, page_limit=20, page_offset=1, course_section_id = 0, category_section_level_id = 0) => 
{
    return Vue.http.get(apiUrlBase + 'questionbanks?PageLimit='+page_limit+'&PageOffset='+page_offset+'&CourseSectionID='+course_section_id+'&LevelID='+category_section_level_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//Get Question Banks associated to  the currently logged in user
export const getQuestionBankDetail = (api_headers, bank_id) => 
{
    return Vue.http.get(apiUrlBase + 'questionbanks/'+bank_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


//Get Bank Questions
export const getQuestionsByBankId = (api_headers, bank_id, page_limit=20, page_offset=1) => 
{
    return Vue.http.get(apiUrlBase + 'questions?BankID='+bank_id+"&PageLimit="+page_limit+"&PageOffset="+page_offset,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

//TODO Answermode and Question Type

//New Question
export const newQuestion = (api_headers, inputs) => 
{
    return Vue.http.post(apiUrlBase + 'question/new',inputs ,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//Update Question
export const updateQuestion = (api_headers, inputs) => 
{
    return Vue.http.post(apiUrlBase + 'question/edit',inputs ,
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//Delete Question
export const deleteQuestion = (api_headers, question_id) => 
{
    return Vue.http.delete(apiUrlBase + 'question/delete/'+question_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}

export const getQuestionDetail = (api_headers, question_id, language = 0) => 
{
    return Vue.http.get(apiUrlBase + 'question/'+question_id + "?language="+language, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}


//Create new Translated Question
export const newTranslatedQuestion = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'translation/question/new', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
//EditTranslated Question
export const editTranslatedQuestion = (api_headers, inputs) =>
{
    return Vue.http.post(apiUrlBase + 'translation/question/edit', inputs, {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });  
}
//Get al Available  Translation for a Question 
export const getQuestAvailableTranslation = (api_headers, question_id) => 
{
    return Vue.http.get(apiUrlBase + 'translation/question/'+question_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}
//DELETE Translated Question
export const deleteTranslatedQuestion = (api_headers, question_id, country_id) => 
{
    return Vue.http.delete(apiUrlBase + 'translation/question/'+question_id + "/" + country_id, 
    {
        headers : {"X-Authorization" : api_headers["X-Authorization"],
        "User-Key" : api_headers["User-Key"]}
    });
}