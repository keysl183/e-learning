import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
Vue.use(Router)

const router =  new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'Index',
      component:  Index
    },
    {
      path: '/complete_register',
      name: 'RegisterComplete',
      component:  resolve => require(['../components/register/Register2'], resolve)
    },
    {
      path: '/membership_payment',
      name: 'Membershiplist',
      component:  resolve => require(['../components/register/Membershiplist'], resolve)
    },
    {
      path: '/membership_payment_complete',
      name: 'MembershipComplete',
      component:  resolve => require(['../components/register/MembershipComplete'], resolve)
    },    
    {
      path: '/pricing',
      name: 'Pricing',
      component:  resolve => require(['../components/register/Pricing'], resolve)
    },
    {
      path: '/courses',
      name: 'CourseList',
      component: resolve => require(['../components/courses/CourseList'], resolve)
    },
    {
      path: '/courses_sub/:course_id',
      name: 'CourseSub',
      component: resolve => require(['../components/courses/CourseSub'], resolve)
    },
    {
      path: '/courses/:course_id',
      name: 'CourseDetail',
      component: resolve => require(['../components/courses/CourseDetail'], resolve)
    },    
    {
      path: '/demo',
      name: 'Demo',
      component: resolve => require(['../components/misc/Demo'], resolve)
    },
    {
      path: '/contact',
      name: 'Contactus',
      component:  resolve => require(['../components/misc/Contact'], resolve)
    },
    {
      path: '/faq',
      name: 'FaqList',
      component:  resolve => require(['../components/misc/FaqList'], resolve)
    },
    {
      path : '/faq/all',
      name:'FaqAll',
      component: resolve => require(['../components/misc/FaqAll'], resolve)
    },
    {
      path: '/privacy_policy',
      name: 'PrivacyPolicy',
      component: resolve => require(['../components/misc/PrivacyPolicy'], resolve)
    },
    {
      path: '/terms_and_condition',
      name: 'TermsCondition',
      component: resolve => require(['../components/misc/TermsCondition'], resolve)
    },          
    {
      path : '/resetpassword',
      name:'ResetPassword',
      component: resolve => require(['../components/subcomponents/modal/ResetPassword.vue'], resolve)
    },    

    {
      path: '/teacher/assignments',
      name: 'TeacherAssignment',
      component: resolve => require(['../components/teacher/subcomponents/assignments/TeacherAssignments'], resolve), 
      meta:{requireAuth : true, parentNav : 'assignment'},
      children:[
        {
          path : 'new',
          name:'NewAssignment',
          component: resolve => require(['../components/teacher/subcomponents/assignments/subcomponents/NewAssignment'], resolve),
          meta:{requireAuth : true, parentNav : 'assignment'},
        },
        {
          path : 'edit/:assign_id',
          name:'EditAssignment',
          component: resolve => require(['../components/teacher/subcomponents/assignments/subcomponents/EditAssignment'], resolve),
          meta:{requireAuth : true, parentNav : 'assignment'},
        },        
        {
          path : 'view/:user_id/:assign_id/:submission_id',
          name:'ViewAssignment',
          component: resolve => require(['../components/teacher/subcomponents/assignments/subcomponents/ViewAssignment'], resolve),
          meta:{requireAuth : true, parentNav : 'assignment'},
        } ,           
      ]
    },  
    {
      path : '/teacher/grades',
      name:'TeacherGrade',
      component: resolve => require(['../components/teacher/subcomponents/grades/TeacherGrades'], resolve),
      meta:{requireAuth : true, parentNav : 'grade'},
      children:[
        {
          path : 'sheet/:category_id',
          name:'GradeSheets',
          component: resolve => require(['../components/teacher/subcomponents/grades/subcomponents/GradeSheets'], resolve),
          meta:{requireAuth : true, parentNav : 'grade'},
        },   
        {
          path : 'schema/:course_id',
          name:'GradeSchema',
          component: resolve => require(['../components/teacher/subcomponents/grades/subcomponents/GradeSchema'], resolve),
          meta:{requireAuth : true, parentNav : 'grade'},
        },  
        {
          path : 'indepth/:course_id/:progress_id',
          name:'GradeIndepthView',
          component: resolve => require(['../components/teacher/subcomponents/grades/subcomponents/IndepthGrade'], resolve),
          meta:{requireAuth : true, parentNav : 'grade'},
        },
        {
          path : 'view/:course_id/:section_item_id/:user_id/:progress_id',
          name:'ViewStudentGrade',
          component: resolve => require(['../components/teacher/subcomponents/grades/subcomponents/ViewStudentGrade'], resolve),
          meta:{requireAuth : true, parentNav : 'grade'},
        },                                  
      ]
    },                
    {
      path: '/teacher',
      name: 'Teacher',
      component : resolve => require(['../components/teacher/TeacherMain'], resolve),
      meta:{requireAuth : true, parentNav : 'exam'},
      children:[
        {
          path : 'question_bank',
          name:'QuestionBanks',
          component: resolve => require(['../components/teacher/subcomponents/question_banks/QuestionBank'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
          children:[
            {
              path : 'new',
              name:'QuestionBankNew',
              component:resolve => require(['../components/teacher/subcomponents/question_banks/NewBank'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },
            {
              path : 'edit/:bank_id',
              name:'QuestionBankEdit',
              component:resolve =>require(['../components/teacher/subcomponents/question_banks/EditBank'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },
            {
              path : 'edit/:bank_id/new_question',
              name:'QuestionNew',
              component:resolve =>require(['../components/teacher/subcomponents/question/NewQuestion'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },
            {
              path : 'edit/:bank_id/edit_question/:question_id',
              name:'QuestionEdit',
              component : resolve =>require(['../components/teacher/subcomponents/question/EditQuestion'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            }            
          ]
          //End Question Banks
        },
        {
          path : 'exams',
          name:'Exams',
          component : resolve => require(['../components/teacher/subcomponents/exams/exams'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
          children:[
            {
              path : 'new',
              name:'NewExam',
              component:resolve =>require(['../components/teacher/subcomponents/exams/NewExam'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },
            {
              path : 'edit/:exam_id',
              name:'EditExam',
              component:resolve =>require(['../components/teacher/subcomponents/exams/EditExam'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },  
          ]
          //End Exam
        },    
        {
          path : 'categories',
          name:'Categories',
          component: resolve =>require(['../components/teacher/subcomponents/exams/Categories'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
          children:[
            {
              path : 'new',
              name:'NewCategory',
              component: resolve =>require(['../components/teacher/subcomponents/exams/NewCategory'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },
            {
              path : 'edit/:category_id',
              name:'EditCategory',
              component:resolve => require(['../components/teacher/subcomponents/exams/EditCategory'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },            
          
          ]
          //End Categories
        },        
        {
          path : 'admin',
          name:'TeacherAdmin',
          component: resolve => require(['../components/teacher/subcomponents/admin/Admin.vue'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
          children:[     
            {
              path : 'new',
              name:'NewTeacher',
              component: resolve =>require(['../components/teacher/subcomponents/admin/subcomponents/NewTeacher.vue'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            }, 
            {
              path : 'view/:user_id',
              name:'ViewTeacher',
              component: resolve =>require(['../components/teacher/subcomponents/admin/subcomponents/viewTeacher.vue'], resolve),
              meta:{requireAuth : true, parentNav : 'exam'},
            },                                                 
          ]
        },       
      ]    
    },
    //Student Routes
    {
      path: '/student/assignments',
      name: 'StudentAssignment',
      component: resolve => require(['../components/student/assignments/StudentAssignments'], resolve), 
      meta:{requireAuth : true},
      children:[
        {
          path : 'answer/:assign_id',
          name:'AnswerAssignment',
          component: resolve => require(['../components/student/assignments/AnswerAssignment'], resolve),
          meta:{requireAuth : true, parentNav : 'assignment'},
        }          
      ]
    },       
    {
      path: '/student/grades',
      name: 'StudentGrade',
      component: resolve => require(['../components/student/Grades'], resolve),
      meta:{requireAuth : true},
    },

    {
      path: '/student/exam',
      name: 'StudentExam',
      component: resolve => require(['../components/student/exams/Exam'], resolve),
      meta:{requireAuth : true, parentNav : 'exam'},
      children:[
        {
          path: 'take/:exam_id/:quiz_id',
          name: 'StudentTakeExam',
          component: resolve => require(['../components/student/exams/TakeExam'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
        },
        {
          path: 'progress/:exam_id/:quiz_id',
          name: 'StudentInProgressExam',
          component: resolve => require(['../components/student/exams/ExamField'], resolve),
          meta:{requireAuth : true, parentNav : 'exam'},
        }                 
      ]
    },   
    {
      path: '/discussions',
      name: 'Discussion',
      component: resolve => require(['../components/student/Discussion'], resolve),
      meta:{requireAuth : true},
    },    
    {
      path: '/activity',
      name: 'Activity',
      component: resolve => require(['../components/Activity'], resolve), 
      meta:{requireAuth : true},
    },
    {
      path: '/messages',
      name: 'Messages',
      component: resolve => require(['../components/student/Messages'], resolve),  
      meta:{requireAuth : true},
    },  

    {
      path: '/profile',
      name: 'Profile',
      component: resolve => require(['../components/Profile'], resolve), 
      meta:{requireAuth : true},
    },   
    {
      path: '*',
      redirect: '/'
    }                        
  ]
});


router.beforeEach(function (to, from, next) { 
  setTimeout(() => {
      window.scrollTo(0, 0);
  }, 200);
  next();
});

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
      // Start the route progress bar.
      NProgress.start();
  }
  next();
})

router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  NProgress.done();
})

export default router;
