import Vue from 'vue';
import Vuex from 'vuex';
import {getCountries, getEducation} from '../api/common/definition.js'
import {getTeacherStats,getMemberStats, getTeacherCourseStats, getTeacherStudentStats, getMemberGradeStats } from '../api/member/member.js'
import {getCategories} from '../api/teacher/exam.js'
Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        isLoggedIn:false,
        apiHeaders:{
            'User-Key':null,
            'X-Authorization':null
        },
        isValidUser:null,
        memberBasicDetail:null, //email and level only
        memberDetail:null,
        memberStats:null,
        memberDiscussions:[],
        memberEnrollSelectedCategory: 0, 
        memberEnrolledCourse:[],
        memberPosts:[],
        memberStats:null,
        memberGradeStatsLoading:false,
        memberGradeStats:null,
        memberMenuNotif:null,
        memberAssignments:null,
        memberRecentAssignments:[],
        memberRecentDiscussionsIsLoaded:false,
        memberRecentDiscussions:[],
        countries:[],
        educations:[],
        categories:[],
        teacherStats:null,
        teacherCourseStats:null,
        teacherCourseStatsPageCount:0,
        teacherStudentStats:null,
        teacherStudentStatsPageCount:0,
        teacherCourses:[], //Courses associated to the logged in teacher 
        teacherQuestionBanks:[],
        teacherRecentAssignmentIsLoaded:false,
        teacherRecentAssignmentsSubmission:[],
        teacherAssignments:[],
        teacherGrades :[],
        teacherRecentGradesSubmission:[],
        teacherGradeSchema:[]
    },
    getters:{
        languages : state => {
            return state.countries.filter(country=>{
                return (country.language != '' && country.language != null);
            });
        },
        recentDiscussion : state =>{
            return state.memberDiscussions.filter(discuss=>{
                var date = new Date(discuss.date_added);
                var current = new Date();
                var timeDiff = Math.abs(current.getTime() - date.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                return (diffDays > 1) ? false : true;
            })
        },
        recentAssignments : state =>{
            if(!state.memberAssignments){
                return [];
            }
            return state.memberAssignments.filter(discuss=>{
                var date = new Date(discuss.date_added);
                var current = new Date();
                var timeDiff = Math.abs(current.getTime() - date.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                return (diffDays > 1) ? false : true;
            })
        }        
        
    },
    mutations:{
        commitAPIHeaders(state, payload){

        },
        commitIsValidUser(state, payload){
            state.isValidUser = payload;
        },
        commitProfileImage(state, payload){
            if(state.memberDetail){
                state.memberDetail.profile_image = payload;
            }
        },
        commitProfileBackdrop(state, payload){
            if(state.memberDetail){
                state.memberDetail.back_drop = payload;
            }
        },        
        saveMemberDetail(state, payload){
            state.memberDetail = payload;
            state.isLoggedIn = true;
        },
        saveMemberStats(state, payload){
            state.memberStats = payload;
        },
        commitMemberNotif(state, payload){
            state.memberMenuNotif = payload;
        },      
        commitMemberNotifSubMessage(state, payload){
            state.memberMenuNotif.Message = payload;
        },            
        commitCountries(state, payload){
            state.countries = payload;
        },
        commitEducation(state, payload){
            state.educations = payload;
        },   
        commitCategories(state, payload){
            state.categories = payload;
        },             
        commitEnrolledExams(state, payload){
            state.memberEnrolledCourse = payload;
        },
        commitEnrollSelectedCategory(state, payload){
            state.memberEnrollSelectedCategory = payload;
        },
        commitMemberPosts(state,payload){
            state.memberPosts = payload;
        },   
        commitMemberAssignments(state,payload){
            state.memberAssignments = payload;
        },           
        commitRecentDiscussions(state, payload){
            state.memberRecentDiscussions = payload;
            if(state.memberRecentDiscussionsIsLoaded){
                return;
            }
            state.memberRecentDiscussionsIsLoaded = true;
        },
        commitDiscussions(state, payload){
            state.memberDiscussions = payload;


            
        },
        removeDiscussionFromRecentDiscussion(state, payload){
            state.memberRecentDiscussions.splice(payload, 1);
            if(state.memberRecentDiscussions.length === 0){
                state.memberMenuNotif.Discussion =  false;
            }
        },        
        commitTeacherCourses(state, payload){
            state.teacherCourses = payload;
        },    
        commitTeacherBanks(state, payload){
            state.teacherQuestionBanks = payload;
        },
        commitTeacherAssignments(state, payload){
            state.teacherAssignments = payload;
        },   
        commitTeacherRecentAssignmentsSubmission(state, payload){
            state.teacherRecentAssignmentIsLoaded = true;
            state.teacherRecentAssignmentsSubmission = payload;
            if(state.teacherRecentGradesSubmission.length === 0){
                state.memberMenuNotif.Assignment =  false;
            }
        },          
        removeAssignmentFromRecentSubmission(state, payload){
            state.teacherRecentAssignmentsSubmission.splice(payload, 1);
            if(state.teacherRecentGradesSubmission.length === 0){
                state.memberMenuNotif.Assignment =  false;
            }
        },
        commitTeacherRecentGradesSubmission(state, payload){
            state.teacherRecentGradesSubmission = payload;
            if(state.teacherRecentGradesSubmission.length === 0){
                state.memberMenuNotif.Grade =  false;
            }            
        },         
        commitTeacherGrades(state, payload){
            state.teacherGrades = payload;
        },               
        commitTeacherStats(state, payload){
            state.teacherStats = payload;
        },
        commitTeacherCourseStats(state, payload){
            state.teacherCourseStats = payload;
        },
        commitTeacherCourseStatsPageCount(state, payload){
            state.teacherCourseStatsPageCount = payload;
        },        
        commitTeacherStudentStats(state, payload){
            state.teacherStudentStats = payload;
        }, 
        commitTeacherStudentStatsPageCount(state, payload){
            state.teacherStudentStatsPageCount = payload;
        },                     
        commitTeacherGradeSchema(state, payload){
            state.teacherGradeSchema = payload;
        },   
        commitMemberStats(state, payload){
            state.memberStats = payload;
        },  
        commitMemberGradeStats(state, payload){
            state.memberGradeStats = payload;
        },             
        commitMemberGradeStatsLoading(state, payload){
            state.memberGradeStatsLoading = payload
        }  ,
        commitMenberRecentAssignments(state, payload){
            state.memberRecentAssignments = payload;
        },      
        removeAssignmentFromRecent(state, payload){
            state.memberRecentAssignments.splice(payload, 1);
            if(state.memberRecentAssignments.length === 0){
                state.memberMenuNotif.Assignment =  false;
            }
        },                       
    },
    actions:{
        fetchCountries:function(context){
            getCountries().then((response) =>{
                context.commit('commitCountries',response.body.Countries);
            });
        },
        fetchEducation:function(context){
            getEducation().then((response) =>{
                context.commit('commitEducation',response.body.Educations);
            });
        },
        fetchCategories:function(context){
            getCategories().then((response)=>{ 
                context.commit('commitCategories',response.body.Categories);
            });            
        },
        fetchTeacherStats:function(context, api_headers){
            getTeacherStats(api_headers).then((response)=>{
                context.commit('commitTeacherStats',response.body.Stats);
            });
        },
        fetchTeacherCourseStats:function(context, payload){
            getTeacherCourseStats(payload.api_headers, payload.PageLimit, payload.PageOffset).then((response)=>{
                context.commit('commitTeacherCourseStats',response.body.CourseStats);
                context.commit('commitTeacherCourseStatsPageCount',response.body.PageCount);
            });
        },  
        fetchTeacherStudentStats:function(context, payload){
            getTeacherStudentStats(payload.api_headers, payload.PageLimit, payload.PageOffset).then((response)=>{
                context.commit('commitTeacherStudentStats',response.body.StudentStats);
                context.commit('commitTeacherStudentStatsPageCount',response.body.PageCount);
            });
        },                   
        fetchMemberStats: function(context, api_headers){
            getMemberStats(api_headers).then((response)=>{
                context.commit('commitMemberStats',response.body.Stats);
            });
        },
        fetchMemberGradeStats:function(context,payload){
            
            context.commit('commitMemberGradeStatsLoading',true);
            getMemberGradeStats(payload.api_headers, payload.CategoryID, payload.SectionID, payload.LevelID, payload.ChapterNo).then(response=>{
                context.commit('commitMemberGradeStats',response.body.GradeStats.History);
            }).finally(()=>{
                context.commit('commitMemberGradeStatsLoading',false);
            })
        }        
    }
});