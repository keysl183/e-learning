<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/validation/validation.php';
require_once '../vendor/braintree/braintree_php/lib/Braintree.php';

//Set up the Debug Configuration
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

//Set up the DB Configuration
// $config['db']['host']   = 'localhost';
// $config['db']['user']   = 'root';
// $config['db']['pass']   = '';
// $config['db']['dbname'] = 'agora_dev_db';

//DEV TEST SITE DB Configuration
$config['db']['host']   = 'localhost';
$config['db']['user']   = 'agoras5_elearning_dev';
$config['db']['pass']   = 'elearningschooldevuser';
$config['db']['dbname'] = 'agoras5_agora_dev_db';

//Email Configuration
$config['mail']['site'] = 'https://www.dev.agora-school.com/';
$config['mail']['from_email'] = 'support@dev.agora.com';
$config['mail']['reply_to'] = 'support@dev.agora.com';
$config['mail']['bcc'] = 'marcjhon18@gmail.com';
//Payment

//Instantiate the Slim App
$app = new \Slim\App(
[
    'settings' => $config
]
);

//Enable CORS. 
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, User-Key, X-Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

//Load Other Injectable dependencies
$container = $app->getContainer();

//Custom System ERror Handler for Debugging Purpose
$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $response->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($exception);
    };
};

//Load the Logger Tool to help us in debugging 
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

//Load Our Database Instance
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname']. ';charset=utf8',
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
    $this->logger->addInfo('Database Loaded');
};

//Load our Validator Library. 
$container['validator'] = function($c)
{
    return new App\Validation\Validator;
};

$container['mail'] = function($c)
{
    return $c['settings']['mail']; 
};

//Load our payment gateway
$container['gateway'] = function($c)
{
    $gateway = new Braintree_Gateway([
        'environment' => 'sandbox',
        'merchantId' => 'zcmwctmyhv5mtks2',
        'publicKey' => 'kdtkd4xcmf6qrw7j',
        'privateKey' => '2b246787d67f8a003e4b2051040eab98'
    ]);
    return $gateway;
};

//Default User Level
$container['USERLEVEL'] = function($c)
{
    $level = array(
          'NO_MEMBERSHIP'=>  0,
          'FREE' => 1,
          'MONTHLY' => 2,
          'ANNUAL' => 3,
          'TEACHER'=> 4                                
    );
    return (object)$level;
};


//Require the Commonly used Mappers and Handlers for reusability
require '../src/validation/request_validation.php';
require '../src/validation/file_upload_validation.php';
require '../src/validation/questions/question_validation.php';

require '../src/mappers/member_mapper.php';
require '../src/mappers/question_mapper.php';
require '../src/mappers/course_mapper.php';
require '../src/mappers/exam_mapper.php';
require '../src/mappers/social_mapper.php';
require '../src/mappers/trans_question_mapper.php';
require '../src/mappers/assignment_mapper.php';
require '../src/mappers/grade_mapper.php';
//Require the Routes
require '../routes/session.php';
require '../routes/common.php';
require '../routes/member_enroll.php';
require '../routes/member_search.php';
require '../routes/member_payment.php';
require '../routes/question.php';
require '../routes/trans_question.php';
require '../routes/course.php';
require '../routes/exam.php';
require '../routes/grades.php';
require '../routes/assignment.php';
require '../routes/social.php';
require '../routes/admin.php';
require '../routes/file_upload.php';
session_start();
$app->run();