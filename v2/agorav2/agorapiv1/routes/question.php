<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/question_bank_handler.php';

/*Summary 
API here are as follow

New Question Bank                   /api/question_bank/new
Update Question Bank                /api/question_bank/edit
Get Question Banks(Author)          /api/questionbanks
Get Question Bank(Author)           /api/questionbanks/{id}
Get Question Types & Answer Modes   /api/question/types
New Question API                    /api/question/new

All Requires Session to be set
*/

//New Question Bank
$app->post('/api/question_bank/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'BankTitle' => v::notEmpty()->length(null, 255),
        // 'BankDescription' => v::length(null, 255),
        'Category' => v::notEmpty()->numeric()->positive(),
        'Section' => v::notEmpty()->numeric()->positive(),
        'Level'=> v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $questtionbank_id = $question->CreateNewQuestionBank($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'QuestionBankID'=> $questtionbank_id)));
});

//Update Question Bank
$app->put('/api/question_bank/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation_rules = array();
    $validation_rules['BankID'] = v::numeric()->positive()->notEmpty();

    
    if(isset($posted_data['Delete'])){
        $validation_rules['Delete'] =  v::numeric()->positive();
    } else{
        $validation_rules['BankTitle'] =  v::notEmpty()->length(null,255);
        // $validation_rules['BankDescription'] =  v::length(null,255);
        $validation_rules['Category'] =  v::notEmpty()->numeric()->positive();
        $validation_rules['Section'] =  v::notEmpty()->numeric()->positive();
        $validation_rules['Level']  = v::notEmpty()->numeric()->positive();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistBank($posted_data["BankID"]);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $question->UpdateQuestionBank($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get Question Banks of the currently logged in User
$app->get('/api/questionbanks',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $course_section_id = $request->getQueryParam('CourseSectionID', $default = 0);
    $section_level_id = $request->getQueryParam('LevelID', $default = 0);
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset,
        "CourseSectionID"=> $course_section_id,
        "LevelID"=> $section_level_id
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0),
        'CourseSectionID' =>  v::numeric()->min(0),
        'LevelID' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new QuestionBankMapper($this->db);
    $bank_count = $mapper->CountQuestionBanks($valid['SessionValue']);
    $results = $mapper->GetUserQuestionBanks($valid['SessionValue'],$course_section_id,$section_level_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
                                "TotalCount"=>$results["TotalCount"],
                                "PageCount"=>$results["PageCount"],
                                "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,                                
                                "QuestionBanks"=> $results["Results"])));
});

//Get Question Bank Details By ID
$app->get('/api/questionbanks/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $bank_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);
    
    $mapper = new QuestionBankMapper($this->db);
    $bank = $mapper->GetQuestionBankById($bank_id, $valid['SessionValue']);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "QuestionBank"=> $bank)));
});

//Get Question Types & Answer Modes
$app->get('/api/question/types',function(Request $request, Response $response){
  
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $mapper = new QuestionBankMapper($this->db);
    $types = $mapper->GetQuestionTypes();
    $ans_modes = $mapper->GetQuestionAnswerModes();
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "QuestionTypes"=> $types, "AnswerModes"=> $ans_modes)));
});

// Create New Question
$app->post('/api/question/new',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
 
    $validation_rules = array(
        'BankID'=>              v::notEmpty()->numeric()->positive(),
        'QuestionType'=>        v::notEmpty()->numeric()->positive(),
        // 'QuestionTitle'=>       v::notEmpty()->length(null, 255),
        'QuestionDescription'=> v::stringType()->notEmpty(),
        'AnswerMode'=>          v::numeric()->positive()->notEmpty()
    );
    if($posted_data["AnswerMode"] == 1){
        if(trim($posted_data["ChoiceOne"]) == ''){
            $validation_rules["ChoiceOne"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceTwo"])  == ''){
            $validation_rules["ChoiceTwo"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceThree"]) == ''){
            $validation_rules["ChoiceThree"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceFour"]) == ''){
            $validation_rules["ChoiceFour"] = v::stringType()->notEmpty();
        }

        $validation_rules["CorrectAnswer"] = v::notEmpty();
    }
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    
    $existenceValidation  = array();
    $bankExistence = $question->ValidateExistBank($posted_data["BankID"]);
    $quesTypeExistence = $question->ValidateExistQuestionType($posted_data["QuestionType"]);
    $answerModeExistence = $question->ValidateExistAnswerMode($posted_data["AnswerMode"]);

    if($bankExistence) array_push($existenceValidation, $bankExistence);
    if($quesTypeExistence) array_push($existenceValidation, $quesTypeExistence);
    if($answerModeExistence) array_push($existenceValidation, $answerModeExistence);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $question->CreateNewQuestion($posted_data);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));
});

$app->post('/api/question/edit',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation_rules = array();
    $validation_rules['QuestionID'] =  v::numeric()->positive();
    $validation_rules['QuestionType'] =  v::numeric()->positive();
    // $validation_rules['QuestionTitle'] =  v::notEmpty()->length(null, 255);
    $validation_rules['QuestionDescription'] =  v::notEmpty();
    $validation_rules['QuestionType'] =  v::numeric()->positive();
    if(isset($posted_data['AnswerMode'])) $validation_rules['AnswerMode'] =  v::numeric()->positive();
    if(isset($posted_data['CorrectAnswer'])) $validation_rules['CorrectAnswer'] =  v::numeric()->positive()->min(1)->max(4);

    if($posted_data["AnswerMode"] == 1){
        if(trim($posted_data["ChoiceOne"]) == ''){
            $validation_rules["ChoiceOne"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceTwo"])  == ''){
            $validation_rules["ChoiceTwo"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceThree"]) == ''){
            $validation_rules["ChoiceThree"] = v::stringType()->notEmpty();
        }
        if(trim($posted_data["ChoiceFour"]) == ''){
            $validation_rules["ChoiceFour"] = v::stringType()->notEmpty();
        }
        $validation_rules["CorrectAnswer"] = v::notEmpty();
    }    
    
    
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    
    $existenceValidation  = array();
    if(isset($posted_data["QuestionType"]))
    {
        $quesTypeExistence = $question->ValidateExistQuestionType($posted_data["QuestionType"]);
        if($quesTypeExistence) array_push($existenceValidation, $quesTypeExistence);
    }

    if(isset($posted_data["AnswerMode"]))
    {
        $answerModeExistence = $question->ValidateExistAnswerMode($posted_data["AnswerMode"]);
        if($answerModeExistence) array_push($existenceValidation, $answerModeExistence);
    }

    $questionExistence =  $question->ValidateExistQuestion($posted_data["QuestionID"]);
    if($questionExistence) array_push($existenceValidation, $questionExistence);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $question->UpdateQuestion($posted_data);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));
});


//Get QuestionID's from the Bank,
$app->get('/api/questions',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $params = $request->getQueryParams();
    $bank_id = $params["BankID"];
    $validation = $this->validator->ValidateByArgs( $params, 
    [
        'BankID' => v::notEmpty()->numeric()->positive()
    ]);

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }  

    
    
    $mapper = new QuestionBankMapper($this->db);
    $question_count = $mapper->CountBankQuestions($bank_id);
    $questions = $mapper->GetQuestionsIdsByBankID($bank_id,$pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=>$question_count,"PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1 ,"Questions"=> $questions)));
});

//Get Question Details
$app->get('/api/question/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $question_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistQuestion($question_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }
    $target_language_to_generate = $request->getQueryParam('language', $default = 0); 

    $mapper = new QuestionBankMapper($this->db);
    $question = $mapper->GetQuestionByID($question_id, $target_language_to_generate);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Question"=>$question)));
});


$app->delete('/api/question/delete/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $question_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistQuestion($question_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }
    $question->DeleteQuestion($question_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    

});



