<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

/*Summary 
API here are as follow

Get All Members(Students)           /api/member
Search Members (Students)           /api/member/search
Get Logged in Member Detail         /api/member/{session_key}
Get Logged in Member Detail (ID)    /api/member_detail/{id}
*/

//Get All Members
$app->get('/api/member',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member_list = $member_mapper->GetMembers();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Members"=> $member_list)));
});

//Search Member
$app->get('/api/member/search',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $params = $request->getQueryParams();
    $search = $params["SearchMember"];
    $validation = $this->validator->ValidateByArgs( $params, 
    [
        'SearchMember' => v::notEmpty()->noWhitespace()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $member_list = $member_mapper->GetMemberSearch($search);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Members"=> $member_list)));
});

//Get Member Details By Session Key
$app->get('/api/member/{session_key}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $session_value = $args['session_key'];
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($session_value);
    $member = $member_mapper->GetMemberByUserIDAssoc($member->user_id);
    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array("Status"=>"OK", "Member"=> $member)));
});

//Get Member Details By ID
$app->get('/api/member_detail/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_id = $args['id'];
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserIDAssoc($member_id);
    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array("Status"=>"OK", "Member"=> $member)));
});




