<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/mappers/common_mapper.php';

/*Summary 
API here are as follow

Get All Countries                           /api/countries
Get Country By Id                           /api/countries/{id}
Get Supported Languages for Translation     /api/language/countries
Get All Education Levels                    /api/education
Get Education Level Detail                  /api/education/{id}

*/


//Get All Countries
$app->get('/api/countries',function(Request $request, Response $response, $args){
    
    $mapper = new CommonMapper($this->db);
    $countries = $mapper->GetCountries();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Countries"=> $countries)));
});

//Get Country By Id
$app->get('/api/countries/{id}',function(Request $request, Response $response, $args){
    $country_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($validation));
    }

    $mapper = new CommonMapper($this->db);
    $countries = $mapper->GetCountryById($country_id);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($countries));

});

//Get Asean Countries with Languages
//Used for translation 
$app->get('/api/language/countries',function(Request $request, Response $response, $args){
    
    $mapper = new CommonMapper($this->db);
    $countries = $mapper->GetCountriesWithLanguage();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Countries"=> $countries)));
});

//Get All Education Level
$app->get('/api/education',function(Request $request, Response $response, $args){
    $mapper = new CommonMapper($this->db);

    $education_levels = $mapper->GetEducationLevels();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Educations"=> $education_levels)));
});

//Get  Education Level By Id
$app->get('/api/education/{id}',function(Request $request, Response $response, $args){
    $education_level_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);
    
    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($validation));
    }

    $mapper = new CommonMapper($this->db);

    $education_levels = $mapper->GetEducationLevels();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($education_levels));
});
