<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/assignment_handler.php';

//TEACHER ASSIGNMENTS 

//Existence Valdations
$app->post('/api/teacher/assignment/new',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'Category' => v::notEmpty()->numeric()->positive(), //ID
        'Item' => v::notEmpty()->numeric()->positive(), //SectionItemID
        'Title' => v::notEmpty()->length(null,255),
        'Description' => v::notEmpty(),
        'AvailableFrom' => v::notEmpty()->date(),
        'AvailableTo' => v::notEmpty()->date(),
        'DueDate' => v::notEmpty()->date(),
        'PassingRate' => v::notEmpty()->numeric()->positive(),
        'Type' => v::notEmpty()->numeric()->positive()->min(1)->max(4),//ID
        'CategorySection' => v::notEmpty()->numeric()->positive(), //add existence val
        'Level' => v::notEmpty()->numeric()->positive(), //add existence val
    );

    //Type 1 = Audio, 2 = MCQ, 3 = Essay, 4 = Others
    if($posted_data['Type'] == 2){
        $rules['NoOfQuestions'] = v::notEmpty()->numeric()->positive();
        $rules['QuestionBank'] = v::notEmpty()->numeric()->positive(); //ID
    }else{
        $rules['Points'] = v::notEmpty()->numeric()->positive();
    }
    if($posted_data['Type'] == 1){
        // $rules['FileAttachment'] = v::notEmpty(); Optional for Teacher
    }
    if($posted_data['Type'] == 4){
        // $rules['FileAttachment'] = v::notEmpty();  Optional for Teacher
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $handler = new AssignmentHandler($this->db);
    $handler->CreateNewAssignment($member->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'res'=> $posted_data)));
});

$app->put('/api/teacher/assignment/edit',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'AssignmentID' => v::notEmpty()->numeric()->positive(), //ID
        'Category' => v::notEmpty()->numeric()->positive(), //ID
        'Item' => v::notEmpty()->numeric()->positive(), //SectionItemID
        'Title' => v::notEmpty()->length(null,255),
        'Description' => v::notEmpty(),
        'AvailableFrom' => v::notEmpty()->date(),
        'AvailableTo' => v::notEmpty()->date(),
        'DueDate' => v::notEmpty()->date(),
        'PassingRate' => v::notEmpty()->numeric()->positive(),
        'Type' => v::notEmpty()->numeric()->positive()->min(1)->max(4),//ID
    );

    //Type 1 = Audio, 2 = MCQ, 3 = Essay, 4 = Others
    if($posted_data['Type'] == 2){
        $rules['NoOfQuestions'] = v::notEmpty()->numeric()->positive();
        $rules['QuestionBank'] = v::notEmpty()->numeric()->positive(); //ID
    }else{
        $rules['Points'] = v::notEmpty()->numeric()->positive();
    }

    if($posted_data['Type'] == 1 ||$posted_data['Type'] == 4){
        $rules['FileAttachment'] = v::notEmpty();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $handler = new AssignmentHandler($this->db);
    $handler->UpdateAssignment($member->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get Assignments the Logged in Teacher Created
$app->get('/api/teacher/assignment',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new AssignmentMapper($this->db);
    $assignments = $mapper->GetTeacherAssignments($member->user_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", 
                                    "TotalCount"=>$assignments["TotalCount"],
                                    "PageCount"=>$assignments["PageCount"],
                                    "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1, 
                                    "Assignments"=> $assignments["Results"]
                                     )));
});

//Get Recent and Not yet checked Submissions
//Login Teacher only
$app->get('/api/teacher/assignment/recent',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }  

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new AssignmentMapper($this->db);
    $assignments = $mapper->GetRecentAssignmentSubmissions($member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=> count($assignments),"Assignment"=> $assignments)));
});

//For all
$app->get('/api/teacher/assignment/{id}',function(Request $request, Response $response, $args){

    $assignment_id = (int)$args['id'];
    $posted_data = array(
        "AssignmentID"=> $assignment_id
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'AssignmentID' =>  v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $mapper = new AssignmentMapper($this->db);
    $assignments = $mapper->GetTeacherAssignmentByID($assignment_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Assignment"=> $assignments)));
});

//DELETE ASSIGNMENT
$app->delete('/api/teacher/assignment/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $assignment_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);
    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $handler = new AssignmentHandler($this->db);
    $handler->DeleteAssignment($member->user_id, $assignment_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    

});

//Get Student Submissions
$app->get('/api/teacher/student/submissions/{assignment_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $assignment_id = (int)$args['assignment_id'];
    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    

    $posted_data = array(
        "assignment_id"=> $assignment_id,
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset        
    );
    
    $validation = $this->validator->ValidateByArgs( $posted_data, 
    [
        'assignment_id' => v::notEmpty()->numeric()->positive(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)                     
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    // $member_mapper = new MemberMapper($this->db);
    // $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new AssignmentMapper($this->db);
    $assignments = $mapper->GetAssignmentSubmissions($assignment_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "TotalCount"=>$assignments["TotalCount"],"Submissions"=> $assignments["Submissions"])));
});

//Views an assignment submission of a student
$app->get('/api/teacher/student/view/{assignment_id}/{user_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $assignment_id = (int)$args['assignment_id'];
    $user_id = (int)$args['user_id'];

    $posted_data = array(
        "assignment_id"=> $assignment_id,
        "user_id"=> $user_id,
    );
    
    $validation = $this->validator->ValidateByArgs( $posted_data, 
    [
        'assignment_id' => v::notEmpty()->numeric()->positive(),
        'user_id' =>  v::notEmpty()->numeric()->positive()                   
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($user_id);
    if(!$member){
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> array("Member"=> "Member does not exists"))));        
    }    


    $mapper = new AssignmentMapper($this->db);
    //reuse someone is using this
    $assignment = $mapper->GetStudentAssignmentByAssignID($user_id, $assignment_id);
    $member_detail = array(
        "fname"=> $member->fname,
        "lname"=> $member->lname,
    );
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Member"=> $member_detail,"Submission"=> $assignment)));
});

//Supplementary to api above
//Get the questions of the student and his answers
//separated for optimizaton
$app->get('/api/teacher/student/question/result/{assignment_id}/{user_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $assignment_id = (int)$args['assignment_id'];
    $student_id = (int)$args['user_id'];

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    

    $posted_data = array(
        "Assignment"=> $assignment_id,
        "Student"=> $student_id,
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset        
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Assignment' =>  v::notEmpty()->numeric()->positive(),
        'Student' =>  v::notEmpty()->numeric()->positive(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)        
    ]);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new AssignmentMapper($this->db);
    $results = $mapper->GetStudentQuestionsToAnswers($student_id, $assignment_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
        "TotalCount"=>$results["TotalCount"],  
        "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,  
        "Questions"=> $results["Questions"]
    )));
});


//API that teacher use to check student pending assignment
$app->post('/api/teacher/student/assignment/approve',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }


    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'AssignmentSubmissionID' => v::notEmpty()->numeric()->positive(), //ID
        'StudentID' => v::notEmpty()->numeric()->positive(), //ID
        'TeacherMark' => v::numeric()->min(0),
        'PassFailed'=> v::numeric()->min(0)->max(1),
        // 'Comment' NNOT REQUIRED
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $handler = new AssignmentHandler($this->db);
    $handler->UpdatePendingAssignment( $posted_data);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//END TEACHER ASSIGNMENTS 


//STUDENT ASSIGNMENTS STARTS
$app->get('/api/student/assignment',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new AssignmentMapper($this->db);
    $assignments = array();
    $assignments = $mapper->GetStudentAssignment($member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Count"=>count($assignments),"Assignments"=> $assignments)));
});

//Submit New Assignment
$app->post('/api/student/assignment/new',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'AssignmentID' => v::notEmpty()->numeric()->positive(), //ID
        'Type' => v::notEmpty()->numeric()->positive()->min(1)->max(4),//ID
    );

    //Type 1 = Audio, 2 = MCQ, 3 = Essay, 4 = Others
    if($posted_data['Type'] == 1 || $posted_data['Type'] == 4  ){
        $rules['FileAttachment'] = v::notEmpty();
    }
    else if($posted_data['Type'] == 2){
        $rules['Answers'] = v::notEmpty();
        $rules['Score'] = v::notEmpty()->numeric()->positive(); //ID
        $rules['Result'] = v::notEmpty();// 1 = PASSED 2 = FAILED 
    }else if($posted_data['Type'] == 3){
        $rules['Essay'] = v::notEmpty();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $handler = new AssignmentHandler($this->db);
    $handler->SubmitAssignment($member->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'res'=> $posted_data)));
});

$app->get('/api/student/assignment/recent',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }  

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new AssignmentMapper($this->db);
    $assignments = $mapper->GetRecentAssignmentsForStudent($member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=> count($assignments),"Assignment"=> $assignments)));
});

$app->post('/api/student/assignment/recent/viewed',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'AssignmentID' => v::notEmpty()->numeric()->positive(), //ID
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $handler = new AssignmentHandler($this->db);
    $handler->InsertViewedAssignment($posted_data["AssignmentID"], $member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//get student assignment if he has a submission
$app->get('/api/student/assignment/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $assignment_id = (int)$args['id'];
    $posted_data = array(
        "AssignmentID"=> $assignment_id
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'AssignmentID' =>  v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    
    $mapper = new AssignmentMapper($this->db);
    $assignments = array();
    $assignments = $mapper->GetStudentAssignmentByAssignID($member->user_id, $assignment_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Assignment"=> $assignments)));
});

//Supplementary to api above
//Get the questions of the student and his answers
//separated for optimizaton
$app->get('/api/student/question/result/{assignment_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $assignment_id = (int)$args['assignment_id'];

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    

    $posted_data = array(
        "Assignment"=> $assignment_id,
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset        
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Assignment' =>  v::notEmpty()->numeric()->positive(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)        
    ]);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);   

    $mapper = new AssignmentMapper($this->db);
    $results = $mapper->GetStudentQuestionsToAnswers($member->user_id, $assignment_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
        "TotalCount"=>$results["TotalCount"],  
        "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,  
        "Questions"=> $results["Questions"]
    )));
});
