<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/course_handler.php';

/*Summary 
API here are as follow

New CourseCategory                  /api/categories/new
Edit CourseCategory                 /api/categories/edit
Get Categories                      /api/categories
Get Categories2                     /api/categories (with Count of Number of Courses)
New Category Section                api/categories/section/new //Client Request


New Course                          /api/course/new
Update Course                       /api/course/edit
Get Courses                         /api/courses
Get Course Detail                   /api/courses/{id}
*/

//New Category
$app->post('/api/categories/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CategoryTitle' => v::notEmpty()->length(3,50),
        // 'CategoryDescription' => v::notEmpty()->length(null,500),
        'CoverPhoto' => v::notEmpty(),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_category = new CourseHandler($this->db,$valid['SessionValue']);
    $course_category->CreateNewCourseCategory($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Edit Course Category
$app->put('/api/categories/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation_rules = array();
    $validation_rules['CategoryID'] = v::numeric()->positive()->notEmpty();
    $validation_rules['CategoryTitle'] =  v::notEmpty()->length(3,50);
    $validation_rules['CoverPhoto'] =  v::notEmpty();
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_category = new CourseHandler($this->db,$valid['SessionValue']);

    $existenceValidation  = $course_category->ValidateExistCategory($posted_data["CategoryID"]);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_category->UpdatCourseCategory($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get Categories
//Does not need Authorization
$app->get('/api/categories',function(Request $request, Response $response){

    $mapper = new CourseMapper($this->db);
    $categories = $mapper->GetCategoryCourse();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Categories"=> $categories)));
});

//Clone with no of Categories
$app->get('/api/categories2',function(Request $request, Response $response){

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $mapper = new CourseMapper($this->db);
    $results = $mapper->GetCategoryCourse2($pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
                        "TotalCount"=>$results["TotalCount"],
                        "PageCount"=>$results["PageCount"],
                        "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,
                        "Categories"=> $results["Results"])));
});

//Clone with no of Categories
//This also shows the sections of the categories
$app->get('/api/categories3',function(Request $request, Response $response){

    $mapper = new CourseMapper($this->db);
    $categories = $mapper->GetCategoryCourse3();

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Categories"=> $categories)));
});

//Get the detail of a section by section id
$app->get('/api/categories3/section/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    $section_id = (int)$args['id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);
 
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $mapper = new CourseMapper($this->db);

    $categories = array();
 
    if($valid["IsValid"]){
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($valid['SessionValue']);
        if($member){
            if($member->user_level != 4){//for not teacher
                $categories = $mapper->GetACategorySection($section_id, true, $member->user_id);
            }else{
                $categories = $mapper->GetACategorySection($section_id);
            }
        }else{
            $categories = $mapper->GetACategorySection($section_id);
        }
    
    }else{
        $categories = $mapper->GetACategorySection($section_id);
    }

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "SectionDetail"=> $categories)));
});

//Gets the chapters with progress and exams
$app->get('/api/categories3/section_level/{id}/{level_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    $section_id = (int)$args['id'];
    $level_id = (int)$args['level_id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive(),
        'level_id' => v::numeric()->positive(),
    ]);
 
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $mapper = new CourseMapper($this->db);
    $categories = array();
    if($valid["IsValid"]){
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($valid['SessionValue']);
        if($member){
            if($member->user_level != 4){//for not teacher
                $categories = $mapper->GetACategorySectionChapters($section_id, $level_id, true, $member->user_id, $pagelimit, $pageoffset);
            }else{
                $categories = $mapper->GetACategorySectionChapters($section_id, $level_id, false, null, $pagelimit, $pageoffset );
            }
        }else{
            $categories = $mapper->GetACategorySectionChapters($section_id, $level_id, false, null, $pagelimit, $pageoffset);
        }
    
    }else{
        $categories = $mapper->GetACategorySectionChapters($section_id, $level_id, false, null, $pagelimit, $pageoffset);
    }

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "SectionLevelDetail"=> $categories)));
});

//Gets the chapters Only
$app->get('/api/categories/section_level/{section_id}/{level_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    $section_id = (int)$args['section_id'];
    $level_id = (int)$args['level_id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'section_id' => v::numeric()->positive(),
        'level_id' => v::numeric()->positive(),
    ]);
 
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $mapper = new CourseMapper($this->db);

    $chapters =  $mapper->GetCategorySectionChapterItems($section_id, $level_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", 
        "TotalCount"=>$chapters["TotalCount"],  
        "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,  
        "PageCount"=> $chapters["PageCount"],
        "Chapters"=> $chapters["Results"])));
});

//Creates a new Section for a Exam Category
$app->post('/api/categories/section/new',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'SectionTitle' => v::notEmpty()->length(null,255),
        // 'SectionDescription' => v::notEmpty(),
        'CategoryID'=> v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->CreateNewCategorySection($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Update a Section for a category
$app->put('/api/categories/section/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'SectionID' => v::notEmpty()->numeric()->positive(),
        'SectionTitle' => v::notEmpty()->length(null,255),
        // 'SectionDescription' => v::notEmpty(),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->UpdateCategorySection($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Delete  a Section for a category
$app->delete('/api/categories/section/delete/{id}',function(Request $request, Response $response, $args){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $section_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::notEmpty()->numeric()->positive()
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->DeleteCategorySection($section_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Sub API for the Category Section, This is for level howver
//Creates a new level for a category
$app->post('/api/categories/section/level/new',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Title' => v::notEmpty()->length(null,255),
        // 'Description' => v::notEmpty(),
        'SectionID'=> v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->CreateNewCategorySectionLevel($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Sub API for the Category Section, This is for level howver
//Update a Level for a category Section
$app->put('/api/categories/section/level/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'LevelID' => v::notEmpty()->numeric()->positive(),
        'Title' => v::notEmpty()->length(null,255),
        // 'Description' => v::notEmpty(),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->UpdateCategorySectionLevel($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Sub API for the Category Section, This is for level howver
//Delete a Level for a category Section
$app->delete('/api/categories/section/level/delete/{id}',function(Request $request, Response $response, $args){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $level_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::notEmpty()->numeric()->positive()
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $course_handler->DeleteCategorySectionLevel($level_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

// Get Category Sections
$app->get('/api/categories/{id}/section',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
   $category_id = (int)$args['id'];

   $validation = $this->validator->ValidateByArgs( $args, 
   [
       'id' => v::numeric()->positive()
   ]);

   if ($validation)
   {
       return $response->withStatus(400)
           ->withHeader('Content-Type', 'application/json')
           ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
   }

   $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

   $mapper = new CourseMapper($this->db);
   $sections = $mapper->GetCategorySection($category_id);

   return $response->withStatus(200)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"OK", "CategorySection"=> $sections)));
});

//Get Categories
//Does not need Authorization
$app->get('/api/categories/{id}',function(Request $request, Response $response, $args){
    
    $category_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new CourseMapper($this->db);
    $categories = $mapper->GetCategoryCourseByID($category_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Categories"=> $categories)));
});

//DELETE CATEGORY
$app->delete('/api/categories/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $category_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    
    $course_category = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $course_category->ValidateExistCategory($category_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_category->DeleteCategory($category_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    

});

//New Course Exam
$app->post('/api/course/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CourseTitle' => v::notEmpty()->length(null,255),
        // 'CourseExcerpt' => v::notEmpty()->length(null,500), renove due to client request
        // 'CourseDescription' => v::notEmpty(), remove due to client request
        'CategoryID'=> v::notEmpty()->numeric()->positive(),
        'SectionID' =>  v::notEmpty()->numeric()->positive(),
        'Level'  => v::notEmpty()->numeric()->positive(),    
        'Chapter'  => v::notEmpty()->numeric()->positive(),   
        'QuestionBank'  => v::notEmpty()->numeric()->positive(),  
        'NoOfQuestion'=> v::notEmpty()->numeric()->positive(),
        'PassingGrade'=> v::notEmpty()->numeric()->positive(),
        'Retake'=> v::notEmpty()->numeric()->positive(),
        'TimeLimit'=> v::numeric()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);

    $existenceValidation  = array();
    
    $categoryExistence  = $course_handler->ValidateExistCategory($posted_data["CategoryID"]);
    $categorySectionExistence =  $course_handler->ValidateExistCategorySection($posted_data["SectionID"]);
    $categorySectionLevelExistence =  $course_handler->ValidateExistCategorySectionLevel($posted_data["Level"]);
    $bankExistence  = $question->ValidateExistBank($posted_data["QuestionBank"]);
    $bankHasEnoughQuestions = $question->_ValidateBankIfEnoughQuestions($posted_data["QuestionBank"], $posted_data["NoOfQuestion"]);

    $errorObj = new stdClass();
    if($categoryExistence) $errorObj->CategoryID =  array_values($categoryExistence);
    if($categorySectionExistence) $errorObj->SectionID =  array_values($categorySectionExistence); 
    if($categorySectionLevelExistence) $errorObj->Level =  array_values($categorySectionLevelExistence);
    if($bankExistence) $errorObj->QuestionBank =  array_values($bankExistence);
    if($bankHasEnoughQuestions)  $errorObj->QuestionBank =  array_values($bankHasEnoughQuestions); 
    $existenceValidation = (array)$errorObj;
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$errorObj)));
    }
    
    $course_id = $course_handler->CreateNewCourse($posted_data);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'CourseID'=> $course_id)));
});

//Update Course
$app->put('/api/course/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation_rules = array(
        'CourseID'  => v::numeric()->positive()->notEmpty(),
        'CourseTitle' => v::notEmpty()->length(1,255),
        'CategoryID'=> v::notEmpty()->numeric()->positive(),   
        'SectionID' =>  v::notEmpty()->numeric()->positive(),
        'Level'  => v::notEmpty()->numeric()->positive(),       
        'Chapter'  => v::notEmpty()->numeric()->positive(),   
        'QuestionBank'  => v::notEmpty()->numeric()->positive(),                 
        'NoOfQuestion'=> v::notEmpty()->numeric()->positive(),
        'PassingGrade'=> v::notEmpty()->numeric()->positive(),
        'Retake'=> v::notEmpty()->numeric()->positive(),
        'TimeLimit'=> v::numeric()
    );
    // $validation_rules['CourseID'] = v::numeric()->positive()->notEmpty();
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);

    $existenceValidation  = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);
    $category_existence  = $course_handler->ValidateExistCategory($posted_data["CategoryID"]);
    $categorySectionExistence =  $course_handler->ValidateExistCategorySection($posted_data["SectionID"]);
    $categorySectionLevelExistence =  $course_handler->ValidateExistCategorySectionLevel($posted_data["Level"]);
    $bankExistence  = $question->ValidateExistBank($posted_data["QuestionBank"]);
    $bankHasEnoughQuestions = $question->_ValidateBankIfEnoughQuestions($posted_data["QuestionBank"], $posted_data["NoOfQuestion"]);
    $newObj = new stdClass();

    if($course_existence)   $newObj->Course =  array_values($course_existence);
    if($category_existence) $newObj->CategoryID =  array_values($category_existence);
    if($categorySectionExistence) $newObj->SectionID =  array_values($categorySectionExistence); 
    if($categorySectionLevelExistence) $newObj->Level =  array_values($categorySectionLevelExistence);
    if($bankExistence) $newObj->QuestionBank =  array_values($bankExistence);
    if($bankHasEnoughQuestions)  $newObj->QuestionBank =  array_values($bankHasEnoughQuestions); 
    
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$newObj)));
    }

    $course_handler->UpdateCourse($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get Courses Default
$app->get('/api/courses',function(Request $request, Response $response){

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $sort = $request->getQueryParam('SortBy', $default = 1); //1 = Popular , 1 = Latest
    $category = $request->getQueryParam('Category', $default = 0);
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset,
        "SortBy"=>$sort,
        "Category"=>$category
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0),
        'SortBy' =>  v::numeric()->min(1)->max(2),
        'Category' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new CourseMapper($this->db);
    $courses = $mapper->GetCourses( $category, $sort, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Courses"=> $courses)));
});

//Get Courses of a Teacher
$app->get('/api/teacher/courses',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $mapper = new CourseMapper($this->db);
    $results = $mapper->GetCoursesOfaTeacher($member->user_id,$pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
                                    "TotalCount"=>$results["TotalCount"],
                                    "PageCount"=>$results["PageCount"],
                                    "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1, 
                                    "Courses"=> $results["Results"])));
});

//Get Course Details
//No Authentication Required
$app->get('/api/courses/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    $course_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $existenceValidation  = $course_handler->ValidateExistCourse($course_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $mapper = new CourseMapper($this->db);
    $course = $mapper->GetCourseDetailByID($course_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Course"=>$course)));
});

//Get Course Details
//Indepth Course Details for Teacher only
//Auth Required
$app->get('/api/teacher/courses/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $course_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);

    $existenceValidation  = $course_handler->ValidateExistCourseTeacher($course_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $mapper = new CourseMapper($this->db);
    $course = $mapper->GetCourseDetailByID($course_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Course"=>$course)));
});

//Delete a Course  for Teacher only
//Auth Required
$app->delete('/api/courses/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $exam_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $course_handler->ValidateExistCourse($exam_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

  

    $existenceValidation = $course_handler->ValidateExistCourse($exam_id);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_handler->DeleteExam($exam_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    

});


//New Course Section 
//07/24/2018 TO BE REMOVE DUE TO CLIENT CHANGE REQUEST
//WILL NOW MOVE THE SECTION TO CATEGORIES
//DEPRECATED
$app->post('/api/course/section/new',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'SectionTitle' => v::notEmpty()->length(null,255),
        'SectionDescription' => v::notEmpty(),
        'CourseID'=> v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation = $course_handler->ValidateExistCourse($posted_data["CourseID"]);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_handler->CreateNewCourseSection($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Update Course Section
//TODO Section ID Validation
//07/24/2018 TO BE REMOVE DUE TO CLIENT CHANGE REQUEST
//WILL NOW MOVE THE SECTION TO CATEGORIES
//DEPRECATED
$app->post('/api/course/section/edit',function(Request $request, Response $response){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'SectionID' => v::notEmpty()->numeric()->positive(),
        'SectionTitle' => v::notEmpty()->length(null,255),
        'SectionDescription' => v::notEmpty(),
        'CourseID'=> v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation = $course_handler->ValidateExistCourse($posted_data["CourseID"]);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_handler->UpdateCourseSection($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//TODO Section ID Validation
//07/24/2018 TO BE REMOVE DUE TO CLIENT CHANGE REQUEST
//WILL NOW MOVE THE SECTION TO CATEGORIES
//DEPRECATED
$app->delete('/api/course/section/delete/{id}',function(Request $request, Response $response, $args){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $section_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::notEmpty()->numeric()->positive()
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    // $existenceValidation = $course_handler->ValidateExistCourse($posted_data["CourseID"]);

    // if($existenceValidation)
    // {
    //     return $response->withStatus(400)
    //     ->withHeader('Content-Type', 'application/json')
    //     ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    // }

    $course_handler->DeleteCourseSection($section_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//GET Course Sections
//FOR ALL
$app->get('/api/course/{id}/section',function(Request $request, Response $response, $args){

     $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    $course_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $course_handler->ValidateExistCourse($course_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $mapper = new CourseMapper($this->db);
    $sections = $mapper->GetCourseSectionByCourseID($course_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "CourseSection"=> $sections)));
});

//Get the Worksheets By Section ID
$app->get('/api/course/worksheets/{id}',function(Request $request, Response $response, $args){

   $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
   $section_id = (int)$args['id'];

   $validation = $this->validator->ValidateByArgs( $args, 
   [
       'id' => v::numeric()->positive()
   ]);

   if ($validation)
   {
       return $response->withStatus(400)
           ->withHeader('Content-Type', 'application/json')
           ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
   }


   $mapper = new CourseMapper($this->db);
   $sectionitems = $mapper->GetCourseSectionItemWorksheetsOnly($section_id);

   return $response->withStatus(200)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"OK", "Worksheets"=> $sectionitems)));
});


//Get Section Item By course and Section Item ID Worksheet/Unit Test etc
$app->get('/api/course/{id}/section/{section_item_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
   // if(!$valid['IsValid'])
   // {
   //     return $response->withStatus(400)
   //         ->withHeader('Content-Type', 'application/json')
   //         ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
   // }

   $course_id = (int)$args['id'];
   $section_item_id = (int)$args['section_item_id'];
   $validation = $this->validator->ValidateByArgs( $args, 
   [
       'id' => v::numeric()->positive(),
       'section_item_id' => v::numeric()->positive()
   ]);

   if ($validation)
   {
       return $response->withStatus(400)
           ->withHeader('Content-Type', 'application/json')
           ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
   }

   $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
   $existenceValidation  = $course_handler->ValidateExistCourse($course_id);
   if($existenceValidation)
   {
       return $response->withStatus(400)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
   }

   $mapper = new CourseMapper($this->db);
   $sections = $mapper->GetCourseSectionItemBySectionItemID($course_id,$section_item_id);

   return $response->withStatus(200)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"OK", "SectionItem"=> $sections)));
});

//New Course Section Exam item
//TODO Unit Test Validation for targeted worksheets
$app->post('/api/course/quiz/new',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation_rules = array(
        'SectionID'=> v::notEmpty()->numeric()->positive(),
        'CourseID'=> v::notEmpty()->numeric()->positive(),
        'Title' => v::notEmpty()->length(null,255),
        // 'Description' => v::notEmpty(),
        'AnswerType'=> v::notEmpty()->numeric()->positive()->min(1)->max(3), //1 for multiple choice 2 for essay 3= MIXED (Unit TEST and EXAM)
        'Type'=> v::notEmpty()->numeric()->positive()->min(1)->max(2), //1 for worksheet 2 for unit test,
        'RetakeCount'=> v::numeric()->min(0),
    );

    if($posted_data['Type'] == 1 ){
        $validation_rules['NoOfQuestion'] = v::notEmpty()->numeric()->positive()->min(3);
        $max_passing_rate = isset($posted_data['NoOfQuestion']) ? $posted_data['NoOfQuestion'] :  0;
        $validation_rules['PassingRate'] = v::notEmpty()->numeric()->positive()->max($max_passing_rate);
        $validation_rules['BankID'] = v::notEmpty()->numeric()->positive();
        $validation_rules['TimeLimit'] = v::numeric()->min(0);

    }else if($posted_data['Type'] == 2){
        $validation_rules['NoOfQuestion'] = v::notEmpty()->numeric()->positive()->min(5);
        $max_passing_rate = isset($posted_data['NoOfQuestion']) ? $posted_data['NoOfQuestion'] :  0;
        $validation_rules['PassingRate'] = v::notEmpty()->numeric()->positive()->max($max_passing_rate);
        $validation_rules['TimeLimit'] = v::notEmpty()->numeric()->positive();
        $validation_rules['Worksheets'] = v::notEmpty()->arrayType();              
    }


    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);

    $errorObj = new stdClass();
    $existenceValidation = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);
    if($course_existence)   $errorObj->CourseID =  array_values($course_existence); 

    if($posted_data['Type'] == 1 ){
        $bank_existence  = $question->ValidateExistBank($posted_data["BankID"]);
        $bankHasEnoughQuestions = $question->_ValidateBankIfEnoughQuestions($posted_data["BankID"], $posted_data["NoOfQuestion"]);
        if($bank_existence)  $errorObj->BankID =  array_values($bank_existence); 
        if($bankHasEnoughQuestions)  $errorObj->NoOfQuestion =  array_values($bankHasEnoughQuestions); 
    }
    $existenceValidation = (array)$errorObj;   
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$errorObj)));
    }

    $course_handler->CreateNewCourseSectionItem($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Update
//TODO TIME LIMIT FIXED
$app->put('/api/course/quiz/edit',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $validation_rules = array(
        'SectionItemID'=> v::notEmpty()->numeric()->positive(),
        'SectionID'=> v::notEmpty()->numeric()->positive(),
        'CourseID'=> v::notEmpty()->numeric()->positive(),
        'Title' => v::notEmpty()->length(null,255),
        // 'Description' => v::notEmpty(),
        'AnswerType'=> v::notEmpty()->numeric()->positive()->min(1)->max(3), //1 for multiple choice 2 for essay, 3= MIXED (Unit TEST and EXAM)
        'Type'=> v::notEmpty()->numeric()->positive()->min(1)->max(2), //1 for worksheet 2 for unit test
        'RetakeCount'=> v::numeric()->min(0),
    );


    if($posted_data['Type'] == 1 ){
        $validation_rules['NoOfQuestion'] = v::notEmpty()->numeric()->positive()->min(3);
        $max_passing_rate = isset($posted_data['NoOfQuestion']) ? $posted_data['NoOfQuestion'] :  0;
        $validation_rules['PassingRate'] = v::notEmpty()->numeric()->positive()->max($max_passing_rate);
        $validation_rules['BankID'] = v::notEmpty()->numeric()->positive();
        $validation_rules['TimeLimit'] = v::numeric()->min(0);
    }else if($posted_data['Type'] == 2){
        $validation_rules['NoOfQuestion'] = v::notEmpty()->numeric()->positive()->min(5);
        $max_passing_rate = isset($posted_data['NoOfQuestion']) ? $posted_data['NoOfQuestion'] :  0;
        $validation_rules['PassingRate'] = v::notEmpty()->numeric()->positive()->max($max_passing_rate);
        $validation_rules['TimeLimit'] = v::notEmpty()->numeric()->positive();
        $validation_rules['Worksheets'] = v::notEmpty()->arrayType();              
    }    

    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);

    $existenceValidation = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);
    if($course_existence) array_push($existenceValidation, $course_existence);

    if($posted_data['Type'] == 1 ){
        $bank_existence  = $question->ValidateExistBank($posted_data["BankID"]);
        if($bank_existence) array_push($existenceValidation, $bank_existence);
    }
    
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $course_handler->UpdateCourseSectionItem($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//TODO Existence Validation
$app->delete('/api/course/quiz/delete/{id}',function(Request $request, Response $response, $args){
    
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $section_item_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::notEmpty()->numeric()->positive()
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    // $existenceValidation = $course_handler->ValidateExistCourse($posted_data["CourseID"]);

    // if($existenceValidation)
    // {
    //     return $response->withStatus(400)
    //     ->withHeader('Content-Type', 'application/json')
    //     ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    // }

    $course_handler->DeleteCourseSectionItem($section_item_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});
