<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

require '../src/handlers/session_handler.php';


/*Summary 
API here are as follow

Log In                              /api/login
Verify User By Session Token        /api/session/verify

*/


//Member Log In
$app->post('/api/login',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    if(!isset($posted_data['Remember'])){
        $posted_data['Remember'] = false;
    }
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Username' => v::noWhitespace()->notEmpty()->length(4,45),
        'Password' => v::noWhitespace()->notEmpty()->length(6,255),
        'Remember' => v::boolVal()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($validation));
    }

    $session = new LoginSessionHandler($this->db);
    $existenceValidation = $session->ValidateLogin($posted_data);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($existenceValidation));
    }

    $session->CreateLoginSession();
    $session_key_value = $session->GetCurrentSession();

    $is_still_valid_user = true;
    if($session->member->user_level == 1){ //Free Trial
        $mapper = new MemberMapper($this->db);
        $worksheet_count = $mapper->CountMemberWorksheet($session->member->user_id);
        if($worksheet_count >= 5){
            $is_still_valid_user = false;
        }
    }

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK', 'Session'=> $session_key_value, 'UserLevel'=>$session->member->user_level, 'UserValid'=> $is_still_valid_user)));
});

//verify log in
$app->get('/api/session/verify',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array("Status"=>"OK", "Valid"=> 'Session is still Valid')));
});
