<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/grade_handler.php';


//Add a new Grading Schema Item
//TODO Existence Validation
$app->post('/api/teacher/grades/new',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'CourseID' => v::notEmpty()->numeric()->positive(), //ID
        'GradeMark' => v::notEmpty()->length(1,25),
        'GradeFrom' => v::numeric()->min(0)->max(100),
        'GradeTo' => v::numeric()->min(0)->max(100)
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $handler = new GradeSchemaHandler($this->db);
    $handler->InsertToGradeSchema($posted_data);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

$app->put('/api/teacher/grades/edit',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'CourseID' => v::notEmpty()->numeric()->positive(), //ID
        'BulkGradeSchemas' => v::notEmpty()->arrayVal(),
    );


    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $inner_validations = array();
    foreach($posted_data["BulkGradeSchemas"] as $schema){
        $inner_rules = array(
            'CourseID' => v::notEmpty()->numeric()->positive(), //ID
            'GradeSchemaID' => v::notEmpty()->numeric()->positive(),
            'GradeMark' => v::notEmpty()->length(1,25),
            'GradeFrom' => v::numeric()->min(0)->max(100),
            'GradeTo' => v::numeric()->min(0)->max(100)
        );    
        $in_validation = $this->validator->ValidateByArgs($schema, $inner_rules);
        if($in_validation){
            array_push($inner_validations, $in_validation);
        }
    }  
    if(count($inner_validations) > 0){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $inner_validations)));
    }
    
    $handler = new GradeSchemaHandler($this->db);
     
    foreach($posted_data["BulkGradeSchemas"] as $schema){
        $handler->UpdateGradeSchema($schema);
    }

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//Delete a grade schema item 
$app->delete('/api/teacher/grades/delete/{grade_schema_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $grade_schema_id = (int)$args['grade_schema_id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'grade_schema_id' => v::numeric()->positive()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $handler = new GradeSchemaHandler($this->db);
    $handler->DeleteGradeSchema($grade_schema_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    

});

//Gets the Grade Schema
$app->get('/api/teacher/grades/{course_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $course_id = (int)$args['course_id'];
    $posted_data = array(
        "Course"=> $course_id
    );
    $validation = $this->validator->ValidateByArgs($posted_data,
    [
        'Course' =>  v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new GradeSchemaMapper($this->db);
    $schema = $mapper->GetGradeSchemaByCourseID($course_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Count"=>count($schema),"GradeSchema"=> $schema)));
});

//Gets the List of Items for teacher
$app->get('/api/teacher/list/grades',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $category_section_id = $request->getQueryParam('CategorySectionID', $default = 0);
    $category_section_level_id = $request->getQueryParam('CategorySectionLevelID', $default = 0);
    $category_section_chapter_id = $request->getQueryParam('CategorySectionLevelChapterID', $default = 0);

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new GradeSchemaMapper($this->db);
    $result = array();
    if($category_section_id > 0 && $category_section_level_id > 0 && $category_section_chapter_id > 0){
        $result = $mapper->GetSectionItemsForGradeByCategorySection($member->user_id, $category_section_id, $category_section_level_id, $category_section_chapter_id,$pagelimit, $pageoffset);
    }else{
        $result = $mapper->GetSectionItemsForGrade($member->user_id, $pagelimit, $pageoffset);
    }

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", 
                                "TotalCount"=> $result["TotalCount"],
                                "PageCount"=>$result["PageCount"],
                                "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1, 
                                "List"=> $result["Results"], "TEST" => $category_section_chapter_id)));
});

//
//Gets the Grades of students for a  course
$app->get('/api/teacher/result/{section_item_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $section_item_id = (int)$args['section_item_id'];
    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    
    $posted_data = array(
        "SectionItemID"=> $section_item_id,
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'SectionItemID' =>  v::notEmpty()->numeric()->positive(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new GradeSchemaMapper($this->db);
    $schema = $mapper->GetGradesForCourse($section_item_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "TotalCount"=>$schema["TotalCount"],"Grades"=> $schema["Users"])));
});

//Get the recent submission for the teacher
$app->get('/api/teacher/recent/grades',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    

    $mapper = new GradeSchemaMapper($this->db);
    $result = $mapper->GetRecentSubmissions($member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","TotalCount"=> count($result),  "Grades"=> $result)));    
});

$app->post('/api/teacher/recent/grades/viewed',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'SectionItemID' => v::notEmpty()->numeric()->positive(), //ID
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $handler = new GradeSchemaHandler($this->db);
    $handler->InsertViewedSectionItem($posted_data["SectionItemID"], $member->user_id);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//Delete a user attempt in a Course
$app->delete('/api/teacher/result/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $course_progress_id = (int)$args['id'];
    $posted_data = array (
        'GradeID' => $course_progress_id
    );
    // $posted_data = $request->getBody();
    // $posted_data = json_decode($posted_data, true);
    $rules = array(
        'GradeID' => v::notEmpty()->numeric()->positive(), //ID
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $handler = new GradeSchemaHandler($this->db);
    $handler->DeleteAttempt($course_progress_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//Get Student Grade for a course
$app->get('/api/teacher/student/result/{course_id}/{student_id}/{progress_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $course_id = (int)$args['course_id'];
    $student_id = (int)$args['student_id'];
    $progress_id = (int)$args['progress_id'];
    $posted_data = array(
        "Course"=> $course_id,
        "Student"=> $student_id,
        "Progress"=> $progress_id
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Course' =>  v::notEmpty()->numeric()->positive(),
        'Student' =>  v::notEmpty()->numeric()->positive(),
        'Progress' =>  v::notEmpty()->numeric()->positive(),
    ]); 

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($student_id);

    if(!$member){

        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> array("Member"=>"Member Does not Exists!"))));        
    }


    $mapper = new GradeSchemaMapper($this->db);
    // $results = $mapper->GetViewMainResultStudent($course_id, $student_id, $progress_id);
    if($progress_id > 0){
        $results = $mapper->GetViewMainResultStudent($course_id, $student_id, $progress_id);
    }else{
        $results = $mapper->GetIndepthResultStudent($course_id, $student_id);
    }
    $secret_member = array(
        'fname' => $member->fname,
        'lname' => $member->lname
    );


    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Student"=> $secret_member,"Results"=> $results)));
});

//Supplementary to api above
//Get the questions of the student and his answers
//separated for optimizaton
$app->get('/api/teacher/student/questions/result/{progress_id}/{student_id}',function(Request $request, Response $response,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $progress_id = (int)$args['progress_id'];
    $student_id = (int)$args['student_id'];

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end    

    $posted_data = array(
        "Progress"=> $progress_id,
        "Student"=> $student_id,
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset        
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Progress' =>  v::notEmpty()->numeric()->positive(),
        'Student' =>  v::notEmpty()->numeric()->positive(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)        
    ]);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new GradeSchemaMapper($this->db);
    $results = $mapper->GetIndepthQuestionsResultStudent($progress_id, $student_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Results"=> $results)));
});

//This is the api responsible for updating the pending of a student
$app->post('/api/teacher/student/approve',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }


    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'ProgressID' => v::notEmpty()->numeric()->positive(), //ID
        'StudentID' => v::notEmpty()->numeric()->positive(), //ID
        'TeacherMark' => v::numeric()->min(0),
        'PassFailed'=> v::numeric()->min(0)->max(1),
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $handler = new GradeSchemaHandler($this->db);
    $handler->UpdatePendingExam($posted_data);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});