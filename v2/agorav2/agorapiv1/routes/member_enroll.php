<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
use Slim\Http\UploadedFile;

require '../src/handlers/member_enroll_handler.php';
require '../src/utils/mail_sender.php';

/*Summary 
API here are as follow

Member Enroll                       /api/member/enroll
Enroll Verification                 /api/member/verify_activation
Resend Expired Verification         /api/member/resend_activation
Complete Profile                    /api/member/complete_profile
Member Edit                         /api/member/edit
Facebook Register/Login             /api/member/fb/register
Google Register/Login               /api/member/google/register
Member Profile Photo Change         /api/member/profile_photo
Member Profile Backdrop Change      /api/member/profile_backdrop
Member Forgot Password              /api/member/forgot_pass
Member Change Password              /api/member/change_pass
Member Reset Password               /api/member/reset_pass
Member Account Deactivation         /api/member/cancellation
Member(Student) Profile Stats       /api/member/stats
Member(Student) Stats for Grades    /api/member/stats/grades
Member(Student) Push Notification   /api/member/stats/recent
Member (Teacher) Profile Stats      /api/teacher/stats
Techer Sub API Course Stats         /api/teacher/stats/courses
Teacher Sub API Student Stats       /api/teacher/stats/students
*/


//Member Enroll
$app->post('/api/member/enroll',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs( $posted_data, 
    [
        'EmailAddress' => v::email()->noWhitespace()->notEmpty(),
        'Username' => v::noWhitespace()->notEmpty()->length(4,10),
        'Password' => v::noWhitespace()->notEmpty()->length(8,255),
        'ConfirmPassword' => v::equals($request->getParam('Password'))->noWhitespace()->notEmpty()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_enroll = new MemberEnrollHandler($this->db);
    $existenceValidation  = array();
    $emailExistence =  $member_enroll->ValidateExistEmail($posted_data["EmailAddress"]);
    $usernameExistence = $member_enroll->ValidateExistUserName($posted_data["Username"]);

    if($emailExistence) array_push($existenceValidation, $emailExistence);
    if($usernameExistence) array_push($existenceValidation, $usernameExistence);
    $newObj = new stdClass();
 
    if($emailExistence){
        $newObj->EmailAddress =  array_values($emailExistence);
    }

    if($usernameExistence){
        $newObj->Username = array_values($usernameExistence);
    }
    if($existenceValidation)
    {
  
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'ExistenceError'=>$newObj)));
    }


    $enrolee_id = $member_enroll->MemberEnroll($posted_data);
    if($enrolee_id >= 1)
    {
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberCredentialsByUserEmail($posted_data["EmailAddress"]);
        $activation_key = md5($member->user_email.$member->registration_date);

        //save the activation key to database
        $member_enroll->SaveActivationKey($enrolee_id, $activation_key);
        //intial selected plan
        $selected_plan = '';
        if(isset($posted_data['SelectedPlan'])){
            if($posted_data['SelectedPlan'] == 'free' 
                || $posted_data['SelectedPlan']  == 'monthly' || $posted_data['SelectedPlan']  == 'yearly')
            $selected_plan = "&SelectedPlan=". $posted_data['SelectedPlan'];
        }

        //send email
        $subject = "Please complete your registration";
        $msg = "
        <strong>Dear Student,</strong>
        <br>
        <p>Thank you for your registration with us.</p>
        
        <p>Before you proceed, please click the link below to complete setting up your profile.</p>
        <br>
        <a href='{$this->mail["site"]}complete_register?ActivationKey={$activation_key}{$selected_plan}'>
        {$this->mail["site"]}complete_register?ActivationKey={$activation_key}{$selected_plan}
        </a>
        <br>
        <br>
        Regards, <br>
        The Agora School <br>
        <br>
        <img src='http://dev.agora-school.com/static/img/main-logo-transparent.733467a.png' width='266' height='101'/>
        ";

        $mailer = new MailSender($this->mail);
        $mailer->SendMail( $posted_data["EmailAddress"], $subject, $msg);

        $this->logger->addInfo('Sended Email : \n'. $msg);

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK')));
    }

    return $response->withStatus(400)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'NG', 'SystemError'=> 'An Unxpected Error Happened')));

});

//Verify Activation Key
$app->get('/api/member/verify_activation',function(Request $request, Response $response){

    $params = $request->getQueryParams();
    $activation_key = $params["ActivationKey"];
    $validation = $this->validator->ValidateByArgs( $params, 
    [
        'ActivationKey' => v::notEmpty()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    
    
    $mapper = new MemberMapper($this->db);
    $member = $mapper->GetMemberIDByActivationKey($activation_key);
    if($member)
    {
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "MemberID"=> $member->user_id, "MemberEmail"=> $member->user_email)));
    }
    else
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> array("ActivationKey"=> "Activation Key is Invalid"))));       
    }

});

//Resend Activation Key
$app->post('/api/member/resend_activation',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs( $posted_data, 
    [
        'ActivationKey' => v::notEmpty()
    ]);

    if($validation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }


    $activation_key = $posted_data['ActivationKey'];
    $mapper = new MemberMapper($this->db);
    $member = $mapper->GetMemberIDByActivationKey($activation_key, true);
    if($member)
    {
        
        $member_enroll = new MemberEnrollHandler($this->db);
        $activation_key = md5($member->user_email.$member->registration_date);
        //save the activation key to database
        $member_enroll->SaveActivationKey($member->user_id, $activation_key);
        //send email
        $subject = "Please complete your registration";
        $msg = "
        <strong>Dear Student,</strong>
        <br>
        <p>Thank you for your registration with us.</p>
        
        <p>Before you proceed, please click the link below to complete setting up your profile.</p>
        <br>
        <a href='{$this->mail["site"]}complete_register?ActivationKey={$activation_key}'>
        {$this->mail["site"]}complete_register?ActivationKey={$activation_key}
        </a>
        <br>
        <br>
        Regards, <br>
        The Agora School <br>
        <br>
        <img src='http://dev.agora-school.com/static/img/main-logo-transparent.733467a.png' width='266' height='101'/>
        ";

        $mailer = new MailSender($this->mail);
        $mailer->SendMail($member->user_email, $subject, $msg);

        $this->logger->addInfo('Sended Email : \n'. $msg);        

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK')));
    }
    else
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> array("ActivationKey"=> "Activation Key is Invalid"))));       
    }    


});


//Member Enroll Completion
$app->post('/api/member/complete_profile',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'UserID'=> v::notEmpty()->positive(),
            'FirstName'=> v::notEmpty()->length(1, 255),
            'LastName'=> v::notEmpty()->length(1, 255),
            'Birthday'=> v::notEmpty()->date()->age(6,60),
            'Age'=> v::numeric()->notEmpty()->min(6)->max(60),
            'CountryID'=> v::notEmpty(),
            'EducationLevel'=> v::numeric()->notEmpty(),
            'Gender'=> v::numeric()->notEmpty()->min(1)->max(2),
        ]
    );

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_enroll = new MemberEnrollHandler($this->db);

    $existenceValidation  = $member_enroll->ValidateAlreadyExistDetails($posted_data['UserID']);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }


    $success = $member_enroll->MemberEnrollCompletion($posted_data);

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($posted_data['UserID']);
    
    $session_handler = new LoginSessionHandler($this->db);

    $session_handler->CreateLoginSessionFirstTime($member);
    
    $first_session = $session_handler->GetCurrentSession();
    
    if ($success)
    {
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK','Session'=> $first_session)));
    }

    return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'SystemError'=> 'An unexpected error happened')));
});

//Update Member Details by ID
$app->put('/api/member/edit',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'EmailAddress' => v::email()->noWhitespace()->notEmpty(),
            'Username' => v::noWhitespace()->notEmpty()->length(4,10),
            'FirstName'=> v::notEmpty()->length(1, 255),
            'LastName'=> v::notEmpty()->length(1, 255),
            'Birthday'=> v::notEmpty(),
            'Age'=> v::numeric(),
            'CountryID'=> v::notEmpty(),
            'EducationLevel'=> v::numeric()->notEmpty(),
            // 'School' => v::notEmpty(),
            'Gender' => v::notEmpty()->numeric()->min(1)->max(2),
        ]
    );

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $posted_data["UserID"] = $sender->user_id;
    $member_enroll = new MemberEnrollHandler($this->db);


    $existenceValidation  = array();
    $emailExistence =  $member_enroll->ValidateExistEmail($posted_data["EmailAddress"],$sender->user_id);
    $usernameExistence = $member_enroll->ValidateExistUserName($posted_data["Username"], $sender->user_id);

    if($emailExistence) array_push($existenceValidation, $emailExistence);
    if($usernameExistence) array_push($existenceValidation, $usernameExistence);
    $newObj = new stdClass();
 
    if($emailExistence){
        $newObj->EmailAddress =  array_values($emailExistence);
    }

    if($usernameExistence){
        $newObj->Username = array_values($usernameExistence);
    }
    if($existenceValidation)
    {
  
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'ExistenceError'=>$newObj)));
    }


    $success = $member_enroll->MemberDetailsUpdate($posted_data);

    if ($success)
    {
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK')));
    }

    return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'SystemError'=> 'An unexpected error happened')));
});

//Facebook Register/Login
$app->post('/api/member/fb/register',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'FBUserID' => v::notEmpty(),
            'EmailAddress' => v::notEmpty()->email()->noWhitespace(),
            'FirstName'=> v::notEmpty()->length(1, 255),
            'LastName'=> v::notEmpty()->length(1, 255),
            // 'Birthday'=> v::notEmpty(),
            // 'Gender'=> v::notEmpty()->numeric()->min(1)->max(2),
        ]
    );
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_handler = new MemberEnrollHandler($this->db);
    $session_handler = new LoginSessionHandler($this->db);

    $member = $member_handler->ValidateExistFacebookID($posted_data["FBUserID"]);
    $isRegistered = false;
    if($member == null){
        $member = $member_handler->GetMemberByEmailAddress($posted_data["EmailAddress"]);
    }

    if($member){
        //already register, initiate log in, generate token
        $session_handler->CreateLoginSessionFirstTime($member);
        
        $first_session = $session_handler->GetCurrentSession();

        $is_still_valid_user = true;
        if($member->user_level == 1){
            $is_still_valid_user = false;
        }
        else if($member->user_level == 1){ //Free Trial
            $now = time(); // or your date as well
            $reg_date = strtotime($member->registration_date);
            $datediff = $now - $reg_date;
            if($datediff > 5){
                $is_still_valid_user = false;
            }
    
        }
        $isRegistered = true;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK','Session'=> $first_session,
            'UserLevel'=>$member->user_level, 'UserValid'=> $is_still_valid_user, 'AlreadyRegistered'=> $isRegistered )));

    }
    //Do a register
    $member_id = $member_handler->SocialFBRegister($posted_data);

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($member_id);
    $session_handler->CreateLoginSessionFirstTime($member);
    $first_session = $session_handler->GetCurrentSession();
    
    $is_still_valid_user = true;
    if($member->user_level == 1){
        $is_still_valid_user = false;
    }
    else if($member->user_level == 1){ //Free Trial
        $now = time(); // or your date as well
        $reg_date = strtotime($member->registration_date);
        $datediff = $now - $reg_date;
        if($datediff > 5){
            $is_still_valid_user = false;
        }

    }

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK','Session'=> $first_session,
            'UserLevel'=>$member->user_level, 'UserValid'=> $is_still_valid_user, 'AlreadyRegistered'=> $isRegistered )));
  

});

//Google Register/Login
$app->post('/api/member/google/register',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'GmailUserID' => v::notEmpty(),
            'EmailAddress' => v::notEmpty()->email()->noWhitespace(),
            'FirstName'=> v::notEmpty()->length(1, 255),
            'LastName'=> v::notEmpty()->length(1, 255),
            // 'Birthday'=> v::notEmpty(),
            // 'Gender'=> v::notEmpty()->numeric()->min(1)->max(2), Unable to get will try to find workarround
        ]
    );
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    

    $member_handler = new MemberEnrollHandler($this->db);
    $session_handler = new LoginSessionHandler($this->db);

    $member = $member_handler->ValidateExistGoogleID($posted_data["GmailUserID"]);

    if($member == null){
        $member = $member_handler->GetMemberByEmailAddress($posted_data["EmailAddress"]);
    }
    
    if($member){
        //already register, initiate log in, generate token
        $session_handler->CreateLoginSessionFirstTime($member);
        
        $first_session = $session_handler->GetCurrentSession();

        $is_still_valid_user = true;
        if($member->user_level == 1){
            $is_still_valid_user = false;
        }
        else if($member->user_level == 1){ //Free Trial
            $now = time(); // or your date as well
            $reg_date = strtotime($member->registration_date);
            $datediff = $now - $reg_date;
            if($datediff > 5){
                $is_still_valid_user = false;
            }
    
        }

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK','Session'=> $first_session,
            'UserLevel'=>$member->user_level, 'UserValid'=> $is_still_valid_user)));

    }
    //Do a register
    $member_id = $member_handler->SocialGoogleRegister($posted_data);

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($member_id);
    $session_handler->CreateLoginSessionFirstTime($member);
    $first_session = $session_handler->GetCurrentSession();
    
    $is_still_valid_user = true;
    if($member->user_level == 1){
        $is_still_valid_user = false;
    }
    else if($member->user_level == 1){ //Free Trial
        $now = time(); // or your date as well
        $reg_date = strtotime($member->registration_date);
        $datediff = $now - $reg_date;
        if($datediff > 5){
            $is_still_valid_user = false;
        }

    }

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK','Session'=> $first_session,
            'UserLevel'=>$member->user_level, 'UserValid'=> $is_still_valid_user)));

    // return $response->withStatus(200)
    // ->withHeader('Content-Type', 'application/json')
    // ->write(json_encode(array('Status'=> 'OK')));    

});

//Profie Photo Change
$app->post('/api/member/profile_photo',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'FileName'=> v::notEmpty()->length(5, 255),
        ]
    );
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $member_handler = new MemberEnrollHandler($this->db);
    $member_handler->UpdateProfile($member->user_id, $posted_data["FileName"]);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));    

});

//Profie Back Drop
$app->post('/api/member/profile_backdrop',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'BackDropID'=>  v::notEmpty()->positive()
        ]
    );
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $member_handler = new MemberEnrollHandler($this->db);
    $member_handler->UpdateProfileBackdrop($member->user_id, $posted_data["BackDropID"]);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));    

});

//Forgot Password
$app->post('/api/member/forgot_pass',function(Request $request, Response $response){

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
        [
            'Email'=> v::notEmpty()->length(5, 255),
        ]
    );

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberCredentialsByUserEmail($posted_data['Email']);
    if(!$member){
        $notExist = array("Email" => "User does not exists");
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $notExist)));
    }

    $member_detail = $member_mapper->GetMemberByUserID($member->user_id);

    $generate_temp_pass = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );
    $member_handler = new MemberEnrollHandler($this->db);

    $ticket_hash = $member_handler->UpdateMemberPassword($member->user_id, $generate_temp_pass, $posted_data['Email']);

    $subject = "Reset Your Password";
    $msg = "
    <strong>Dear {$member_detail->fname}  {$member_detail->lname}</strong>
    <br>
    <p>You have received this email because you have indicated you have forgotten your password.</p>
    <p>To proceed creating a new password, please use the confirmation code below and click on the link to continue. <br> </p>

    <br>
     Reset Code : {$generate_temp_pass}
    <br>
    <a href='{$this->mail["site"]}resetpassword?reset_ticket={$ticket_hash}'>
    {$this->mail["site"]}resetpassword?reset_ticket={$ticket_hash}</a> 
    <br>
    Regards, <br>
    The Agora School <br>
    <br>
    <img src='{$this->mail["site"]}/static/img/main-logo-transparent.733467a.png' width='266' height='101'/>
    ";
    $this->logger->addInfo('Forgot Password : \n'. $msg);
    $mailer = new MailSender($this->mail);
    $mailer->SendMail( $posted_data["Email"], $subject, $msg);    

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));    

});

//ChangePassword
$app->post('/api/member/change_pass',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'OldPassword'=> v::notEmpty()->length(5, 255),
        'NewPassword'=> v::notEmpty()->length(5, 255),

    );
    $rules['ConfirmPassword'] = v::identical($posted_data['NewPassword']);
    $validation = $this->validator->ValidateByArgs($posted_data, $rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    if(!$member){
        $notExist = array("Member" => "User does not exists");
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $notExist)));
    }

    if($member->user_pass != md5($posted_data['OldPassword'])){
        $notExist = array("Member" => "Your current password is incorrect");
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $notExist)));        
    }
 
    $member_handler = new MemberEnrollHandler($this->db);
    $member_handler->UpdateMemberPassword($member->user_id, $posted_data['NewPassword']);
    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));    

});

//Reset Password
$app->post('/api/member/reset_pass',function(Request $request, Response $response){
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'ResetTicket'=>v::notEmpty()->length(5, 255),
        'ConfirmationCode'=> v::notEmpty()->length(5, 255),
        'NewPassword'=> v::notEmpty()->length(5, 255),
    );
    $rules['ConfirmPassword'] = v::identical($posted_data['NewPassword']);
    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    // ValidateResetTicketAndCode
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    
    $member_handler = new MemberEnrollHandler($this->db);
    $detail = $member_handler->ValidateResetTicketAndCode($posted_data['ResetTicket'], $posted_data['ConfirmationCode']);

    if(!$detail){
        $notExist = array("Member" => "User does not exists");
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $notExist)));
    }

    if($detail->user_pass != md5($posted_data['ConfirmationCode'])){
        $notExist = array("Member" => "Your current password is incorrect");
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $notExist)));        
    }
 
    $member_handler = new MemberEnrollHandler($this->db);
    $member_handler->UpdateMemberPassword($detail->user_id, $posted_data['NewPassword']);
    $member_handler->SetResetTicketToInactive($posted_data['ResetTicket']);
    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));    

});

//Deactivate Account. Also Deactivate the existing plan
$app->post('/api/member/cancellation',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
    [
        'Reason'=> v::notEmpty(),
    ]
    );

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }


    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);   
    
    $handler = new MemberEnrollHandler($this->db);
    
    $result = $handler->DeactivateAccount($this->gateway, $member, $posted_data);   

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK'))); 
});

//Member Stats
$app->get('/api/member/stats',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $history = $exam_mapper->GetMemberStatsForProfile($member->user_id);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 'Stats'=> $history)));
});

$app->get('/api/member/stats/grades',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $cate_id = $request->getQueryParam('CategoryId', $default = 0);
    $cate_sec_id = $request->getQueryParam('CategorySectionId', $default = 0);
    $cate_sec_level_id = $request->getQueryParam('CategorySectionLevelId', $default = 0);
    $chap_no = $request->getQueryParam('ChapterNo', $default = 0);
    $posted_data = array(
        "CategoryId"=> $cate_id,
        "CategorySectionId" => $cate_sec_id,
        "CategorySectionLevelId" => $cate_sec_level_id,
        "ChapterNo" => $chap_no
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CategoryId' =>  v::numeric(),
        'CategorySectionId' => v::numeric(),
        'CategorySectionLevelId' => v::numeric(),
        'ChapterNo'=> v::numeric()
    ]);  

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }


    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $history = $exam_mapper->GetMemberSubStatsGrades($member->user_id, $cate_id, $cate_sec_id, $cate_sec_level_id, $chap_no);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 'GradeStats'=> $history)));
});

//This is used for menu red circles
//Gets true of false for menu if they have red circles
$app->get('/api/member/stats/recent',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $result = array();
    if($member->user_level == 4){
        //teacher
        $result = $exam_mapper->GetTeacherMenuStats($member->user_id);
    }else{
        //student
        $result = $exam_mapper->GetMemberMenuStats($member->user_id);
    }

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 'Notifs' => $result )));
});


//Teacher Stats
$app->get('/api/teacher/stats',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $history = $exam_mapper->GetTeacherStatsForProfile($member->user_id);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 'Stats'=> $history)));
});

//Get the Stats of courses of a teacher
$app->get('/api/teacher/stats/courses',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    
    $pagelimit = $request->getQueryParam('PageLimit', $default = 10);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    




    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $history = $exam_mapper->GetTeacherSubStatsCourse($member->user_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 
                "TotalCount"=> $history["TotalCount"],
                "PageCount"=>$history["PageCount"],
                "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,
                'CourseStats'=> $history["Results"])));
});

//Get the Stats of students for a teacher
$app->get('/api/teacher/stats/students',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 10);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $exam_mapper = new ExamMapper($this->db);
    $history = $exam_mapper->GetTeacherSubStatsStudents($member->user_id, $pagelimit, $pageoffset  );

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK', 
                                "TotalCount"=> $history["TotalCount"],
                                "PageCount"=>$history["PageCount"],
                                "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,
                                "StudentStats"=>  $history["Results"]
    )));
});