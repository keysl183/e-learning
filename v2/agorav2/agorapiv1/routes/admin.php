<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

/*Summary 
To access the api endpoints the user must be 1. Teacher and 2. has an admin privileges
Both is tickable in the database. user_level = 4 ANd admin_privilege = 1

API here are as follow

New Teacher                 /api/admin/teacher/new
Change Teacher Active       /api/admin/teacher/delete/{id}
Grant Admin to Teacher      /api/admin/teacher/grant
Get Teachers                /api/admin/teacher
*/

//Creates new Teacher Account
$app->post('/api/admin/teacher/new',function(Request $request, Response $response){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    
    //Check if the teacher account has an admin privelege 
    if($member->user_level != $this->USERLEVEL->TEACHER 
        || !$member->admin_privelege){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));        
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $rules = array(
        'EmailAddress' => v::email()->noWhitespace()->notEmpty(),
        'Username' => v::noWhitespace()->notEmpty()->length(4,10)->regex('/^[a-zA-Z0-9_]*$/'),
        'Password' => v::noWhitespace()->notEmpty()->length(8,255),
        'ConfirmPassword' => v::equals($request->getParam('Password'))->noWhitespace()->notEmpty(),
        'FirstName'=> v::notEmpty()->length(1, 255),
        'LastName'=> v::notEmpty()->length(1, 255),
        'Birthday'=> v::notEmpty(),
        'Age'=> v::numeric(),
        'CountryID'=> v::notEmpty(),
        'EducationLevel'=> v::numeric()->notEmpty(),
        'Gender'=> v::numeric()->notEmpty()->min(1)->max(2),
    );
    if(isset($posted_data["AdminPrivilege"])){
        $rules["AdminPrivilege"] = v::numeric()->min(0)->max(1);
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_enroll = new MemberEnrollHandler($this->db);
    $existenceValidation  = array();
    $emailExistence =  $member_enroll->ValidateExistEmail($posted_data["EmailAddress"]);
    $usernameExistence = $member_enroll->ValidateExistUserName($posted_data["Username"]);

    if($emailExistence) array_push($existenceValidation, $emailExistence);
    if($usernameExistence) array_push($existenceValidation, $usernameExistence);
    $newObj = new stdClass();
 
    if($emailExistence){
        $newObj->EmailAddress =  array_values($emailExistence);
    }

    if($usernameExistence){
        $newObj->Username = array_values($usernameExistence);
    }
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$newObj)));
    }    

    ///Save to Database
    $enrolee_id = $member_enroll->MemberEnroll($posted_data);
    $posted_data["UserID"] = $enrolee_id;
    $success = $member_enroll->MemberEnrollCompletion($posted_data);
    if($success){
        //Set the Level
        $user_level = 4;
        $hasAdminPriv = 0;
        if(isset($posted_data["AdminPrivilege"])){
            $hasAdminPriv = $posted_data["AdminPrivilege"];
        }
        $member_enroll->SetAccountUserLevel($enrolee_id, $user_level, $hasAdminPriv);
    }
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Change Teacher Status
$app->put('/api/admin/teacher/change/{id}',function(Request $request, Response $response , $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    
    //Check if the teacher account has an admin privelege 
    if($member->user_level != $this->USERLEVEL->TEACHER 
        || !$member->admin_privelege){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));        
    }

    $member_id = (int)$args['id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);
    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $target_member = $member_mapper->GetMemberByUserID($member_id);
    if(!$target_member){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Member'=> 'Member Does not Exists')));    
    }

    $member_enroll = new MemberEnrollHandler($this->db);
    
    $hasAdminPrivelege = $target_member->admin_privelege ? 1 : 0;
    $isActiveOrNot = ($target_member->active == 1) ? 0 : 1; //toggle active 
    //revert a teacher account into a no subscription (user_level = 0) and if he has a admin privilege set it also to 0
    // $success = $member_enroll->SetAccountUserLevel($member_id, 0, 0);
    //Only deactivate the teacher acciybt
    $success = $member_enroll->SetAccountUserLevel($member_id, 4, $hasAdminPrivelege, $isActiveOrNot);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'Active'=>$isActiveOrNot)));
});

//Grant Teacher Admin Rights 
$app->post('/api/admin/teacher/grant',function(Request $request, Response $response , $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);

    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    
    //Check if the teacher account has an admin privelege 
    if($member->user_level != $this->USERLEVEL->TEACHER 
        || !$member->admin_privelege){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));        
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs( $posted_data, 
    [
        'UserID' => v::numeric()->positive()
    ]);
    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    
    $member_id = $posted_data["UserID"];

    $target_member = $member_mapper->GetMemberByUserID($member_id);
    if(!$target_member){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Member'=> 'Member Does not Exists')));    
    }

    $member_enroll = new MemberEnrollHandler($this->db);
    //revert a teacher account into a no subscription (user_level = 0) and if he has a admin privilege set it also to 0
    $success = $member_enroll->SetAccountUserLevel($member_id,4, 1);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get all teacher accounts
$app->get('/api/admin/teacher',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);    
    //Check if the teacher account has an admin privelege 
    if($member->user_level != $this->USERLEVEL->TEACHER 
        || !$member->admin_privelege){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));        
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }   
    //Get only the Teachers
    $result = $member_mapper->GetMembers(true, false, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK",
                                    "TotalCount"=> $result["TotalCount"],
                                    "PageCount"=>$result["PageCount"],
                                    "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,
                                    "Teachers"=>  $result["Results"]
                                    )));
});
