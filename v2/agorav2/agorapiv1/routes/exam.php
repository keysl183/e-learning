<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/exam_handler.php';

/*Summary 
API here are as follow

Enroll Exam                             /api/exam/enroll
Get Enrolled Categories                 /api/exam/courses
Get Courses Under Enrolled Category     /api/exam/sub_courses/{section_id}/{level_id}/{chapter_no}
Get Question for a Course (Generate)    /api/exam/course/{course_id}/{section_id}
Submit Completed Exam (UT, Ex, WS)      /api/exam/section/complete
Get Category Section Progress           /api/student/course/{id}/section
Get Exam Items (UT, EX, WS)             /api/exam/items
Get Category Section Items              /api/exam/section_item
Get Category Section Levels             /api/exam/section_levels
*/

//Enroll
$app->post('/api/exam/enroll',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CourseID' => v::notEmpty()->numeric()->positive(),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $exam = new ExamHandler($this->db,$valid['SessionValue']);
    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);
    $additional_validation = $exam->ValidateAlreadyEnrolled($posted_data["CourseID"]);

    if($course_existence) array_push($existenceValidation, $course_existence);
    if($additional_validation) array_push($existenceValidation, $additional_validation);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $exam->EnrollNewCourse($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Get Member Enrolled Categories . Courses was move to the sub_courses api
//Change due to  client request
$app->get('/api/exam/courses',function(Request $request, Response $response ){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $category_id =  $request->getQueryParam('Category', $default = 0);
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset,
        "Category"=> $category_id
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Category' =>  v::numeric()->positive()->notEmpty(),
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $mapper = new ExamMapper($this->db);
    $courses = $mapper->GetEnrolledCourses($member->user_id, $category_id, $pagelimit, $pageoffset);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Category"=> $courses)));    
});

//SUB API for exam_courses
$app->get('/api/exam/sub_courses/{section_id}/{level_id}/{chapter_no}',function(Request $request, Response $response ,  $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $section_id = (int)$args['section_id'];
    $level_id = (int)$args['level_id'];
    $chapter_no = (int)$args['chapter_no'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'section_id' => v::numeric()->positive(),
        'level_id' => v::numeric()->positive(),
        'chapter_no' => v::numeric()->positive()
    ]);
 
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }



    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }


    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $mapper = new ExamMapper($this->db);
    $courses = $mapper->GetSubEnrolledCourses($member->user_id, $section_id, $level_id, $chapter_no ,$pagelimit, $pageoffset);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", 
        "PageCount"=>$courses["PageCount"],
        "TotalCount"=>$courses["TotalCount"],  
        "PageLimit"=> $pagelimit, "PageOffset"=>$pageoffset+1,  
        "Courses"=> $courses["Results"])));    
});

//TODO NEW API //api/exam/course_sections
//Reason, to splitthe api/exam/courses

//Get Questions from the course_exam and section id
//IF there is no questions generated, generate one
$app->get('/api/exam/course/{course_id}/{section_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $course_id = (int)$args['course_id'];
    $quizz_id = (int)$args['section_id']; //Quizzrefers to worksheet and unit test this is acually section_item_id
    $posted_data = array(
        "CourseID"=> $course_id,
        "SectionID"=> $quizz_id
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CourseID' =>  v::notEmpty()->numeric()->positive(),
        'SectionID' =>  v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    //TODO SECTION EXISTENCE VALIDATION
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $isRetake = $request->getQueryParam('Retake', $default = false); //0 = false
    $target_language_to_generate = $request->getQueryParam('language', $default = 0); //0 = false
    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $exam = new ExamHandler($this->db,$valid['SessionValue']);

    $existenceValidation = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);
    $max_retake_validation = $exam->ValidateRetakeCout($member->user_id, $posted_data["SectionID"]);

    if($course_existence) array_push($existenceValidation, $course_existence);
    if($max_retake_validation) array_push($existenceValidation, $max_retake_validation);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }    

    //Auto re enroll if the user is not yet enrolled
    //Added this because the enroll was remove due to client request
    //Initialy in order to add the exam to the list. you need to enroll now it is done automatically
    $additional_validation = $exam->ValidateAlreadyEnrolled($posted_data["CourseID"]);
    if(!$additional_validation){
        //Enroll
        $exam->EnrollNewCourse($posted_data);
    }


    $mapper = new ExamMapper($this->db);
    $section_obj = $mapper->GetCourseSectionItemByID($posted_data["CourseID"], $posted_data["SectionID"]);
    $generated_questions = $mapper->GetSectionItemQuestions($member->user_id, $section_obj, $isRetake, $target_language_to_generate);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Courses"=> $generated_questions)));
});


//Exam Section Item Completed
//TODO ALOT OF VALIDATIONS
//SECTION ITEM EXISTENCE
$app->post('/api/exam/section/complete',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'ProgressID'  => v::notEmpty()->numeric()->positive(),
        'CourseID' => v::notEmpty()->numeric()->positive(),
        'SectionItemID' => v::notEmpty()->numeric()->positive(),
        'NoOfCorrectAnswer' => v::numeric()->min(0), //score
        'Result' => v::numeric()->min(0)->max(2),
        // 'Score' => v::notEmpty()->numeric()->min(0),
         'Answers' => v::arrayVal(),
         'WrongAnswerQuestionID' => v::arrayVal(),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $exam = new ExamHandler($this->db,$valid['SessionValue']);
    $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
    $existenceValidation = array();
    $course_existence = $course_handler->ValidateExistCourse($posted_data["CourseID"]);

    if($course_existence) array_push($existenceValidation, $course_existence);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $exam->CompleteExamSectionItem($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//GET Exam Course Sections for Student
//This also fetch the progress if student have
$app->get('/api/student/course/{id}/section',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
   if(!$valid['IsValid'])
   {
       return $response->withStatus(400)
           ->withHeader('Content-Type', 'application/json')
           ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
   }

   $course_id = (int)$args['id'];

   $validation = $this->validator->ValidateByArgs( $args, 
   [
       'id' => v::numeric()->positive()
   ]);

   if ($validation)
   {
       return $response->withStatus(400)
           ->withHeader('Content-Type', 'application/json')
           ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
   }

   $course_handler = new CourseHandler($this->db,$valid['SessionValue']);
   $existenceValidation  = $course_handler->ValidateExistCourse($course_id);
   if($existenceValidation)
   {
       return $response->withStatus(400)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
   }

   $member_mapper = new MemberMapper($this->db);
   $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

   $mapper = new CourseMapper($this->db);
   $sections = $mapper->GetCourseSectionByCourseIDAndStudentID($course_id, $member->user_id);

   return $response->withStatus(200)
       ->withHeader('Content-Type', 'application/json')
       ->write(json_encode(array("Status"=>"OK", "CourseSection"=> $sections)));
});

//Get items for assignments use
$app->get('/api/exam/items',function(Request $request, Response $response){

    $cate_id = $request->getQueryParam('CategoryID', $default = null);
    $cate_section_id = $request->getQueryParam('CategorySectionID', $default = null);
    $cate_section_level_id = $request->getQueryParam('CategorySectionLevelID', $default = null);
    $posted_data = array(
        "CategoryID"=> $cate_id,
        "CategorySectionID"=> $cate_section_id,
        "CategorySectionLevelID"=> $cate_section_level_id
    );
    $rules = array();

    if($posted_data["CategoryID"]){
        $rules['CategoryID'] = v::numeric()->positive()->notEmpty();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new ExamMapper($this->db);
    $items = $mapper->GetExamSectionsForAssignment($cate_id, $cate_section_id, $cate_section_level_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=> count($items), "Items"=> $items)));
});

//For Creation of Question Banks
$app->get('/api/exam/section_items',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $cate_id = $request->getQueryParam('CategoryID', $default = null);
    $posted_data = array(
        "CategoryID"=> $cate_id
    );

    $rules = array();

    if($posted_data["CategoryID"]){
        $rules['CategoryID'] = v::numeric()->positive()->notEmpty();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $mapper = new ExamMapper($this->db);
    $items = $mapper->GetExamSectionsForQuestionBank($member->user_id, $cate_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=> count($items), "Items"=> $items)));
});
//Get Levels based on Section selected at api above
$app->get('/api/exam/section_levels',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $cate_section_id = $request->getQueryParam('SectionID', $default = null);
    $posted_data = array(
        "SectionID"=> $cate_section_id
    );
    
    $rules = array();

    if($posted_data["SectionID"]){
        $rules['SectionID'] = v::numeric()->positive()->notEmpty();
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $mapper = new ExamMapper($this->db);
    $items = $mapper->GetLevelsBySectionIDForQuestionBank( $cate_section_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","Count"=> count($items), "Levels"=> $items)));
});