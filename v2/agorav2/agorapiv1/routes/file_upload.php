<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
use Slim\Http\UploadedFile;

require '../src/handlers/file_upload_handler.php';

//Member Enroll Completion
$app->post('/api/file/file_upload',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = array(); //nothing here just keep going

    $file_validator = new FileUploadValidation();
    $file_validation = $file_validator->CheckFileUpload("file_upload", "Media File"); //not yet working
    
    if($file_validation){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Errors'=> $file_validation)));        
    }

    $handler = new FileUploadHandler();
    $result = $handler->UploadFile($posted_data);

    if ($result["Success"])
    {
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK', "File"=> array("FileName" => $result["Filename"], "FileUrl" => $this->mail["site"].$result["FileUrl"]  ))));
    }

    return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Errors'=> 'Unable to Upload the file. Please retry')));
});

$app->post('/api/file/profile',function(Request $request, Response $response){

    //Remove for now so initial registration profile can be accomodated :D
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    // if(!$valid['IsValid'])
    // {
    //     return $response->withStatus(400)
    //         ->withHeader('Content-Type', 'application/json')
    //         ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    // }

    $posted_data = array(); //nothing here just keep going

    $file_validator = new FileUploadValidation();
    $file_validation = $file_validator->CheckFileUpload("file_upload", "Media File"); //not yet working
    
    if($file_validation){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Errors'=> $file_validation)));        
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);


    $handler = new FileUploadHandler();
    $result = $handler->UploadProfile($posted_data);

    if ($result["Success"])
    {
        // $member_handler = new MemberEnrollHandler($this->db);
        // $member_handler->UpdateProfile($member->user_id, $result["Filename"]);
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=> 'OK', "File"=> array("FileName" => $result["Filename"], "FileUrl" => $this->mail["site"].$result["FileUrl"]  ))));
    }

    return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=>'NG', 'Errors'=> 'Unable to Upload the file. Please retry')));
});

