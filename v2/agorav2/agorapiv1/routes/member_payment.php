<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

/*Summary 
API here are as follow

Braintree New Payment Token         /api/membership/client_token
Braintree Payment Execute           /api/membership/payment_execute
New Membership Trial                /api/membership/trial
Subscription Cancellation           /api/membership/cancellation

*/

//generate token for the braintree payment
//used in front to access the braintree drop in ui payment
$app->post('/api/membership/client_token',function(Request $request, Response $response){

    $token = $this->gateway->ClientToken()->generate();

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK', 'ClientToken'=> $token)));  
});

//Execute the Payment using Braintree Gateway.
$app->post('/api/membership/payment_execute',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
 
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data,
    [
        'selectedMembershipPlanID'=> v::notEmpty(),
        'paymentNonce'=> v::notEmpty()
    ]
    );

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_enroll = new MemberEnrollHandler($this->db);

    //TODO IF UPGRADE
    $existenceValidation = $member_enroll->ValidateMembershipExist($posted_data["selectedMembershipPlanID"]);
    if ($existenceValidation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $existenceValidation)));
    } 
    
    $result = $member_enroll->ExecuteBrainTreePayment($posted_data, $valid['SessionValue'], $this->gateway, $this->mail);
    if($result["isValid"] == false){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $result["Errors"])));      
    }

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK', 'Transaction'=> $result["Transaction"])));  
});

//Start the Trial for Members
$app->post('/api/membership/trial',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $member_enroll = new MemberEnrollHandler($this->db);
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $existenceValidation = $member_enroll->ValidateMemberTrial($member->user_id);
    if ($existenceValidation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $existenceValidation)));
    } 

    $member_enroll->ExecuteTrialSubscription($valid['SessionValue']);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK', 'ClientToken'=> $token)));  
});

//Cancel the Subscription of User
$app->post('/api/membership/cancellation',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);   
    
    $handler = new MemberEnrollHandler($this->db);
    $result = array();
    if($member->user_level == 1){
        $result = $handler->ExecuteMemberCancellationForFree($member);
    }else{
        $result = $handler->ExecuteMemberCancellation($this->gateway, $member);   
    }

    if($result["isValid"] == false){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> $result["Errors"])));      
    }

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=>'OK', 'Transaction'=> $result["Transaction"]))); 
});
