<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
require '../src/handlers/trans_question_handler.php';


// Create New Translation of a Question
$app->post('/api/translation/question/new',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
 
    $validation_rules = array(
        'Language'=>           v::notEmpty()->numeric()->positive(),
        'QuestionID'=>          v::notEmpty()->numeric()->positive(),
        // 'QuestionTitle'=>       v::notEmpty()->length(null, 255),
        'QuestionDescription'=> v::notEmpty(),
        'AnswerMode'=>          v::numeric()->positive()->notEmpty()
    );
    if($posted_data["AnswerMode"] == 1){
        $validation_rules["ChoiceOne"] = v::notEmpty();
        $validation_rules["ChoiceTwo"] = v::notEmpty();
        $validation_rules["ChoiceThree"] = v::notEmpty();
        $validation_rules["ChoiceFour"] = v::notEmpty();
    }
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $posted_data["CountryID"] = $posted_data["Language"];

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = array();
    if(isset($posted_data["AnswerMode"]))
    {
        $answerModeExistence = $question->ValidateExistAnswerMode($posted_data["AnswerMode"]);
        if($answerModeExistence) array_push($existenceValidation, $answerModeExistence);
    }

    $questionExistence =  $question->ValidateExistQuestion($posted_data["QuestionID"]);
    if($questionExistence) array_push($existenceValidation, $questionExistence);    

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }
    $trans_handler = new TransQuestionBankHandler($this->db);
    $trans_handler->CreateNewTransQuestion($posted_data);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));
});


$app->post('/api/translation/question/edit',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
 
    $validation_rules = array(
        'TransQuestionID'=>           v::notEmpty()->numeric()->positive(),
        'Language'=>           v::notEmpty()->numeric()->positive(),
        'QuestionID'=>          v::notEmpty()->numeric()->positive(),
        // 'QuestionTitle'=>       v::notEmpty()->length(null, 255),
        'QuestionDescription'=> v::notEmpty(),
        'AnswerMode'=>          v::numeric()->positive()->notEmpty()
    );
    if($posted_data["AnswerMode"] == 1){
        $validation_rules["ChoiceOne"] = v::notEmpty();
        $validation_rules["ChoiceTwo"] = v::notEmpty();
        $validation_rules["ChoiceThree"] = v::notEmpty();
        $validation_rules["ChoiceFour"] = v::notEmpty();
    }
    $validation = $this->validator->ValidateByArgs($posted_data, $validation_rules);    

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $posted_data["CountryID"] = $posted_data["Language"];

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = array();
    if(isset($posted_data["AnswerMode"]))
    {
        $answerModeExistence = $question->ValidateExistAnswerMode($posted_data["AnswerMode"]);
        if($answerModeExistence) array_push($existenceValidation, $answerModeExistence);
    }

    $questionExistence =  $question->ValidateExistQuestion($posted_data["QuestionID"]);
    if($questionExistence) array_push($existenceValidation, $questionExistence);    

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }
    $trans_handler = new TransQuestionBankHandler($this->db);
    $trans_handler->UpdateTransQuestion($posted_data);

    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array('Status'=> 'OK')));
});

//Get Questions all available translation
$app->get('/api/translation/question/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $question_id = (int)$args['id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistQuestion($question_id);
    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $mapper = new TransQuestionMapper($this->db);
    $question = $mapper->GetTransQuestionByID($question_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Translation"=>$question)));
});


//TODO Language Valdation
$app->get('/api/translation/question/{id}/{lang_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $question_id = (int)$args['id'];
    $country_id = (int)$args['lang_id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive(),
        'lang_id' => v::numeric()->positive()
    ]);

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistQuestion($question_id);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $mapper = new TransQuestionMapper($this->db);
    $question = $mapper->GetTransQuestionByIDAndCountry($question_id, $country_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Question"=>$question)));
});


//TODO Language Valdation
$app->delete('/api/translation/question/{id}/{lang_id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $question_id = (int)$args['id'];
    $country_id = (int)$args['lang_id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive(),
        'lang_id' => v::numeric()->positive()
    ]);

    $question = new QuestionBankHandler($this->db,$valid['SessionValue']);
    $existenceValidation  = $question->ValidateExistQuestion($question_id);

    if($existenceValidation)
    {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"NG", 'Errors'=>$existenceValidation)));
    }

    $trans_handler = new TransQuestionBankHandler($this->db);
    $trans_handler->DeleteTranslation($question_id, $country_id);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});