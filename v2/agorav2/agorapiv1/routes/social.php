<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

require '../src/handlers/social_handler.php';

$app->get('/api/message',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $mapper = new SocialMapper($this->db);

    $client_time_zone = $request->getQueryParam('ClientTimeZone', $default = null);

    $messages = $mapper->GetMemberMessagesPreview($sender->user_id, $client_time_zone);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Messages"=> $messages)));
});


$app->get('/api/message/{receiver_id}',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $receiver_id = (int)$args['receiver_id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'receiver_id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $mapper = new SocialMapper($this->db);
    $messages = $mapper->GetMemberMessagesWith($sender->user_id, $receiver_id);
    $default_timezone = date_default_timezone_get();
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK","TimeZone"=>$default_timezone, "Messages"=> $messages)));
});

$app->post('/api/message/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

 
    $rules =  array(
        'ReceiverID' => v::notEmpty()->numeric()->positive(),
        'Message' => v::notEmpty()->length(5, 1000)
    );

    if($posted_data["NewMessage"]){
        $rules["Title"] =  v::notEmpty()->length(0, 255);
    }else{
        $rules["Title"] =  v::length(0, 255);
    }

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberByUserID($posted_data["ReceiverID"]);

    if($member == null){
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode (array('Status'=>'NG', 'Errors'=> array("Receiver"=>"Message Receiver does not exists."))));
    }

    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $message_head_id = $handler->SendMessage($sender->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK', 'MessageHeadID'=> $message_head_id)));
});

$app->post('/api/message/viewed',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'MessageHeadID' => v::notEmpty()->numeric()->positive(),
    ]);

    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->ViewedMessage($sender->user_id,$posted_data['MessageHeadID'] );

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});
//POST New Discussion Comment
//TODO EXISTENCE VALIDATION
$app->post('/api/discussion/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'CourseID' => v::notEmpty()->numeric()->positive(),
        'SectionItemID' => v::notEmpty()->numeric()->positive(),
        'Comment' => v::notEmpty()->length(2, 1000),
        'IsReply' => v::numeric(),
        'ReplyTo' => v::numeric()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->SendComment($sender->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

$app->put('/api/discussion/edit',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'DiscussionID' => v::notEmpty()->numeric()->positive(),
        'Comment' => v::notEmpty()->length(5, 1000),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->UpdateComment($sender->user_id, $posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

//Delete a Comment
$app->delete('/api/discussion/delete/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $comment_id = (int)$args['id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->DeleteComment($sender->user_id, $comment_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    
});

//GET Course Discussions in which the member participates
$app->get('/api/discussion',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $mapper = new SocialMapper($this->db);
    $discussion = null;
    //teacher
    if($sender->user_level == 4 ){
        $discussion = $mapper->GetTeacherDiscussionPreview($sender->user_id);
    }else{
        //student
        $discussion = $mapper->GetMemberDiscussionPreview($sender->user_id);
    }
    

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Discussion"=> $discussion)));
});

$app->get('/api/discussion/recent',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $mapper = new SocialMapper($this->db);
    $discussion = null;
    //teacher
    if($sender->user_level == 4 ){
        $discussion = $mapper->GetTeacherRecentDiscussion($sender->user_id);
    }else{
        //student
        $discussion = $mapper->GetMemberRecentDiscussion($sender->user_id);
    }
    

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "RecentDiscussion"=> $discussion)));
});

$app->post('/api/discussion/recent/viewed',function(Request $request, Response $response){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $rules = array(
        'DiscussionID' => v::notEmpty()->numeric()->positive(), //ID
    );

    $validation = $this->validator->ValidateByArgs($posted_data, $rules);
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    
    $member_mapper = new MemberMapper($this->db);
    $member = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $handler = new SocialHandler($this->db);
    $handler->InsertViewedDiscussion($posted_data["DiscussionID"], $member->user_id);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//TODO EXISTENCE VALIDATION
$app->get('/api/discussion/{course_id}/{section_item_id}',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $course_id = (int)$args['course_id'];
    $section_item_id = (int)$args['section_item_id'];

    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'course_id' => v::notEmpty()->numeric()->positive(),
        'section_item_id' => v::notEmpty()->numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }


    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $mapper = new SocialMapper($this->db);
    $comments = $mapper->GetDiscussionComments($sender->user_id, $course_id, $section_item_id, true);

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Comments"=> $comments)));
});



$app->get('/api/activity',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $course_section_id = $request->getQueryParam('CourseSectionID', $default = 0);
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset,
    );

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }    

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    
    $mapper = new SocialMapper($this->db);
    
    $post = array();
    if($sender->user_level == 4){
        //Teacher
        $post = $mapper->GetTeacherMemberPosts($sender->user_id, $pagelimit, $pageoffset);

    }else{
        //Student
        $post = $mapper->GetMemberPosts($sender->user_id, $pagelimit, $pageoffset);
    }

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", 
                                            "TotalCount"=>$post["TotalCount"],
                                            "PageCount"=>$post["PageCount"],
                                            "Post"=> $post["Results"])));
});


$app->post('/api/activity/new',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Post' => v::notEmpty()->length(5, 1000),
        'PostType'=> v::notEmpty()->numeric()->positive()->min(1)->max(3)
    ]);
    //PostType 1 = Normal, 2 = Unlocked, 3=  Achievement

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);

    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->NewPost($sender->user_id, $posted_data);
    

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});


$app->put('/api/activity/edit',function(Request $request, Response $response){
    
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PostID' => v::notEmpty()->numeric()->positive(),
        'Post' => v::notEmpty()->length(5, 1000),
    ]);
    //PostType 1 = Normal, 2 = Unlocked, 3=  Achievement

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->UpdatePost($sender->user_id, $posted_data);
    

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});

$app->delete('/api/activity/delete/{id}',function(Request $request, Response $response, $args){

    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    
    $post_id = (int)$args['id'];
    $validation = $this->validator->ValidateByArgs( $args, 
    [
        'id' => v::numeric()->positive()
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }
    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);

    $handler = new SocialHandler($this->db);
    $handler->DeletePost($sender->user_id, $post_id);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));    
});


//Inquiry
$app->post('/api/inquiry/new',function(Request $request, Response $response){
    //$valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);

    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'Name' => v::notEmpty()->length(5, 255),
        'Email' => v::notEmpty()->email()->noWhitespace(),
        'Message' => v::notEmpty()->length(5, 2500)
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $handler = new SocialHandler($this->db);
    $handler->SendInquiry($posted_data);
    
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array('Status'=> 'OK')));
});


$app->post('/api/inquiry/viewed',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }

    $posted_data = $request->getBody();
    $posted_data = json_decode($posted_data, true);
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'InquiryID' => v::notEmpty()->numeric()->positive(),
    ]);

    
    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }

    $member_mapper = new MemberMapper($this->db);
    $sender = $member_mapper->GetMemberBySession($valid['SessionValue']);
    $handler = new SocialHandler($this->db);
    $handler->ViewedInquiry($sender->user_id,$posted_data['InquiryID'] );

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK")));
});

//Get all of the inquiry
//for teacher only
$app->get('/api/inquiry',function(Request $request, Response $response, $args){
    $valid = HttpRequestValidator::ValidateRequiredSession($request,$this->db);
    if(!$valid['IsValid'])
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array('Status'=>'NG', 'UnauthorizeError'=> 'You are unauthorize to access this page.\n Please Log in')));
    }
    $pagelimit = $request->getQueryParam('PageLimit', $default = 20);
    $pageoffset = $request->getQueryParam('PageOffset', $default = 1);
    $pageoffset = $pageoffset - 1; //so pager will start at 1 in the front end
    $posted_data = array(
        "PageLimit"=> $pagelimit,
        "PageOffset"=> $pageoffset,
    );
    $validation = $this->validator->ValidateByArgs($posted_data, 
    [
        'PageLimit' =>  v::numeric()->positive()->notEmpty(),
        'PageOffset' =>  v::numeric()->min(0),
    ]);

    if ($validation)
    {
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode (array('Status'=>'NG', 'Errors'=> $validation)));
    }        

    $mapper = new SocialMapper($this->db);

    $inquiries = $mapper->GetInquiries();
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("Status"=>"OK", "Inquiry"=> $inquiries)));
});

