<?php

class MailSender
{
    private $headers;
    private $subject;
    private $message;
    private $send_to;
    public $generated_mail;
    
    public function __construct($headers)
    {
        $this->headers = $headers;
    }

    public function SendMail($send_to, $subject, $message)
    {
        $headers =  'From: '.$this->headers["from_email"].'' . "\r\n" .
                    'Reply-To:'.$this->headers["reply_to"].'' . "\r\n" .
                    "MIME-Version: 1.0" . "\r\n" .
                    "Content-type: text/html; charset=ISO-8859-1". "\r\n" .
                    "Bcc:".$this->headers["bcc"].'' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
        $pre_message =  "<html><head></head><body>";
        $end_message = "</body></html>";
        
        $message = 
        $pre_message.
        '<p>'.$message.'</p><br><br>'.
        $end_message;
        mail($send_to, $subject, $message, $headers);            
    }
}


