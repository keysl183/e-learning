<?php

class FileUploadHelper
{
	private $image_upload_directory = '../public/resources/images/';
    private $audio_upload_directory = '../public/resources/audios/';
    private $others_upload_directory = '../public/resources/others/';
    private $profile_upload_directory = '../public/resources/profiles/';

	public function move_uploaded_image($media_type,$name, $isProfile = false, $extension = null, $isOthers = false)
    {
        $upload_directory = ($media_type == 1) ? $this->image_upload_directory : $this->audio_upload_directory;

        if($isOthers){
            $upload_directory =  $this->others_upload_directory;
        }

        if($isProfile){
            $upload_directory = $this->profile_upload_directory;
        }
        if ($this->is_set_upload_directory($upload_directory))
        {
            $new_directory = $upload_directory . $name;
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $new_directory);

            if($media_type == 1){ //optimize if  image
                $image = $this->create_image_for_optimization($extension, $new_directory);
                $result = imagejpeg($image, $new_directory, 75); //overwrite the first image
                imagedestroy($image);  // Free up memory
            }
           
            return true;
        }
        
        return false;
    }

    function is_set_upload_directory($dir)
    {
        if (!file_exists($dir)) 
        {
            mkdir($dir, 0777, true);
        }

        return true;
    }

    private function create_image_for_optimization($extension, $file_path){
        if (is_null($extension)){
            return null;
        }
        if($extension == 'jpg' || $extension == 'jpeg'){
            return imageCreateFromJpeg($file_path);
        }else if($extension == 'png'){
            return  imageCreateFromPng($file_path);
        }else if($extension == 'gif'){
            return imageCreateFromGif($file_path);
        }
    }

}