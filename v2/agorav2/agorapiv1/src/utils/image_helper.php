<?php

class ImageHelper
{
	private $upload_directory = '../public/resources/profiles/';

	public function move_uploaded_image($name)
    {
        if ($this->is_set_upload_directory($this->upload_directory))
        {
            move_uploaded_file($_FILES['prof_image']['tmp_name'], $this->upload_directory . $name);
            return true;
        }
        
        return false;
    }

    function is_set_upload_directory($dir)
    {
        if (!file_exists($dir)) 
        {
            mkdir($dir, 0777, true);
        }

        return true;
    }
}