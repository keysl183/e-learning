<?php 

class HttpRequestValidator
{
    private $allowed_headers = array('HTTP_USER_KEY', 'HTTP_X_AUTHORIZATION');
    public function CheckRequestHeaders($request, $toCheckHeaders)
    {
        $headers = $request->getHeaders();
        $toPass = count($toCheckHeaders);
        $passed = 0; 
        foreach($toCheckHeaders as $targetHeader)
        {
            if(!array_key_exists($targetHeader, $headers))
            {
                return false;
            }
            $header = $headers[$targetHeader];
            if($header)
            {
                if(count($header) > 0)
                {
                    $passed++;
                }
            }
        }
        return ($passed == $toPass) ? true : false;
    }

    public function ValidateRequiredSession($request, $db)
    {
        $validHeaders = HttpRequestValidator::CheckRequestHeaders($request, array('HTTP_USER_KEY', 'HTTP_X_AUTHORIZATION'));
        if(!$validHeaders) return;
        $request_headers = $request->getHeaders();
        $sql = "SELECT COUNT(*) FROM tbl_session WHERE session_key = :sk AND session_value = :sv AND active = 1";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':sk', $request_headers['HTTP_USER_KEY'][0]);
        $stmt->bindParam(':sv', $request_headers['HTTP_X_AUTHORIZATION'][0]);
        $stmt->execute();
        
        $count = $stmt->fetchColumn();
        $session_value = $request_headers['HTTP_X_AUTHORIZATION'][0];

        $session = array("IsValid"=> false);
        if($count == 1 ) //valid
        {
            $session['IsValid'] = true;
            $session['SessionKey'] =  $request_headers['HTTP_USER_KEY'][0];
            $session['SessionValue'] =  $request_headers['HTTP_X_AUTHORIZATION'][0];
        }

        return $session;
    }
    
}