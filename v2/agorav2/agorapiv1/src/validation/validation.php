<?php 

namespace App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $errors;

    public function Validate($request, array $rules)
    { 
        foreach($rules as $field => $rule)
        {
            try
            {
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            }catch(NestedValidationException $e)
            {
                $this->errors[$field] = $e->getMessages();
            }
        }

        return $this->errors;
    }

    public function ValidateByArgs($args, array $rules )
    {
        if(!is_array($args)) $args = array();
        foreach($rules as $field => $rule)
        {
            try
            {
                if(array_key_exists($field,$args))
                {
                    $rule->setName(ucfirst($field))->assert($args[$field]);
                }else
                {
                    $rule->setName(ucfirst($field))->assert('');
                }
               
            }catch(NestedValidationException $e)
            {
                $this->errors[$field] = $e->getMessages();
            }
        }
        return $this->errors;
      
    }
}