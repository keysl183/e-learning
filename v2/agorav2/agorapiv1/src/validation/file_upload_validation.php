<?php 

class FileUploadValidation
{
    public function CheckFileUpload($file_key, $field_name)
    {
        if (!isset($_FILES[$file_key]['name']))
        {
            return array($field_name => "{$field_name} is a required item");
        }

        if ($_FILES[$file_key]['error'] > 0)
        {
            return array($field_name => $_FILES[$file_key]['error']);
        }
    }

}