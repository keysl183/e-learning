<?php
/*Handles tbl_user and tbl_user_detail */
class MemberMapper 
{
	private $db = null;
	public function __construct($db)
	{
		$this->db = $db;
	}

	public function GetMemberCredentialsByUserEmail($email)
	{
		$sql = "SELECT * FROM tbl_users WHERE user_email = :email";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':email', $email);
		$stmt->execute();
		$member = $stmt->fetch(PDO::FETCH_OBJ);
		return $member;
	}
	
	public function GetMemberIDByActivationKey($activation_key, $ignore_date = false)
	{
		$sql = "SELECT  ak.*,us.user_email, us.registration_date FROM `tbl_activation_keys` as ak 
				JOIN tbl_users as us ON ak.user_id = us.user_id 
				WHERE ak.activation_key =:ak
				AND ak.issue_date >= now() - INTERVAL 1 DAY
				";

		if($ignore_date){
			$sql = "SELECT  ak.*,us.user_email, us.registration_date FROM `tbl_activation_keys` as ak 
			JOIN tbl_users as us ON ak.user_id = us.user_id 
			WHERE ak.activation_key = :ak";
		}

		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':ak', $activation_key);
		$stmt->execute();
		$member = $stmt->fetch(PDO::FETCH_OBJ);
		return $member;
	}	


	//if both teachersOnly and studenst only are present or both of them are not
	//show all
	public function GetMembers($teachersOnly = false, $studentsOnly = false , $limit = 0, $page_offset = 0)
	{
		$count_sql = "SELECT COUNT(u.user_login) 
		FROM tbl_users_detail as d 
		JOIN tbl_users as u ON u.user_id = d.user_id ";

		$sql = "SELECT u.user_login, u.user_email, u.user_level,
		u.admin_privelege, u.registration_date, u.active,  d.* 
		FROM tbl_users_detail as d 
		JOIN tbl_users as u ON u.user_id = d.user_id ";

		if($teachersOnly){
			$count_sql .= " WHERE u.user_level = 4 ";
			$sql .= " WHERE u.user_level = 4 ";
		}
		if($studentsOnly){
			$count_sql .= " WHERE u.user_level < 4 ";
			$sql .= " WHERE u.user_level = 4 ";
		}

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
			} 
		} 
		$holder  = array();

		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$members = $stmt->fetchAll(PDO::FETCH_OBJ);
		$holder["Results"] = $members;

		$stmt = $this->db->prepare($count_sql);
		$stmt->execute();      
		$count = $stmt->fetchColumn();
		$holder["TotalCount"] = $count;

		$totalPages = 0;
		if(is_array($holder["Results"]) && $limit > 0){
			$totalPages = ceil( $holder["TotalCount"]  / $limit);
		}
		$holder["PageCount"] = $totalPages;

		return $holder;
	}

	public function GetMemberSearch($search)
	{
		$search = "%".$search."%";
		$sql = "SELECT u.user_id,u.user_email, ud.fname, ud.lname FROM tbl_users_detail as ud 
				JOIN tbl_users as u ON ud.user_id = u.user_id
				WHERE u.user_email LIKE :search OR ud.fname  LIKE :search OR ud.lname LIKE :search 
				OR u.user_login LIKE :search LIMIT 25";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':search', $search);		
		$stmt->execute();
		$members = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $members;
	}	

	public function GetMemberByUserID($id)
	{
		$sql = "SELECT ud.*, u.user_login, u.user_email, u.user_level,u.registration_date, u.active, u.admin_privelege FROM tbl_users_detail as ud
				JOIN tbl_users as u ON ud.user_id = u.user_id WHERE ud.user_id = :id";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$member = $stmt->fetch(PDO::FETCH_OBJ);
		return $member;
	}

	public function GetMemberByUserIDAssoc($id)
	{
		$sql = "SELECT ud.*, u.user_login, u.user_email, u.user_level,u.admin_privelege, u.registration_date, 
		ud.back_drop, u.fb_register_id, u.gmail_id, u.active
		FROM tbl_users_detail as ud
				JOIN tbl_users as u ON ud.user_id = u.user_id WHERE ud.user_id = :id";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$member = $stmt->fetch(PDO::FETCH_ASSOC);
		if($member){
			$user_level = $member["user_level"];
			if($user_level == 1){
				$member["subscription_type"] = "Free Trial";
			}else if($user_level == 2){
				$member["subscription_type"] = "Monthly";
			}else if($user_level == 3){
				$member["subscription_type"] = "Annual";
			}

			$student_array = [0, 1, 2, 3];
			if(in_array($user_level, $student_array) ){
				$member["w_user_level"] = 'Student';
			}else{
				$member["w_user_level"] = 'Teacher';
			}

			$common_mapper = new CommonMapper($this->db);
			$country = $common_mapper->GetCountryById($member["country_id"]);
			$education = $common_mapper->GetEducationLevelById($member["education_level"]);
			
			$member["w_country"] = (isset($country->country_name)) ? $country->country_name : '';
			$member["w_education"] = (isset($education->edu_level_description)) ? $education->edu_level_description : '';

		}
		return $member;
	}

	public function GetMemberBySession($session_value,$db = null)
	{
		$sql = "SELECT * FROM tbl_users  as u JOIN tbl_session as s ON u.user_id = s.session_user_id  WHERE s.session_value = :session_value";
		
		if($db)
		{
			$stmt = $db->prepare($sql);
		}else
		{
			$stmt = $this->db->prepare($sql);
		}
		
		$stmt->bindParam(':session_value', $session_value);
		$stmt->execute();
		$member = $stmt->fetch(PDO::FETCH_OBJ);
		return $member;
	}


	public function CountMemberWorksheet($user_id){
        $worksheet_count_sql = "SELECT COUNT(*) as worksheet_count
                FROM(
                SELECT ts.*,MAX(ts.date_added), i.course_id, i.section_id,i.title, i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type FROM tbl_course_section_progress as ts
                JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
                WHERE ts.status = 3 AND i.item_type = 1 AND user_id = :uid
                GROUP BY ts.section_item_id
                    
                ) as c";
        $stmt = $this->db->prepare($worksheet_count_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $worksheet_count = $stmt->fetch(PDO::FETCH_OBJ);
		$stats["WorksheetCount"] = $worksheet_count->worksheet_count;		
		return $worksheet_count;

	}
}