<?php

class SocialMapper
{
    private $db = null;
    private $message_table = 'tbl_message';
    private $discussion_table = 'tbl_message';
    private $discussion_viewed_table = 'tbl_viewed_discussion';
    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';


    public function __construct($db)
    {
        $this->db = $db;
    }

    public function GetMemberMessagesPreview($user_id, $client_time_zone = null){

        $es = "SELECT h.*, CONVERT_TZ(h.last_active_date, @@session.time_zone, '+00:00') as last_active_date_utc,
        i.user_id as init_user_id, i.fname as init_fname, i.lname as init_lname, i.profile_image as init_profile_image,
        t.user_id as to_user_id, t.fname as to_fname, t.lname as to_lname, t.profile_image as to_profile_image
        FROM `tbl_message_heads` as h
        JOIN tbl_users_detail as i ON h.initiator_id = i.user_id
        JOIN tbl_users_detail as t ON h.to_id = t.user_id 
        WHERE h.initiator_id = :sid OR h.to_id = :sid 
        ORDER BY h.last_active_date DESC";

        $stmt = $this->db->prepare($es);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $messages = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $new_messages = array();
        foreach($messages as $mess){
            $temp_message = array();
            $temp_message["message_head"] = $mess["message_heads_id"];
            if($mess["init_user_id"] == $user_id){
                $temp_message["receiver_id"] = $mess["to_user_id"]; 
                $temp_message["profile_image"] = $mess["to_profile_image"];
                $temp_message["receiver"] = $mess["to_fname"] . ' '. $mess["to_lname"];

                if($mess["viewed_by_initiator"] == 1){
                    $temp_message["viewed"] = true;
                }
                else{
                    $temp_message["viewed"] = false;
                }

            }else{ 
                $temp_message["receiver_id"] = $mess["init_user_id"]; 
                $temp_message["profile_image"] = $mess["init_profile_image"];
                $temp_message["receiver"] = $mess["init_fname"] . ' '. $mess["init_lname"];
                if($mess["viewed_by_to"] == 1){
                    $temp_message["viewed"] = true;
                }
                else{
                    $temp_message["viewed"] = false;
                }                
            }
            $temp_message["subject"] =  $mess["message_subject"];
            $temp_message["message"] =  $mess["last_message"];
            $temp_message["urgency"] =  $mess["last_urgency"];


            $current_date = new DateTime('NOW');
            $last_active_date = new DateTime($mess["last_active_date_utc"]);

            $last_active = strtotime($mess["last_active_date_utc"]);
            $new_date = date("M, d, Y h:i A", $last_active);
            $new_date_hours = date("h:i A", $last_active);
            $date_difference = $current_date->diff($last_active_date);
            $isNotDate = false;
            $showHourOnly = false;
            if($date_difference->days > 2){
                $temp_message["last_active"] = $new_date;
                $isNotDate = false;
                $showHourOnly = false;
            }
            else if($date_difference->days >= 1){
                $temp_message["last_active"] = "Yesterday";
                $isNotDate = true;
                $showHourOnly = false;
            }
            else{
                $temp_message["last_active"] = $new_date;
                $showHourOnly = true;
                $isNotDate = true;
                //  date("h:i A", $last_active); 
            }

            $temp_message["is_not_date"] =  $isNotDate;
            $temp_message["show_hour_only"] =  $showHourOnly;

       



            //FOR DEBUG
            // $temp_message["last_active_date"] = $last_active_date;
            // $temp_message["days_inverval"] = $date_difference;
            array_push($new_messages, $temp_message);

        }
        return $new_messages;
    }

    public function GetMemberMessagesWith($user_id, $receiver_id){

        $sql = "SELECT * FROM
        (
        SELECT m.receiver_id,m.sender_id, m.title, m.message, CONVERT_TZ(m.date_added, @@session.time_zone, '+00:00') as date_added, m.urgency, CONCAT(u.fname) as receiver, u.profile_image 
                FROM tbl_message as m
                JOIN tbl_users_detail as u
                ON u.user_id = m.sender_id
                WHERE m.sender_id = :sid AND m.receiver_id = :rid
        UNION        
        SELECT m.receiver_id,m.sender_id , m.title, m.message, CONVERT_TZ(m.date_added, @@session.time_zone, '+00:00') as date_added, m.urgency, CONCAT(u.fname) as receiver, u.profile_image 
                FROM tbl_message as m
                JOIN tbl_users_detail as u
                ON u.user_id = m.sender_id
                WHERE m.sender_id = :rid AND m.receiver_id = :sid
        ) as a
        ORDER BY date_added ASC";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->bindParam(':rid', $receiver_id);
        $stmt->execute();
        $messages = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $messages;
    }

    public function GetMemberDiscussionPreview($user_id){
        $sql ="SELECT ud.user_id, ud.fname, ud.lname, ud.profile_image ,d.discussion_id, d.course_id, d.section_item_id, d.user_id, d.comment,d.date_added,
        c.course_title, c.course_category as category_id, c.category_section_id, c.category_section_level_id,
        s.section_title, l.level_title, c.course_cover_image, cs.title, cs.item_type 
        FROM tbl_discussion as d 
        JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id          
        WHERE  c.active = 1 AND  d.user_id = :sid AND d.discussion_id 
        IN (
            SELECT MAX(d.discussion_id) as discussion_id
            FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
            JOIN tbl_course as c ON d.course_id = c.course_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE d.user_id = :sid  AND discussion_id
            GROUP BY (d.section_item_id) DESC                    
        )  ORDER BY d.date_added DESC";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $discussions = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $discussions;
    }    

    public function GetMemberRecentDiscussion($user_id){


        $discussion_heads = $this->GetMemberDiscussionPreview($user_id);
        $discussion_section_item = array();
        foreach($discussion_heads as $head){
            array_push($discussion_section_item, $head->section_item_id);
        }
        if(count($discussion_section_item) == 0){
            return array();
        }
        
        $in_criteria=implode(",", $discussion_section_item); 
        
        $sql ="SELECT ud.user_id, ud.fname, ud.lname, ud.profile_image,d.discussion_id, d.course_id, d.section_item_id, d.user_id, d.comment,d.date_added,
        c.course_title, c.course_category as category_id, c.category_section_id, c.category_section_level_id,
        s.section_title, l.level_title, c.course_cover_image, cs.title, cs.item_type 
        FROM tbl_discussion as d 
        JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id          
        WHERE  c.active = 1 AND d.discussion_id 
        IN (
            SELECT MAX(d.discussion_id) as discussion_id
            FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
            JOIN tbl_course as c ON d.course_id = c.course_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE d.user_id != :sid  AND discussion_id NOT IN (
                SELECT v.discussion_id FROM  {$this->discussion_viewed_table} as v WHERE v.viewed_user_id = :sid 
            )
            GROUP BY (d.section_item_id) DESC                    
        ) 
        AND d.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY) 
        AND d.section_item_id IN ({$in_criteria})
         ORDER BY d.date_added DESC";

        try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':sid', $user_id);
            $stmt->execute();
            $discussions = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $discussions;
        }catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }
     
    }    

    public function GetTeacherDiscussionPreview($user_id){
        $sql ="SELECT ud.user_id, ud.fname, ud.lname, ud.profile_image ,d.discussion_id, d.course_id, d.section_item_id, d.user_id, d.comment, d.date_added ,
        c.course_title, c.course_category as category_id, c.category_section_id, c.category_section_level_id,
        s.section_title, l.level_title, c.course_cover_image ,cs.title, cs.item_type 
        FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id      
        WHERE c.active = 1 AND c.author_user_id = :sid  AND d.discussion_id 
        IN (
        SELECT MAX(d.discussion_id) as discussion_id
        FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        WHERE c.author_user_id = :sid  AND discussion_id
        GROUP BY (d.section_item_id) DESC
        ) ORDER BY d.date_added DESC ";
        try
        {
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $discussions = $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            var_dump($e);
        }

        return $discussions;
    }   

    public function GetTeacherRecentDiscussion($user_id){
        $sql ="SELECT ud.user_id, ud.fname, ud.lname, ud.profile_image ,d.discussion_id, d.course_id, d.section_item_id, d.user_id, d.comment, d.date_added ,
        c.course_title, c.course_category as category_id, c.category_section_id, c.category_section_level_id,
        s.section_title, l.level_title, c.course_cover_image ,cs.title, cs.item_type 
        FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id      
        WHERE c.active = 1 AND c.author_user_id = :sid  AND d.discussion_id 
        IN (
            SELECT MAX(d.discussion_id) as discussion_id
            FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
            JOIN tbl_course as c ON d.course_id = c.course_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE c.author_user_id = :sid  AND discussion_id NOT IN (
                    SELECT discussion_id FROM  {$this->discussion_viewed_table} as v WHERE v.viewed_user_id = :sid 
                )
        GROUP BY (d.section_item_id) DESC
        ) 
        AND d.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)  AND d.user_id != :sid
        ORDER BY d.date_added DESC ";
        try
        {
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $discussions = $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            var_dump($e);
        }

        return $discussions;
    }   


    public function GetDiscussionComments($user_id, $course_id, $section_item_id, $get_replies = false){
        $sql ="SELECT d.*, ud.fname, ud.lname, ud.profile_image,u.user_level 
                FROM `tbl_discussion` as d 
                JOIN tbl_users as u ON d.user_id = u.user_id 
                JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
                WHERE d.course_id = :cid AND d.section_item_id = :sid AND is_reply = :isreply 
                ORDER BY date_added ASC";

        $replies_sql ="SELECT d.*, ud.fname, ud.lname, ud.profile_image,u.user_level 
            FROM `tbl_discussion` as d 
            JOIN tbl_users as u ON d.user_id = u.user_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE d.course_id = :cid AND d.section_item_id = :sid AND d.reply_to = :rto AND is_reply = 1 
            ORDER BY date_added ASC";                


        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $isReply = 0;
        $stmt->bindParam(':sid', $section_item_id);
        $stmt->bindParam(':isreply', $isReply); //do not get the reply
        $new_comments = array();
        if($get_replies == true){
            $stmt->execute();
            $comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $isReply = 1;
            foreach($comments as $comment){
                //get the replies for each 
                $replies = array();
                $stmt = $this->db->prepare($replies_sql);
                $stmt->bindParam(':cid', $course_id);
                $stmt->bindParam(':sid', $section_item_id);
                $stmt->bindParam(':rto',$comment["discussion_id"]);
                $stmt->execute();
                $replies = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $comment["replies"] = $replies;
                array_push($new_comments, $comment);
            }
            return $new_comments;
        }else{
            $stmt->execute();
            $comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comments;
        }
    }    
    
    public function GetMemberPosts($user_id , $limit = 0, $page_offset = 0){

        $count_sql ="SELECT COUNT(*) FROM tbl_posts WHERE user_id = :uid";

        $sql ="SELECT post_id, post_type, post_content, user_id, active,
        CONVERT_TZ(date_added, @@session.time_zone, '+00:00') as date_added FROM tbl_posts WHERE user_id = :uid ORDER BY date_added DESC ";
        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        }           

        $holder  = array();
        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $posts = $stmt->fetchAll(PDO::FETCH_OBJ);
        $holder["Results"] = $posts;


        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;  


        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;

        return $holder;
    }    
    public function GetTeacherMemberPosts($user_id, $limit = 0, $page_offset = 0){

        $count_sql = "SELECT COUNT(*) FROM tbl_posts WHERE post_id IN (
    		SELECT post_id FROM tbl_posts p2
            JOIN tbl_enroll as e ON p2.user_id = e.user_id
            JOIN tbl_users_detail as ud ON e.user_id = ud.user_id
            WHERE e.course_id IN 
            (
            SELECT c.course_id FROM tbl_course  c WHERE c.author_user_id = :uid
            ) AND p2.post_type != 1 AND p2.user_id != :uid
            )
            OR user_id = :uid";

        $sql = "SELECT post_id, post_type, post_content, user_id, active,
           CONVERT_TZ(date_added, @@session.time_zone, '+00:00') as date_added
             FROM tbl_posts WHERE post_id IN (
			
    		SELECT post_id FROM tbl_posts p2
            JOIN tbl_enroll as e ON p2.user_id = e.user_id
            JOIN tbl_users_detail as ud ON e.user_id = ud.user_id
            WHERE e.course_id IN 
            (
            SELECT c.course_id FROM tbl_course  c WHERE c.author_user_id = :uid
            ) AND p2.post_type != 1 AND p2.user_id != :uid
            )
            OR user_id = :uid
            ORDER BY date_added DESC ";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        }           
        $holder  = array();
        
        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $posts = $stmt->fetchAll(PDO::FETCH_OBJ);
        $holder["Results"] = $posts;


        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;        


        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;
                

        return $holder;
    }        
    
    public function GetInquiries($limit = 0, $page_offset = 0){
        $sql = "SELECT i.*,
        CONVERT_TZ(i.date_added, @@session.time_zone, '+00:00') as date_added_utc 
        FROM tbl_inquiry as i ORDER BY date_added DESC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $inquiry = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $new_inquiries = array();

        try{
        foreach($inquiry as $inq){
            $current_date = new DateTime();
            $last_active_date = new DateTime($inq["date_added_utc"]);

            $last_active = strtotime($inq["date_added_utc"]);
            $new_date = date("M, d, Y h:i A", $last_active);
            $date_difference = $current_date->diff($last_active_date);
            $isNotDate = false;
            $showHourOnly = false;


            if($date_difference->days > 2){
                $inq["date_added"] = $new_date;
                $isNotDate = false;
                $showHourOnly = false;
            }
            else if($date_difference->days >= 1){
                $inq["date_added"] = "Yesterday";
                $isNotDate = true;
                $showHourOnly = false;
            }
            else{
                $inq["date_added"] = $new_date; 
                $isNotDate = true;
                $showHourOnly = true;
            }

            $inq["is_not_date"] =  $isNotDate;
            $inq["show_hour_only"] =  $showHourOnly;
            if(is_null($inq["viewed_by_user_id"])){
                $inq["viewed"] = false;
            }else{
                $inq["viewed"] = true;
            }

            array_push($new_inquiries, $inq);
        }
    }catch(Exception $e){
        var_dump($e);
    }
        return $new_inquiries;
    }   


}