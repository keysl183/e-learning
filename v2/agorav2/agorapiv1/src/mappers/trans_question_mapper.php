<?php

class TransQuestionMapper
{
    private $db = null;
    private $bank_table = 'tbl_question_bank';
    private $quest_type_table = 'tbl_question_type';
    private $quest_ans_mode_table = 'tbl_question_answer_mode';
    private $trans_question_table = 'tbl_trans_question';
    private $trans_question_ans_table = 'tbl_trans_question_answers';
    
    public function __construct($db)
	{
		$this->db = $db;
    }

    //Gets all translation available for a question
    public function GetTransQuestionByID($id)
    {
        $sql = "SELECT c.*,q.*,a.question_choice FROM {$this->trans_question_table} as q 
                JOIN  {$this->trans_question_ans_table}  as a 
                ON q.trans_question_id = a.trans_question_id 
                JOIN tbl_countries as c ON c.id = q.trans_country_id
                WHERE q.question_id = :qid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $id);
        $stmt->execute();
        $translations = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $newTranslations = array();
        foreach($translations as $trans){
            $trans["choices"] = unserialize($trans["question_choice"]);
            array_push($newTranslations, $trans);
        }
        return $newTranslations;
    }

    //Gets all translation available for a question
    public function GetTransQuestionByIDAndCountry($id,$country_id)
    {
        $sql = "SELECT c.country_name,q.*, a.* FROM {$this->trans_question_table} as q 
                JOIN  {$this->trans_question_ans_table}  as a ON q.trans_question_id = a.trans_question_id 
                JOIN tbl_countries as c ON c.id = q.trans_country_id
                WHERE q.question_id = :qid AND q.trans_country_id = :tcid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $id);
        $stmt->bindParam(':tcid', $country_id);
        $stmt->execute();
        $question = $stmt->fetch(PDO::FETCH_ASSOC);
        if($question){
            $question["choices"] = unserialize($question["question_choice"]);
            return $question;
        }
        return null;
    }    

}