<?php

class ExamMapper
{
    private $db = null;
    private $enroll_table = 'tbl_enroll';
    private $course_table = 'tbl_course';
    private $course_section_item_table = 'tbl_course_section_item';
    private $course_section_progress_table = 'tbl_course_section_progress';
    private $category_table = 'tbl_category';
    private $user_detail_table = 'tbl_users_detail';
    private $question_table = 'tbl_question';

    private $assignment_table = 'tbl_assignment';
    private $assignment_submit_table = 'tbl_assingment_submission';
    private $discussion_viewed_table = 'tbl_viewed_discussion';

    public function __construct($db)
	{
		$this->db = $db;
    }

    public function GetEnrolledCourses($user_id, $category_id = 0, $limit = 0, $page_offset = 0)
    {
        //get the categories the user has record
        $category_sql = "SELECT DISTINCT(c.course_category), ct.* FROM tbl_course as c
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        WHERE ct.active =1 AND c.active = 1 AND c.course_id IN (
            SELECT e.course_id FROM tbl_enroll e WHERE e.user_id = :euid 
        )";

        if($category_id > 0){
            $category_sql = "SELECT DISTINCT(c.course_category), ct.* FROM tbl_course as c
            JOIN tbl_category as ct ON c.course_category = ct.category_id
            WHERE ct.active =1 AND c.active = 1 AND c.course_category = :cateid
                AND c.course_id IN (
                SELECT e.course_id FROM tbl_enroll e WHERE e.user_id = :euid 
            )"; 
        }

        $course_mapper = new CourseMapper($this->db);
        
            //Original, before client request
    //     $sql = "SELECT  c.course_id, c.course_title, c.course_excerpt, c.course_description, c.course_cover_image,
    //     c.no_of_questions, c.passing_rate, CONCAT(u.fname, ' ', u.lname) as author, c.date_added,c.student_enrolled,c.section_item_id, ct.category_id,
    //     ct.category_title,
    //      e.date_enrolled  
    //    FROM {$this->course_table}  as c 
    //    JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
    //    JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
    //    JOIN {$this->enroll_table} as e ON c.course_id = e.course_id  
    //    WHERE e.user_id = :euid  AND c.category_section_level_id = :catelevelid
    //    ORDER BY c.date_added DESC";        
        
        $sql = "SELECT  c.course_id, c.course_title, c.course_excerpt, c.course_description, c.course_cover_image,
         c.no_of_questions, c.passing_rate, CONCAT(u.fname, ' ', u.lname) as author, c.date_added,c.student_enrolled,c.section_item_id, ct.category_id,
         ct.category_title 
        FROM {$this->course_table}  as c 
        JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
        JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
        WHERE c.category_section_level_id = :catelevelid AND c.active = 1
        ORDER BY c.date_added DESC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 

        try{

            $stmt = $this->db->prepare($category_sql);
            $stmt->bindParam(':euid', $user_id);
            if($category_id >0){
                $stmt->bindParam(':cateid', $category_id);
            }
            $stmt->execute();
            $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $new_categories = array();
            foreach($categories as $category){
                $category_sections = $course_mapper->GetCategorySection($category["category_id"]);
                $new_category_sections = array();

                foreach($category_sections as $category_section){
                    $new_category_levels = array();
                    $passed_level_count = 0;
                    foreach($category_section["section_item"] as $level){
                        //fetch the courses related to the student
                        $stmt = $this->db->prepare($sql);
                        $stmt->bindParam(':catelevelid', $level["category_section_level_id"]);
                        $stmt->execute();
                        $courses = $stmt->fetchAll(PDO::FETCH_OBJ);
                        $level["Courses"] = $courses; //enrolled courses only

                        $level_passed = $this->GetNoOfLevelPassFailed($user_id, $level["category_section_level_id"], count($courses));
                        if($level_passed){
                            $passed_level_count = $passed_level_count + 1;
                        }
                        array_push($new_category_levels, $level);
                        //check level completeness
                    }
                    $category_section["section_item"] = $new_category_levels;
                    $category_section["PassedLevelCount"] = $passed_level_count;
                    array_push($new_category_sections, $category_section);
                }
                // $category_section["section_item"] = $new_category_sections;

                $category["Sections"] = $new_category_sections;
                
                array_push($new_categories, $category);
            }


            return $new_categories;     

        }catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }
    //GetEnrolled Courses was splitted into two
    //First one above is for the categories - section - level - chapters
    //This is for the courses
    //TODO ADD PROGRESS FOR COMPLETE
    public function GetSubEnrolledCourses($user_id, $category_section, $category_level_id, $category_chapter_no, $limit = 0, $page_offset = 0)
    {

        $count_sql = "SELECT  COUNT(c.course_id)
                FROM {$this->course_table}  as c 
                JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
                JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
                WHERE c.category_section_level_id = :catelevelid AND c.chapter_no = :chno AND c.active = 1";

        $sql = "SELECT  c.course_id, c.course_title, c.course_excerpt, c.course_description, c.course_cover_image,
                c.no_of_questions, c.passing_rate, CONCAT(u.fname, ' ', u.lname) as author,
                c.date_added,c.student_enrolled,c.section_item_id, ct.category_id,
                ct.category_title 
                FROM {$this->course_table}  as c 
                JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
                JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
                WHERE c.category_section_level_id = :catelevelid AND c.chapter_no = :chno AND c.active = 1
                ORDER BY c.date_added DESC";

       if($limit > 0)
       {
           $sql .= " LIMIT {$limit}";
           if($page_offset > 0)
           {
               $total_offset = $limit * $page_offset;
               $sql .= " OFFSET {$total_offset}";
           } 
       } 
       $holder  = array();

       $stmt = $this->db->prepare($sql);
       // $stmt->bindParam(':euid', $user_id); 
       $stmt->bindParam(':catelevelid', $category_level_id);
       $stmt->bindParam(':chno', $category_chapter_no);
       $stmt->execute();
       $courses = $stmt->fetchAll(PDO::FETCH_ASSOC);
       $holder["Results"] = $courses;
       
       $stmt = $this->db->prepare($count_sql);
       $stmt->bindParam(':catelevelid', $category_level_id);
       $stmt->bindParam(':chno', $category_chapter_no);
       $stmt->execute();
       $count = $stmt->fetchColumn();
       $holder["TotalCount"] = $count;


       $totalPages = 0;
       if(is_array($holder["Results"]) && $limit > 0){
           $totalPages = ceil( $holder["TotalCount"]  / $limit);
       }
       $holder["PageCount"] = $totalPages;

       return $holder;

    }

    //Helper function for GetEnrolled Courses
    //Calculate if the level is passed
    private function GetNoOfLevelPassFailed($user_id, $level_id, $actual_level_course_count){
        $sql = "SELECT COUNT(*) FROM tbl_course_section_progress as p2
                WHERE p2.section_progress_id IN (
                    SELECT MAX(p.section_progress_id)  FROM tbl_course as c 
                    JOIN  tbl_category_section_level as l 
                    ON c.category_section_level_id = l.category_section_level_id
                    JOIN tbl_course_section_progress as p ON p.section_item_id = c.section_item_id
                    WHERE c.active = 1 AND l.active = 1  
                    AND l.category_section_level_id = :catelevelid AND p.user_id = :uid
                )";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':catelevelid', $level_id);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();   
        if($count == 0){
            return false;
        } 
        if($count == $actual_level_course_count){
            //Level Passed
            return true;
        }
            
        return false;

    }

	public function GetCourseSectionItemByID($course_id,$id)
	{
        $sql = "SELECT * FROM {$this->course_section_item_table} 
                WHERE section_item_id = :sid AND course_id = :cid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $id);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();
		$section_item = $stmt->fetch(PDO::FETCH_OBJ);
		return $section_item;
    }   

    //trg_country_id = refers to country_id of the language we wish to generate question from
    public function GetSectionItemQuestions($user_id, $section_item_obj ,$isRetake = false,  $trg_country_id = 0){
        if($section_item_obj == null){
            return;
        }

        $sql = "SELECT * FROM {$this->course_section_progress_table} 
        WHERE section_item_id = :sid AND user_id = :usid
        ORDER BY section_progress_id DESC LIMIT 1";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $section_item_obj->section_item_id);
        $stmt->bindParam(':usid', $user_id);
        $stmt->execute();
        $progress_item = $stmt->fetch();
        $wrong_questions_ids = array(); //questions that gotten wrong by student at last try
        if($progress_item != null && !$isRetake){
            $uns_questions =  unserialize($progress_item["questions"]);
            $progress_item["questions"] =$uns_questions;
            $progress_item["no_of_questions"] = count($uns_questions);
            $wrong_questions_ids = unserialize($progress_item["wrong_answer_question_ids"]);
            return $progress_item;
        }
        if($progress_item == null || $isRetake){
            //data 
            if($progress_item != null){
                $wrong_questions_ids_temp = unserialize($progress_item["wrong_answer_question_ids"]);
                if($wrong_questions_ids_temp){
                    $wrong_questions_ids  = $wrong_questions_ids_temp; 
                }
            }
            //generate here
            $noOfQuesToGenerate = $section_item_obj->no_of_question;
            $generate_sql = "SELECT  question_id FROM {$this->question_table} 
                     ";

            //worksheet
            if($section_item_obj->item_type == 1)
            {
                $generate_sql .= "WHERE question_bank_id = {$section_item_obj->ws_bank_id}";
            }
            //unittest
            else if($section_item_obj->item_type == 2){
                //get the bank id of worksheet under the same section in which this unit test resides
                $banks_sql = "SELECT ws_bank_id FROM `tbl_course_section_item` 
                            WHERE section_id = :bkid AND item_type = 1";
                $stmt = $this->db->prepare($banks_sql);
                $stmt->bindParam(':bkid', $section_item_obj->section_id);
                $stmt->execute();
                $bank_ids = $stmt->fetchAll(PDO::FETCH_COLUMN);
                $str_bank_ids = implode(", ", $bank_ids);
                $generate_sql .= "WHERE question_bank_id IN ({$str_bank_ids})";
            }
            //exams
            else if($section_item_obj->item_type == 3){
                $banks_sql = "SELECT ws_bank_id FROM `tbl_course_section_item` 
                            WHERE course_id = :cid AND item_type = 1";
                $stmt = $this->db->prepare($banks_sql);
                $stmt->bindParam(':cid', $section_item_obj->course_id);
                $stmt->execute();
                $bank_ids = $stmt->fetchAll(PDO::FETCH_COLUMN);
                $str_bank_ids = implode(", ", $bank_ids);
                $generate_sql .= "WHERE question_bank_id IN ({$str_bank_ids})";                
            }

            if($trg_country_id > 0){
                //ignore the generated sql above
                //for other language than english
                //to be refactored later
                $generate_sql = "SELECT tq.question_id FROM `tbl_trans_question` as tq 
                JOIN tbl_question as q ON tq.question_id = q.question_id
                WHERE q.question_bank_id = {$section_item_obj->ws_bank_id} 
                AND tq.trans_country_id = {$trg_country_id}";
            }

            if (in_array('0', $wrong_questions_ids)) 
            {
                $wrong_questions_ids = array_diff($wrong_questions_ids, array('0'));
                // unset($array[array_search('0',$wrong_questions_ids)]);
            }

            $generate_sql .= " ORDER BY RAND()
                            LIMIT {$noOfQuesToGenerate} ";

            $stmt = $this->db->prepare($generate_sql);
            $stmt->execute();
            $section_questions = $stmt->fetchAll(); //fetch array

            if(count($wrong_questions_ids) > 0){
                //merge the array of wrong questions from last to the generated new one
                $new_arr_section_questions = array(); //np key
                foreach($section_questions as $quest){
                    array_push($new_arr_section_questions, $quest['question_id']);
                }
                $new_arr_section_questions = array_unique(array_merge($wrong_questions_ids,$new_arr_section_questions));
                $real_arr_section_questions = array();
                foreach($new_arr_section_questions as $quest){
                    $_tempItem = array();
                    $_tempItem['question_id'] = $quest;
                    array_push($real_arr_section_questions,$_tempItem);
                }
                $section_questions = $real_arr_section_questions;

                //get only the wanted number of question when it exceeds
                if(count($section_questions) > $noOfQuesToGenerate){
                    $section_questions = array_slice($section_questions, 0, $noOfQuesToGenerate);
                }
            }

            

            //TODO WHAT IF GENERATED QUESTIONS DOES NOT EQUAL TO DESIRED QUESTION
            if( count ($section_questions) > 0){
                //insert
                $insertsql = "INSERT INTO {$this->course_section_progress_table} 
                (`user_id`,`section_item_id`,`questions`)
                VALUES (:uid, :sid, :qst)";
                $stmt = $this->db->prepare($insertsql);
                $stmt->bindParam(':uid', $user_id);
                $stmt->bindParam(':sid',  $section_item_obj->section_item_id);
                $temp = serialize($section_questions); //dunno php serialize hates it directly
                $stmt->bindParam(':qst', $temp);
                $stmt->execute();
                //to get the refreshed data
                //boilderplate codeez
                //Get the Questions
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':sid', $section_item_obj->section_item_id);
                $stmt->bindParam(':usid', $user_id);
                $stmt->execute();
                $progress_item = $stmt->fetch();
                if($progress_item != null){
                    $uns_questions =  unserialize($progress_item["questions"]);
                    $progress_item["questions"] =$uns_questions;
                    $progress_item["no_of_questions"] = count($uns_questions);
                    return $progress_item;
                }
            }
        }
    }

    public function GetExamSectionsForAssignment($category_id = null, $category_section_id = null, $category_section_level_id =null){
        $sql = "SELECT t.*,ct.category_id, ct.category_title FROM `tbl_course_section_item` as t 
        JOIN tbl_course as c ON t.course_id = c.course_id 
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        WHERE ct.active = 1 AND c.active = 1 ";
        
        if($category_id != null){
            $sql .= " AND ct.category_id = :cateid AND c.category_section_id = :catesecid AND c.category_section_level_id = :catelevelid" ;
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':cateid', $category_id);
            $stmt->bindParam(':catesecid', $category_section_id);
            $stmt->bindParam(':catelevelid', $category_section_level_id);
        }else{
            $stmt = $this->db->prepare($sql);
        }
        $stmt->execute();

        $items = $stmt->fetchAll();
        return $items;
    }

    //REMOVED DUE TO CLIENT REQUEST
    // public function GetExamSectionsForQuestionBank($user_id, $category_id = null){
    //     $sql = "SELECT t.*,ct.category_id, ct.category_title  FROM tbl_course_section as t
    //     JOIN tbl_course as c ON t.course_id = c.course_id
    //     JOIN tbl_category as ct ON c.course_category = ct.category_id
    //     WHERE c.author_user_id = :auid AND  ct.active = 1 AND c.active = 1";
        
    //     if($category_id != null){
    //         $sql .= " AND ct.category_id = :cateid";
    //         $stmt = $this->db->prepare($sql);
    //         $stmt->bindParam(':cateid', $category_id);
    //         $stmt->bindParam(':auid', $user_id);
    //     }else{
    //         $stmt = $this->db->prepare($sql);
    //     }
    //     $stmt->execute();

    //     $items = $stmt->fetchAll();
    //     return $items;
    // }    

    public function GetExamSectionsForQuestionBank($user_id, $category_id = null){
        $sql = "SELECT * FROM tbl_category_section as s 
        JOIN tbl_category as c ON c.category_id = s.category_id
        WHERE c.active = 1 AND s.active = 1 AND s.category_id = :cateid
         ORDER BY section_title ASC";
        
        if($category_id != null){
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':cateid', $category_id);
            // $stmt->bindParam(':auid', $user_id);
        }else{
            $stmt = $this->db->prepare($sql);
        }
        $stmt->execute();

        $items = $stmt->fetchAll();
        return $items;
    }    

    public function GetLevelsBySectionIDForQuestionBank($category_section_id){
        $sql = "SELECT * FROM tbl_category_section_level as l 
        WHERE l.category_section_id = :cslid AND l.active = 1 ORDER BY level_title ASC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cslid', $category_section_id);
        $stmt->execute();
        return $stmt->fetchAll();
    }    
        

    public function GetMemberStatsForProfile($user_id){
        $stats = array();
        $history_sql = "SELECT ts.section_progress_id, ts.user_id,ts.section_item_id,ts.score ,ts.date_added,
                        MAX(ts.date_added), i.course_id, i.section_id, i.title, 
                        i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type,
                        (
                            SELECT COUNT(ip.section_progress_id) FROM tbl_course_section_progress as ip 
                            WHERE ip.section_item_id = ts.section_item_id AND ip.user_id = :uid
                        ) as attempt_count
                        FROM tbl_course_section_progress as ts
                        JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
                        WHERE ts.status = 3 AND user_id = :uid
                        GROUP BY ts.section_item_id ORDER BY ts.date_added DESC LIMIT 10";

        $stmt = $this->db->prepare($history_sql);
        $stmt->bindParam(':uid', $user_id);   
        $stmt->execute();
        $history = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stats["History"] = $history;


        $worksheet_count_sql = "SELECT COUNT(*) as worksheet_count
                FROM(
                SELECT ts.*,MAX(ts.date_added), i.course_id, i.section_id,i.title, i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type FROM tbl_course_section_progress as ts
                JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
                WHERE ts.status = 3 AND i.item_type = 1 AND user_id = :uid
                GROUP BY ts.section_item_id
                    
                ) as c";
        $stmt = $this->db->prepare($worksheet_count_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $worksheet_count = $stmt->fetch(PDO::FETCH_OBJ);
        $stats["WorksheetCount"] = $worksheet_count->worksheet_count;


        $unit_test_count_sql = "SELECT COUNT(*) as unittest_count
                FROM(
                SELECT ts.*,MAX(ts.date_added), i.course_id, i.section_id,i.title, i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type FROM tbl_course_section_progress as ts
                JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
                WHERE ts.status = 3 AND i.item_type = 2 AND user_id = :uid
                GROUP BY ts.section_item_id
                    
                ) as c";
        
        $stmt = $this->db->prepare($unit_test_count_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $unit_test_count = $stmt->fetch(PDO::FETCH_OBJ);
        $stats["UnitTestCount"] = $unit_test_count->unittest_count;

        //TODO ASSIGNMENTS AND EXAMS
        $exam_count_sql = "SELECT COUNT(*) as exam_count
                FROM(
                SELECT ts.*,MAX(ts.date_added), i.course_id, i.section_id,i.title, i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type FROM tbl_course_section_progress as ts
                JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
                WHERE ts.status = 3 AND i.item_type = 3 AND user_id = :uid
                GROUP BY ts.section_item_id
                    
                ) as c";
        
        $stmt = $this->db->prepare($exam_count_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $exam_count = $stmt->fetch(PDO::FETCH_OBJ);
        $stats["ExamCount"] = $exam_count->exam_count;        

        $assign_count_sql ="SELECT COUNT(*) as assignment_count FROM `tbl_assingment_submission` WHERE user_id = :uid";
        $stmt = $this->db->prepare($assign_count_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $assignment_count = $stmt->fetch(PDO::FETCH_OBJ);
        $stats["AssignmentCount"]  = 0;
        if($assignment_count){
            $stats["AssignmentCount"] = $assignment_count->assignment_count;    
        }
        

        //TODO
        $grades_sql = "";

        //Progress

        $category_sql = "SELECT DISTINCT(c.course_category) as category_id, ct.category_title FROM tbl_course as c
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        WHERE ct.active =1 AND c.active = 1 AND c.course_id IN (
        SELECT e.course_id FROM tbl_enroll e WHERE e.user_id = :uid 
        )";
        $stmt = $this->db->prepare($category_sql);
        $stmt->bindParam(':uid', $user_id);  
        $stmt->execute();
        $enrolled_categories = $stmt->fetchAll(PDO::FETCH_OBJ);

        $courses_progress_sql = "SELECT (
            SELECT COUNT(*) FROM tbl_course_section_item as s JOIN 
                tbl_course as c ON c.course_id = s.course_id
                WHERE c.course_category = :cateid AND c.active = 1
        ) as total_exams_in_course,
        (
            SELECT COUNT(DISTINCT(p.section_item_id))
            FROM tbl_course_section_progress as p 
            WHERE section_item_id IN (
                SELECT s.section_item_id
                FROM tbl_course_section_item as s JOIN 
                tbl_course as c ON c.course_id = s.course_id
                WHERE c.course_category = :cateid AND c.active = 1
            )
            AND p.pass_failed = 1 AND p.status = 3  AND p.active = 1 AND p.user_id = :uid
            
        ) as total_exams_completed";

        $category_progress = array();
        foreach($enrolled_categories as $cate){
            $stmt = $this->db->prepare($courses_progress_sql);
            $id= $cate->category_id;
            $stmt->bindParam(':cateid', $id);  
            $stmt->bindParam(':uid', $user_id);  
            $stmt->execute();
            $prog = $stmt->fetch(PDO::FETCH_OBJ);
            $course["category_title"] = $cate->category_title;
            $course['progress'] = $prog;
            array_push($category_progress, $course);
        }

        $stats["CourseProgresses"] = $category_progress;

        return $stats;
    }

    public function GetTeacherStatsForProfile($user_id){
        $stats = array();

        $worksheet_count_sql = "SELECT COUNT(*) as worksheet_count FROM tbl_course_section_item as i
        JOIN tbl_course as c ON c.course_id = i.course_id
        WHERE i.item_type = 1 AND c.active =1  AND c.author_user_id = :auid";    
        $stmt = $this->db->prepare($worksheet_count_sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $worksheet_count = $stmt->fetch(PDO::FETCH_OBJ);
        if($worksheet_count){
            $stats["WorksheetCount"] = $worksheet_count->worksheet_count;
        }else{
            $stats["WorksheetCount"] = 0;
        }
        

        $unit_test_count_sql = "SELECT COUNT(*) as unittest_count FROM tbl_course_section_item as i
        JOIN tbl_course as c ON c.course_id = i.course_id
        WHERE i.item_type = 2 AND c.active = 1  AND c.author_user_id = :auid";         
        $stmt = $this->db->prepare($unit_test_count_sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $unit_test_count = $stmt->fetch(PDO::FETCH_OBJ);
        if($unit_test_count){
            $stats["UnitTestCount"] = $unit_test_count->unittest_count;
        }else{
            $stats["UnitTestCount"] = 0;
        }
        

        $exam_count_sql = "SELECT COUNT(*) as exams_count FROM tbl_course WHERE active = 1 AND author_user_id = :auid";
        $stmt = $this->db->prepare($exam_count_sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $exams_count = $stmt->fetch(PDO::FETCH_OBJ);
            
        if($exams_count){
            $stats["ExamCount"] = $exams_count->exams_count;
        }else{
            $stats["ExamCount"] = 0;
        }
        
        $assign_count_sql = "SELECT COUNT(a.assignment_id) as assign_count 
        FROM tbl_assignment as a  
        JOIN tbl_category as ct ON ct.category_id = a.category_id
        JOIN tbl_category_section as cts ON cts.category_section_id= a.section_id
        JOIN tbl_category_section_level as ctsl ON ctsl.category_section_level_id= a.level_id
        WHERE a.active = 1 AND ct.active = 1 AND cts.active = 1 AND ctsl.active = 1
        AND a.author_user_id = :auid";
        $stmt = $this->db->prepare($assign_count_sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $assignment_count = $stmt->fetch(PDO::FETCH_OBJ);
       
        if($assignment_count){
            $stats["AssignmentCount"] = $assignment_count->assign_count;
        }else{
            $stats["AssignmentCount"] = 0;
        }       
        
        // $students_count_sql ="SELECT COUNT(DISTINCT(e.user_id)) as student_count FROM `tbl_enroll` as e 
        // JOIN  tbl_course as c ON e.course_id = c.course_id
        // WHERE c.active = 1 AND c.author_user_id = :auid";

        $students_count_sql = "SELECT COUNT(DISTINCT(u.user_id)) as student_count FROM tbl_users as u
        JOIN tbl_enroll as e ON e.user_id = u.user_id
        JOIN tbl_course as c ON c.course_id = e.course_id
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        WHERE c.active = 1 AND u.active = 1 AND c.author_user_id = :auid AND ct.active = 1";

        $stmt = $this->db->prepare($students_count_sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $student_count = $stmt->fetch(PDO::FETCH_OBJ);
        if($student_count){
            $stats["StudentCount"] = $student_count->student_count;
        }else{
            $stats["StudentCount"] = 0;
        }           
            
        return $stats;

    }  

    public function GetTeacherSubStatsCourse($user_id , $limit = 0, $page_offset = 0){

        $count_sql = "SELECT  COUNT(c.course_category)
        FROM `tbl_course` as c 
        JOIN `tbl_category` as ct ON c.course_category = ct.category_id
        WHERE c.active = 1 AND c.author_user_id = :auid
        GROUP BY course_category";

        $sql = "SELECT  c.course_category, ct.category_title,
        SUM((SELECT COUNT(e.user_id) FROM tbl_enroll  as e JOIN tbl_users as u ON e.user_id = u.user_id WHERE e.course_id = c.course_id AND u.active = 1 AND c.active)) as StudentCount,
        SUM((SELECT COUNT(i.section_item_id) FROM tbl_course_section_item as i WHERE i.course_id = c.course_id AND i.item_type = 1 )) as WorksheetCount,
        SUM((SELECT COUNT(i.section_item_id) FROM tbl_course_section_item as i WHERE i.course_id = c.course_id AND i.item_type = 2 )) as UnitTestCount,
        SUM((
        SELECT COUNT(a.assignment_id) as assign_count FROM tbl_assignment as a
        JOIN tbl_course_section_item as i ON i.section_item_id = a.section_item_id
        WHERE a.active = 1 AND i.course_id = c.course_id
        )) as AssignmentCount
        FROM `tbl_course` as c 
        JOIN `tbl_category` as ct ON c.course_category = ct.category_id
        WHERE c.active = 1 AND c.author_user_id = :auid
        GROUP BY course_category";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $holder  = array();


        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $substats = $stmt->fetchAll();
        $holder["Results"]  = $substats;

        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':auid', $user_id);  
		$stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
		if(is_array($holder["Results"]) && $limit > 0){
			$totalPages = ceil( $holder["TotalCount"]  / $limit);
		}
        $holder["PageCount"] = $totalPages;


        return $holder;
    }

    public function GetTeacherSubStatsStudents($user_id, $limit = 0, $page_offset = 0){

        $students_count_sql = "SELECT COUNT(DISTINCT(u.user_id)) as student_count FROM tbl_users as u
        JOIN tbl_enroll as e ON e.user_id = u.user_id
        JOIN tbl_course as c ON c.course_id = e.course_id
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        WHERE c.active = 1 AND u.active = 1 AND ct.active = 1 AND c.author_user_id = :auid ";

        $sql = "SELECT u.user_id, ud.fname, ud.lname, cr.country_name , ud.age,
        GROUP_CONCAT(DISTINCT ct.category_title SEPARATOR ', ') as courses
        FROM tbl_users_detail as ud 
        JOIN tbl_users as u ON ud.user_id = u.user_id
        JOIN tbl_enroll as e ON e.user_id = ud.user_id
        JOIN tbl_course as c ON c.course_id = e.course_id
        JOIN tbl_category as ct ON c.course_category = ct.category_id
        JOIN tbl_countries as cr ON cr.id = ud.country_id
        WHERE c.active = 1 AND u.active = 1 AND ct.active = 1 AND c.author_user_id = :auid
        GROUP BY (ud.user_id)";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $holder  = array();

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':auid', $user_id);  
        $stmt->execute();
        $substats = $stmt->fetchAll();
        $holder["Results"]  = $substats;



        $stmt = $this->db->prepare($students_count_sql);
        $stmt->bindParam(':auid', $user_id);  
		$stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
		if(is_array($holder["Results"]) && $limit > 0){
			$totalPages = ceil( $holder["TotalCount"]  / $limit);
		}
        $holder["PageCount"] = $totalPages;
        

        return $holder;
    }

    public function GetMemberSubStatsGrades($user_id, $cate_id = null, $category_section_id = null, $category_section_level_id = null, $chapter_no = null){
        $stats = array();
        $history_sql = "SELECT ts.section_progress_id, ts.user_id,ts.section_item_id,ts.score ,ts.date_added,
        MAX(ts.date_added) as date_added , i.course_id, i.section_id, i.title, 
        c.course_category, c.category_section_id, c.category_section_level_id, c.chapter_no,
        i.description, i.answer_type, i.passing_rate, i.no_of_question,i.item_type,
        (
            SELECT COUNT(ip.section_progress_id) FROM tbl_course_section_progress as ip 
            WHERE ip.section_item_id = ts.section_item_id AND ip.user_id = :uid
        ) as attempt_count
        FROM tbl_course_section_progress as ts
        JOIN tbl_course_section_item as i ON ts.section_item_id = i.section_item_id
        JOIN tbl_course as c ON i.course_id = c.course_id
        WHERE ts.status = 3 AND user_id = :uid ";

        if($cate_id > 0){
            $history_sql .= " AND c.course_category = :cateid ";
        }
        
        if($category_section_id > 0){
            $history_sql .= " AND c.category_section_id = :cate_sec_id ";
        }
        
        if($category_section_level_id > 0){
            $history_sql .= " AND c.category_section_level_id = :cate_sec_level_id ";
        }

        if($chapter_no > 0){
            $history_sql .= " AND c.chapter_no = :chap_no ";
        }        

        $history_sql .= " GROUP BY ts.section_item_id ORDER BY ts.date_added DESC LIMIT 10";        

        $stmt = $this->db->prepare($history_sql);
        $stmt->bindParam(':uid', $user_id);   

        if($cate_id > 0){
            $stmt->bindParam(':cateid', $cate_id);   
        }
        if($category_section_id > 0){
            $stmt->bindParam(':cate_sec_id', $category_section_id);   
        }        
        if($category_section_level_id > 0){
            $stmt->bindParam(':cate_sec_level_id', $category_section_level_id);   
        }   
        if($chapter_no > 0){
            $stmt->bindParam(':chap_no', $chapter_no);   
        }              

        $stmt->execute();
        $history = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stats["History"] = $history;     
        
        return $stats;
    }
    

    //For Red Circles Student
    public function GetMemberMenuStats($user_id){
        $holder = array();


        $discussion_head_sql ="SELECT d.discussion_id, d.course_id, 
        d.section_item_id, d.user_id, d.comment,d.date_added,
        c.course_title, c.course_category as category_id, c.category_section_id, c.category_section_level_id
        FROM tbl_discussion as d 
        JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id        
        WHERE  c.active = 1 AND  d.user_id = :sid AND d.discussion_id 
        IN (
            SELECT MAX(d.discussion_id) as discussion_id
            FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
            JOIN tbl_course as c ON d.course_id = c.course_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE d.user_id = :sid  AND discussion_id
            GROUP BY (d.section_item_id) DESC                    
        )  ORDER BY d.date_added DESC";

        $stmt = $this->db->prepare($discussion_head_sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $discussion_heads =$stmt->fetchAll(PDO::FETCH_OBJ);
      
        $discussion_section_item = array();
        foreach($discussion_heads as $head){
            array_push($discussion_section_item, $head->section_item_id);
        }
        
        $in_criteria=implode(",", $discussion_section_item); 
        // var_dump($in_criteria);

        $discussion_sql ="SELECT COUNT(d.discussion_id)
        FROM tbl_discussion as d 
        JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id         
        WHERE  c.active = 1 AND d.discussion_id 
        IN (
            SELECT MAX(d.discussion_id) as discussion_id
            FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
            JOIN tbl_course as c ON d.course_id = c.course_id 
            JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
            WHERE d.user_id != :sid  AND discussion_id  NOT IN (
                                SELECT discussion_id FROM  {$this->discussion_viewed_table} as v 
                                WHERE v.viewed_user_id = :sid 
                            )
            GROUP BY (d.section_item_id) DESC                    
        )  
        AND d.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY) ";
        $count = 0;
        if(count($discussion_section_item) > 0){
            $discussion_sql.= "AND d.section_item_id IN ({$in_criteria}) ";
            $discussion_sql.= " ORDER BY d.date_added DESC";
            $stmt = $this->db->prepare($discussion_sql);
            $stmt->bindParam(':sid', $user_id);  
            $stmt->execute();
            $count = $stmt->fetchColumn();
        }

        if($count > 0){
            $holder["Discussion"] = true;
        }else{
            $holder["Discussion"] = false;
        }


        // $assignment_sql = "SELECT  COUNT(a.assignment_id)
        // FROM {$this->assignment_table}  as a 
        // WHERE a.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY) 
        // AND a.assignment_id NOT IN(
        //     SELECT assignment_id from tbl_viewed_assignments WHERE  viewed_user_id = :uid
        // )";
        $assignment_sql = "SELECT COUNT(a.assignment_id) FROM tbl_assignment as a 
        JOIN tbl_course_section_item as cs ON a.section_item_id = cs.section_item_id
        JOIN tbl_course as c ON c.course_id = cs.course_id
        JOIN tbl_enroll as e ON e.course_id = c.course_id            
        WHERE e.user_id = :uid 
        AND a.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)
        AND a.assignment_id NOT IN (
            SELECT assignment_id from tbl_viewed_assignments WHERE  viewed_user_id = :uid 
            AND date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)
        ) ";
        $stmt = $this->db->prepare($assignment_sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $assign_count = $stmt->fetchColumn();
        if($assign_count > 0){
            $holder["Assignment"] = true;
        }else{
            $holder["Assignment"] = false;
        }    

        $holder["Grade"] = false;


        //Messages
        $message_sql_initiator = "SELECT COUNT(*) FROM `tbl_message_heads` 
        WHERE initiator_id = :auid AND viewed_by_initiator = 0";

        $stmt = $this->db->prepare($message_sql_initiator);
        $stmt->bindParam(':auid', $user_id);
        $stmt->execute();        
        $message_count = $stmt->fetchColumn();
        if($message_count == 0){
            $message_sql_initiator = "SELECT COUNT(*) FROM `tbl_message_heads` 
            WHERE to_id = :auid AND viewed_by_to = 0";
            $stmt = $this->db->prepare($message_sql_initiator);
            $stmt->bindParam(':auid', $user_id);
            $stmt->execute();        
            $message_count = $stmt->fetchColumn();            
        }
        if($message_count > 0){
            $holder["Message"] = true;
        }else{
            $holder["Message"] = false;
        }    



        return $holder;
    }
    //For Red Circles Teacher
    public function GetTeacherMenuStats($user_id){
        $holder = array();

        //Discussion
        $discussion_sql = "SELECT COUNT(d.discussion_id)
        FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id    
        WHERE c.active = 1 AND c.author_user_id = :sid  AND d.discussion_id 
        IN (
        SELECT MAX(d.discussion_id) as discussion_id
        FROM tbl_discussion as d JOIN tbl_course_section_item as cs ON d.section_item_id = cs.section_item_id 
        JOIN tbl_course as c ON d.course_id = c.course_id 
        JOIN tbl_users_detail ud ON d.user_id = ud.user_id 
        WHERE c.author_user_id = :sid  AND discussion_id NOT IN (
                                SELECT discussion_id FROM  {$this->discussion_viewed_table} as v WHERE v.viewed_user_id = :sid 
                            )
        GROUP BY (d.section_item_id) DESC
        ) 
        AND d.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)  AND d.user_id != :sid
        ORDER BY d.date_added DESC";

        $stmt = $this->db->prepare($discussion_sql);
        $stmt->bindParam(':sid', $user_id);  
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count > 0){
            $holder["Discussion"] = true;
        }else{
            $holder["Discussion"] = false;
        }
  
        //Assignments
        $assignment_sql = "SELECT COUNT(a.assignment_id) FROM {$this->assignment_table} as a 
        WHERE a.author_user_id = :uid AND a.active = 1 AND a.assignment_id 
        IN 
        (
            SELECT assignment_id FROM {$this->assignment_submit_table} WHERE date_checked  IS NULL
        )
        ORDER BY a.date_added DESC";
        $stmt = $this->db->prepare($assignment_sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $assign_count = $stmt->fetchColumn();
        if($assign_count > 0){
            $holder["Assignment"] = true;
        }else{
            $holder["Assignment"] = false;
        }    

        //Grades
        $grade_sql = "SELECT COUNT(DISTINCT(ct.category_id))
        FROM tbl_course_section_progress as p 
        JOIN tbl_course_section_item as i ON p.section_item_id = i.section_item_id
        JOIN tbl_course as c ON i.course_id = c.course_id
        JOIN tbl_category as ct ON ct.category_id = c.course_category
        WHERE p.status IN (2,3) AND c.author_user_id = :auid 
        AND p.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)
        AND c.active = 1 AND i.section_item_id NOT IN(
            SELECT section_item_id FROM tbl_viewed_grades WHERE date_added >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)
        )";

        $stmt = $this->db->prepare($grade_sql);
        $stmt->bindParam(':auid', $user_id);
        $stmt->execute();        
        $grade_count = $stmt->fetchColumn();
        if($assign_count > 0){
            $holder["Grade"] = true;
        }else{
            $holder["Grade"] = false;
        }    


        //Messages
        $message_sql_initiator = "SELECT COUNT(*) FROM `tbl_message_heads` 
            WHERE initiator_id = :auid AND viewed_by_initiator = 0";

        $stmt = $this->db->prepare($message_sql_initiator);
        $stmt->bindParam(':auid', $user_id);
        $stmt->execute();        
        $message_count = $stmt->fetchColumn();
        if($message_count == 0){
            $message_sql_initiator = "SELECT COUNT(*) FROM `tbl_message_heads` 
            WHERE to_id = :auid AND viewed_by_to = 0";
            $stmt = $this->db->prepare($message_sql_initiator);
            $stmt->bindParam(':auid', $user_id);
            $stmt->execute();        
            $message_count = $stmt->fetchColumn();            
        }
        if($message_count > 0){
            $holder["Message"] = true;
        }else{
            $holder["Message"] = false;
        }    



        return $holder;
    }


}