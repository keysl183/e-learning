<?php
//Handles Question Bank and Questions
class QuestionBankMapper
{
    private $db = null;
    private $bank_table = 'tbl_question_bank';
    private $quest_type_table = 'tbl_question_type';
    private $quest_ans_mode_table = 'tbl_question_answer_mode';
    private $question_table = 'tbl_question';
    private $question_ans_table = 'tbl_question_answers';
    private $category_table = 'tbl_category';
    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';

    public function __construct($db)
	{
		$this->db = $db;
    }
    
	public function GetQuestionBanks()
	{
        $sql = "SELECT * FROM {$this->bank_table} ORDER BY date_added DESC";
		$stmt = $this->db->query($sql);
		$banks = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $banks;
    }

	public function CountQuestionBanks($session_value)
	{
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($session_value);
        $sql = "SELECT COUNT(*) FROM {$this->bank_table} WHERE author_user_id = :auid AND active = 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':auid',  $member->user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
		return $count;
    }
    //get only the question banks of the author
	public function GetUserQuestionBanks($session_value,$course_section_id = 0, $section_level_id = 0 ,$limit = 0, $page_offset = 0)
	{
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($session_value);


        // $count_sql = "SELECT COUNT(question_bank_id) FROM {$this->bank_table} 
        // WHERE author_user_id = :auid AND active = 1";

        $count_sql ="SELECT COUNT(question_bank_id) FROM {$this->bank_table} as b 
        JOIN {$this->category_table} as c ON b.category_id = c.category_id
        JOIN {$this->category_section_table} as s ON b.course_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON b.category_level_id = l.category_section_level_id 
        WHERE b.author_user_id = :auid AND b.active = 1";
       
        //course_section is actually category_section 
        $sql = "SELECT b.*, c.category_title, s.section_title,l.level_title FROM {$this->bank_table} as b 
        JOIN {$this->category_table} as c ON b.category_id = c.category_id
        JOIN {$this->category_section_table} as s ON b.course_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON b.category_level_id = l.category_section_level_id 
        WHERE b.author_user_id = :auid AND b.active = 1";

        if($course_section_id > 0 && $section_level_id > 0){
        $sql .= " AND b.course_section_id = {$course_section_id} AND b.category_level_id = {$section_level_id}";
        }

        $sql .=" ORDER BY b.question_bank_title ASC ";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $holder  = array();

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':auid',  $member->user_id);
        $stmt->execute();
        $banks = $stmt->fetchAll(PDO::FETCH_OBJ);
        $holder["Results"] = $banks;


        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':auid',  $member->user_id);
        $stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;

		return $holder;
    }
    
    
    //Get Question Bank By Id
    public function GetQuestionBankById($id, $session_value)
	{
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($session_value);

        $sql = "SELECT b.*,cst.category_section_id, cst.section_title  
                FROM {$this->bank_table} as b 
                JOIN {$this->category_section_table} as cst ON b.course_section_id = cst.category_section_id
                WHERE b.question_bank_id = :qbid AND b.author_user_id = :auid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qbid', $id);
        $stmt->bindParam(':auid',  $member->user_id);
        $stmt->execute();
		$bank = $stmt->fetch(PDO::FETCH_OBJ);
		return $bank;
    }
    
    //Get Question Types Current : (text, image , audio)
    public function GetQuestionTypes()
	{
        $sql = "SELECT * FROM {$this->quest_type_table}";
        $stmt = $this->db->query($sql);
        $question_types = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $question_types;
    }

    //Get all of the Question Answer Modes (multi_choice, essay)
    public function GetQuestionAnswerModes()
	{
        $sql = "SELECT * FROM {$this->quest_ans_mode_table}";
        $stmt = $this->db->query($sql);
        $question_types = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $question_types;
    }

    //Get Question Type By Id
    public function GetQuestionTypeById($id)
	{
        $sql = "SELECT * FROM {$this->quest_type_table} WHERE question_type_id = qtid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qtid', $id);
        $stmt->execute();
        $question_type = $stmt->fetch(PDO::FETCH_OBJ);
        return $question_type;
    }

    //Get Question Type By Id
    public function GetQuestionModeById($id)
	{
        $sql = "SELECT * FROM {$this->quest_ans_mode_table} WHERE question_answer_mode_id = qamid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qamid', $id);
        $stmt->execute();
        $question_type = $stmt->fetch(PDO::FETCH_OBJ);
        return $question_type;
    }


    //Get Questions By Bank ID
    public function GetQuestionsByBankID($id, $limit = 0, $page_offset = 0)
	{
        $sql = "SELECT * FROM {$this->question_table} WHERE question_bank_id = :qbid";
        
        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if(page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qbid', $id);
        $stmt->execute();
        $questions = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $questions;
    }

	public function CountBankQuestions($id)
	{
        $sql = "SELECT COUNT(question_id) FROM {$this->question_table} WHERE question_bank_id = :qbid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qbid', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
		return $count;
    }


    //Get Questions By Bank ID
    public function GetQuestionsIdsByBankID($id, $limit = 0, $page_offset = 0)
	{
        $sql = "SELECT question_id,question_title, question_description,question_type_id,question_answer_mode FROM {$this->question_table} WHERE question_bank_id = :qbid";
        
        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qbid', $id);
        $stmt->execute();
        $questions = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $questions;
    }

    //Get Question By Question ID
    public function GetQuestionByID($id, $language = 0)
    {
        $sql = "SELECT * FROM {$this->question_table} as q 
                JOIN  {$this->question_ans_table}  as a ON q.question_id = a.question_id 
                WHERE q.question_id = :qid";
        if($language > 0){
            $sql =   "SELECT  q.question_bank_id, q.question_type_id, tq.trans_question_title as question_title, tq.trans_question_description as question_description,
             q.question_attachment, q.question_answer_mode, q.date_added,
             a.question_choice_attachments, a.correct_answer, tqa.question_choice FROM tbl_question as q 
            JOIN  tbl_question_answers  as a ON q.question_id = a.question_id 
            JOIN tbl_trans_question as tq ON q.question_id = tq.question_id
            JOIN tbl_trans_question_answers as tqa ON tqa.trans_question_id = tq.trans_question_id
            WHERE q.question_id = :qid  AND tq.trans_country_id = :tcid";
        }        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $id);
        if($language > 0){
            $stmt->bindParam(':tcid', $language);
        }
        $stmt->execute();
        $question = $stmt->fetch(PDO::FETCH_ASSOC);
        $question["choices"] = unserialize($question["question_choice"]);
        if(array_key_exists('question_choice_attachments', $question)){
            $question["choices_attachments"] = unserialize($question["question_choice_attachments"]);
        }
        
        return $question;
    }

    //Get Question Answers By Question ID
    public function GetQuestionAnswersByID($id)
    {
        $sql = "SELECT * FROM {$this->question_ans_table} WHERE question_id = :qid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $id);
        $stmt->execute();
        $question_ans = $stmt->fetch(PDO::FETCH_OBJ);
        return $question_ans;
    }

    

}
