<?php 

class CommonMapper
{
    private $db = null;
    public function __construct($db)
    {
        $this->db = $db;
    }


    public function GetCountries()
    {
        $sql = "SELECT * FROM tbl_countries";
        $stmt = $this->db->query($sql);
        $countries = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $countries;
    }    

    public function GetCountryById($country_id)
    {
        $sql = "SELECT * FROM tbl_countries WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $country_id);
        $stmt->execute();
        $countries = $stmt->fetch(PDO::FETCH_OBJ);
        return $countries;
    }

    public function GetCountriesWithLanguage()
    {
        $sql = "SELECT * FROM `tbl_countries` WHERE language !=''";
        $stmt = $this->db->query($sql);
        $countries = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $countries;
    }     

    public function GetEducationLevels()
    {
        $sql = "SELECT * FROM tbl_education_level";
        $stmt = $this->db->query($sql);
        $education_levels = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $education_levels;
    }  
    
    public function GetEducationLevelById($education_id)
    {
        $sql = "SELECT * FROM tbl_education_level WHERE edu_level_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $education_id);
        $stmt->execute();
        $education_level = $stmt->fetch(PDO::FETCH_OBJ);
        return $education_level;
    }        


}


