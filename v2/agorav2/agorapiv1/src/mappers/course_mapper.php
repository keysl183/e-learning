<?php

class CourseMapper
{
    private $db = null;
    private $category_table = 'tbl_category';
    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';
    private $course_table = 'tbl_course';
    private $user_detail_table = 'tbl_users_detail';
    private $course_table_section = 'tbl_course_section';
    private $course_table_item_section = 'tbl_course_section_item';
    private $course_section_progress_table = 'tbl_course_section_progress';

    public function __construct($db)
	{
		$this->db = $db;
    }

    //Get all Active Categories
    public function GetCategoryCourse()
	{
        $sql = "SELECT * FROM {$this->category_table} 
                WHERE active = 1
                ORDER BY category_title ASC";
		$stmt = $this->db->query($sql);
		$categories = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $categories;
    }
    public function GetCategoryCourse2($limit = 0, $page_offset = 0)
	{
        $count_sql = "SELECT COUNT(category_id) FROM {$this->category_table} 
                WHERE active = 1";

        $sql = "SELECT c.*, (
                    SELECT COUNT(cr.course_id) FROM tbl_course as cr WHERE cr.active = 1 AND cr.course_category = c.category_id 
                    ) as course_count 
                FROM {$this->category_table}  as c
                WHERE c.active = 1
                ORDER BY c.category_title ASC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        }      

        $holder  = array();           
		$stmt = $this->db->query($sql);
        $categories = $stmt->fetchAll(PDO::FETCH_OBJ);
        $holder["Results"] =  $categories;


        $stmt = $this->db->prepare($count_sql);
        $stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;


		return $holder;
    }

    public function GetCategoryCourse3()
	{
        $sql = "SELECT * FROM {$this->category_table} 
                WHERE active = 1
                ORDER BY date_added ASC";
        

        $cate_section_sql = "SELECT * FROM {$this->category_section_table} 
                WHERE category_id = :cid ORDER BY section_title ASC";


        $stmt = $this->db->query($sql);
        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $new_categories = array();

        $stmt = $this->db->prepare($cate_section_sql);
        foreach($categories as $category){
            $stmt->bindParam(':cid', $category["category_id"]);
            $stmt->execute();
            $category["sections"] =$stmt->fetchAll(PDO::FETCH_ASSOC);
            array_push($new_categories, $category);
        }
		return $new_categories;
    }    
    

	public function GetCategoryCourseByID($id)
	{
        $sql = "SELECT * FROM {$this->category_table} 
                WHERE category_id = :cid AND active = 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $id);
        $stmt->execute();
		$bank = $stmt->fetch(PDO::FETCH_OBJ);
		return $bank;
    }    

    public function GetCategorySection($category_id)
	{
        $sql = "SELECT * FROM {$this->category_section_table} 
                WHERE category_id = :cid ORDER BY section_title ASC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $category_id);
        $stmt->execute();
        $category_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $item_sql = "SELECT * FROM {$this->category_section_level_table} 
        WHERE category_section_id = :csid ORDER BY level_title ASC";
        $stmt = $this->db->prepare($item_sql);
        $new_category_section = array();
        foreach($category_sections as $section){
            $stmt->bindParam(':csid', $section['category_section_id']);
            $stmt->execute();
            $section_items = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $new_section_items = array();
            //levels
            foreach($section_items as $item){
                array_push($new_section_items, $item);
            }
            $section['section_item'] = $new_section_items;
            array_push($new_category_section, $section);
        }
		return $new_category_section;
    }
    //Singular 
    public function GetACategorySection($category_section_id, $isStudentLoggedIn = false, $student_user_id = 0)
	{
        $sql = "SELECT * FROM {$this->category_section_table} 
                WHERE category_section_id = :csid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':csid', $category_section_id);
        $stmt->execute();
        $category_section = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $item_sql = "SELECT * FROM {$this->category_section_level_table} 
        WHERE category_section_id = :csid ORDER BY level_title ASC";
        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':csid', $category_section_id);
        $stmt->execute();
        $levels =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $course_detail_sql = "SELECT course_id,course_title, course_description, course_category
         category_section_id, category_section_level_id, course_cover_image, no_of_questions, passing_rate,retake_allowed,
         time_limit,student_enrolled  FROM {$this->course_table}
        WHERE category_section_level_id = :cslvid AND active = 1";


        $new_levels = array();
        foreach($levels as $level){
            // $stmt = $this->db->prepare($course_detail_sql);
            // $stmt->bindParam(':cslvid', $level['category_section_level_id']);
            // $stmt->execute();
            // $courses =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            // $new_courses = array();

            // //Get the course section// this were the previos section implementation.
            // //To avoid heavy customization it was retained but becomes static
            // //Each Course will have One Section
            // foreach($courses as $course){
            //     $course_section_detail = array();
            //     if(!$isStudentLoggedIn){
            //         $course_section_detail = $this->GetCourseSectionByCourseID($course["course_id"]);
            //     }
            //     else{
            //         $course_section_detail = $this->GetCourseSectionByCourseIDAndStudentID($course["course_id"], $student_user_id);
            //     }
              
            //     $course["course_section"] = $course_section_detail;

            //     //TODO Implement the Main Exam Details from course_section_detail
            //     array_push($new_courses, $course);
            // }

            // $level["course_detail"] = $new_courses;
            // k

            array_push($new_levels, $level);           
        }

        $category_section['section_item']  = $new_levels;
 
		return $category_section;
    }
    
    public function GetCategorySectionChapterItems($category_section_id, $category_section_level_id ,$limit = 0, $page_offset = 0){
        
        $count_sql = "SELECT COUNT(DISTINCT(chapter_no)) FROM {$this->course_table} 
                        WHERE category_section_id = :csid 
                        AND category_section_level_id = :cslvlid AND  active = 1";      
        
        $sql = "SELECT DISTINCT (chapter_no), course_cover_image FROM {$this->course_table} 
                    WHERE category_section_id = :csid AND category_section_level_id = :cslvlid AND  active = 1
                    AND chapter_no != ''
                    ORDER BY chapter_no ASC";
        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $holder  = array();

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':csid', $category_section_id );
        $stmt->bindParam(':cslvlid', $category_section_level_id);
        $stmt->execute();
        $chapters =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $holder["Results"] = $chapters;


        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':csid', $category_section_id );
        $stmt->bindParam(':cslvlid', $category_section_level_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;


        return $holder;
    }

    public function GetACategorySectionChapters($category_section_id, $category_section_level_id, $isStudentLoggedIn = false, $student_user_id = 0 ,$limit = 0, $page_offset = 0){
        $item_sql = "SELECT * FROM {$this->category_section_level_table} 
        WHERE category_section_level_id = :cslvlid ORDER BY level_title ASC";

        $course_detail_sql = "SELECT course_id,course_title, course_description, course_category
        category_section_id, category_section_level_id,chapter_no, course_cover_image, no_of_questions, passing_rate,retake_allowed,
        time_limit,student_enrolled  FROM {$this->course_table}
        WHERE category_section_level_id = :cslvid AND chapter_no = :chno AND active = 1";      

        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':cslvlid', $category_section_level_id);
        $stmt->execute();
        $level =  $stmt->fetch(PDO::FETCH_ASSOC);
        
        $chapters = $this->GetCategorySectionChapterItems($category_section_id, $category_section_level_id, $limit, $page_offset);
        $new_chapters = array();

        $level["PageCount"] = $chapters["PageCount"];
        $level["TotalCount"] = $chapters["TotalCount"];

        foreach($chapters["Results"] as $chapter){
            $stmt = $this->db->prepare($course_detail_sql);
            $stmt->bindParam(':cslvid', $level['category_section_level_id']);
            $stmt->bindParam(':chno', $chapter['chapter_no']);
            $stmt->execute();
            $courses =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            $new_courses = array();

            foreach($courses as $course){
                $course_section_detail = array();
                if(!$isStudentLoggedIn){
                    $course_section_detail = $this->GetCourseSectionByCourseID($course["course_id"]);
                }
                else{
                    $course_section_detail = $this->GetCourseSectionByCourseIDAndStudentID($course["course_id"], $student_user_id);
                }
                $course["course_section"] = $course_section_detail;
                array_push($new_courses, $course);
            }
            $chapter["course_detail"] = $new_courses;
            array_push($new_chapters, $chapter);     
        }

        $level["chapter_details"] = $new_chapters;
        
        return $level;
    }

	public function GetCourseDetailByID($id)
	{
        $sql = "SELECT c.course_title, c.course_excerpt, c.course_description, 
        c.no_of_questions, c.passing_rate, c.retake_allowed, c.time_limit, c.chapter_no,c.question_bank_id,
        CONCAT(u.fname, ' ', u.lname) as author, c.date_added,c.course_cover_image,
        c.student_enrolled, ct.category_id, ct.category_title, cst.category_section_id, cst.section_title, cslt.category_section_level_id,cslt.level_title
        FROM {$this->course_table}  as c 
        JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
        JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
        JOIN {$this->category_section_table} as cst ON c.category_section_id = cst.category_section_id
        JOIN {$this->category_section_level_table} as cslt ON c.category_section_level_id = cslt.category_section_level_id
        WHERE c.course_id = :cid";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $id);
        $stmt->execute();
		$bank = $stmt->fetch(PDO::FETCH_OBJ);
		return $bank;
    }
    
    public function GetCourses($cate = 0, $sort = 1, $limit = 0, $page_offset = 0)
    {
        $sql = "SELECT  c.course_id, c.course_title, c.course_excerpt, c.course_description, c.course_cover_image,
        CONCAT(u.fname, ' ', u.lname) as author, c.date_added,c.student_enrolled, ct.category_id, ct.category_title  
        FROM {$this->course_table}  as c 
        JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
        JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
        WHERE c.active = 1 ";

        if($cate > 0){
            $sql .= " AND ct.category_id = ".$cate;//haha dayaaa
        }

        if($sort == 1){
            //popular
            $sql  .= " ORDER BY c.student_enrolled DESC ";
        }else if($sort == 2){
            //Latest
            $sql  .= " ORDER BY c.date_added DESC ";
        }

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 

        try{
            $stmt = $this->db->prepare($sql);

            $stmt->execute();
            $courses = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $courses;     

        }catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }
    
    public function GetCoursesOfaTeacher($teacher_id, $limit = 0, $page_offset = 0)
    {

        $count_sql = "SELECT  COUNT(c.course_id)
        FROM {$this->course_table}  as c 
        JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
        JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
        JOIN {$this->category_section_table} as cst ON c.category_section_id = cst.category_section_id
        JOIN {$this->category_section_level_table} as cslt ON c.category_section_level_id = cslt.category_section_level_id
        WHERE c.author_user_id = {$teacher_id} AND c.active = 1";

        $sql = "SELECT c.course_id, c.course_title, c.course_excerpt, c.course_description, c.course_cover_image, c.no_of_questions, c.passing_rate,
        CONCAT(u.fname, ' ', u.lname) as author, c.date_added, ct.category_id, ct.category_title  ,cst.section_title,cslt.level_title
        FROM {$this->course_table}  as c 
        JOIN {$this->user_detail_table} as u ON c.author_user_id = u.user_id 
        JOIN {$this->category_table} as ct ON c.course_category = ct.category_id
        JOIN {$this->category_section_table} as cst ON c.category_section_id = cst.category_section_id
        JOIN {$this->category_section_level_table} as cslt ON c.category_section_level_id = cslt.category_section_level_id        
        WHERE c.author_user_id = {$teacher_id} AND c.active = 1
        ORDER BY c.date_added DESC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 

        try{
            $holder  = array();

            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $courses = $stmt->fetchAll(PDO::FETCH_OBJ);
            $holder["Results"] = $courses;


            $stmt = $this->db->prepare($count_sql);
            $stmt->execute();      
            $count = $stmt->fetchColumn();
            $holder["TotalCount"] = $count;

            $totalPages = 0;
            if(is_array($holder["Results"]) && $limit > 0){
                $totalPages = ceil( $holder["TotalCount"]  / $limit);
            }
            $holder["PageCount"] = $totalPages;

            return $holder;     

        }catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }
    //For Teacher
    public function GetCourseSectionByCourseID($course_id)
	{
        $sql = "SELECT * FROM {$this->course_table_section} 
                WHERE course_id = :cid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();
        $course_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $item_sql = "SELECT * FROM {$this->course_table_item_section} 
        WHERE course_id = :cid AND section_id = :csid";
        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':cid', $course_id);
        $new_course_section = array();
        foreach($course_sections as $section){
            $stmt->bindParam(':csid', $section['course_section_id']);
            $stmt->execute();
            $section_items = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $new_section_items = array();

            foreach($section_items as $item){
                $tmp_unsrlz = unserialize($item['ut_serialize_ws_id']);
                if($tmp_unsrlz){
                    $item['WorksheetsIDs'] = $tmp_unsrlz;
                }else{
                    $item['WorksheetsIDs'] = array();
                }
               
                array_push($new_section_items, $item);
            }

            $section['section_item'] = $new_section_items;
            array_push($new_course_section, $section);
        }
		return $new_course_section;
    }
    //For Student
    public function GetCourseSectionByCourseIDAndStudentID($course_id, $student_user_id)
	{
        $sql = "SELECT * FROM {$this->course_table_section} 
                WHERE course_id = :cid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();
        $course_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $item_sql = "SELECT * FROM {$this->course_table_item_section} 
        WHERE course_id = :cid AND section_id = :csid";
       
       

        $progress_sql = "SELECT * FROM {$this->course_section_progress_table}
                        WHERE section_item_id = :stid AND user_id = :uid AND active = 1 
                        ORDER BY date_added DESC LIMIT 1 ";

        $retake_sql = "SELECT COUNT(*) as total_retakes FROM {$this->course_section_progress_table}
                        WHERE section_item_id = :stid AND user_id = :uid AND active = 1 
                        ";                        

        $language_sql = "SELECT tq.trans_country_id, tq.question_id, c.id as country_id , c.country_name, 
                        c.language, COUNT(*) as translated_count 
                        FROM `tbl_trans_question` as tq JOIN tbl_question as q ON tq.question_id = q.question_id 
                        JOIN tbl_countries as c ON tq.trans_country_id = c.id WHERE q.question_bank_id = :qbid 
                        GROUP BY tq.trans_country_id";
     

        $new_course_section = array();
        foreach($course_sections as $section){
            $stmt = $this->db->prepare($item_sql);
            $stmt->bindParam(':cid', $course_id);
            $stmt->bindParam(':csid', $section['course_section_id']);
            $stmt->execute();
            $section_items =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            //FOR PROGRESS
            //REFACTOR THIS LATER
            $new_section_item = array();
            $is_section_unit_test_passed = 0;
            foreach($section_items as $sec_item){
                $stmt = $this->db->prepare($progress_sql);
                $stmt->bindParam(':stid',  $sec_item["section_item_id"]);
                $stmt->bindParam(':uid', $student_user_id);
                $stmt->execute();
                $section_item_progress =  $stmt->fetch(PDO::FETCH_OBJ);

                $stmt = $this->db->prepare($retake_sql);
                $stmt->bindParam(':stid',  $sec_item["section_item_id"]);
                $stmt->bindParam(':uid', $student_user_id);
                $stmt->execute();
                $retake_count =  $stmt->fetchColumn();
                $sec_item["student_retakes"]  =  $retake_count;


                if($section_item_progress == null){
                    $sec_item["user_progress"]  = null;
                   
                }else{
                    $sec_item["user_progress"] = $section_item_progress;
                    //Check the Unit Test if pass
                    if($sec_item["item_type"]== 2 && $section_item_progress->pass_failed == 1){
                        $is_section_unit_test_passed = 1;
                    }
                }
           
                //Get the languages available for the section
                //Check if there is ample no of trans question
                if($sec_item["item_type"]==1){//Worksheet
                    $new_languages = array();
                    $stmt = $this->db->prepare($language_sql);
                    $stmt->bindParam(':qbid',  $sec_item["ws_bank_id"]);
                    $stmt->execute();
                    $languages =  $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $sec_item["languages"] = array();
                    foreach($languages as $lang){
                        if($lang["translated_count"] >= $sec_item["no_of_question"]){
                            array_push($sec_item["languages"], $lang);
                        }
                    }
                }  


                array_push($new_section_item, $sec_item);
            }

            $section['unit_test_pass_failed'] = $is_section_unit_test_passed;
            $section['section_item'] = $new_section_item;

            array_push($new_course_section, $section);
        }

		return $new_course_section;
    }    
    public function GetCourseSectionItemBySectionItemID($course_id, $section_item_id)
	{
        $item_sql = "SELECT * FROM {$this->course_table_item_section} 
        WHERE course_id = :cid AND section_item_id = :stid LIMIT 1";
        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->execute();
        $section_item = $stmt->fetch(PDO::FETCH_OBJ);

		return $section_item;
    } 
    
    public function GetCourseSectionItemBySectionItemIDForPost($section_item_id)
	{
        $item_sql = "SELECT * FROM {$this->course_table_item_section} 
        WHERE  section_item_id = :stid LIMIT 1";
        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->execute();
        $section_item = $stmt->fetch(PDO::FETCH_OBJ);

		return $section_item;
    }     

    public function GetCourseSectionItemWorksheetsOnly($section_id)
	{
        $item_sql = "SELECT * FROM tbl_course_section_item WHERE section_id = :stid AND item_type = 1";
        $stmt = $this->db->prepare($item_sql);
        $stmt->bindParam(':stid', $section_id);
        $stmt->execute();
        $section_items = $stmt->fetchAll(PDO::FETCH_OBJ);

		return $section_items;
    }    
}

