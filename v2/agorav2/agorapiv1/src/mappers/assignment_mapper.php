<?php

class AssignmentMapper
{
    private $db = null;
    private $category_table = 'tbl_category';
    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';
    private $course_table = 'tbl_course';
    private $course_item_section_table = 'tbl_course_section_item';
    private $assignment_table = 'tbl_assignment';
    private $assignment_submit_table = 'tbl_assingment_submission';


    private $question_table = 'tbl_question';
    private $question_ans_table = 'tbl_question_answers';  



    public function __construct($db)
	{
		$this->db = $db;
    }

    public function GetTeacherAssignments($user_id,  $limit = 0, $page_offset = 0)
	{
        $count_sql = "SELECT COUNT(a.assignment_id) FROM {$this->assignment_table} as a 
                JOIN {$this->category_table} as c ON a.category_id = c.category_id
                JOIN {$this->category_section_table} as s ON a.section_id = s.category_section_id 
                JOIN {$this->category_section_level_table} as l ON a.level_id = l.category_section_level_id     
                WHERE a.author_user_id = :sid AND a.active = 1";

        $sql = "SELECT a.* , c.category_title, s.section_title,l.level_title FROM {$this->assignment_table}  as a
                JOIN {$this->category_table} as c ON a.category_id = c.category_id
                JOIN {$this->category_section_table} as s ON a.section_id = s.category_section_id 
                JOIN {$this->category_section_level_table} as l ON a.level_id = l.category_section_level_id         
                WHERE a.author_user_id = :sid AND a.active = 1
                ORDER BY a.date_added DESC";


        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        $holder  = array();

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();
        $holder["Results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':sid', $user_id);
        $stmt->execute();      
        $count = $stmt->fetchColumn();
        $holder["TotalCount"] = $count;

        $totalPages = 0;
        if(is_array($holder["Results"]) && $limit > 0){
            $totalPages = ceil( $holder["TotalCount"]  / $limit);
        }
        $holder["PageCount"] = $totalPages;
		return $holder;
    }  

    public function GetTeacherAssignmentByID($assignment_id)
	{
        $sql = "SELECT a.title as assignment_title, a.*, i.title, l.level_title, s.section_title 
        FROM {$this->assignment_table}  as a
        JOIN tbl_course_section_item as i 
        ON a.section_item_id = i.section_item_id
        JOIN {$this->category_section_table} as s 
        ON a.section_id = s.category_section_id
        JOIN {$this->category_section_level_table} as l
        ON a.level_id = l.category_section_level_id
        WHERE  a.assignment_id = :aid AND a.active = 1
        ORDER BY date_added DESC";                
        $stmt = $this->db->prepare($sql);
        // $stmt->bindParam(':sid', $user_id);
        $stmt->bindParam(':aid', $assignment_id);
        $stmt->execute();
        $assignment = $stmt->fetch();
        $assignment["questions"] = unserialize($assignment["srlz_questions"]);
		return $assignment;
    }    

    public function GetAssignmentSubmissions($assignment_id,  $limit = 0, $page_offset = 0){
       
        $count_sql = "SELECT COUNT(*) FROM {$this->assignment_submit_table}  as s 
        JOIN tbl_users_detail as ud ON s.user_id = ud.user_id
        WHERE s.assignment_id =:assid";
        
        $sql = "SELECT s.*, ud.user_id, ud.fname, ud.lname, ud.profile_image FROM {$this->assignment_submit_table}  as s 
        JOIN tbl_users_detail as ud ON s.user_id = ud.user_id
        WHERE s.assignment_id =:assid ORDER BY s.date_added DESC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        }  

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':assid', $assignment_id);
        $stmt->execute();
        $submissions = array();
        
        $submissions["Submissions"] =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $this->db->prepare($count_sql);
        $stmt->bindParam(':assid', $assignment_id);
        $stmt->execute();
        $counts =  $stmt->fetchColumn();


        $submissions["TotalCount"] = $counts;
        
		return $submissions;      
    }

    //Get the recent assignment submissions for teacher
    public function GetRecentAssignmentSubmissions($user_id){
        $sql = "SELECT c.category_title,i.title,i.item_type, a.assignment_id,a.section_item_id, a.category_id, 
        a.title, a.description, a.cover_photo FROM {$this->assignment_table} as a 
        JOIN {$this->category_table} as c ON a.category_id = c.category_id 
        JOIN {$this->course_item_section_table} as i ON i.section_item_id = a.section_item_id
        JOIN {$this->course_table} as ct ON ct.course_id = i.course_id
        WHERE a.author_user_id = :uid AND a.active = 1 
        AND ct.active = 1
        AND a.assignment_id 
        IN 
        (
            SELECT assignment_id FROM {$this->assignment_submit_table} WHERE date_checked  IS NULL
        )
        ORDER BY a.date_added DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);        
    }










    //STUDENT

    //Get the assignments associated to the user
    public function GetStudentAssignment($user_id){
        $sql = "SELECT a.*,c.course_cover_image, ct.category_title, s.section_title,l.level_title 
        ,
        (
            SELECT COUNT(assignment_id) from tbl_viewed_assignments as v 
            WHERE  a.assignment_id = v.assignment_id AND  viewed_user_id = :uid 
        ) as viewed
        FROM tbl_assignment as a 
        JOIN tbl_course_section_item as cs ON a.section_item_id = cs.section_item_id
        JOIN tbl_course as c ON c.course_id = cs.course_id
        JOIN tbl_enroll as e ON e.course_id = c.course_id
        JOIN {$this->category_table} as ct ON a.category_id = ct.category_id
        JOIN {$this->category_section_table} as s ON a.section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON a.level_id = l.category_section_level_id             
        WHERE e.user_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
		$assignments = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $assignments;        

    }
    public function GetStudentAssignmentByAssignID($user_id, $assignment_id){
        $sql = "SELECT a.title as assignment_title, a.*, i.title , l.level_title, s.section_title 
        FROM {$this->assignment_table}  as a
        JOIN tbl_course_section_item as i    ON a.section_item_id = i.section_item_id
        JOIN {$this->category_section_table} as s ON a.section_id = s.category_section_id
        JOIN {$this->category_section_level_table} as l ON a.level_id = l.category_section_level_id        
        WHERE  a.assignment_id = :aid AND a.active = 1
        ORDER BY date_added DESC";                
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':aid', $assignment_id);
        $stmt->execute();
        $assignment = $stmt->fetch();
        
        $submission_sql = "SELECT * FROM {$this->assignment_submit_table} WHERE
        user_id = :uid AND assignment_id = :aid LIMIT 1";
        $stmt = $this->db->prepare($submission_sql);
        $stmt->bindParam(':aid', $assignment_id);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $submission = $stmt->fetch();
        $assignment["submission"] = $submission;
        $assignment["questions"] = unserialize($assignment["srlz_questions"]);
		return $assignment;        
    }

    public function GetRecentAssignmentsForStudent($user_id){

        $sql = "SELECT a.assignment_id, a.category_id, a.section_id, a.level_id, a.section_item_id, a.title, 
        a.description, a.cover_photo, a.date_added
        FROM {$this->assignment_table}  as a 
        JOIN tbl_course_section_item as cs ON a.section_item_id = cs.section_item_id
        JOIN tbl_course as c ON c.course_id = cs.course_id
        JOIN tbl_enroll as e ON e.course_id = c.course_id
        WHERE a.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND e.user_id = :uid 
        AND a.assignment_id NOT IN(
            SELECT assignment_id from tbl_viewed_assignments WHERE  viewed_user_id = :uid
        )
        ORDER BY date_added DESC LIMIT 5";
           try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid', $user_id);
            $stmt->execute();
            
            return  $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $e)
        {
            var_dump($e->getMessage());
        }       
    }

    public function GetStudentQuestionsToAnswers($user_id, $assignment_id,  $limit = 0, $page_offset = 0){
        $holder = array();
        $assignment = $this->GetStudentAssignmentByAssignID($user_id, $assignment_id);
        $question_ids = $assignment["questions"];
        $unser_answers = unserialize($assignment["submission"]["answers"]);
        $new_question_ids = array();
        $new_question_ids_with_answers = array();
        $index = 0;

        foreach($question_ids as $id){
            array_push($new_question_ids, $id);
            $temp = array(
                'id'=> $id,
                'ans_index' => $unser_answers[$index]
            );

            array_push($new_question_ids_with_answers, $temp);
            $index++;
        }
        $str_question_ids = implode(", ", $new_question_ids);          
        

        $count_question_sql = "SELECT COUNT(*) FROM {$this->question_table} as q 
        JOIN  {$this->question_ans_table}  as a ON q.question_id = a.question_id 
        WHERE q.question_id  IN ({$str_question_ids})  ";
        $stmt = $this->db->prepare($count_question_sql);
        $stmt->execute();   
        $question_count = $stmt->fetchColumn();
        $holder["TotalCount"] = $question_count;
        

        $question_sql = "SELECT * FROM {$this->question_table} as q 
        JOIN  {$this->question_ans_table}  as a ON q.question_id = a.question_id 
        WHERE q.question_id  IN ({$str_question_ids})  ";

        if($limit > 0)
        {
            $question_sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $question_sql .= " OFFSET {$total_offset}";
            } 
        }             
            
        
        $stmt = $this->db->prepare($question_sql);
        $stmt->execute();   
        $questions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $new_questions = array();
        foreach($questions as $q){
            $q['choice'] = unserialize($q['question_choice']);
            $q['attachments'] = unserialize($q['question_choice_attachments']);
            $answer_index = array_search($q['question_id'], $new_question_ids);
            $q['selected_answer'] =  $new_question_ids_with_answers[$answer_index]["ans_index"];
            array_push($new_questions, $q);
        }
        
        $holder["Questions"]= $new_questions; 
        $holder["IdtoAnswer"] = $new_question_ids_with_answers;

        return $holder;
    }

    
    
    



}