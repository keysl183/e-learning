<?php

class GradeSchemaMapper
{
    private $db = null;
    private $grade_schema_table = 'tbl_grade_schema';

    private $course_table_section = 'tbl_course_section';
    private $course_table_item_section = 'tbl_course_section_item';
    private $course_section_progress_table = 'tbl_course_section_progress';   

    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';

    private $question_table = 'tbl_question';
    private $question_ans_table = 'tbl_question_answers';    
    
    public function __construct($db)
	{
		$this->db = $db;
    }    

    public function GetGradeSchemaByCourseID($course_id){
        $sql = "SELECT * FROM {$this->grade_schema_table} 
                WHERE course_id = :cid
                ORDER BY grade_to DESC";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();
		$schema = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $schema;      
    }
    //Get Single Schema
    //Used in editing
    public function GetAGradeSchemaByCourseIDAndSchemaID($course_id, $schema_id){
        $sql = "SELECT * FROM {$this->grade_schema_table} 
                WHERE course_id = :cid AND g_schema_id = :scid
                ORDER BY date_added ASC";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->bindParam(':scid', $schema_id);
        $stmt->execute();
		$schema = $stmt->fetch(PDO::FETCH_OBJ);
		return $schema;      
    }    

    //TODO PAGER
    public function GetSectionItemsForGrade($user_id,  $limit = 0, $page_offset = 0){
        $count_sql = "SELECT COUNT(*) FROM tbl_course_section_item as i 
        JOIN tbl_course as c ON c.course_id = i.course_id
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id              
        WHERE c.active = 1 AND c.author_user_id = :auid";

        $sql = "SELECT i.section_item_id, i.section_item_id, i.course_id, i.section_id, i.item_type, c.course_category as category_id, 
        i.title, i.description, s.section_title,l.level_title
        FROM tbl_course_section_item as i 
        JOIN tbl_course as c ON c.course_id = i.course_id
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id          
        WHERE c.active = 1 AND c.author_user_id = :auid ORDER BY i.date_added DESC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':auid', $user_id);
            $stmt->execute();
            $holder  = array();
            $holder["Results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = $this->db->prepare($count_sql);
            $stmt->bindParam(':auid', $user_id);
            $stmt->execute();      
            $count = $stmt->fetchColumn();
            $holder["TotalCount"] = $count;


            $totalPages = 0;
            if(is_array($holder["Results"]) && $limit > 0){
                $totalPages = ceil( $holder["TotalCount"]  / $limit);
            }
            $holder["PageCount"] = $totalPages;



            return $holder;  
        }catch(PDOException $e){
            var_dump($e);
        }
    }


    public function GetSectionItemsForGradeByCategorySection($user_id,$category_section_id, $category_level_id, 
                                                                $chapter_no,  $limit = 0, $page_offset = 0){
        $count_sql = "SELECT COUNT(*) FROM tbl_course_section_item as i 
        JOIN tbl_course as c ON c.course_id = i.course_id
        JOIN {$this->category_section_table} as s ON c.category_section_id = s.category_section_id 
        JOIN {$this->category_section_level_table} as l ON c.category_section_level_id = l.category_section_level_id              
        WHERE c.active = 1 AND c.author_user_id = :auid  AND c.category_section_id = :csid AND c.chapter_no = :chno 
        AND l.category_section_level_id = :cslid";

        $sql = "SELECT i.section_item_id, i.section_item_id, i.course_id, i.section_id, i.item_type, c.course_category as category_id, 
        i.title, i.description, s.section_title,l.level_title
        FROM tbl_course_section_item as i 
        JOIN tbl_course as c ON c.course_id = i.course_id
        JOIN tbl_category_section  as s ON c.category_section_id = s.category_section_id 
        JOIN tbl_category_section_level as l ON c.category_section_level_id = l.category_section_level_id          
        WHERE c.active = 1 AND c.author_user_id = :auid 
        AND c.category_section_id = :csid AND c.chapter_no = :chno  
        AND l.category_section_level_id = :cslid ORDER BY i.title ASC";

        if($limit > 0)
        {
            $sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $sql .= " OFFSET {$total_offset}";
            } 
        } 
        try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':auid', $user_id);
            $stmt->bindParam(':csid', $category_section_id);
            $stmt->bindParam(':chno', $chapter_no);
            $stmt->bindParam(':cslid', $category_level_id);
            $stmt->execute();
            $holder  = array();
            $holder["Results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = $this->db->prepare($count_sql);
            $stmt->bindParam(':auid', $user_id);
            $stmt->bindParam(':csid', $category_section_id);
            $stmt->bindParam(':chno', $chapter_no);
            $stmt->bindParam(':cslid', $category_level_id);            
            $stmt->execute();      
            $count = $stmt->fetchColumn();
            $holder["TotalCount"] = $count;


            $totalPages = 0;
            if(is_array($holder["Results"]) && $limit > 0){
                $totalPages = ceil( $holder["TotalCount"]  / $limit);
            }
            $holder["PageCount"] = $totalPages;



            return $holder;  
        }catch(PDOException $e){
            var_dump($e);
        }
    }

    public function GetRecentSubmissions($user_id){
        
        //Get 3 days only
        //add the not in later
        $distinct_category_sql = "SELECT DISTINCT(ct.category_id),  ct.category_title,  ct.category_desc
        FROM tbl_course_section_progress as p 
        JOIN tbl_course_section_item as i ON p.section_item_id = i.section_item_id
        JOIN tbl_course as c ON i.course_id = c.course_id
        JOIN tbl_category as ct ON ct.category_id = c.course_category
        WHERE p.status IN (2,3) AND c.author_user_id = :auid 
        AND p.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)
        AND c.active = 1 AND i.section_item_id NOT IN(
            SELECT section_item_id FROM tbl_viewed_grades WHERE date_added >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)
        ) ORDER BY ct.category_title ASC";

        $stmt = $this->db->prepare($distinct_category_sql);
        $stmt->bindParam(':auid', $user_id);
        $stmt->execute();
        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $new_category = array();
        foreach($categories as $category){
            $category["Items"] = $this->GetRecentSubmissionItems($category["category_id"], $user_id);
            array_push($new_category, $category);
        }

        return  $new_category;
    }
    //Helper method for GetRecentSubmission,
    //Get Course Section Items and it's corresponding number of submissions
    private function GetRecentSubmissionItems($category_id, $user_id){
        $section_item_sql = "SELECT ct.category_id, 
        ct.category_title, c.course_id, c.category_section_id, c.category_section_level_id, 
        c.course_title, c.chapter_no, i.section_item_id, i.course_id, i.title,i.description , i.item_type, p.section_progress_id, COUNT(*) as new_count,
        cts.section_title, ctsl.level_title
        FROM tbl_course_section_progress as p 
        JOIN tbl_course_section_item as i ON p.section_item_id = i.section_item_id
        JOIN tbl_course as c ON i.course_id = c.course_id
        JOIN tbl_category as ct ON ct.category_id = c.course_category
        JOIN tbl_category_section as cts  ON cts.category_section_id =  c.category_section_id
        JOIN tbl_category_section_level as ctsl ON ctsl.category_section_level_id = c.category_section_level_id
        WHERE p.status IN (2,3) AND c.author_user_id = :auid
        AND ct.category_id = :cateid
        AND p.date_added >= DATE_ADD(CURDATE(), INTERVAL -3 DAY)
        AND c.active = 1 
        AND i.section_item_id NOT IN(
            SELECT section_item_id FROM tbl_viewed_grades WHERE date_added >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)
        )
        GROUP BY i.section_item_id";
        $stmt = $this->db->prepare($section_item_sql);
        $stmt->bindParam(':auid', $user_id);
        $stmt->bindParam(':cateid', $category_id);
        $stmt->execute();        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
     
    //TODO PAGER FIX 07142018
    public function GetGradesForCourse($section_item_id, $limit = 0, $page_offset = 0){

        $count_distinct_users_sql = "SELECT COUNT(DISTINCT(ud.user_id)) 
        FROM tbl_course_section_progress as cs JOIN tbl_course_section_item as i ON cs.section_item_id = i.section_item_id 
        JOIN tbl_users_detail as ud ON cs.user_id = ud.user_id 
        WHERE cs.status IN (2,3) AND i.section_item_id =  :stid";

        $distinct_users_sql = "SELECT DISTINCT(ud.user_id), ud.* 
        FROM tbl_course_section_progress as cs JOIN tbl_course_section_item as i ON cs.section_item_id = i.section_item_id 
        JOIN tbl_users_detail as ud ON cs.user_id = ud.user_id 
        WHERE cs.status IN (2,3) AND i.section_item_id =  :stid";

        $sql = "SELECT i.course_id,i.item_type,cs.section_progress_id, cs.user_id, cs.section_item_id, cs.score, 
        i.no_of_question, cs.pass_failed, cs.status,cs.date_added ,ud.fname, ud.lname, ud.profile_image  
        FROM tbl_course_section_progress as cs 
        JOIN tbl_course_section_item as i ON cs.section_item_id = i.section_item_id
        JOIN tbl_users_detail as ud ON cs.user_id = ud.user_id
        WHERE cs.status IN (2,3)  AND i.section_item_id = :stid AND ud.user_id = :uid
        ORDER BY cs.date_added ASC  ";


        if($limit > 0)
        {
            // $sql .= " LIMIT {$limit}";
            $distinct_users_sql .= " LIMIT {$limit}";
            if($page_offset > 0)
            {
                $total_offset = $limit * $page_offset;
                $distinct_users_sql .= " OFFSET {$total_offset}";
            } 
        } 
        try{
            $stmt = $this->db->prepare($distinct_users_sql);
            $stmt->bindParam(':stid', $section_item_id);
            $stmt->execute();
            $users=  $stmt->fetchAll();        
            $new_users = array();
            foreach($users as $user){
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':stid', $section_item_id);
                $stmt->bindParam(':uid', $user['user_id']);
                $stmt->execute();
                $grades =  $stmt->fetchAll(PDO::FETCH_OBJ);
                $user["Grades"] = $grades;
                array_push($new_users, $user);
            }


            $stmt = $this->db->prepare($count_distinct_users_sql);
            $stmt->bindParam(':stid', $section_item_id);
            $stmt->execute();
            $count =  $stmt->fetchColumn();     

            // $users["TotalCount"] = $count;
            $holder = array (
                'Users'=> $new_users,
                "TotalCount" => $count
            );
            return $holder;
        }catch(PDOException $e){
            var_dump($e);
        }
    }


    //Get the Result of Main Exam
    public function GetViewMainResultStudent($course_id, $user_id, $progress_id){

        $main_exam = "SELECT p.* FROM `tbl_course_section_progress` as p
        JOIN tbl_course_section_item as s ON p.section_item_id = s.section_item_id
        WHERE p.user_id = :uaid  AND p.status IN (2,3) AND p.active = 1 
        AND s.course_id = :cid AND  section_progress_id = :spid ORDER BY p.date_added DESC LIMIT 1";
        $stmt = $this->db->prepare($main_exam);
        $stmt->bindParam(':cid', $course_id);
        $stmt->bindParam(':uaid', $user_id);
        $stmt->bindParam(':spid', $progress_id);
        $stmt->execute();
        $main_result = $stmt->fetch(PDO::FETCH_ASSOC);

       
        if($main_result){
            $main_result["uns_answers"] = unserialize($main_result['answers']);
 
            $main_result["wrong_ans_ids"] = unserialize($main_result['wrong_answer_question_ids']);
        }
 

        return $main_result;
    }

    //Get All latest Result for each Exam Section
    public function GetIndepthResultStudent($course_id, $user_id){


        
        
        $sql = "SELECT * FROM {$this->course_table_section} 
                WHERE course_id = :cid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();
        $course_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);
        

        $progress_sql = "SELECT * FROM {$this->course_section_progress_table} 
        WHERE user_id = :uaid AND section_item_id = :sitd AND status = 3 AND active = 1 ORDER BY date_added DESC LIMIT 1";



        $item_sql = "SELECT * FROM {$this->course_table_item_section} 
        WHERE course_id = :cid AND section_id = :csid";

        $new_course_section = array();
        foreach($course_sections as $section){




            $stmt = $this->db->prepare($item_sql);
            $stmt->bindParam(':cid', $course_id);            
            $stmt->bindParam(':csid', $section['course_section_id']);
            $stmt->execute();
            $section_items = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $new_section_items = array();

            foreach($section_items as $item){
                if($item["item_type"]== 2){
                    $tmp_unsrlz = unserialize($item['ut_serialize_ws_id']);
                    if($tmp_unsrlz){
                        $item['WorksheetsIDs'] = $tmp_unsrlz;
                    }else{
                        $item['WorksheetsIDs'] = array();
                    }
                }

                
                $stmt = $this->db->prepare($progress_sql);
                $stmt->bindParam(':uaid', $user_id);
                $stmt->bindParam(':sitd', $item['section_item_id']);
                $stmt->execute();   
                $section_grade = $stmt->fetch(PDO::FETCH_ASSOC);
                $item["Grade"] = $section_grade;                
               
                array_push($new_section_items, $item);
            }

            $section['section_item'] = $new_section_items;

            array_push($new_course_section, $section);
        }
		return $new_course_section;        
    }

    public function GetIndepthQuestionsResultStudent($progress_id, $user_id , $limit = 0, $page_offset = 0){
        $progress_sql = "SELECT * FROM {$this->course_section_progress_table} 
        WHERE user_id = :uaid AND section_progress_id = :sitd 
        AND status IN (2,3) AND active = 1 ORDER BY date_added DESC LIMIT 1";

        $stmt = $this->db->prepare($progress_sql);
        $stmt->bindParam(':uaid', $user_id);
        $stmt->bindParam(':sitd', $progress_id);
        $stmt->execute();   
        $section_grade = $stmt->fetch(PDO::FETCH_ASSOC);
    
        if($section_grade){
            $question_ids =  unserialize( $section_grade['questions'] ) ;
           
            $new_question_ids = array();
            $new_question_ids_with_answers = array();
            $index = 0;
            $unser_answers =  unserialize($section_grade['answers']);


            foreach($question_ids as $id){
                array_push($new_question_ids, $id['question_id']);

                $temp = array(
                    'id'=> $id['question_id'],
                    'ans_index' => $unser_answers[$index]
                );
                array_push($new_question_ids_with_answers, $temp);
                $index++;
            }
            $str_question_ids = implode(", ", $new_question_ids);   
     

            $count_question_sql = "SELECT COUNT(*) FROM {$this->question_table} as q 
            JOIN  {$this->question_ans_table}  as a ON q.question_id = a.question_id 
            WHERE q.question_id  IN ({$str_question_ids})  ";
            $stmt = $this->db->prepare($count_question_sql);
            $stmt->execute();   
            $question_count = $stmt->fetchColumn();
            $section_grade["TotalCount"] = $question_count;

            $question_sql = "SELECT * FROM {$this->question_table} as q 
            JOIN  {$this->question_ans_table}  as a ON q.question_id = a.question_id 
            WHERE q.question_id  IN ({$str_question_ids})  ";

            if($limit > 0)
            {
                $question_sql .= " LIMIT {$limit}";
                if($page_offset > 0)
                {
                    $total_offset = $limit * $page_offset;
                    $question_sql .= " OFFSET {$total_offset}";
                } 
            }             
            
            $stmt = $this->db->prepare($question_sql);
            $stmt->execute();   
            $questions = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $new_questions = array();

    
            foreach($questions as $q){
                $q['choice'] = unserialize($q['question_choice']);
                $q['attachments'] = unserialize($q['question_choice_attachments']);
                $answer_index = array_search($q['question_id'], $new_question_ids);
                $q['selected_answer'] =  $answer_index;
                array_push($new_questions, $q);
            }


            $section_grade["uns_answers"]= unserialize($section_grade['answers']);
             $section_grade["Questions"]= $new_questions; 
             $section_grade["IdtoAnswer"] = $new_question_ids_with_answers;
        
        }

            

        // $course_details["Grade"] = $section_grade;       
        return $section_grade;
                
        

    }    
    

}