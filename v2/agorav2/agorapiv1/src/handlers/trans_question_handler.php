<?php 
class TransQuestionBankHandler
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function CreateNewTransQuestion($params)
    {
        $sql = "INSERT INTO tbl_trans_question 
        (`question_id`,`trans_country_id`,`trans_question_description`)
        VALUES (:qid, :tcid, :tqd)";


        $choices_sql = "INSERT INTO tbl_trans_question_answers 
                        (`trans_question_id`,`question_choice`) VALUES
                        (:tqid, :qa)";
       try
       {
           $this->db->beginTransaction();
           $stmt = $this->db->prepare($sql);
           $stmt->bindParam(':qid', $params['QuestionID']);
           $stmt->bindParam(':tcid', $params['CountryID']);
        //    $stmt->bindParam(':tqt', $params['QuestionTitle']);
           $stmt->bindParam(':tqd', $params['QuestionDescription']);
           $stmt->execute();

           $trans_question_id = $this->db->lastInsertId(); 
           $choices = serialize(
               array(
                   $params['ChoiceOne'],
                   $params['ChoiceTwo'],
                   $params['ChoiceThree'],
                   $params['ChoiceFour']
               )
           );

           $stmt = $this->db->prepare($choices_sql);
           $stmt->bindParam(':tqid', $trans_question_id);
           $stmt->bindParam(':qa', $choices);
           $stmt->execute();
           $this->db->commit();
       }
       catch (PDOException $e)
       {
           var_dump($e);
           $this->db->rollback(); 
       }                        

    }

    public function UpdateTransQuestion($params)
    {
        $sql = "UPDATE tbl_trans_question 
                SET question_id = :qid, trans_country_id = :tcid , 
                 trans_question_description = :tqd
                WHERE trans_question_id = :tqid";

        $choices_sql = "INSERT INTO tbl_trans_question_answers 
                        (`trans_question_id`,`question_choice`) VALUES
                        (:tqid, :qa)";
        $choices_sql = "UPDATE tbl_trans_question_answers SET 
         `question_choice` = :qa 
        WHERE `trans_question_id` = :tqid";
                    
       try
       {
           $this->db->beginTransaction();
           $stmt = $this->db->prepare($sql);
           $stmt->bindParam(':tqid', $params['TransQuestionID']);
           $stmt->bindParam(':qid', $params['QuestionID']);
           $stmt->bindParam(':tcid', $params['CountryID']);

           $stmt->bindParam(':tqd', $params['QuestionDescription']);
           $stmt->execute();

           $trans_question_id = $this->db->lastInsertId(); 
           $choices = serialize(
               array(
                   $params['ChoiceOne'],
                   $params['ChoiceTwo'],
                   $params['ChoiceThree'],
                   $params['ChoiceFour']
               )
           );

           $stmt = $this->db->prepare($choices_sql);
           $stmt->bindParam(':tqid', $params['TransQuestionID']);
           $stmt->bindParam(':qa', $choices);
           $stmt->execute();
           $this->db->commit();
       }
       catch (PDOException $e)
       {
           var_dump($e);
           $this->db->rollback(); 
       }                        

    }    

    public function DeleteTranslation($question_id, $country_id){
        $sql = "DELETE FROM tbl_trans_question 
                WHERE question_id = :qid AND trans_country_id = :tcid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $question_id);
        $stmt->bindParam(':tcid', $country_id);
        return $stmt->execute();                    
    }

}