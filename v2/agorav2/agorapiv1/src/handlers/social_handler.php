<?php
class SocialHandler
{
    private $db = null;
    private $message_table = 'tbl_message';
    private $discussion_table = 'tbl_discussion';
    private $discussion_viewed_table = 'tbl_viewed_discussion';
    private $posts_table = 'tbl_posts';
    private $inquiry_table = 'tbl_inquiry';

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function CreateNewConverstion($user_id, $params){
        $discussion_id = null;
        $check = "SELECT * FROM tbl_message_heads as m WHERE m.initiator_id = :inid AND m.to_id = :tid LIMIT 1";
        $check2 = "SELECT * FROM tbl_message_heads as m WHERE m.initiator_id = :tid AND m.to_id = :inid LIMIT 1";

        $message_head = null;
        $stmt = $this->db->prepare($check);
        $stmt->bindParam(':inid', $user_id);
        $stmt->bindParam(':tid',  $params['ReceiverID']);
        $stmt->execute();
        $checkhead1 = $stmt->fetch(PDO::FETCH_OBJ);
 
        if($checkhead1){
            $discussion_id = $checkhead1->message_heads_id;
            $message_head =  $checkhead1;
        }else{
            $stmt = $this->db->prepare($check2);
            $stmt->bindParam(':inid', $user_id);
            $stmt->bindParam(':tid',  $params['ReceiverID']);
            $stmt->execute();
            $checkhead2 = $stmt->fetch(PDO::FETCH_OBJ);
            if($checkhead2){
                $discussion_id = $checkhead2->message_heads_id;
                $message_head =  $checkhead2;
            }
        }
 

        if($checkhead1 == false && $checkhead2 == false){
            //Create NEW  Conversation head
            $sql = "INSERT INTO tbl_message_heads (`initiator_id`, `to_id`, `last_message`,`message_subject`,`viewed_by_initiator` ) VALUES
                    (:inid, :tid, :msg, :sbj, 1)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':inid', $user_id);
            $stmt->bindParam(':tid',  $params['ReceiverID']);
            $stmt->bindParam(':msg',  $params['Message']);
            $stmt->bindParam(':sbj',  $params['Title']);
            $stmt->execute();   
            $discussion_id = $this->db->lastInsertId();    
        }else{
            //Update 
            $update_sql = "UPDATE tbl_message_heads
                    SET last_message = :msg, last_active_date = NOW() ";

            if($message_head->initiator_id == $user_id){
                $update_sql .= ",  viewed_by_initiator = 1, viewed_by_to = 0 ";
            }else if($message_head->to_id == $user_id){
                $update_sql .= ",  viewed_by_to = 1, viewed_by_initiator = 0 ";
            }

            $update_sql .=" WHERE message_heads_id = :mhid "; 
            $stmt = $this->db->prepare($update_sql);
            $stmt->bindParam(':mhid', $discussion_id);
            $stmt->bindParam(':msg',  $params['Message']);
            $stmt->execute();                         

        }

        return $discussion_id;
        
    }

    public function SendMessage($user_id, $params)
    {

        $discussion_id = $this->CreateNewConverstion($user_id, $params);

        $sql = "INSERT INTO {$this->message_table} 
        (`message_head_id`,`sender_id`,`receiver_id`, `title`, `message`)
        VALUES (:mhid,:sid, :rid, :t, :m)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':mhid', $discussion_id);
            $stmt->bindParam(':sid', $user_id);
            $stmt->bindParam(':rid',  $params['ReceiverID']);
            $stmt->bindParam(':t',  $params['Title']);
            $stmt->bindParam(':m', $params['Message']);
            $stmt->execute();
            return $discussion_id;
        }
        catch (PDOException $e)
        {
        }
    }

    //Use to toggle the unread flag of the message head
    public function ViewedMessage($user_id, $message_head_id){
        $sql = "SELECT * FROM tbl_message_heads as m 
        WHERE m.message_heads_id = :mhid LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mhid', $message_head_id);
        $stmt->execute();       
        $message_head = $stmt->fetch(PDO::FETCH_OBJ); 
        if($message_head){
            $toUpdate = false;
            $update_sql = "UPDATE tbl_message_heads SET ";
            if($message_head->initiator_id == $user_id){
                $update_sql .= "viewed_by_initiator = 1 "; 
                $toUpdate = true;
            }else if($message_head->to_id == $user_id){
                $update_sql .= "viewed_by_to  = 1 "; 
                $toUpdate = true;
            }
            $update_sql .= "WHERE  message_heads_id = :mhid";

            if($toUpdate){
                $stmt = $this->db->prepare($update_sql);
                $stmt->bindParam(':mhid', $message_head_id);
                $stmt->execute();       
            }
        }
    }

    public function SendComment($user_id, $params)
    {
        $sql = "INSERT INTO {$this->discussion_table} 
        (`course_id`,`section_item_id`, `user_id`, `comment`, `is_reply`, `reply_to`)
        VALUES (:cid, :stid, :uid, :comm, :ir, :rt)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':cid', $params['CourseID']);
            $stmt->bindParam(':stid',  $params['SectionItemID']);
            $stmt->bindParam(':uid',  $user_id);
            $stmt->bindParam(':comm', $params['Comment']);
            $stmt->bindParam(':ir', $params['IsReply']);
            $stmt->bindParam(':rt', $params['ReplyTo']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }   
    

    public function UpdateComment($user_id, $params)
    {
        $sql = "UPDATE {$this->discussion_table} 
        SET comment = :comm
        WHERE discussion_id = :did AND user_id = :uid";
        // (`course_id`,`section_item_id`, `user_id`, `comment`, `is_reply`, `reply_to`)
        // VALUES (:cid, :stid, :uid, :comm, :ir, :rt)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid',  $user_id);
            $stmt->bindParam(':comm', $params['Comment']);
            $stmt->bindParam(':did', $params['DiscussionID']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }   
    

    public function DeleteComment($user_id, $discussion_id){
        $sql = "DELETE  FROM {$this->discussion_table}
            WHERE discussion_id= :did";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':did', $discussion_id);
        return $stmt->execute();
    }    
    
    
    public function NewPost($user_id, $params)
    {
        $sql = "INSERT INTO {$this->posts_table} 
        (`post_type`,`post_content`,`user_id`)
        VALUES (:pt, :pc, :uid)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':pt', $params['PostType']);
            $stmt->bindParam(':pc',  $params['Post']);
            $stmt->bindParam(':uid',  $user_id);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }

    public function UpdatePost($user_id, $params)
    {
        $sql = "UPDATE {$this->posts_table} 
        SET post_content = :pc
        WHERE post_id = :pid AND user_id = :uid";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid',  $user_id);
            $stmt->bindParam(':pc', $params['Post']);
            $stmt->bindParam(':pid', $params['PostID']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
            
        }
    }  
    

    public function DeletePost($user_id, $post_id){
        $sql = "DELETE  FROM {$this->posts_table}
            WHERE post_id= :pid AND user_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid',  $user_id);
        $stmt->bindParam(':pid', $post_id);
        return $stmt->execute();
    }    


    public function SendInquiry( $params)
    {
        $sql = "INSERT INTO {$this->inquiry_table} 
        (`inquirer_name`,`inquirer_email`,`inquirer_message`)
        VALUES (:in,:ie, :im)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':in', $params['Name']);
            $stmt->bindParam(':ie', $params['Email']);
            $stmt->bindParam(':im',  $params['Message']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }    

 //Use to toggle the unread flag of the Inquiry`
 public function ViewedInquiry($teacher_user_id, $inquiry_id){
    $sql = "SELECT * FROM  {$this->inquiry_table}  as i 
    WHERE i.inquiry_id = :inqid LIMIT 1";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':inqid', $inquiry_id);
    $stmt->execute();       
    $inquiry = $stmt->fetch(PDO::FETCH_OBJ); 
    if($inquiry){
        $update_sql = "UPDATE {$this->inquiry_table} 
                    SET date_viewed = NOW(), viewed_by_user_id = :uid
                    WHERE inquiry_id = :inqid";
            $stmt = $this->db->prepare($update_sql);
            $stmt->bindParam(':uid', $teacher_user_id);
            $stmt->bindParam(':inqid', $inquiry_id);
            $stmt->execute();       
    }
}    
        
    public function InsertViewedDiscussion($discussion_id, $user_id){
        $sql = "INSERT INTO {$this->discussion_viewed_table} 
        (`discussion_id`,`viewed_user_id`)
        VALUES (:did, :auid)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':did', $discussion_id);
        $stmt->bindParam(':auid', $user_id);
        return $stmt->execute();        
    }


}