<?php 
require '../src/utils/upload_helper.php';

class FileUploadHandler{

    public function UploadFile($params){

        $ret_array = array("Success"=>false);

        $path_parts = pathinfo($_FILES["file_upload"]["name"]);
        $extension = $path_parts['extension'];

        $allowed_image_files = array("jpg","jpeg","png","gif");
        $allowed_audio_files = array("mp3","wav");
        $allowed_other_files = array("docx", "txt", "pdf");
        $is_other_file_type = false;
        $allowed = false;

        $file_name_prefix = "other_";
        if(in_array($extension, $allowed_image_files)){
            $params["FileType"] = 1;
            $allowed = true;
            $file_name_prefix = "image_";
        }

        if(in_array($extension, $allowed_audio_files)){
            $params["FileType"] = 2;
            $allowed = true;
            $file_name_prefix = "audio_";
        }

        if(in_array($extension, $allowed_other_files)){
            $params["FileType"] = 3;
            $allowed = true;
            $is_other_file_type = true;
        }
        
        if(!$allowed){
            return;// do not process
        }

        // $file_name_prefix = ($params["FileType"] == 1) ? "image_" : "audio_";
        $file_name =  uniqid($file_name_prefix.date('Ymd').'-') . "." . $extension;

        $upload_helper = new FileUploadHelper();

        $isSuccess = $upload_helper->move_uploaded_image($params["FileType"],  $file_name, false, $extension, $is_other_file_type);


        $media_url =  "public/resources/others/";
        if($params["FileType"] == 1){
            $media_url =  "public/resources/images/";
        }
        else if($params["FileType"] == 2){
            $media_url =  "public/resources/audios/";
        }

        $media_url = $media_url.$file_name;
  
        if($isSuccess){
            $ret_array = array("Success"=>true, "Filename"=>$file_name, "FileUrl" => $media_url );
        }
        return $ret_array;
    }


    public function UploadProfile($params){

        $ret_array = array("Success"=>false);

        $path_parts = pathinfo($_FILES["file_upload"]["name"]);
        $extension = $path_parts['extension'];

        $allowed_image_files = array("jpg","jpeg","png","gif");
        $allowed = false;
        if(in_array($extension, $allowed_image_files)){
            $params["FileType"] = 1;
            $allowed = true;
        }

        
        if(!$allowed){
            return;// do not process
        }

        $file_name_prefix = "prof_image_";
        $file_name =  uniqid($file_name_prefix.date('Ymd').'-') . "." . $extension;

        $upload_helper = new FileUploadHelper();

        $isSuccess = $upload_helper->move_uploaded_image($params["FileType"],  $file_name, true, $extension);
        $media_url = "public/resources/profiles/";
        $media_url = $media_url.$file_name;
  
        if($isSuccess){
            $ret_array = array("Success"=>true, "Filename"=>$file_name, "FileUrl" => $media_url );
        }
        return $ret_array;
    }    

}
