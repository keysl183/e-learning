<?php 

class GradeSchemaHandler{
    
    private $db = null;
    private $grade_schema_table = 'tbl_grade_schema';
    private $grade_viewed_table = 'tbl_viewed_grades';
    public function __construct($db)
    {
        $this->db = $db;
    }

    public function InsertToGradeSchema($params){

        $sql = "INSERT INTO {$this->grade_schema_table} 
        (`course_id`,`grade_from`,`grade_to`,`grade_mark`)
        VALUES (:cid, :gf, :gt, :gm)";   

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $params['CourseID']);
        $stmt->bindParam(':gf', $params['GradeFrom']);
        $stmt->bindParam(':gt', $params['GradeTo']);
        $stmt->bindParam(':gm', $params['GradeMark']);  
        return $stmt->execute();
    }

    public function UpdateGradeSchema($params){

        $sql = "UPDATE {$this->grade_schema_table} 
        SET  `grade_from` = :gf, `grade_to` = :gt, `grade_mark` = :gm
        WHERE `course_id` = :cid AND `g_schema_id` = :gsid";   

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $params['CourseID']);
        $stmt->bindParam(':gsid', $params['GradeSchemaID']);
        $stmt->bindParam(':gf', $params['GradeFrom']);
        $stmt->bindParam(':gt', $params['GradeTo']);
        $stmt->bindParam(':gm', $params['GradeMark']);  
        return $stmt->execute();
    }    

    public function DeleteGradeSchema($g_schema_id){

        $sql = "DELETE FROM  {$this->grade_schema_table}  
                WHERE g_schema_id = :gsid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':gsid', $g_schema_id);
        return $stmt->execute();
    }    

    public function DeleteAttempt($user_progress_item_id){

        $sql = "DELETE FROM  tbl_course_section_progress
                WHERE section_progress_id = :spid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':spid', $user_progress_item_id);
        return $stmt->execute();
    }    

    //Update Pending Exam 
    //Typically for exams with Essay
    public function UpdatePendingExam($params){

        $sql = "UPDATE tbl_course_section_progress 
        SET status = 3, pass_failed = :pf, score = :sc, date_approved = NOW()
        WHERE user_id = :auid AND section_progress_id = :spid";   

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':pf', $params['PassFailed']);
        $stmt->bindParam(':sc', $params['TeacherMark']);
        $stmt->bindParam(':auid', $params['StudentID']);
        $stmt->bindParam(':spid', $params['ProgressID']);  
        return $stmt->execute();
    }    
    

    public function InsertViewedSectionItem($section_item_id, $user_id){
        $sql = "INSERT INTO {$this->grade_viewed_table} 
        (`section_item_id`,`viewed_user_id`)
        VALUES (:stid, :auid)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->bindParam(':auid', $user_id);
        return $stmt->execute();        

    }
}