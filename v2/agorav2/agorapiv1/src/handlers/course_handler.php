<?php 

class CourseHandler
{
    private $db = null;
    private $session_value = null;
    private $category_table = 'tbl_category';
    private $category_section_table = 'tbl_category_section';
    private $category_section_level_table = 'tbl_category_section_level';
    private $course_table = 'tbl_course';
    private $course_section_table = 'tbl_course_section';
    private $course_section_item_table = 'tbl_course_section_item';
    private $grade_schema_table = 'tbl_grade_schema';
    public function __construct($db, $session_value)
    {
        $this->db = $db;
        $this->session_value = $session_value;
    }
    
    //Create New Course Category
    public function CreateNewCourseCategory($params)
    {
        $sql = "INSERT INTO {$this->category_table} 
        (`category_title`, `cover_photo`)
        VALUES (:ct,  :cp)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ct', $params['CategoryTitle']);
            // $stmt->bindParam(':cd', $params['CategoryDescription']);
            $stmt->bindParam(':cp', $params['CoverPhoto']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
        }
    }

    //Update New Course
    public function UpdatCourseCategory($params)
    {
        $course_mapper = new CourseMapper($this->db);
        $course_category = $course_mapper->GetCategoryCourseByID($params['CategoryID']);

        $category_title = (isset($params['CategoryTitle']))  ?   $params['CategoryTitle'] : $course_category->category_title;
        // $category_desc = (isset($params['CategoryDescription']))  ?   $params['CategoryDescription'] : $course_category->category_desc;

        $sql = "UPDATE {$this->category_table} 
                SET category_title=:ct,   cover_photo =:cp 
                WHERE category_id=:cid";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ct', $category_title);
            $stmt->bindParam(':cid', $params['CategoryID']);
            $stmt->bindParam(':cp', $params['CoverPhoto']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }

    //Delete Category
    public function DeleteCategory($category_id){

        $sql = "UPDATE {$this->category_table}
                SET active = 0 
                WHERE category_id=".$category_id;
        $stmt = $this->db->prepare($sql);
        return $stmt->execute();
    }       

    //Creates new Category Section
    //For new client request change
    public function CreateNewCategorySection($params){
        $sql = "INSERT INTO {$this->category_section_table} 
        (`section_title`,`category_id`)
        VALUES (:st, :ci)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':st', $params['SectionTitle']);
            // $stmt->bindParam(':sd', $params['SectionDescription']);
            $stmt->bindParam(':ci', $params['CategoryID']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }

    //UpdateCourseSection
        //For new client request change
    public function UpdateCategorySection($params)
    {

        $sql = "UPDATE {$this->category_section_table}  
        SET `section_title` = :st
        WHERE `category_section_id` = :csid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':csid', $params['SectionID']);
            $stmt->bindParam(':st', $params['SectionTitle']);
            // $stmt->bindParam(':sd', $params['SectionDescription']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }      
    public function DeleteCategorySection($section_id)
    {

        $sql = "DELETE FROM {$this->category_section_table}  
        WHERE  `category_section_id` = :csid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':csid', $section_id);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }         

    //Create a new Level for a Category Section
    public function CreateNewCategorySectionLevel($params){
        $sql = "INSERT INTO {$this->category_section_level_table} 
        (`level_title`,`category_section_id`)
        VALUES (:st, :csid)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':st', $params['Title']);
            // $stmt->bindParam(':sd', $params['Description']);
            $stmt->bindParam(':csid', $params['SectionID']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }

    public function UpdateCategorySectionLevel($params)
    {

        $sql = "UPDATE {$this->category_section_level_table}  
        SET `level_title` = :lt 
        WHERE `category_section_level_id` = :lvid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':lvid', $params['LevelID']);
            $stmt->bindParam(':lt', $params['Title']);
            // $stmt->bindParam(':ld', $params['Description']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }  

    public function DeleteCategorySectionLevel($level_id)
    {

        $sql = "DELETE FROM {$this->category_section_level_table}  
        WHERE  `category_section_level_id` = :lvid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':lvid', $level_id);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }    



    //Create New Course
    public function CreateNewCourse($params)
    {
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);

        $sql = "INSERT INTO {$this->course_table} 
        (`course_title`,`course_description`,`course_cover_image`,
        `course_category`, `author_user_id`, `no_of_questions`, `passing_rate`, 
        `retake_allowed`,`time_limit`, `category_section_id`, `category_section_level_id`, `question_bank_id`, `chapter_no`)
        VALUES (:ct, :cd,:ccp, :cc, :auid, :noq, :pr, :ra, :tl, :ctsid, :cslid, :qbid, :chno)";

        if($params['CourseCoverPhoto'] == ''){
            $params['CourseCoverPhoto'] = 'default_exam.jpg';
        }

        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ct', $params['CourseTitle']);
            // $stmt->bindParam(':ce', $params['CourseExcerpt']);
            $stmt->bindParam(':cd', $params['CourseDescription']);
            $stmt->bindParam(':ccp', $params['CourseCoverPhoto']);
            $stmt->bindParam(':cc', $params['CategoryID']);
            $stmt->bindParam(':auid', $member->user_id);
            $stmt->bindParam(':noq', $params['NoOfQuestion']);
            $stmt->bindParam(':pr', $params['PassingGrade']);
            $stmt->bindParam(':ra', $params['Retake']);
            $stmt->bindParam(':tl', $params['TimeLimit']);
            $stmt->bindParam(':ctsid', $params['SectionID']);
            $stmt->bindParam(':cslid', $params['Level']);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            $stmt->bindParam(':chno', $params['Chapter']);
            $stmt->execute();
            $course_id = $this->db->lastInsertId();
            
            //Insert the default grading schema in the db
            // if($course_id && $gradeschema)
            // {
            //     foreach($gradeschema as $schema){
            //         $stmt = $this->db->prepare($grade_schema_sql);
            //         $stmt->bindParam(':cid', $course_id);
            //         $stmt->bindParam(':gf', $schema['grade_from']);
            //         $stmt->bindParam(':gt', $schema['grade_to']);
            //         $stmt->bindParam(':gm', $schema['grade_mark']);
            //         $stmt->execute();
            //     }
            // }
            $stmt->bindParam(':st', $params['SectionTitle']);
            $stmt->bindParam(':sd', $params['SectionDescription']);
            $stmt->bindParam(':ci', $params['CourseID']);
            $course_section_params = array(
                "SectionTitle" => $params["CourseTitle"],
                "SectionDescription"=> $params["CourseDescription"],
                "CourseID"=> $course_id ,
            );

            $section_id = $this->CreateNewCourseSection($course_section_params);

            //TODO THE UPDATE FIXED
            $section_item_params = array(
                "Type" => 3, //1 = WS, 2 = UT, 3 is for course exam
                "CourseID" => $course_id,
                "SectionID" => $section_id, 
                "Title" =>  $params['CourseTitle'],
                "Description" => $params['CourseDescription'],
                "AnswerType" => 3,
                "NoOfQuestion" => $params['NoOfQuestion'],
                "PassingRate" => $params['PassingGrade'],
                "TimeLimit" => $params['TimeLimit'],
                "RetakeCount" => $params['Retake'],
                "Worksheets" => array(),
                "BankID" => $params['QuestionBank'],
            );

            $section_item_id = $this->CreateNewCourseSectionItem($section_item_params);
    
            $this->FinalizeInsertCourse($course_id, $section_item_id);




            return $course_id;
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }

    //Helper method for CreatNewCourse
    public function FinalizeInsertCourse($course_id, $section_item_id){
        $update_sql = "UPDATE {$this->course_table} 
                SET section_item_id = :stid WHERE course_id=:cid";
        $stmt = $this->db->prepare($update_sql);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->bindParam(':cid', $course_id);
        $stmt->execute();              
    }


    //Update New Course
    public function UpdateCourse($params)
    {
        $course_mapper = new CourseMapper($this->db);
        $course = $course_mapper->GetCourseDetailByID($params['CourseID']);

        $course_title = (isset($params['CourseTitle']))  ?   $params['CourseTitle'] : $course->course_title;
        $course_excerpt = (isset($params['CourseExcerpt']))  ?   $params['CourseExcerpt'] : $course->course_excerpt;
        $course_desc = (isset($params['CourseDescription']))  ?   $params['CourseDescription'] : $course->course_description;
        $category_id = (isset($params['CategoryID']))  ?   $params['CategoryID'] : $course->category_id;
        $course_cover_photo = (isset($params['CourseCoverPhoto']))  ?   $params['CourseCoverPhoto'] : $course->course_cover_image;
        
        $sql = "UPDATE {$this->course_table} 
                SET course_title=:ct, course_excerpt=:ce,
                    course_description= :cd, course_category = :cc,
                    no_of_questions = :noq, passing_rate = :pr, retake_allowed = :ra, time_limit =:tl , course_cover_image = :ccp,
                    question_bank_id = :qbid, chapter_no = :chno
                WHERE course_id=:cid";

        $item_sql = "UPDATE {$this->course_section_item_table}  
        SET `title` = :t, `description` = :d, `no_of_question`= :noq, 
        `passing_rate` = :pr  ,`retake_count` = :rc
        WHERE section_item_id = :sitid"; 
        
        $get_item_sql = "SELECT * FROM {$this->course_section_item_table} 
         WHERE item_type = 3 AND course_id = :cid LIMIT 1";

        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ct', $course_title);
            $stmt->bindParam(':ce', $course_excerpt);
            $stmt->bindParam(':cd', $course_desc);
            $stmt->bindParam(':cc', $category_id);
            $stmt->bindParam(':cid', $params['CourseID']);
            $stmt->bindParam(':noq', $params['NoOfQuestion']);
            $stmt->bindParam(':pr', $params['PassingGrade']);
            $stmt->bindParam(':ra', $params['Retake']);
            $stmt->bindParam(':tl', $params['TimeLimit']);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            $stmt->bindParam(':chno', $params['Chapter']);            
            $stmt->bindParam(':ccp', $course_cover_photo);
            $stmt->execute();
            
            //get the corresponding section item of this exam
            $stmt = $this->db->prepare($get_item_sql);
            $stmt->bindParam(':cid', $params['CourseID']);
            $stmt->execute();
            $section_item = $stmt->fetch(PDO::FETCH_OBJ);

            //Update the section item
            if($section_item){
                $stmt = $this->db->prepare($item_sql);
                $stmt->bindParam(':t', $course_title);
                $stmt->bindParam(':d', $course_desc);
                $stmt->bindParam(':noq', $params['NoOfQuestion']);
                $stmt->bindParam(':pr', $params['PassingGrade']);
                $stmt->bindParam(':rc', $params['Retake']);
                $stmt->bindParam(':sitid', $section_item->section_item_id);
                $stmt->execute();
            }

             return true;
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }    

    //Delete a Exam
    public function DeleteExam($exam_id){

        $sql = "UPDATE {$this->course_table}
                SET active = 0 
                WHERE course_id=".$exam_id;
        $stmt = $this->db->prepare($sql);
        return $stmt->execute();
    }    


    //CreateNewCourseSection
    public function CreateNewCourseSection($params)
    {
        $sql = "INSERT INTO {$this->course_section_table} 
        (`section_title`,`section_description`,`course_id`)
        VALUES (:st, :sd, :ci)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':st', $params['SectionTitle']);
            $stmt->bindParam(':sd', $params['SectionDescription']);
            $stmt->bindParam(':ci', $params['CourseID']);
            $stmt->execute();
            return  $this->db->lastInsertId();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }
    //UpdateCourseSection
    public function UpdateCourseSection($params)
    {

        $sql = "UPDATE {$this->course_section_table}  
        SET `section_title` = :st, `section_description` = :sd WHERE `course_id` = :ci AND `course_section_id` = :csid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':st', $params['SectionTitle']);
            $stmt->bindParam(':sd', $params['SectionDescription']);
            $stmt->bindParam(':ci', $params['CourseID']);
            $stmt->bindParam(':csid', $params['SectionID']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }    

    public function DeleteCourseSection($section_id)
    {

        $sql = "DELETE FROM {$this->course_section_table}  
        WHERE  `course_section_id` = :csid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':csid', $section_id);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }        
    
    //CreateNewCourseSection
    public function CreateNewCourseSectionItem($params)
    {
        $sql = "INSERT INTO {$this->course_section_item_table} 
        (`item_type`,`course_id`,`section_id`,`title`,`description`,`ws_bank_id`,`ut_serialize_ws_id`, `answer_type`,`no_of_question`,`passing_rate`,`time_limit`,`retake_count`)
        VALUES (:it, :ci, :si, :t, :d, :wsbid, :utsbid, :anst, :noq, :pr, :tl, :rc)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':it', $params['Type']);
            $stmt->bindParam(':ci', $params['CourseID']);
            $stmt->bindParam(':si', $params['SectionID']);
            $stmt->bindParam(':t', $params['Title']);
            $stmt->bindParam(':d', $params['Description']);
            $stmt->bindParam(':wsbid', $params['BankID']);
            $sr_bank_ids = serialize($params["Worksheets"]);
            $stmt->bindParam(':utsbid', $sr_bank_ids);
            $stmt->bindParam(':anst', $params['AnswerType']);
            $stmt->bindParam(':noq', $params['NoOfQuestion']);
            $stmt->bindParam(':pr', $params['PassingRate']);
            $stmt->bindParam(':tl', $params['TimeLimit']);
            $stmt->bindParam(':rc', $params['RetakeCount']);
            $stmt->execute();
            return $this->db->lastInsertId();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    } 
    
    public function UpdateCourseSectionItem($params)
    {

        $sql = "UPDATE {$this->course_section_item_table}  
                SET `item_type` = :it, `course_id` = :ci, `section_id` = :si,
                `title` = :t, `description` = :d, `ws_bank_id`= :wsbid, `ut_serialize_ws_id` = :utsbid,
                `answer_type`= :anst, `no_of_question`= :noq, `passing_rate` = :pr ,`time_limit` = :tl ,`retake_count` = :rc
                WHERE section_item_id = :sitid";        
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':it', $params['Type']);
            $stmt->bindParam(':ci', $params['CourseID']);
            $stmt->bindParam(':si', $params['SectionID']);
            $stmt->bindParam(':t', $params['Title']);
            $stmt->bindParam(':d', $params['Description']);
            $stmt->bindParam(':wsbid', $params['BankID']);
            $sr_bank_ids = serialize($params["Worksheets"]);
            $stmt->bindParam(':utsbid', $sr_bank_ids);
            $stmt->bindParam(':anst', $params['AnswerType']);
            $stmt->bindParam(':noq', $params['NoOfQuestion']);
            $stmt->bindParam(':sitid', $params['SectionItemID']);

            $stmt->bindParam(':pr', $params['PassingRate']);
            $stmt->bindParam(':tl', $params['TimeLimit']);
            $stmt->bindParam(':rc', $params['RetakeCount']);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }        
    

    public function DeleteCourseSectionItem($section_item_id)
    {

        $sql = "DELETE FROM {$this->course_section_item_table}  
        WHERE  `section_item_id` = :sitid";          
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':sitid', $section_item_id);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e->getMessage());
        }
    }       

    /*Validation Functions*/
    public function ValidateExistCategory($id)
    {   
        $sql = "SELECT COUNT(*) FROM {$this->category_table} WHERE category_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("CourseCategory"=>'CourseCategory does not exists!');
        }
    }

    public function ValidateExistCategorySection($id)
    {   
        $sql = "SELECT COUNT(*) FROM {$this->category_section_table} WHERE category_section_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("CategorySection"=>'CategorySection does not exists!');
        }
    }    
    public function ValidateExistCategorySectionLevel($id)
    {   
        $sql = "SELECT COUNT(*) FROM {$this->category_section_level_table} WHERE category_section_level_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("CategorySectionLevel"=>'CategorySectionLevel does not exists!');
        }
    }  

    public function ValidateExistCourse($id)
    {   
        $sql = "SELECT COUNT(*) FROM {$this->course_table} WHERE course_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("Course"=>'Course does not exists!');
        }
    }

    public function ValidateExistCourseTeacher($id)
    {   
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);

        $sql = "SELECT COUNT(*) FROM {$this->course_table} WHERE course_id = :id AND author_user_id = :auid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':auid', $member->user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("Course"=>'Course does not exists!');
        }
    }      
    /*End Validation Functions*/

}