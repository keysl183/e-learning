<?php

class ExamHandler{

    private $db = null;
    private $session_value = null;

    private $enroll_table = 'tbl_enroll';
    private $course_section_progress_table = 'tbl_course_section_progress';
    private $course_table = 'tbl_course';

    public function __construct($db, $session_value=null)
    {
        $this->db = $db;
        $this->session_value = $session_value;
    }


    //Enroll is new Course
    public function EnrollNewCourse($params)
    {
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);
        $member_detail = $member_mapper->GetMemberByUserID($member->user_id);

        $course_mapper = new CourseMapper($this->db);
        $course_detail = $course_mapper->GetCourseDetailByID($params['CourseID']);

        $sql = "INSERT INTO {$this->enroll_table} 
        (`course_id`,`user_id`)
        VALUES (:ci, :ui)";

        $post_sql = "INSERT INTO tbl_posts
        (`post_type`,`post_content`,`user_id`)
        VALUES (:pt, :pc, :uid)";  

        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ci', $params['CourseID']);
            $stmt->bindParam(':ui',  $member->user_id);
            $stmt->execute();
            //increment the enrolled number
            $this->IncrementEnrolledStudents( $params['CourseID']);
            
            //Create a new achievement post
            $stmt = $this->db->prepare($post_sql);
            $post_type = '2';
            $stmt->bindParam(':pt', $post_type);
            $message = " {$member_detail->fname} added a course named  {$course_detail->course_title} ";
            $stmt->bindParam(':pc',  $message);
            $stmt->bindParam(':uid',  $member->user_id);
            return $stmt->execute();            
            
            return true;
        }
        catch (PDOException $e)
        {
        }
    }

    //Complete a Exam Section Item
    public function CompleteExamSectionItem($params)
    {
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);
        $member_detail = $member_mapper->GetMemberByUserID($member->user_id);

        $course_mapper = new CourseMapper($this->db);
        $section_item = $course_mapper->GetCourseSectionItemBySectionItemIDForPost( $params['SectionItemID']);
       
        //TODO PENDING
        $sql = "UPDATE {$this->course_section_progress_table} 
                SET score = :sc, answers = :ans, status = :st, wrong_answer_question_ids = :waqids, pass_failed = :result
                WHERE section_item_id = :sti AND user_id = :uid AND section_progress_id = :spid" ;  

        $post_sql = "INSERT INTO tbl_posts
                (`post_type`,`post_content`,`user_id`)
                VALUES (:pt, :pc, :uid)";                      
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':sc', $params['NoOfCorrectAnswer']);
            $srlz_answers = serialize( $params['Answers']);
            $stmt->bindParam(':ans',$srlz_answers);
            $srlz_wrng_answer_ids = serialize( $params['WrongAnswerQuestionID']);
            $stmt->bindParam(':waqids',$srlz_wrng_answer_ids); 
            $stmt->bindParam(':result', $params['Result']);
            $stmt->bindParam(':spid', $params['ProgressID']);
            // status is set to 3 for now, no pending FIXED 07112018

            //2 is for pending
            $completed_status = 3;
            if($section_item->answer_type == 2){
                $completed_status = 2;
            }
        
            $stmt->bindParam(':st', $completed_status);

            $stmt->bindParam(':sti', $params['SectionItemID']);
            $stmt->bindParam(':uid',  $member->user_id);
            $stmt->execute();

            if($params['Result'] == 1){
                //Post Achievement
                $stmt = $this->db->prepare($post_sql);
                $post_type = '3';
                $stmt->bindParam(':pt', $post_type);
                $message = " {$member_detail->fname} answered {$section_item->title} and scored {$params['NoOfCorrectAnswer']} Points!";
                $stmt->bindParam(':pc',  $message);
                $stmt->bindParam(':uid',  $member->user_id);
                return $stmt->execute();
            }

            return true;
        }
        catch (PDOException $e)
        {
        }
    }

    public function IncrementEnrolledStudents($course_id){
        $sql = "UPDATE {$this->course_table} 
        SET student_enrolled =  student_enrolled + 1
        WHERE course_id = :cid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':cid', $course_id);
        return $stmt->execute();
    }


    //Validations
    public function ValidateAlreadyEnrolled($id)
    {   
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);

        $sql = "SELECT COUNT(*) FROM {$this->enroll_table} WHERE course_id = :id AND user_id = :uid";
  
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':uid', $member->user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count > 0) //alreadyEnrolled
        {
            return array("Course"=>'User was already Enrolled');
        }
    }

    public function ValidateRetakeCout($user_id, $section_item_id)
    {   
        $course_mapper = new CourseMapper($this->db);
        $section_item = $course_mapper->GetCourseSectionItemBySectionItemIDForPost( $section_item_id);
        $take_count_sql = "SELECT COUNT(p.section_progress_id) 
                        FROM tbl_course_section_progress as p 
                        WHERE p.section_item_id = :stid AND user_id = :uid";

        $stmt = $this->db->prepare($take_count_sql);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();

        if($count> $section_item->retake_count + 1) //Already reach max retake
        {
            return array("Course"=>'You already reach the max number of retakes');
        }
    }


}
