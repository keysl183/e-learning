<?php 
require '../src/utils/image_helper.php';


class MemberEnrollHandler
{
    private $db = null;
    public function __construct($db)
    {
        $this->db = $db;
    }

    public function MemberEnroll($args)
    {
        $sql = "INSERT INTO tbl_users (`user_login`,`user_pass`,`user_email`,`user_level`)
                VALUES (:ul, :up, :ue, 0)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ul', $args['Username']);
            $hashPass = md5($args['Password']);
            $stmt->bindParam(':up',$hashPass);
            $stmt->bindParam(':ue', $args['EmailAddress']);
            $stmt->execute();
            return  $enrolee_id = $this->db->lastInsertId();
        }catch(PDOException $e)
        {
            var_dump($e);
        }

        return false;
    }

    public function SaveActivationKey($user_id, $activation_key)
    {
        $sql = "INSERT INTO tbl_activation_keys (`user_id`,`activation_key`)
                VALUES (:uid, :ak)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid', $user_id);
            $stmt->bindParam(':ak', $activation_key);
            return $stmt->execute();
        }catch(PDOException $e)
        {
            
        }

        return false;

    }

    public function MemberEnrollCompletion($args)
    {
        $this->InsertMemberDetails($args);
        $this->SetUserToActive($args['UserID']);
         return true;
    }

    function InsertMemberDetails($params, $img_name = null)
    {
        $sql = "INSERT INTO tbl_users_detail 
                (`user_id`,`fname`,`lname`,`mname`, `birthday`, `age`, `country_id`, `zip_code`, `education_level`, `profile_image`,`school`, `gender`)
                VALUES (:uid, :udf, :udl, :udm, :udb, :uda, :udc, :udz, :ude, :udp, :usch, :gdr)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid', $params['UserID']);
            $stmt->bindParam(':udf', $params['FirstName']);
            $stmt->bindParam(':udl', $params['LastName']);
            $stmt->bindParam(':udm', $params['MiddleName']);

            $parse_bday = strtotime($params['Birthday']);
            $new_bday = date('Y-m-d',$parse_bday);
            $stmt->bindParam(':udb', $new_bday);
            $stmt->bindParam(':uda', $params['Age']);
            $stmt->bindParam(':udc', $params['CountryID']);
            $stmt->bindParam(':udz', $params['ZipCode']);
            $stmt->bindParam(':ude', $params['EducationLevel']);

            if(isset($params['ProfilePhoto'])){
                $file = $params['ProfilePhoto'];
            }else{
                 //default fallback
                $file = 'defaultprofile.jpg';
            }           
            // $file = 'Student_Avatar1.jpg';
            $stmt->bindParam(':udp', $file);
            $stmt->bindParam(':usch', $params['School']);
            $stmt->bindParam(':gdr', $params['Gender']);


            
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
           
        }
    }
    public function MemberDetailsUpdate($args)
    {
        $member = $this->GetMemberUpdateDetails($args['UserID']);
        if ($member)
        {
            $fname =            (isset($args['FirstName']))         ?   $args['FirstName'] : $member['fname'];
            $lname =            (isset($args['LastName']))          ?   $args['LastName'] : $member['lname'];
            $mname =            (isset($args['MiddleName']))        ?   $args['MiddleName'] : $member['mname'];
            $birthday =         (isset($args['Birthday']))          ?   $args['Birthday'] : $member['birthday'];
            $age =              (isset($args['Age']))               ?   $args['Age'] : $member['age'];
            $country_id =       (isset($args['CountryID']))         ?   $args['CountryID'] : $member['country_id'];
            $zip_code =         (isset($args['ZipCode']))           ?   $args['ZipCode'] : $member['zip_code'];
            $education_level =  (isset($args['EducationLevel']))    ?   $args['EducationLevel'] : $member['education_level'];
            $school =  (isset($args['School']))    ?   $args['School'] : $member['school'];
            $social_fb =  (isset($args['Facebook']))    ?   $args['Facebook'] : $member['social_fb'];
            $social_gmail =  (isset($args['Gmail']))    ?   $args['Gmail'] : $member['social_gmail'];
           

            $sql = "UPDATE tbl_users_detail 
                    SET fname=:udf, lname=:udl, mname=:udm, birthday=:udb, age=:uda, 
                    country_id=:udc, zip_code=:udz, education_level=:ude, school = :sch,
                    social_fb =:scfb, social_gmail = :scgm
                    WHERE user_id=:uid";
            $username_sql = "UPDATE tbl_users SET user_login = :usl WHERE user_id = :uid";
            try
            {
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':uid', $args['UserID']);
                $stmt->bindParam(':udf', $fname);
                $stmt->bindParam(':udl', $lname);
                $stmt->bindParam(':udm', $mname);
                $stmt->bindParam(':udb', $birthday);
                $stmt->bindParam(':uda', $age);
                $stmt->bindParam(':udc', $country_id);
                $stmt->bindParam(':udz', $zip_code);
                $stmt->bindParam(':ude', $education_level);
                $stmt->bindParam(':sch', $school);
                $stmt->bindParam(':scfb', $social_fb);
                $stmt->bindParam(':scgm', $social_gmail);
                $stmt->execute();
           
                $stmt = $this->db->prepare($username_sql);
                $stmt->bindParam(':uid', $args["UserID"]); 
                $stmt->bindParam(':usl', $args["Username"]);
                return $stmt->execute();
                

            }
            catch(PDOException $e)
            {
                var_dump($e);
            }
        }
        
        return false;
    }

    function GetMemberUpdateDetails($id)
    {
        $sql = "SELECT * FROM tbl_users_detail WHERE user_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $member = $stmt->fetch();
        return $member;
    }

    public function SocialFBRegister($params){
        $initial_enroll = array(
            "Username" => $params["FBUserID"],
            "Password" => $params["FBUserID"],
            "EmailAddress" => $params["EmailAddress"],
        );
        $enroll_id = $this->MemberEnroll($initial_enroll);
        $complete_enroll  = array(
            "UserID" => $enroll_id,
            "FirstName" => $params["FirstName"],
            "LastName" => $params["LastName"],
            "MiddleName" => '',
            "Birthday" => $params["Birthday"],
            "Age" => null,
            "CountryID" => null, 
            "ZipCode" => '',
            "EducationLevel" => null,
            "ProfilePhoto" => 'defaultprofile.jpg',
            "School" => '',
            "Gender" => $params["Gender"],
        );
      $this->MemberEnrollCompletion($complete_enroll);

      $sql = "UPDATE tbl_users SET fb_register_id = :fbid WHERE user_id = :uid";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':uid', $enroll_id);
      $stmt->bindParam(':fbid', $params["FBUserID"]);
        $stmt->execute();
        return $enroll_id;
    }


    public function SocialGoogleRegister($params){
        $initial_enroll = array(
            "Username" => $params["GmailUserID"],
            "Password" => $params["GmailUserID"],
            "EmailAddress" => $params["EmailAddress"],
        );
        $enroll_id = $this->MemberEnroll($initial_enroll);
        $complete_enroll  = array(
            "UserID" => $enroll_id,
            "FirstName" => $params["FirstName"],
            "LastName" => $params["LastName"],
            "MiddleName" => '',
            "Birthday" => null,
            "Age" => null,
            "CountryID" => null, 
            "ZipCode" => '',
            "EducationLevel" => null,
            "ProfilePhoto" => 'defaultprofile.jpg',
            "School" => '',
            "Gender" => null,
        );
      $this->MemberEnrollCompletion($complete_enroll);
        try{
            $sql = "UPDATE tbl_users SET gmail_id = :fbid WHERE user_id = :uid";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':uid', $enroll_id);
            $stmt->bindParam(':fbid', $params["GmailUserID"]);
            $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
        
        }
        return $enroll_id;
    }

    function SetUserToActive($user_id)
    {
        $sql = "UPDATE tbl_users SET active=:active WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
    }
    function UpdateMemberPassword($user_id, $new_pass, $user_email = '')
    {
        $sql = "UPDATE tbl_users SET user_pass =:ups WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $hashPass = md5($new_pass);
        $stmt->bindParam(':ups', $hashPass);
        $success =$stmt->execute();
        $ticket_success = false;
        $ticket = '';
        if($success){
            //generate a ticket hash so we can identify this later
            //act as second layer of security
            $toHash = $user_id . $user_email . $new_pass;
            $ticket = md5($toHash);
            $ticket_sql = "INSERT INTO tbl_reset_password_tickets (`user_id`, `reset_ticket`)
                VALUES (:uid, :rst)";
            $stmt = $this->db->prepare($ticket_sql);
            $stmt->bindParam(':uid', $user_id);
            $stmt->bindParam(':rst', $ticket);
            $ticket_success =$stmt->execute();
        }

        return $ticket;
        //
    }    
    function UpdateProfile($user_id, $filename)
    {
        $sql = "UPDATE tbl_users_detail SET profile_image =:profimg WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->bindParam(':profimg', $filename);
        return $stmt->execute();
    }

    function UpdateProfileBackdrop($user_id, $backropid)
    {
        $sql = "UPDATE tbl_users_detail SET back_drop =:bdid WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->bindParam(':bdid', $backropid);
        return $stmt->execute();
    }

    function SetResetTicketToInactive($reset_ticket)
    {
        $sql = "UPDATE tbl_reset_password_tickets SET active = :act WHERE reset_ticket=:rst";
        $stmt = $this->db->prepare($sql);
        $active = 0;
        $stmt->bindParam(':act', $active);
        $stmt->bindParam(':rst', $reset_ticket);
        return $stmt->execute();
    }

    function SetAccountUserLevel($user_id, $user_level , $hasAdminPrivilege = 0, $active = 1)
    {
        $sql = "UPDATE tbl_users SET  user_level = :ulvl, admin_privelege = :ap, active = :act
                WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':ulvl', $user_level);
        $stmt->bindParam(':ap', $hasAdminPrivilege);
        $stmt->bindParam(':act', $active);
        $stmt->bindParam(':uid', $user_id);
        return $stmt->execute();
    }

    //It is required that member is logged in to execute this hence we get the session value to get
    //the associated member
    function ExecuteBrainTreePayment($params, $session_value, $gateway, $mail){
        $response = array();
        $errors = array();
        $transaction = null;
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($session_value);
        $member_detail = $member_mapper->GetMemberByUserID($member->user_id);
        $membership_plan = $this->GetMembershipPlan($params["selectedMembershipPlanID"]);
        
        //Create our customer in our gateway provider
        $result = $gateway->customer()->create(array(
            'firstName' => $member_detail->fname,
            'lastName' => $member_detail->lname,
            'paymentMethodNonce' => $params["paymentNonce"]
        ));

        if ($result->success) {
            $member_payment_id = $result->customer->id; //unique id associated to the user from the paypal
            $token = $result->customer->paymentMethods[0]->token; //get the payment method token selected by user
            
            //execute the subscription. This is the place where the payment will be executed
            //TODO https://developers.braintreepayments.com/guides/recurring-billing/create/php
            //SET UP THE AUTOMATIC EMAIL WHEN AUTOMATIC PAYMENT HAS BEEN SENT
            $subscription_result = $gateway->subscription()->create([
                'paymentMethodToken' => $token,
                'planId' => $params["selectedMembershipPlanID"],
                'merchantAccountId' => 'klfreelance'
            ]);
            // var_dump($subscription_result);
          

            if ($subscription_result->success || !is_null($subscription_result->transactions)) {
                $transaction = $subscription_result->subscription->transactions[0];
                //successs
                //Update our Member's 
                //Insert into our Subscription & Transaction Table 
                //user_payment_id = member_payment_id
                $sql = "UPDATE tbl_users SET member_payment_id=:mpi, user_level = :mlvl
                        WHERE user_id=:uid;
                        
                        INSERT INTO tbl_subscription(`user_id`, `user_subscription_id`) 
                        VALUES (:uid, :usid);
                        
                        
                        INSERT INTO tbl_transaction(`user_id`, `membership_plan_id`, `subscription_id`,
                        `transaction_id`, `user_payment_id`) 
                        VALUES (:uid, :mpid, :usid, :tid, :mpi);";

                $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':mpi', $member_payment_id);
                $stmt->bindParam(':mlvl', $membership_plan->user_level_id);
                $stmt->bindParam(':uid', $member->user_id);
                $stmt->bindParam(':usid',  $subscription_result->subscription->id);
                $stmt->bindParam(':mpid',  $membership_plan->level_payment_id);
                $stmt->bindParam(':tid',  $transaction->id);
                $stmt->execute();
                //do a email send here later

                $subject = "Thank you for becoming our students!";
                $msg = "Greetings here from Agora School. Thanks for becoming our student.
                        You current subscription has been applied. You can now check our wide range of courses.
                        ";
        
                //$mailer = new MailSender($mail);
               // $mailer->SendMail( $member->user_email, $subject, $msg);
                
                
            }else{
                foreach($subscription_result->errors->deepAll() AS $error) {
                    array_push($errors, array($error->code => $error->message));
                }
            }

            // echo($result->customer->id);
            // echo($result->customer->paymentMethods[0]->token);
        } else {
            foreach($result->errors->deepAll() AS $error) {
                array_push($errors, array($error->code => $error->message));
                // echo($error->code . ": " . $error->message . "\n");
            }
        }

        if($errors){
            $response =  array("isValid"=> false, "Errors" => $errors);
        }
        else{
            $response = array("isValid"=> true,"Transaction" => json_encode($transaction));
        }

        return $response;
    }


    public function ExecuteTrialSubscription($session_value){
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($session_value);

        $sql = "UPDATE tbl_users SET user_level = 1
        WHERE user_id=:uid;
        
        INSERT INTO tbl_subscription_trials(`user_id`) 
        VALUES (:uid);";  

        $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $member->user_id);

        return $stmt->execute();
    }


    //Cancels a subscription of a user
    public function ExecuteMemberCancellation($gateway, $member){
        $response = array();
        $errors = array();

        if(is_null($member)){
            return;
        }

        $subscription = $this->GetMemberSubscriptionID($member->user_id);
        if(!$subscription){
            $notOk = array ("Member"=> "You don't have an existing subscription");
            $response = array("isValid"=> false,"Errors" => $notOk);
            return $response;
        }

        $result = $gateway->subscription()->cancel($subscription->user_subscription_id);
        if ($result->success) {
        $sql = "UPDATE tbl_users SET user_level = 0
        WHERE user_id=:uid;
        
        UPDATE tbl_subscription SET status = 0 , cancelled_date = NOW()
        WHERE user_id=:uid AND user_subscription_id = :usubid;";  

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $member->user_id);
        $stmt->bindParam(':usubid', $subscription->user_subscription_id);
        $stmt->execute();

        } else {
            foreach($result->errors->deepAll() AS $error) {
                array_push($errors, array($error->code => $error->message));
            }
        }
        if($errors){
            $response =  array("isValid"=> false, "Errors" => $errors);
        }
        else{
            $response = array("isValid"=> true,"Transaction" => "OK");
        }

        return $response;
    }

    public function ExecuteMemberCancellationForFree($member){
        $sql = "UPDATE tbl_users SET user_level = 0
        WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $member->user_id);
        $stmt->execute();
        return array("isValid"=> true,"Transaction" => "OK");
    }

    //Deactivates User pernamently in the earth
    public function DeactivateAccount($gateway, $member, $params){
        //First Deactivate the subscrption
        $subscription = $this->ExecuteMemberCancellation($gateway, $member);

        //Deactivate the account
        $sql = "UPDATE tbl_users SET active= 0  WHERE user_id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $member->user_id);
        $stmt->execute();
        $reason_sql = "INSERT INTO tbl_cancellation_reasons (`user_id`, `reasons`) VALUES
        (:uid, :reason)" ;
        $stmt = $this->db->prepare($reason_sql);
        $stmt->bindParam(':uid', $member->user_id);
        $stmt->bindParam(':reason', $params["Reason"]);

        return $stmt->execute();
    }

    public function GetMemberSubscriptionID($user_id){
        $sql = "SELECT * FROM tbl_subscription WHERE user_id = :uid AND status = 1 ORDER BY subscription_date DESC LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);        
    }




    /*Validation Functions*/
    public function ValidateExistEmail($email, $exclude_user_id = 0)
    {   

        $sql = "SELECT COUNT(*) FROM tbl_users WHERE user_email = :email AND user_id != :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':uid', $exclude_user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count != 0) //does not exists
        {
            return array("EmailAddress"=>'Email has been already registered');
        }

    }
    
    public function ValidateExistUserName($username, $exclude_user_id = 0)
    {   

        $sql = "SELECT COUNT(*) FROM tbl_users WHERE user_login = :uname  AND user_id != :uid" ;
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uname', $username);
        $stmt->bindParam(':uid', $exclude_user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count != 0) //does not exists
        {
            return array("Username"=>'Username has been already taken');
        }
    }
    
    public function ValidateAlreadyExistDetails($user_id)
    {   

        $sql = "SELECT COUNT(*) FROM tbl_users_detail WHERE user_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count != 0) //does not exists
        {
            return array("User"=>'User has been already been registered');
        }
    }

    public function ValidateExistFacebookID($fb_register_id)
    {   

        $sql = "SELECT * FROM tbl_users WHERE fb_register_id = :fbid LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':fbid', $fb_register_id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
      
    }    

    
    public function GetMemberByEmailAddress($email)
    {   
        try{
        $sql = "SELECT * FROM tbl_users WHERE user_email = :usm LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':usm', $email);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e)
        {
            var_dump($e);
        }        
    }     

    public function ValidateExistGoogleID($google_register_id)
    {   
        try{
        $sql = "SELECT * FROM tbl_users WHERE gmail_id = :gid LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':gid', $google_register_id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e)
        {
            var_dump($e);
        }        
    }        
    
    //membership_plan_id is generated at the braintree/paypal acct
    public function ValidateMembershipExist($membership_plan_id)
    {   
        $sql = "SELECT COUNT(*) FROM tbl_user_level WHERE level_payment_id = :mpid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mpid', $membership_plan_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("MembershipPlan"=>'Membership Plan Does not Exists!');
        }
    } 

    public function ValidateMemberTrial($user_id)
    {
        $sql = "SELECT COUNT(*) FROM tbl_subscription_trials WHERE user_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count != 0) //does exists
        {
            return array("MembershipTrial"=>'Free Trial has been rendered');
        }      
    }
    
    public function GetMembershipPlan($membership_plan_id)
    {
        $sql = "SELECT * FROM tbl_user_level WHERE level_payment_id = :mpid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mpid', $membership_plan_id);
        $stmt->execute();
        $plan = $stmt->fetch(PDO::FETCH_OBJ);
        return $plan;
    }

    public function ValidateResetTicketAndCode($reset_ticket, $code){
        $sql = "SELECT r.*, u.user_pass FROM tbl_reset_password_tickets as r 
        JOIN tbl_users as u ON r.user_id = u.user_id
        WHERE u.active = 1 AND r.active = 1 
        AND r.reset_ticket = :rst 
        AND u.user_pass = :up";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':rst', $reset_ticket);
        $hashedcode = md5($code);
        $stmt->bindParam(':up', $hashedcode);
        $stmt->execute();
        return$stmt->fetch(PDO::FETCH_OBJ);
    }
    
      /*End Validation Functions*/
}