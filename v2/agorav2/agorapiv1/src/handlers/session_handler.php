<?php 

class LoginSessionHandler
{
    private $db = null;
    public $member = null;
    private $session_key = 'agora_user_';
    public function __construct($db)
    {
        $this->db = $db;
    }

    public function ValidateLogin($args)
    {
        $sql = "SELECT * FROM tbl_users WHERE user_login = :user_login AND user_pass = :user_pass AND active = 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_login',$args['Username']);
        $encrypt_pass = md5($args['Password']);
        $stmt->bindParam(':user_pass',$encrypt_pass);
        $stmt->execute();
        $member = $stmt->fetch(PDO::FETCH_OBJ);
        if(!$member) //does not exists
        {
            return array("Error"=>'Either Usename or Password is Incorrect');
        }
        $this->member = $member;
        return null;
    }
    
    public function CreateLoginSession()
    {
        $sql = "INSERT INTO tbl_session 
                    (`session_key`,`session_value`,
                    `session_expiry`,`session_user_id`) VALUES
                    (:sk, :sv, :se, :sui)";
        
        $this->session_key = $this->session_key.substr(md5($this->member->user_id),0,12);
        
        $session_key = $this->session_key;
        //our salt will be email + user_id since it's both unique
        $session_value = md5($this->member->user_email.$this->member->user_id.date("Y-m-d H:i:s"));
        $session_validity = 1 * 60 * 60;
        $_SESSION[$session_key] = $session_value; //set the session

        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':sk', $session_key);
            $stmt->bindParam(':sv',$session_value);
            $stmt->bindParam(':se', $session_validity ); //1 hour validity
            $stmt->bindParam(':sui',   $this->member->user_id); 
            return $stmt->execute();
        }catch(PDOException $e)
        {
        }
    }

    public function GetCurrentSession()
    {
        if($_SESSION[$this->session_key])
        {
            return array("SessionKey"=> $this->session_key, "SessionValue"=>$_SESSION[$this->session_key]);
        }
        return null;
    }

    //This is used in Complete Registration
    public function CreateLoginSessionFirstTime($first_member)
    {
        $sql = "INSERT INTO tbl_session 
                    (`session_key`,`session_value`,
                    `session_expiry`,`session_user_id`) VALUES
                    (:sk, :sv, :se, :sui)";
        
        $this->session_key = $this->session_key.substr(md5($first_member->user_id),0,12);
        
        $session_key = $this->session_key;
        //our salt will be email + user_id since it's both unique
        $session_value = md5($first_member->user_email.$first_member->user_id.date("Y-m-d H:i:s"));
        $session_validity = 1 * 60 * 60;
        $_SESSION[$session_key] = $session_value; //set the session

        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':sk', $session_key);
            $stmt->bindParam(':sv',$session_value);
            $stmt->bindParam(':se', $session_validity ); //1 hour validity
            $stmt->bindParam(':sui',   $first_member->user_id); 
            return $stmt->execute();
        }catch(PDOException $e)
        {
            var_dump($e);
            
        }
    }        

}