<?php 

class AssignmentHandler
{
    private $db = null;
    private $assignment_table = 'tbl_assignment';
    private $assignment_submit_table = 'tbl_assingment_submission';
    private $assignment_viewed_table = 'tbl_viewed_assignments';
    
    public function __construct($db)
    {
        $this->db = $db;
    }

    public function CreateNewAssignment($user_id, $params)
    {
        //question_answer_mode = 1 MCQ
        $questions_sql =  'SELECT question_id FROM tbl_question
                        WHERE question_answer_mode = 1
                        AND question_bank_id = :qbid LIMIT '. $params['NoOfQuestions'];
        $questions = array();
        //MCQ
        if($params['Type'] == 2){
            $stmt = $this->db->prepare($questions_sql);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            $stmt->execute();
            $questions  =   $stmt->fetchAll(PDO::FETCH_COLUMN);
        }


        $sql = "INSERT INTO {$this->assignment_table} 
        (`category_id`,`section_item_id`, `title`,`description`, `available_from`, `available_to`,
        `due_date`, `points`,`no_of_questions`, `passing_rate`,`type_id`,`author_user_id`, 
        `attachment_file`, `question_bank_id`, `srlz_questions`, `section_id`, `level_id`)
        VALUES (:cid, :stid, :t, :d, :af, :at, :dd, :poi, :noq, :pr, :tid, :auid, :atf, :qbid, :srlzq, :si, :li)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':cid', $params['Category']);
            $stmt->bindParam(':stid', $params['Item']); //SectionItemID
            $stmt->bindParam(':si', $params['CategorySection']); //CategorySection
            $stmt->bindParam(':li', $params['Level']); //CategorySection
            $stmt->bindParam(':t', $params['Title']);
            $stmt->bindParam(':d', $params['Description']);

            $parse_afrom = strtotime($params['AvailableFrom']);
            $afrom = date('Y-m-d',$parse_afrom);
            $stmt->bindParam(':af', $afrom);

            $parse_ato = strtotime($params['AvailableTo']);
            $ato = date('Y-m-d',$parse_ato);
            $stmt->bindParam(':at', $ato);

            $parse_dd = strtotime($params['DueDate']);
            $dd = date('Y-m-d',$parse_dd);
            $stmt->bindParam(':dd', $dd);

            $stmt->bindParam(':poi', $params['Points']);
            $stmt->bindParam(':noq', $params['NoOfQuestions']);
            $stmt->bindParam(':pr', $params['PassingRate']);
            $stmt->bindParam(':tid', $params['Type']);
            $stmt->bindParam(':auid', $user_id); 
            $stmt->bindParam(':atf', $params['FileAttachment']);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            if($params['Type'] == 2){ 
                $serialized_questions = serialize($questions);
                $stmt->bindParam(':srlzq', $serialized_questions);
            }else{
                $serialized_questions = serialize($questions);
                $stmt->bindParam(':srlzq',$serialized_questions);
            }

            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }



    public function UpdateAssignment($user_id, $params)
    {
        //question_answer_mode = 1 MCQ
        $questions_sql =  'SELECT question_id FROM tbl_question
                        WHERE question_answer_mode = 1
                        AND question_bank_id = :qbid LIMIT '. $params['NoOfQuestions'];
        $questions = array();
        //MCQ
        if($params['Type'] == 2){
            $stmt = $this->db->prepare($questions_sql);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            $stmt->execute();
            $questions  =   $stmt->fetchAll(PDO::FETCH_COLUMN);
        }


        // $sql = "INSERT INTO {$this->assignment_table} 
        // (`category_id`,`section_item_id`, `title`,`description`, `available_from`, `available_to`,
        // `due_date`, `points`,`no_of_questions`, `passing_rate`,`type_id`,`author_user_id`, 
        // `attachment_file`, `question_bank_id`, `srlz_questions`)
        // VALUES (:cid, :stid, :t, :d, :af, :at, :dd, :poi, :noq, :pr, :tid, :auid, :atf, :qbid,:srlzq)";

        $sql = "UPDATE {$this->assignment_table} 
        SET `category_id` = :cid, `section_item_id` = :stid,  `title` =:t , `description` = :d,
         `available_from` = :af, `available_to` = :at,  `due_date` = :dd, `points` =:poi,
         `no_of_questions` = :noq, `passing_rate` =:pr,`type_id` = :tid,  `author_user_id` = :auid,
         `attachment_file` = :atf, `question_bank_id` = :qbid, `srlz_questions` = :srlzq, `cover_photo` =:cph
         WHERE assignment_id = :assid ";        

        
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':assid', $params['AssignmentID']);
            $stmt->bindParam(':cid', $params['Category']);
            $stmt->bindParam(':stid', $params['Item']); //SectionItemID
            $stmt->bindParam(':t', $params['Title']);
            $stmt->bindParam(':d', $params['Description']);

            $parse_afrom = strtotime($params['AvailableFrom']);
            $afrom = date('Y-m-d',$parse_afrom);
            $stmt->bindParam(':af', $afrom);

            $parse_ato = strtotime($params['AvailableTo']);
            $ato = date('Y-m-d',$parse_ato);
            $stmt->bindParam(':at', $ato);

            $parse_dd = strtotime($params['DueDate']);
            $dd = date('Y-m-d',$parse_dd);
            $stmt->bindParam(':dd', $dd);

            $stmt->bindParam(':poi', $params['Points']);
            $stmt->bindParam(':noq', $params['NoOfQuestions']);
            $stmt->bindParam(':pr', $params['PassingRate']);
            $stmt->bindParam(':tid', $params['Type']);
            $stmt->bindParam(':auid', $user_id); 
            $stmt->bindParam(':atf', $params['FileAttachment']);
            $stmt->bindParam(':qbid', $params['QuestionBank']);
            $stmt->bindParam(':cph', $params['CoverPhoto']);
            if($params['Type'] == 2){ 
                $serialized_questions = serialize($questions);
                $stmt->bindParam(':srlzq', $serialized_questions);
            }else{
                $serialized_questions = serialize($questions);
                $stmt->bindParam(':srlzq',$serialized_questions);
            }

            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
        }
    }    
    
    
    public function DeleteAssignment($user_id, $assignment_id){
        $sql = "DELETE FROM {$this->assignment_table} 
                WHERE author_user_id = :auid AND assignment_id = :aid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':auid', $user_id);
        $stmt->bindParam(':aid', $assignment_id); //SectionItemID
        return $stmt->execute();
    } 

    
    public function UpdatePendingAssignment($params){

        $sql = "UPDATE {$this->assignment_submit_table} 
        SET status = 1, pass_failed = :pf, score = :sc, teacher_comments = :tc,  date_checked = NOW()
        WHERE user_id = :auid AND assign_sumission_id = :asbid";   

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':pf', $params['PassFailed']);
        $stmt->bindParam(':sc', $params['TeacherMark']);
        $stmt->bindParam(':auid', $params['StudentID']);
        $stmt->bindParam(':tc', $params['Comment']);
        $stmt->bindParam(':asbid', $params['AssignmentSubmissionID']);  
        return $stmt->execute();
    }    





    //Srudent
    public function SubmitAssignment($user_id, $params)
    {
        $sql = "INSERT INTO {$this->assignment_submit_table} 
        (`assignment_id`,`user_id`, `score`,`answers`, 
        `answer_essay`, `answer_attachment`, `status`, `type`, `pass_failed`)
        VALUES (:aid, :uid, :sc, :ans, :anse, :anst, :s, :ty, :pf)";
        try
        {
            $status = 1;
            $stmt = $this->db->prepare($sql);
            if($params['Type'] == 2){  //MCQ
                $status = 1; //Complete
            }else{
                $status = 2; //Pending to be check by teacher
            }
            $stmt->bindParam(':aid', $params['AssignmentID']);
            $serialized_ans = serialize($params['Answers']);
            $stmt->bindParam(':ans', $serialized_ans);
            $stmt->bindParam(':sc', $params['Score']);
            $stmt->bindParam(':anse', $params['Essay']);
            $stmt->bindParam(':anst', $params['FileAttachment']);
            $stmt->bindParam(':s', $status);  
            $stmt->bindParam(':ty', $params['Type']);  
            $stmt->bindParam(':pf', $params['Result']);  
            $stmt->bindParam(':uid', $user_id);  
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
            var_dump($e);
        }
    }    

    public function InsertViewedAssignment($section_item_id, $user_id){
        $sql = "INSERT INTO {$this->assignment_viewed_table} 
        (`assignment_id`,`viewed_user_id`)
        VALUES (:stid, :auid)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':stid', $section_item_id);
        $stmt->bindParam(':auid', $user_id);
        return $stmt->execute();        
    }

}