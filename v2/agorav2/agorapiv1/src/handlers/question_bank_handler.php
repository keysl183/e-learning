<?php 
class QuestionBankHandler
{
    private $db = null;
    private $session_value = null;
    private $table_name = 'tbl_question_bank';
    private $question_table = 'tbl_question';
    private $question_ans_table = 'tbl_question_answers';

    public function __construct($db, $session_value)
    {
        $this->db = $db;
        $this->session_value = $session_value;
    }

    public function CreateNewQuestionBank($params)
    {
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);

        $sql = "INSERT INTO tbl_question_bank 
        (`question_bank_title`,`question_bank_desc`, `category_id`, `course_section_id`,`category_level_id` ,`author_user_id`)
        VALUES (:qbt, :qbd, :ci, :csid, :ctlvid, :aud)";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':qbt', $params['BankTitle']);
            $stmt->bindParam(':qbd', $params['BankDescription']);
            $stmt->bindParam(':ci', $params['Category']); 
            $stmt->bindParam(':csid', $params['Section']);
            $stmt->bindParam(':ctlvid', $params['Level']);
            $stmt->bindParam(':aud', $member->user_id);
            $stmt->execute();
            return  $this->db->lastInsertId();
        }
        catch (PDOException $e)
        {
        }
    }

    public function UpdateQuestionBank($params)
    {
        $question_mapper = new QuestionBankMapper($this->db);
        $question_bank = $question_mapper->GetQuestionBankById($params['BankID'], $this->session_value);
        $bank_title = (isset($params['BankTitle']))  ?   $params['BankTitle'] : $question_bank->question_bank_title;
        $bank_desc = (isset($params['BankDescription']))  ?   $params['BankDescription'] : $question_bank->question_bank_desc;
        $active = (isset($params['Delete']))  ?   0 : $question_bank->active;

        $sql = "UPDATE {$this->table_name} 
                SET question_bank_title=:qbt, question_bank_desc=:qbd, active = :active
                WHERE question_bank_id=:qbid";
        try
        {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':qbt', $bank_title);
            $stmt->bindParam(':qbd', $bank_desc);
            $stmt->bindParam(':qbid', $params['BankID']);
            $stmt->bindParam(':active', $active);
            return $stmt->execute();
        }
        catch (PDOException $e)
        {
        }
    }

    public function CreateNewQuestion($params)
    {
        $sql = "INSERT INTO tbl_question 
        (`question_bank_id`,`question_type_id`,`question_description`,
         `question_answer_mode`,`question_attachment`)
        VALUES (:qbid, :qtid, :qd, :qam, :qat)";


        $choices_sql = "INSERT INTO tbl_question_answers 
                        (`question_id`,`question_choice`,`correct_answer`, `question_choice_attachments`) VALUES
                        (:qid, :qa, :ca, :qca)";

        try
        {
            $this->db->beginTransaction();
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':qbid', $params['BankID']);
            $stmt->bindParam(':qtid', $params['QuestionType']);
            // $stmt->bindParam(':qt', $params['QuestionTitle']);
            $stmt->bindParam(':qd', $params['QuestionDescription']);
            $stmt->bindParam(':qam', $params['AnswerMode']);
            $stmt->bindParam(':qat', $params['QuestionAttachment']);
            $stmt->execute();

            $question_id = $this->db->lastInsertId(); 
            $choices = serialize(
                array(
                    $params['ChoiceOne'],
                    $params['ChoiceTwo'],
                    $params['ChoiceThree'],
                    $params['ChoiceFour']
                )
            );
            $choices_attachments = serialize(
                array(
                    $params['ChoiceOneAttachment'],
                    $params['ChoiceTwoAttachment'],
                    $params['ChoiceThreeAttachment'],
                    $params['ChoiceFourAttachment']
                )
            );

            $stmt = $this->db->prepare($choices_sql);
            $stmt->bindParam(':qid', $question_id);
            $stmt->bindParam(':qa', $choices);
            $stmt->bindParam(':ca', $params['CorrectAnswer']);
            $stmt->bindParam(':qca', $choices_attachments);
            $stmt->execute();
            $this->db->commit();
        }
        catch (PDOException $e)
        {
            var_dump($e);
            $this->db->rollback(); 
        }
    }

    public function UpdateQuestion($params)
    {
        $question_mapper = new QuestionBankMapper($this->db);
        $question = $question_mapper->GetQuestionByID($params['QuestionID']);
        $question_type = (isset($params['QuestionType']))  ?   $params['QuestionType'] : $question->question_type_id;
        // $question_title = (isset($params['QuestionTitle']))  ?   $params['QuestionTitle'] : $question->question_title;
        $question_desc = (isset($params['QuestionDescription']))  ?   $params['QuestionDescription'] : $question->question_description;
        $question_ans_mode = (isset($params['AnswerMode']))  ?   $params['AnswerMode'] : $question->question_answer_mode;

        $question_answers = $question_mapper->GetQuestionAnswersByID($params['QuestionID']);
        $choices = unserialize($question_answers->question_choice);

        $choice_one = (isset($params['ChoiceOne']))         ?   $params['ChoiceOne']   :   $choices[0];
        $choice_two = (isset($params['ChoiceTwo']))         ?   $params['ChoiceTwo']   :   $choices[1];
        $choice_three = (isset($params['ChoiceThree']))     ?   $params['ChoiceThree'] :   $choices[2];
        $choice_four = (isset($params['ChoiceFour']))       ?   $params['ChoiceFour']  :   $choices[3];
        $choice_correct = (isset($params['CorrectAnswer'])) ?   $params['CorrectAnswer'] : $question_answers->correct_answer;


        $sql = "UPDATE {$this->question_table} 
                SET question_type_id    = :qtid,
                    question_description = :qd, question_answer_mode = :qam ,
                    question_attachment = :qta
                WHERE question_id=:qid";

        $sql_ans = "UPDATE {$this->question_ans_table} 
                SET question_choice    = :qc, correct_answer = :ca,
                question_choice_attachments = :qca
                WHERE question_id=:qid";
        
        try
        {
            $this->db->beginTransaction();
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':qtid', $question_type);
            $stmt->bindParam(':qd', $question_desc);
            $stmt->bindParam(':qam', $question_ans_mode);
            $stmt->bindParam(':qta', $params['QuestionAttachment']);
            $stmt->bindParam(':qid', $params['QuestionID']);
            $stmt->execute();
            
            //Answers
            $choices_ans = serialize(
                array(
                    $choice_one,
                    $choice_two,
                    $choice_three,
                    $choice_four
                )
            );

            $choices_attachments = serialize(
                array(
                    $params['ChoiceOneAttachment'],
                    $params['ChoiceTwoAttachment'],
                    $params['ChoiceThreeAttachment'],
                    $params['ChoiceFourAttachment']
                )
            );


            $stmt = $this->db->prepare($sql_ans);
            $stmt->bindParam(':qc', $choices_ans);
            $stmt->bindParam(':ca', $choice_correct);
            $stmt->bindParam(':qca', $choices_attachments);
            $stmt->bindParam(':qid', $params['QuestionID']);
            $stmt->execute();
            $this->db->commit();
        }
        catch (PDOException $e)
        {
            //$e->getMessage();
            $this->db->rollback(); 
        }
    }

    public function DeleteQuestion($question_id){

        $sql = "DELETE FROM {$this->question_table} 
                WHERE question_id=".$question_id;
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':qid', $question_id);
        return $stmt->execute();
    }






    /*Validation Functions*/
    public function ValidateExistBank($id)
    {   
        $member_mapper = new MemberMapper($this->db);
        $member = $member_mapper->GetMemberBySession($this->session_value);

        $sql = "SELECT COUNT(*) FROM tbl_question_bank WHERE question_bank_id = :id AND author_user_id = :auid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':auid', $member->user_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("QuestionBank"=>'Question Bank does not exists!');
        }
    }
    //Static
    public function _ValidateBankIfEnoughQuestions($id, $no_of_questions, $db=null)
    {   
        $sql = "SELECT COUNT(*) FROM tbl_question WHERE question_bank_id = :id";
        $db = isset($db) ? $db : $this->db;
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count < $no_of_questions) //does not exists
        {
            return array("QuestionBank"=> 
            'Question Bank does not have enough questions!');
        }
    }


    public function ValidateExistQuestionType($id)
    {   
        $sql = "SELECT COUNT(*) FROM tbl_question_type WHERE question_type_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("QuestionType"=>'Question Type does not exists!');
        }
    }

    public function ValidateExistAnswerMode($id)
    {   
        $sql = "SELECT COUNT(*) FROM tbl_question_answer_mode WHERE question_answer_mode_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("AnswerMode"=>'Answer Mode does not exists!');
        }
    }

    public function ValidateExistQuestion($id)
    {   
        $sql = "SELECT COUNT(*) FROM tbl_question WHERE question_id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        if($count == 0) //does not exists
        {
            return array("QuestionID"=>'Question does not exists!');
        }
    }
      /*End Validation Functions*/


}