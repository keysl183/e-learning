-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2018 at 08:21 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agora_dev_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activation_keys`
--

CREATE TABLE `tbl_activation_keys` (
  `activation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(200) NOT NULL,
  `active` int(11) DEFAULT '1',
  `issue_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activation_keys`
--

INSERT INTO `tbl_activation_keys` (`activation_id`, `user_id`, `activation_key`, `active`, `issue_date`) VALUES
(1, 31, '9ae9ec6efe4cac131e032b0f67a60929', 1, '2018-05-01 18:31:49'),
(2, 32, '566ce6b0cc0d6d98e4993e1b151b79b5', 1, '2018-05-03 23:02:03'),
(3, 33, '941992a92e14423fbcd17eda76687825', 1, '2018-05-03 23:07:20'),
(4, 34, '596231889abdabd0fc734f4bff51cd82', 1, '2018-05-03 23:08:57'),
(5, 3, 'a57916875e3f90802c627b1116f7d38a', 1, '2018-05-17 21:05:59'),
(6, 4, '76d715b3c8b116c1ae7f8ef13e16759f', 1, '2018-05-27 19:37:11'),
(7, 5, 'dda2c36a4f63e17b99d112856dfc3c05', 1, '2018-06-05 22:33:27'),
(8, 6, 'a5bd969a5d385fde223d02c0939414d7', 1, '2018-06-17 03:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(50) NOT NULL,
  `category_desc` varchar(500) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_title`, `category_desc`, `active`, `date_added`) VALUES
(1, 'Math', 'MATH CATEGORY', 1, '2018-05-14 18:49:06'),
(2, 'English', 'English', 1, '2018-05-27 22:27:49'),
(3, 'tes', 'tea', 0, '2018-05-30 23:02:08'),
(4, 'asd', 'asd', 0, '2018-06-02 19:10:11'),
(5, 'asd', 'asd', 0, '2018-06-02 19:11:08'),
(6, 'sad', 'asdasd', 0, '2018-06-02 19:11:15'),
(7, 'asd', 'asdasd', 0, '2018-06-02 19:11:22'),
(8, 'Science', '', 1, '2018-06-06 00:11:53'),
(9, 'Literature', 'Literature', 1, '2018-06-10 23:09:48'),
(10, 'Algebra', 'Algebra', 1, '2018-06-10 23:10:12'),
(11, 'Chemistry', 'Chemistry', 1, '2018-06-10 23:10:19'),
(12, 'Physics', 'Physics', 1, '2018-06-10 23:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `country_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AX', 'Åland Islands'),
(3, 'AL', 'Albania'),
(4, 'DZ', 'Algeria'),
(5, 'AS', 'American Samoa'),
(6, 'AD', 'Andorra'),
(7, 'AO', 'Angola'),
(8, 'AI', 'Anguilla'),
(9, 'AQ', 'Antarctica'),
(10, 'AG', 'Antigua and Barbuda'),
(11, 'AR', 'Argentina'),
(12, 'AM', 'Armenia'),
(13, 'AW', 'Aruba'),
(14, 'AU', 'Australia'),
(15, 'AT', 'Austria'),
(16, 'AZ', 'Azerbaijan'),
(17, 'BS', 'Bahamas'),
(18, 'BH', 'Bahrain'),
(19, 'BD', 'Bangladesh'),
(20, 'BB', 'Barbados'),
(21, 'BY', 'Belarus'),
(22, 'BE', 'Belgium'),
(23, 'BZ', 'Belize'),
(24, 'BJ', 'Benin'),
(25, 'BM', 'Bermuda'),
(26, 'BT', 'Bhutan'),
(27, 'BO', 'Bolivia, Plurinational St'),
(28, 'BQ', 'Bonaire, Sint Eustatius a'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British Indian Ocean Terr'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CA', 'Canada'),
(41, 'CV', 'Cape Verde'),
(42, 'KY', 'Cayman Islands'),
(43, 'CF', 'Central African Republic'),
(44, 'TD', 'Chad'),
(45, 'CL', 'Chile'),
(46, 'CN', 'China'),
(47, 'CX', 'Christmas Island'),
(48, 'CC', 'Cocos (Keeling) Islands'),
(49, 'CO', 'Colombia'),
(50, 'KM', 'Comoros'),
(51, 'CG', 'Congo'),
(52, 'CD', 'Congo, the Democratic Rep'),
(53, 'CK', 'Cook Islands'),
(54, 'CR', 'Costa Rica'),
(55, 'CI', 'Côte d`Ivoire'),
(56, 'HR', 'Croatia'),
(57, 'CU', 'Cuba'),
(58, 'CW', 'Curaçao'),
(59, 'CY', 'Cyprus'),
(60, 'CZ', 'Czech Republic'),
(61, 'DK', 'Denmark'),
(62, 'DJ', 'Djibouti'),
(63, 'DM', 'Dominica'),
(64, 'DO', 'Dominican Republic'),
(65, 'EC', 'Ecuador'),
(66, 'EG', 'Egypt'),
(67, 'SV', 'El Salvado'),
(68, 'GQ', 'Equatorial Guinea'),
(69, 'ER', 'Eritrea'),
(70, 'EE', 'Estonia'),
(71, 'ET', 'Ethiopia'),
(72, 'FK', 'Falkland Islands (Malvina'),
(73, 'FO', 'Faroe Islands'),
(74, 'FJ', 'Fiji'),
(75, 'FI', 'Finland'),
(76, 'FR', 'France'),
(77, 'GF', 'French Guiana'),
(78, 'PF', 'French Polynesia'),
(79, 'TF', 'French Southern Territori'),
(80, 'GA', 'Gabon'),
(81, 'GM', 'Gambia'),
(82, 'GE', 'Georgia'),
(83, 'DE', 'Germany'),
(84, 'GH', 'Ghana'),
(85, 'GI', 'Gibraltar'),
(86, 'GR', 'Greece'),
(87, 'GL', 'Greenland'),
(88, 'GD', 'Grenada'),
(89, 'GP', 'Guadeloupe'),
(90, 'GU', 'Guam'),
(91, 'GT', 'Guatemala'),
(92, 'GG', 'Guernsey'),
(93, 'GN', 'Guinea'),
(94, 'GW', 'Guinea-Bissau'),
(95, 'GY', 'Guyana'),
(96, 'HT', 'Haiti'),
(97, 'HM', 'Heard Island and McDonald'),
(98, 'VA', 'Holy See (Vatican City St'),
(99, 'HN', 'Honduras'),
(100, 'HK', 'Hong Kong'),
(101, 'HU', 'Hungary'),
(102, 'IS', 'Iceland'),
(103, 'IN', 'India'),
(104, 'ID', 'Indonesia'),
(105, 'IR', 'Iran, Islamic Republic of'),
(106, 'IQ', 'Iraq'),
(107, 'IE', 'Ireland'),
(108, 'IM', 'Isle of Man'),
(109, 'IL', 'Israel'),
(110, 'IT', 'Italy'),
(111, 'JM', 'Jamaica'),
(112, 'JP', 'Japan'),
(113, 'JE', 'Jersey'),
(114, 'JO', 'Jordan'),
(115, 'KZ', 'Kazakhstan'),
(116, 'KE', 'Kenya'),
(117, 'KI', 'Kiribati'),
(118, 'KP', 'Korea, Democratic People`'),
(119, 'KR', 'Korea, Republic of'),
(120, 'KW', 'Kuwait'),
(121, 'KG', 'Kyrgyzstan'),
(122, 'LA', 'Lao People`s Democratic R'),
(123, 'LV', 'Latvia'),
(124, 'LB', 'Lebanon'),
(125, 'LS', 'Lesotho'),
(126, 'LR', 'Liberia'),
(127, 'LY', 'Libya'),
(128, 'LI', 'Liechtenstein'),
(129, 'LT', 'Lithuania'),
(130, 'LU', 'Luxembourg'),
(131, 'MO', 'Macao'),
(132, 'MK', 'Macedonia, the former Yug'),
(133, 'MG', 'Madagascar'),
(134, 'MW', 'Malawi'),
(135, 'MY', 'Malaysia'),
(136, 'MV', 'Maldives'),
(137, 'ML', 'Mali'),
(138, 'MT', 'Malta'),
(139, 'MH', 'Marshall Islands'),
(140, 'MQ', 'Martinique'),
(141, 'MR', 'Mauritania'),
(142, 'MU', 'Mauritius'),
(143, 'YT', 'Mayotte'),
(144, 'FM', 'Micronesia, Federated Sta'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestinian Territory, Oc'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Réunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'BL', 'Saint Barthélemy'),
(185, 'SH', 'Saint Helena, Ascension a'),
(186, 'KN', 'Saint Kitts and Nevis'),
(187, 'LC', 'Saint Lucia'),
(188, 'MF', 'Saint Martin (French part'),
(189, 'PM', 'Saint Pierre and Miquelon'),
(190, 'VC', 'Saint Vincent and the Gre'),
(191, 'WS', 'Samoa'),
(192, 'SM', 'San Marino'),
(193, 'ST', 'Sao Tome and Principe'),
(194, 'SA', 'Saudi Arabia'),
(195, 'SN', 'Senegal'),
(196, 'RS', 'Serbia'),
(197, 'SC', 'Seychelles'),
(198, 'SL', 'Sierra Leone'),
(199, 'SG', 'Singapore'),
(200, 'SX', 'Sint Maarten (Dutch part)'),
(201, 'SK', 'Slovakia'),
(202, 'SI', 'Slovenia'),
(203, 'SB', 'Solomon Islands'),
(204, 'SO', 'Somalia'),
(205, 'ZA', 'South Africa'),
(206, 'GS', 'South Georgia and the Sou'),
(207, 'SS', 'South Sudan'),
(208, 'ES', 'Spain'),
(209, 'LK', 'Sri Lanka'),
(210, 'SD', 'Sudan'),
(211, 'SR', 'Suriname'),
(212, 'SJ', 'Svalbard and Jan Mayen'),
(213, 'SZ', 'Swaziland'),
(214, 'SE', 'Sweden'),
(215, 'CH', 'Switzerland'),
(216, 'SY', 'Syrian Arab Republic'),
(217, 'TW', 'Taiwan, Province of China'),
(218, 'TJ', 'Tajikistan'),
(219, 'TZ', 'Tanzania, United Republic'),
(220, 'TH', 'Thailand'),
(221, 'TL', 'Timor-Leste'),
(222, 'TG', 'Togo'),
(223, 'TK', 'Tokelau'),
(224, 'TO', 'Tonga'),
(225, 'TT', 'Trinidad and Tobago'),
(226, 'TN', 'Tunisia'),
(227, 'TR', 'Turkey'),
(228, 'TM', 'Turkmenistan'),
(229, 'TC', 'Turks and Caicos Islands'),
(230, 'TV', 'Tuvalu'),
(231, 'UG', 'Uganda'),
(232, 'UA', 'Ukraine'),
(233, 'AE', 'United Arab Emirates'),
(234, 'GB', 'United Kingdom'),
(235, 'US', 'United States'),
(236, 'UM', 'United States Minor Outly'),
(237, 'UY', 'Uruguay'),
(238, 'UZ', 'Uzbekistan'),
(239, 'VU', 'Vanuatu'),
(240, 'VE', 'Venezuela, Bolivarian Rep'),
(241, 'VN', 'Viet Nam'),
(242, 'VG', 'Virgin Islands, British'),
(243, 'VI', 'Virgin Islands, U.S.'),
(244, 'WF', 'Wallis and Futuna'),
(245, 'EH', 'Western Sahara'),
(246, 'YE', 'Yemen'),
(247, 'ZM', 'Zambia'),
(248, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_excerpt` varchar(500) DEFAULT NULL,
  `course_description` text,
  `course_category` int(11) NOT NULL,
  `course_cover_image` text,
  `author_user_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_enrolled` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_id`, `course_title`, `course_excerpt`, `course_description`, `course_category`, `course_cover_image`, `author_user_id`, `date_added`, `student_enrolled`, `active`) VALUES
(1, 'KL CrashCourse to PHPUPD', 'This is a comprehensive Course to PHPUPD', 'This is a test description', 1, '', 2, '2018-05-01 13:41:36', 2, 1),
(2, 'KL CrashCourse to PHP', 'This is a comprehensive Course to PHP', 'This is a test description', 1, '', 2, '2018-05-01 14:20:26', 4, 1),
(3, 'TEST', 'TEST', 'TEST', 1, '', 4, '2018-05-27 22:58:31', 1, 0),
(4, 'TEST', 'TEST', 'TEST', 1, '', 4, '2018-05-27 23:01:33', 0, 0),
(5, 'TEST22', 'TEST22', 'TEST', 2, '', 4, '2018-05-27 23:09:38', 0, 0),
(6, 'PENGUIN22', 'TESTPENGUION', 'TEST', 1, 'image_20180528-5b0ba55abd4dc.jpg', 4, '2018-05-27 23:44:50', 0, 1),
(7, 'TEST EXAM 2', 'RWA', 'asdasdasd', 1, 'image_20180530-5b0e2798d4fa6.jpg', 4, '2018-05-29 21:24:58', 0, 1),
(8, 'English Exam For dummies', 'This is a comprehensive Course that enhances your English Skills for better.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2, NULL, 4, '2018-05-29 21:35:01', 0, 1),
(9, 'asd', 'asd', 'sadasd', 1, NULL, 4, '2018-06-02 19:39:29', 0, 0),
(10, 'sad', 'asd', 'asd', 1, NULL, 4, '2018-06-02 19:40:39', 0, 0),
(11, 'Elementary Math', 'A Simple Course Exam for kids', 'Lorem ipsum dolor sit amet, mea maiorum scaevola expetenda ex, nihil impetus sea ne, quo cu ullum senserit.\n\n     Ne putant eripuit suavitate est, ea mea diceret appellantur, vim ea mollis feugait, im ea mollis feugait, im ea mollis feugait, im ea mollis feugait ea mea diceret appellantur, vim ea mollis feugait, im ea mollis feugait, im ea mollis feugait, im ea mollis feugait', 1, 'image_20180606-5b179b40440d5.jpg', 4, '2018-06-06 01:28:55', 5, 1),
(12, 'asda', 'sdasd', 'asdasd', 1, '', 4, '2018-06-10 19:53:04', 0, 0),
(13, 'asd', 'asdas', 'dasdas', 1, '', 4, '2018-06-10 19:57:42', 0, 0),
(14, 'asdas', 'asd', 'asdasdasd', 1, '', 4, '2018-06-10 20:00:24', 0, 0),
(15, 'aasdas', 'dasd', 'asdasdada', 1, '', 4, '2018-06-10 20:02:29', 0, 0),
(16, 'asdasdas', 'asdasdasd', 'dasdasd', 1, '', 4, '2018-06-10 20:02:44', 0, 0),
(17, 'asdad', 'asdas', 'dasdad', 1, '', 4, '2018-06-10 20:05:13', 0, 0),
(18, 'KL22', 'zxcxzcxzc', 'xc', 1, '', 4, '2018-06-10 20:09:08', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section`
--

CREATE TABLE `tbl_course_section` (
  `course_section_id` int(11) NOT NULL,
  `section_title` varchar(255) NOT NULL,
  `section_description` text,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section`
--

INSERT INTO `tbl_course_section` (`course_section_id`, `section_title`, `section_description`, `course_id`) VALUES
(1, 'test', 'test', 6),
(2, 'tes2', 'test2', 7),
(3, 'update2', 'asdasd', 8),
(4, 'test21', 'sdasdas', 6),
(5, 'test3sad', 'sadas', 7),
(6, 'test', 'hj', 8),
(7, 'SECTION 3', 'SECTIO', 6),
(8, 'TEST SECTION', 'TEST SECTION', 8),
(9, 'Basic Addition', 'Contains series of worksheets that can jumpstart your brain. It provides a lot of mathematical thinking involving Addition. Futher more enhancing your kid.', 11),
(10, 'TES', 'TEst', 18),
(11, 'Basic Substraction', 'Contains series of Substraction Questions', 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section_item`
--

CREATE TABLE `tbl_course_section_item` (
  `section_item_id` int(11) NOT NULL,
  `item_type` int(11) NOT NULL COMMENT '1 = WorkSheet, 2 = Unit Test',
  `course_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `ws_bank_id` int(11) DEFAULT NULL COMMENT 'applicable only if item is worksheet type',
  `ut_serialize_bank_id` text COMMENT 'applicable only if item is unit test type',
  `answer_type` int(11) DEFAULT NULL COMMENT '1 = multi_choice 2 = essay',
  `no_of_question` int(11) NOT NULL DEFAULT '10',
  `passing_rate` int(11) NOT NULL DEFAULT '5',
  `time_limit` int(11) DEFAULT '0',
  `retake_count` int(11) DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section_item`
--

INSERT INTO `tbl_course_section_item` (`section_item_id`, `item_type`, `course_id`, `section_id`, `title`, `description`, `ws_bank_id`, `ut_serialize_bank_id`, `answer_type`, `no_of_question`, `passing_rate`, `time_limit`, `retake_count`, `date_added`) VALUES
(1, 1, 8, 3, 'New WorkSheet', 'New WorkSheet', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:25:36'),
(2, 1, 8, 3, 'TEST WORKSHEET 2', 'TEST WORKSHEET 2', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:28:30'),
(3, 1, 7, 2, 'asd', 'asdasd', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:29:44'),
(4, 1, 7, 5, 'asd', 'asdsad', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:30:40'),
(5, 1, 8, 3, 'TESTWORKSHEET3', 'TESTWORKSHEET3', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:53:28'),
(6, 1, 8, 6, 'TESTWORKSHEET', 'TESTWORKSHEET', 24, NULL, 1, 10, 5, 0, 0, '2018-06-03 00:18:22'),
(7, 2, 8, 6, 'TEST', 'TEST', NULL, NULL, 1, 10, 5, 0, 0, '2018-06-03 00:20:57'),
(8, 1, 8, 8, 'TEST WORKSHEET', 'TEST WORKSHEET', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 10:28:54'),
(9, 1, 11, 9, 'Basic Addition I', 'Basic Addition I', 29, NULL, 1, 10, 5, 0, 0, '2018-06-06 01:30:32'),
(10, 1, 11, 9, 'Basic Addition II', 'Basic Addition II', 29, NULL, 1, 12, 5, 0, 0, '2018-06-12 11:49:16'),
(11, 1, 8, 3, 'test', 'test', 24, NULL, 1, 10, 8, NULL, NULL, '2018-06-15 02:10:37'),
(12, 2, 8, 6, 'tes', 'test', NULL, NULL, 1, 15, 8, 25, 2, '2018-06-15 02:18:32'),
(13, 2, 8, 6, 'te', 'sada', NULL, NULL, 1, 10, 8, 25, 2, '2018-06-15 02:19:57'),
(14, 1, 11, 11, 'Basic Substraction I', 'Basic Substraction I', 30, NULL, 1, 3, 2, NULL, NULL, '2018-06-15 11:16:01'),
(15, 1, 11, 11, 'Basic Substraction II', 'Basic Substraction II', 30, NULL, 1, 3, 2, NULL, NULL, '2018-06-15 11:18:38'),
(16, 2, 11, 9, 'Basic Addition Unit Test', 'Basic Addition Unit Test', NULL, NULL, 1, 12, 8, 5, 2, '2018-06-17 02:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section_progress`
--

CREATE TABLE `tbl_course_section_progress` (
  `section_progress_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `section_item_id` int(11) NOT NULL,
  `questions` text NOT NULL COMMENT 'SERIALIZE Question ID''s',
  `score` int(11) DEFAULT '0',
  `answers` text,
  `status` int(11) DEFAULT '1' COMMENT '0 = CANCELLED 1=INPROGRESS 2 = PENDING = 3 COMPLETED',
  `wrong_answer_question_ids` text,
  `pass_failed` int(11) DEFAULT '0' COMMENT '0 = not  completed, 1 = Passed, 2= Failed',
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section_progress`
--

INSERT INTO `tbl_course_section_progress` (`section_progress_id`, `user_id`, `section_item_id`, `questions`, `score`, `answers`, `status`, `wrong_answer_question_ids`, `pass_failed`, `active`, `date_added`) VALUES
(13, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-12 21:35:44'),
(14, 2, 5, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"15\";}i:1;a:1:{s:11:\"question_id\";s:2:\"11\";}i:2;a:1:{s:11:\"question_id\";s:2:\"14\";}i:3;a:1:{s:11:\"question_id\";s:2:\"13\";}i:4;a:1:{s:11:\"question_id\";s:2:\"12\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:22:59'),
(15, 2, 10, 'a:11:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"22\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"24\";}i:10;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:27:13'),
(16, 2, 6, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"14\";}i:1;a:1:{s:11:\"question_id\";s:2:\"11\";}i:2;a:1:{s:11:\"question_id\";s:2:\"12\";}i:3;a:1:{s:11:\"question_id\";s:2:\"15\";}i:4;a:1:{s:11:\"question_id\";s:2:\"13\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:27:31'),
(17, 4, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"20\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:28:42'),
(18, 2, 8, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"15\";}i:1;a:1:{s:11:\"question_id\";s:2:\"12\";}i:2;a:1:{s:11:\"question_id\";s:2:\"11\";}i:3;a:1:{s:11:\"question_id\";s:2:\"13\";}i:4;a:1:{s:11:\"question_id\";s:2:\"14\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-14 22:05:48'),
(19, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 1, 'a:3:{i:0;i:2;i:1;i:4;i:2;i:2;}', 3, 'a:3:{i:0;s:2:\"29\";i:1;s:2:\"27\";i:2;i:0;}', 2, 1, '2018-06-15 11:20:49'),
(20, 2, 16, 'a:11:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"26\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}i:10;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-17 02:57:41'),
(21, 2, 1, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"12\";}i:1;a:1:{s:11:\"question_id\";s:2:\"14\";}i:2;a:1:{s:11:\"question_id\";s:2:\"11\";}i:3;a:1:{s:11:\"question_id\";s:2:\"15\";}i:4;a:1:{s:11:\"question_id\";s:2:\"13\";}}', 0, 'a:5:{i:0;i:2;i:1;i:2;i:2;i:2;i:3;i:2;i:4;i:4;}', 3, 'a:5:{i:0;s:2:\"12\";i:1;s:2:\"14\";i:2;s:2:\"11\";i:3;s:2:\"15\";i:4;s:2:\"13\";}', 2, 1, '2018-06-20 22:41:36'),
(22, 1, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"21\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"17\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-20 22:53:22'),
(23, 1, 15, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 1, 'a:3:{i:0;i:2;i:1;i:4;i:2;i:2;}', 3, 'a:3:{i:0;i:0;i:1;s:2:\"29\";i:2;s:2:\"27\";}', 2, 1, '2018-06-20 22:53:43'),
(24, 1, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 1, 'a:3:{i:0;i:2;i:1;i:2;i:2;i:2;}', 3, 'a:3:{i:0;i:0;i:1;s:2:\"27\";i:2;s:2:\"29\";}', 2, 1, '2018-06-20 22:54:25'),
(25, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:10:51'),
(26, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:11:04'),
(27, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"21\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"23\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"22\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:11:09'),
(28, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"19\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"22\";}i:4;a:1:{s:11:\"question_id\";s:2:\"21\";}i:5;a:1:{s:11:\"question_id\";s:2:\"30\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:15:02'),
(29, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"21\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"25\";}i:7;a:1:{s:11:\"question_id\";s:2:\"22\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"24\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:15:04'),
(30, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"24\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:09'),
(31, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"24\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:12'),
(32, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"17\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"25\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:15'),
(33, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"25\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"20\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:19'),
(34, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"19\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"22\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"20\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:25'),
(35, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"19\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:34'),
(36, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"18\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:20:37'),
(37, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"24\";}i:1;a:1:{s:11:\"question_id\";s:2:\"26\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"16\";}i:6;a:1:{s:11:\"question_id\";s:2:\"25\";}i:7;a:1:{s:11:\"question_id\";s:2:\"19\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:37:32'),
(38, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"25\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"21\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:38:20'),
(39, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"23\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:38:23'),
(40, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"24\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"18\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:38:54'),
(41, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"16\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"30\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"22\";}i:9;a:1:{s:11:\"question_id\";s:2:\"25\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:41:24'),
(42, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"18\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:47:13'),
(43, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"24\";}i:1;a:1:{s:11:\"question_id\";s:2:\"16\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"17\";}i:4;a:1:{s:11:\"question_id\";s:2:\"18\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:47:15'),
(44, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:47:55'),
(45, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:47:56'),
(46, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"23\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 11:48:00'),
(47, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"20\";}i:3;a:1:{s:11:\"question_id\";s:2:\"16\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:06'),
(48, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"18\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:08'),
(49, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"24\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"21\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:11'),
(50, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"21\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"26\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"16\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"20\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:14'),
(51, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"30\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"16\";}i:4;a:1:{s:11:\"question_id\";s:2:\"18\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:17'),
(52, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"30\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"22\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:19'),
(53, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:04:22'),
(54, 2, 16, 'a:12:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"17\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"21\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}i:10;a:1:{s:11:\"question_id\";s:2:\"30\";}i:11;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:05:18'),
(55, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"17\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:05:23'),
(56, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"23\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-21 12:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discussion`
--

CREATE TABLE `tbl_discussion` (
  `discussion_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `section_item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discussion`
--

INSERT INTO `tbl_discussion` (`discussion_id`, `course_id`, `section_item_id`, `user_id`, `comment`, `date_added`) VALUES
(1, 11, 14, 2, 'TEST COMMENT', '2018-06-20 21:29:24'),
(2, 11, 14, 2, 'test12', '2018-06-20 22:30:47'),
(3, 11, 14, 2, 'test 3', '2018-06-20 22:35:10'),
(4, 11, 14, 2, 'test 4', '2018-06-20 22:35:13'),
(5, 11, 14, 2, 'test3', '2018-06-20 22:36:10'),
(6, 11, 14, 2, 'test3', '2018-06-20 22:36:37'),
(7, 11, 14, 2, 'test3', '2018-06-20 22:37:20'),
(8, 8, 1, 2, 'test comment here', '2018-06-20 22:44:20'),
(9, 8, 1, 2, 'test comment here2', '2018-06-20 22:44:24'),
(10, 8, 1, 2, 'test comment here3', '2018-06-20 22:44:27'),
(11, 8, 1, 2, 'test comment here5', '2018-06-20 22:44:30'),
(12, 8, 1, 2, 'test comment here6', '2018-06-20 22:44:36'),
(13, 8, 1, 2, 'test comment here7', '2018-06-20 22:44:54'),
(14, 8, 1, 2, 'test comment her7easd', '2018-06-20 22:44:58'),
(15, 8, 1, 2, 'test comment her7easdasd', '2018-06-20 22:44:59'),
(16, 8, 1, 2, 'test comment her7easdasdads', '2018-06-20 22:45:01'),
(17, 11, 14, 1, 'HEY HEY', '2018-06-20 22:54:37'),
(18, 11, 14, 1, 'HEY HEY2', '2018-06-20 22:54:57'),
(19, 11, 14, 1, 'HEY HEY2', '2018-06-20 22:55:06'),
(20, 11, 14, 1, 'HEY HEY2', '2018-06-20 22:55:35'),
(21, 11, 14, 1, 'HEY HEY2', '2018-06-20 22:55:37'),
(22, 11, 15, 1, 'TEST HERE AGAIN', '2018-06-20 23:57:35'),
(23, 11, 15, 1, 'TEST ', '2018-06-21 00:15:51'),
(24, 11, 15, 1, 'TEST555', '2018-06-21 00:17:45'),
(25, 11, 15, 1, 'ZXczxcxzczxczczxcz', '2018-06-21 00:18:44'),
(27, 11, 14, 2, 'TEST COMMENTzzzzz', '2018-06-21 09:38:54'),
(28, 11, 14, 2, 'HIdadasdasd', '2018-06-21 09:45:25'),
(29, 11, 14, 2, 'asdASDASDASD:KA:DA', '2018-06-21 09:45:31'),
(30, 11, 14, 2, 'axasdasdasdasdas', '2018-06-21 09:45:54'),
(31, 11, 14, 2, '22222222222', '2018-06-21 09:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education_level`
--

CREATE TABLE `tbl_education_level` (
  `edu_level_id` int(11) NOT NULL,
  `edu_level_description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_education_level`
--

INSERT INTO `tbl_education_level` (`edu_level_id`, `edu_level_description`) VALUES
(1, 'Elementary'),
(2, 'Secondary'),
(3, 'Undergraduate'),
(4, 'Bachelors'),
(5, 'Post Graduate');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enroll`
--

CREATE TABLE `tbl_enroll` (
  `enroll_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_enroll`
--

INSERT INTO `tbl_enroll` (`enroll_id`, `course_id`, `user_id`, `date_enrolled`) VALUES
(1, 8, 2, '2018-06-07 21:15:55'),
(4, 11, 2, '2018-06-07 21:36:13'),
(5, 11, 6, '2018-06-17 16:01:38'),
(6, 11, 1, '2018-06-20 22:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_log`
--

CREATE TABLE `tbl_mail_log` (
  `mail_id` int(11) NOT NULL,
  `mail_send_to` varchar(255) DEFAULT NULL,
  `mail_bcc` varchar(255) DEFAULT NULL,
  `mail_subject` text,
  `mail_content` text,
  `mail_send_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urgency` int(11) NOT NULL DEFAULT '1' COMMENT '1=normal, 2= urgent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`message_id`, `sender_id`, `receiver_id`, `title`, `message`, `date_added`, `urgency`) VALUES
(1, 4, 4, 'TEST TITLE', 'TEST MESSAGE', '2018-06-12 22:05:09', 1),
(2, 2, 4, 'TEST TITLE', 'TEST MESSAGE', '2018-06-12 22:11:59', 1),
(3, 2, 4, 'TEST TITLE2', 'TEST MESSAGE', '2018-06-12 22:15:08', 1),
(4, 2, 5, 'TEST TITLE2', 'TEST MESSAGE', '2018-06-12 22:32:40', 1),
(5, 2, 5, 'TEST TITLE2UPD', 'TEST MESSAGE', '2018-06-12 22:33:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question`
--

CREATE TABLE `tbl_question` (
  `question_id` int(11) NOT NULL,
  `question_bank_id` int(11) NOT NULL,
  `question_type_id` int(11) NOT NULL,
  `question_title` varchar(255) DEFAULT NULL,
  `question_description` text NOT NULL,
  `question_attachment` text COMMENT 'This is in serialize format',
  `question_answer_mode` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question`
--

INSERT INTO `tbl_question` (`question_id`, `question_bank_id`, `question_type_id`, `question_title`, `question_description`, `question_attachment`, `question_answer_mode`, `date_added`) VALUES
(1, 1, 1, 'Simple Math 101upd2', '1+1 is equal to 2. How about 1+5 Then?', NULL, 1, '2018-05-27 00:14:58'),
(2, 1, 1, 'Simple Math 102', '2+2 is equal to 5. How about 2+5 Then?', NULL, 2, '2018-05-27 00:14:58'),
(3, 1, 1, 'TEST', 'TEST', NULL, 1, '2018-05-27 00:14:58'),
(4, 1, 2, 'zxcz', 'zxczxc', 'image_20180525-5b07b0c9793dc.jpg', 1, '2018-05-27 00:14:58'),
(6, 1, 2, 'test with images', 'test', NULL, 1, '2018-05-27 00:14:58'),
(7, 1, 1, 'test essauy', 'test essay', NULL, 2, '2018-05-27 00:14:58'),
(8, 1, 1, 'asd', 'asdsad', NULL, 2, '2018-05-27 00:14:58'),
(9, 1, 2, 'test with images2', 'test with image with cover photo', 'image_20180527-5b0a2b6f6e602.jpg', 1, '2018-05-27 11:52:56'),
(10, 23, 1, '1', '1', NULL, 1, '2018-05-27 12:15:09'),
(11, 24, 1, 'Your son had promised to call you to USA,____?', ' Choose the most appropriate words to complete the sentence', NULL, 1, '2018-05-27 19:45:53'),
(12, 24, 2, 'How do koala looks like?', 'Choose appropriate Image', 'image_20180528-5b0b6dc0e1d2e.jpg', 1, '2018-05-27 19:47:45'),
(13, 24, 1, 'He has _____________________________ knowledge on the subject.', 'Choose the most appropriate set of words', NULL, 1, '2018-05-27 19:54:02'),
(14, 24, 3, 'Audio Question Test', 'What sounds like Kalimba', 'image_20180528-5b0b70fc38d3d.jpg', 1, '2018-05-27 20:01:47'),
(15, 24, 1, '1', '1', NULL, 1, '2018-06-02 20:50:03'),
(16, 29, 1, 'Simple Addition', '1+2', NULL, 1, '2018-06-06 01:27:06'),
(17, 29, 1, 'Simple Addition', '4+5', NULL, 1, '2018-06-06 01:27:29'),
(18, 29, 1, 'Simple Addition', '1+2', NULL, 1, '2018-06-12 21:30:30'),
(19, 29, 1, 'Simple Addition', '6+1', NULL, 1, '2018-06-12 21:30:49'),
(20, 29, 1, 'Simple Addition', '4+6', NULL, 1, '2018-06-12 21:31:03'),
(21, 29, 1, 'Simple Addition', '2+4', NULL, 1, '2018-06-12 21:31:23'),
(22, 29, 1, 'Simple Addition', '11+4', NULL, 1, '2018-06-12 21:31:37'),
(23, 29, 1, 'Simple Addition', '1+7', NULL, 1, '2018-06-12 21:31:57'),
(24, 29, 1, 'Simple Addition', '0+9', NULL, 1, '2018-06-12 21:32:18'),
(25, 29, 1, 'Simple Addition', '34+1', NULL, 1, '2018-06-12 21:32:39'),
(26, 29, 1, 'Simple Addition', '1+4', NULL, 1, '2018-06-12 21:33:03'),
(27, 30, 1, 'Simple Substraction', '10-6', NULL, 1, '2018-06-15 11:10:52'),
(28, 30, 1, 'Simple Substraction', '1-3', NULL, 1, '2018-06-15 11:11:08'),
(29, 30, 1, 'Simple Substraction', '50-2', NULL, 1, '2018-06-15 11:11:27'),
(30, 29, 1, 'Simple Addition', '1+2+3', NULL, 1, '2018-06-17 03:37:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_answers`
--

CREATE TABLE `tbl_question_answers` (
  `question_answers_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_choice` text,
  `question_choice_attachments` text NOT NULL,
  `correct_answer` int(11) DEFAULT NULL COMMENT 'Index of the correct answer'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_answers`
--

INSERT INTO `tbl_question_answers` (`question_answers_id`, `question_id`, `question_choice`, `question_choice_attachments`, `correct_answer`) VALUES
(1, 1, 'a:4:{i:0;s:8:\"TEST1upd\";i:1;s:8:\"TEST2upd\";i:2;s:8:\"TEST3upd\";i:3;s:8:\"TEST4upd\";}', '', 1),
(2, 2, 'a:4:{i:0;s:1:\"7\";i:1;s:1:\"5\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', '', 1),
(3, 3, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', '', 1),
(4, 4, 'a:4:{i:0;s:8:\"zxczxczx\";i:1;s:11:\"zxczxcxzczx\";i:2;s:6:\"zxczxc\";i:3;s:5:\"zxczx\";}', '', 1),
(5, 5, 'a:4:{i:0;s:2:\"aa\";i:1;s:1:\"a\";i:2;s:2:\"aa\";i:3;s:2:\"aa\";}', '', 1),
(6, 6, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180526-5b0952550cf8a.jpg\";i:1;s:32:\"image_20180526-5b09526b98815.jpg\";i:2;N;i:3;N;}', 1),
(7, 7, 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', NULL),
(8, 8, 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(9, 9, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180527-5b0a2b7ab774f.jpg\";i:1;s:32:\"image_20180527-5b0a2b7e2a1da.jpg\";i:2;N;i:3;N;}', 1),
(10, 10, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(11, 11, 'a:4:{i:0;s:9:\"a little \";i:1;s:6:\"a few \";i:2;s:8:\"another \";i:3;s:12:\"none of this\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(12, 12, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180528-5b0b6dc470c1d.jpg\";i:1;s:32:\"image_20180528-5b0b6dc72c8e9.jpg\";i:2;s:32:\"image_20180528-5b0b6dcbede20.jpg\";i:3;s:32:\"image_20180528-5b0b6dcf03df1.jpg\";}', 3),
(13, 13, 'a:4:{i:0;s:9:\"a little \";i:1;s:6:\"a few \";i:2;s:8:\"another \";i:3;s:13:\"none of this \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(14, 14, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"audio_20180528-5b0b710a49769.mp3\";i:1;s:32:\"audio_20180528-5b0b710deb64d.mp3\";i:2;s:32:\"audio_20180528-5b0b7110976e5.mp3\";i:3;s:32:\"audio_20180528-5b0b7113484d3.mp3\";}', 1),
(15, 15, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"1\";i:2;s:1:\"1\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(16, 15, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"1\";i:2;s:1:\"1\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(17, 16, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(18, 17, 'a:4:{i:0;s:1:\"2\";i:1;s:2:\"56\";i:2;s:1:\"9\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(19, 18, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"1\";i:2;s:1:\"4\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(20, 19, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"6\";i:2;s:2:\"32\";i:3;s:1:\"7\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(21, 20, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:2:\"10\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(22, 21, 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"4\";i:3;s:1:\"6\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(23, 22, 'a:4:{i:0;s:2:\"15\";i:1;s:1:\"2\";i:2;s:1:\"4\";i:3;s:2:\"21\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(24, 23, 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"8\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(25, 24, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"9\";i:2;s:1:\"6\";i:3;s:1:\"3\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(26, 25, 'a:4:{i:0;s:2:\"32\";i:1;s:2:\"35\";i:2;s:2:\"33\";i:3;s:2:\"32\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(27, 26, 'a:4:{i:0;s:1:\"5\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(28, 27, 'a:4:{i:0;s:1:\"4\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(29, 28, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(30, 29, 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:2:\"48\";i:3;s:1:\"6\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(31, 30, 'a:4:{i:0;s:1:\"5\";i:1;s:1:\"1\";i:2;s:1:\"6\";i:3;s:1:\"2\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_answer_mode`
--

CREATE TABLE `tbl_question_answer_mode` (
  `question_answer_mode_id` int(11) NOT NULL,
  `question_type_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_answer_mode`
--

INSERT INTO `tbl_question_answer_mode` (`question_answer_mode_id`, `question_type_desc`) VALUES
(1, 'multi_choice'),
(2, 'essay');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_bank`
--

CREATE TABLE `tbl_question_bank` (
  `question_bank_id` int(11) NOT NULL,
  `question_bank_title` varchar(255) NOT NULL,
  `question_bank_desc` varchar(255) DEFAULT NULL,
  `author_user_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_bank`
--

INSERT INTO `tbl_question_bank` (`question_bank_id`, `question_bank_title`, `question_bank_desc`, `author_user_id`, `active`, `date_added`) VALUES
(1, 'English Vocabulary Bank', 'TEST DESCR2', 2, 1, '2018-05-14 12:55:15'),
(2, 'TESTBANK2', 'TEST DESCR2', 2, 1, '2018-05-14 12:55:15'),
(3, 'TESTBANK3', 'TEST DESCR3', 2, 1, '2018-05-14 12:55:15'),
(4, 'TESTBANK3', 'TEST DESCR3', 2, 0, '2018-05-14 12:55:15'),
(14, 'asd', 'asd', 2, 0, '2018-05-14 12:55:15'),
(15, 'asd', 'asd', 2, 0, '2018-05-14 12:55:15'),
(16, 'asd', 'asdasd', 2, 0, '2018-05-14 12:55:15'),
(17, 'TESTBANK1UPD55', 'TEST DESCR2', 2, 0, '2018-05-14 12:55:15'),
(18, 'ds', 'fsdf', 1, 1, '2018-05-14 12:55:15'),
(19, 'sad', 'asd', 2, 0, '2018-05-14 18:47:19'),
(20, 'xzczxc', 'zxczc', 2, 0, '2018-05-14 18:47:27'),
(21, 'xz', 'zxzx', 2, 0, '2018-05-14 18:47:30'),
(22, 'xzc', 'xzc', 2, 0, '2018-05-14 18:58:16'),
(23, 'TEST NEW  BANK', 'TEST', 2, 1, '2018-05-27 12:14:46'),
(24, 'ENG Vocab MultiChoice', 'English vocabulary multiple choices question bank.\nContains text, images and audios', 4, 1, '2018-05-27 19:40:47'),
(25, 'test2', 'test2', 4, 0, '2018-05-27 23:19:38'),
(26, 'wads', 'asda', 4, 0, '2018-06-02 19:43:45'),
(27, 'TEST QUESTION BANK', 'TEST QUESTION BANK', 4, 1, '2018-06-02 20:48:54'),
(28, 'sad', 'asdasdasz', 4, 0, '2018-06-02 23:14:02'),
(29, 'MATH Simple Questions', 'Contains series of elementary level Math questions that can stirr up your mind', 4, 1, '2018-06-06 01:26:43'),
(30, 'MATH Simple Substraction', 'Simple Substraction', 4, 1, '2018-06-15 11:10:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_type`
--

CREATE TABLE `tbl_question_type` (
  `question_type_id` int(11) NOT NULL,
  `question_type_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_type`
--

INSERT INTO `tbl_question_type` (`question_type_id`, `question_type_desc`) VALUES
(1, 'text'),
(2, 'image'),
(3, 'audio');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `session_id` int(11) NOT NULL,
  `session_key` varchar(255) NOT NULL,
  `session_value` text,
  `session_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `session_expiry` bigint(20) DEFAULT NULL,
  `session_user_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`session_id`, `session_key`, `session_value`, `session_created`, `session_expiry`, `session_user_id`, `active`) VALUES
(25, 'agora_user_c81e728d9d4c', '00f392e6ba5d1dc1026717ad23a77ed8', '2018-04-29 23:20:56', 3600, 2, 1),
(26, 'agora_user_c81e728d9d4c', '0560cc66d845f69aef4b7852b9af795d', '2018-05-01 12:45:59', 3600, 2, 1),
(27, 'agora_user_c81e728d9d4c', '8177c70b7d9e50e474fc2ca7753b6bfc', '2018-05-01 19:34:12', 3600, 2, 1),
(28, 'agora_user_c81e728d9d4c', '751b736de8199a452dee72bc6100650d', '2018-05-01 19:34:18', 3600, 2, 1),
(29, 'agora_user_c81e728d9d4c', '2425d264b5defcaaec4d1dc881d2dea3', '2018-05-01 19:34:49', 3600, 2, 1),
(30, 'agora_user_c81e728d9d4c', '50fb6b59422b3bccbfa5285e70849fc7', '2018-05-01 19:34:52', 3600, 2, 1),
(31, 'agora_user_c81e728d9d4c', '7889583427c8f3dc99e6452f5d1ee5e0', '2018-05-02 20:31:29', 3600, 2, 1),
(32, 'agora_user_c81e728d9d4c', '5bcd8b0f8dfeffbd8532de8130420575', '2018-05-02 20:58:02', 3600, 2, 1),
(33, 'agora_user_c81e728d9d4c', 'e04780dfc5a9b9ae51d7374338ec9e87', '2018-05-03 21:22:08', 3600, 2, 1),
(34, 'agora_user_c81e728d9d4c', '6759771f4218f75e50da0a594f07c767', '2018-05-03 21:32:35', 3600, 2, 1),
(35, 'agora_user_c81e728d9d4c', '94d0de620f13b6de12dabdaceabada91', '2018-05-03 21:33:01', 3600, 2, 1),
(36, 'agora_user_c81e728d9d4c', 'baae9fe7f30cd36166ccc076ef347d9d', '2018-05-03 21:33:02', 3600, 2, 1),
(37, 'agora_user_c81e728d9d4c', '00081ba7423edb6069ffbace0c8c9486', '2018-05-03 21:33:05', 3600, 2, 1),
(38, 'agora_user_c81e728d9d4c', 'd5ad857bfdca117b34384a874b116668', '2018-05-03 21:33:06', 3600, 2, 1),
(39, 'agora_user_c81e728d9d4c', '39c6d62b2a5677568eed4c0bbe3dc9e7', '2018-05-03 21:33:07', 3600, 2, 1),
(40, 'agora_user_c81e728d9d4c', '5d6ffc3364f6438d398388dd0518b0b2', '2018-05-03 21:33:10', 3600, 2, 1),
(41, 'agora_user_c81e728d9d4c', 'a7a48bd6a6b25642b87b758fffd5f7f1', '2018-05-03 21:33:11', 3600, 2, 1),
(42, 'agora_user_c81e728d9d4c', 'c23095351c9ede900d72a101bdbe4516', '2018-05-03 21:33:12', 3600, 2, 1),
(43, 'agora_user_c81e728d9d4c', 'a4a8c08e5334420ee1cefb1b180aabcd', '2018-05-03 21:33:26', 3600, 2, 1),
(44, 'agora_user_c81e728d9d4c', 'cdd94992790093119cee73be033106e0', '2018-05-03 21:34:12', 3600, 2, 1),
(45, 'agora_user_c81e728d9d4c', '59198c2314acd369fa367819b2beb6ad', '2018-05-03 23:22:48', 3600, 2, 1),
(46, 'agora_user_c81e728d9d4c', 'b0fae60e85beb0920e55a556ea65d0b4', '2018-05-12 07:48:04', 3600, 2, 1),
(47, 'agora_user_c81e728d9d4c', 'b57e9d03252ce82126404827e7debfe9', '2018-05-14 02:30:03', 3600, 2, 1),
(48, 'agora_user_c81e728d9d4c', '8593524348586c0665378aa9963f1acb', '2018-05-14 02:45:07', 3600, 2, 1),
(49, 'agora_user_c81e728d9d4c', 'a25f77eed600bfd1db0a6f3a1f7052af', '2018-05-14 04:04:38', 3600, 2, 1),
(50, 'agora_user_c81e728d9d4c', '67d3b324ca45e7951c618086ba69b122', '2018-05-14 04:05:11', 3600, 2, 1),
(51, 'agora_user_c81e728d9d4c', 'ebc8712bf858836d8b8e1d9f3f10fdb3', '2018-05-14 18:31:28', 3600, 2, 1),
(52, 'agora_user_c81e728d9d4c', '2f8e617fe36b24e98998dd7da2676ec0', '2018-05-17 20:31:14', 3600, 2, 1),
(53, 'agora_user_c81e728d9d4c', '3b9567c9ea1e7d5c58b40625eab0fbde', '2018-05-17 20:34:19', 3600, 2, 1),
(54, 'agora_user_c81e728d9d4c', 'c3dc7325b3a081478ce3adcb6f33a96a', '2018-05-17 20:59:15', 3600, 2, 1),
(55, 'agora_user_c81e728d9d4c', '91d6c5b8a9ff614f9a7e2bf6fab09ded', '2018-05-17 21:04:20', 3600, 2, 1),
(56, 'agora_user_eccbc87e4b5c', 'bbd243c72cef7e9e45f5ac184fa6f7a1', '2018-05-17 21:17:28', 3600, 3, 1),
(57, 'agora_user_c81e728d9d4c', 'afc8caf9797f50c9f34c23005e6f9b35', '2018-05-24 21:37:57', 3600, 2, 1),
(58, 'agora_user_c81e728d9d4c', '1cdc033fa384b187bcaf57b39b9f492f', '2018-05-24 22:23:41', 3600, 2, 1),
(59, 'agora_user_c81e728d9d4c', 'd46626f342b0f500d9e6cebe94d5990b', '2018-05-24 22:24:44', 3600, 2, 1),
(60, 'agora_user_c81e728d9d4c', '6af00ac3cc3851d5c0c68bf77014fbfc', '2018-05-24 22:28:25', 3600, 2, 1),
(61, 'agora_user_c81e728d9d4c', '93514962a6c1ffca3bb0045843fd0e62', '2018-05-26 19:46:33', 3600, 2, 1),
(62, 'agora_user_a87ff679a2f3', 'c7dc65488606fc16da686b950dc6abe4', '2018-05-27 19:38:32', 3600, 4, 1),
(63, 'agora_user_a87ff679a2f3', '734d19058280dd346c288a07a90fb6be', '2018-05-27 19:39:12', 3600, 4, 1),
(64, 'agora_user_a87ff679a2f3', 'd393fed47c8088a26a93129136d2a9db', '2018-05-29 20:29:24', 3600, 4, 1),
(65, 'agora_user_a87ff679a2f3', 'd393fed47c8088a26a93129136d2a9db', '2018-05-29 20:29:24', 3600, 4, 1),
(66, 'agora_user_a87ff679a2f3', '6d79e192c8620d12641e9b2c0cc28189', '2018-05-29 20:30:01', 3600, 4, 1),
(67, 'agora_user_a87ff679a2f3', '74614bfedeca1a26ae84b458f2d62ef9', '2018-05-30 20:40:53', 3600, 4, 1),
(68, 'agora_user_c81e728d9d4c', '78e25e8ec553452df6327fb9929067c0', '2018-05-30 21:01:05', 3600, 2, 1),
(69, 'agora_user_a87ff679a2f3', '5dd95982d168b3ace7785b8a5bd495ff', '2018-05-30 21:01:09', 3600, 4, 1),
(70, 'agora_user_a87ff679a2f3', '60b57ba966e6551b698eeae33bc4ffd1', '2018-05-31 20:40:12', 3600, 4, 1),
(71, 'agora_user_a87ff679a2f3', '50ab29b2a77b8d4632af09a966f5a89d', '2018-05-31 23:24:40', 3600, 4, 1),
(72, 'agora_user_a87ff679a2f3', '5cd5446408d01a611f16d6fb699e7c3b', '2018-05-31 23:26:44', 3600, 4, 1),
(73, 'agora_user_c81e728d9d4c', '4b517c4233a0972af79ece564005b2ba', '2018-06-02 13:59:28', 3600, 2, 1),
(74, 'agora_user_c81e728d9d4c', 'c051a1cc8ede5651233835b3250612d3', '2018-06-02 14:01:47', 3600, 2, 1),
(75, 'agora_user_c81e728d9d4c', '13cbf0ce3ac4f4a8df6e8ead2cf8b590', '2018-06-02 14:03:08', 3600, 2, 1),
(76, 'agora_user_a87ff679a2f3', '730e53ef9ac5c16ef27c0726b8f10031', '2018-06-02 14:06:10', 3600, 4, 1),
(77, 'agora_user_a87ff679a2f3', '83b1e005671fd5efed558dfd6eecf07c', '2018-06-02 14:06:19', 3600, 4, 1),
(78, 'agora_user_a87ff679a2f3', '833f7112c7b4f631636e40fc31d992ef', '2018-06-02 15:34:28', 3600, 4, 1),
(79, 'agora_user_c81e728d9d4c', 'c49d179f316ad25c4472d8406d97b22a', '2018-06-02 22:41:34', 3600, 2, 1),
(80, 'agora_user_c81e728d9d4c', 'c7edcbed3e16397913735bf1b6951bd2', '2018-06-02 22:43:02', 3600, 2, 1),
(81, 'agora_user_a87ff679a2f3', '7a0d8d3736859274f3887450e3949ec6', '2018-06-02 22:43:20', 3600, 4, 1),
(82, 'agora_user_c81e728d9d4c', 'd0f8fc5d4085cf21bf1445dac18e173b', '2018-06-03 00:48:40', 3600, 2, 1),
(83, 'agora_user_a87ff679a2f3', '156f885a9d511641e6d2ef74e887b0c3', '2018-06-02 10:27:16', 3600, 4, 1),
(84, 'agora_user_c81e728d9d4c', 'eddd6203bf29cbaec293fe301de26b16', '2018-06-02 10:31:32', 3600, 2, 1),
(85, 'agora_user_a87ff679a2f3', 'c10bb695453f81711a40d5502f303531', '2018-06-02 10:46:10', 3600, 4, 1),
(86, 'agora_user_a87ff679a2f3', '868ec29818e6ad2fc5418ba01c8d2260', '2018-06-02 20:30:54', 3600, 4, 1),
(87, 'agora_user_c4ca4238a0b9', 'cf89e3758f5a91d8a03d215cc4e489a5', '2018-06-02 20:36:44', 3600, 1, 1),
(88, 'agora_user_a87ff679a2f3', '0858c918f6609f58d8461aa16b81f6da', '2018-06-03 03:16:05', 3600, 4, 1),
(89, 'agora_user_c81e728d9d4c', '1dceba490b536056e43de928303f0462', '2018-06-03 08:10:47', 3600, 2, 1),
(90, 'agora_user_a87ff679a2f3', 'd7e5d5f6701d04f91116dd83fd146c5e', '2018-06-03 08:12:26', 3600, 4, 1),
(91, 'agora_user_a87ff679a2f3', '7ce267d35aa6e0555e0508d95469b6f9', '2018-06-03 08:38:00', 3600, 4, 1),
(92, 'agora_user_a87ff679a2f3', 'dbca9bc0ef7b2af6a2ad927515a19b7c', '2018-06-03 08:43:29', 3600, 4, 1),
(93, 'agora_user_a87ff679a2f3', '0c2a4bf6f335944f349d1d5d9d6a14e3', '2018-06-04 06:38:22', 3600, 4, 1),
(94, 'agora_user_a87ff679a2f3', '2981ba6fab53d02633ab6498a8d99fd3', '2018-06-05 00:47:18', 3600, 4, 1),
(95, 'agora_user_a87ff679a2f3', '03ed0c5d7dab8b41360aaae065bdaf45', '2018-06-05 00:57:14', 3600, 4, 1),
(96, 'agora_user_a87ff679a2f3', '1902e9366aa826d4dc937b2d587feb84', '2018-06-05 05:31:26', 3600, 4, 1),
(97, 'agora_user_c81e728d9d4c', 'aa9f5e210509de4970783c4ed133806a', '2018-06-05 06:11:38', 3600, 2, 1),
(98, 'agora_user_a87ff679a2f3', 'd37150751cfa30c71f59190ef23bfe22', '2018-06-05 06:58:02', 3600, 4, 1),
(99, 'agora_user_a87ff679a2f3', 'de4cee0c7f50d9e42126b2f3344e4995', '2018-06-05 19:51:45', 3600, 4, 1),
(100, 'agora_user_a87ff679a2f3', '101d0d02e18383cd7b13f49e74ad5c1f', '2018-06-05 20:53:03', 3600, 4, 1),
(101, 'agora_user_e4da3b7fbbce', '2a02f9d72e3ce23518f3c172f14d83d2', '2018-06-05 22:34:48', 3600, 5, 1),
(102, 'agora_user_a87ff679a2f3', 'a27a5941f11a286fca488522b77ed723', '2018-06-06 00:11:44', 3600, 4, 1),
(103, 'agora_user_a87ff679a2f3', '63e6786f349dda55221fb1fc5a64cd40', '2018-06-06 01:25:03', 3600, 4, 1),
(104, 'agora_user_a87ff679a2f3', '4ec8fd2d7b4ec72ddef6aecf61421a71', '2018-06-07 20:52:32', 3600, 4, 1),
(105, 'agora_user_a87ff679a2f3', '8f4d039df860a4d0a34670e6cd84f74a', '2018-06-07 22:04:45', 3600, 4, 1),
(106, 'agora_user_a87ff679a2f3', '78ef751237f7b251fb130ce951c45570', '2018-06-10 19:16:54', 3600, 4, 1),
(107, 'agora_user_a87ff679a2f3', 'e0fb739afbad03813b5335bc86656405', '2018-06-10 19:41:45', 3600, 4, 1),
(108, 'agora_user_a87ff679a2f3', 'e1c7ada2a5beb5d8d7d76d5e34d62f21', '2018-06-10 22:33:00', 3600, 4, 1),
(109, 'agora_user_a87ff679a2f3', 'f22c5ce53b6c06d219aafe5923c4444b', '2018-06-10 23:09:13', 3600, 4, 1),
(110, 'agora_user_c81e728d9d4c', 'ce4719fcf042bcd63f8b9b21162c04e2', '2018-06-11 00:20:35', 3600, 2, 1),
(111, 'agora_user_a87ff679a2f3', '535b4cce2f6f1a12e3be868881bed6ba', '2018-06-11 00:20:59', 3600, 4, 1),
(112, 'agora_user_a87ff679a2f3', '73af8934f3ed5a10d82aa6d52b5a20ca', '2018-06-11 20:59:20', 3600, 4, 1),
(113, 'agora_user_c81e728d9d4c', '01fc0f711ed83640738dc59b7e8d01fd', '2018-06-11 21:00:45', 3600, 2, 1),
(114, 'agora_user_c81e728d9d4c', '98b636c805ddf5d56d3a956c570e674b', '2018-06-12 11:31:02', 3600, 2, 1),
(115, 'agora_user_a87ff679a2f3', 'e9e60a89673ef8ac5d9d83ac5fb62e35', '2018-06-12 11:40:10', 3600, 4, 1),
(116, 'agora_user_c81e728d9d4c', '13e82a23147176042e7111c704e90fe5', '2018-06-12 11:42:28', 3600, 2, 1),
(117, 'agora_user_a87ff679a2f3', '5f45ba235c19957cfa0f66c50ccd72aa', '2018-06-12 11:47:04', 3600, 4, 1),
(118, 'agora_user_c81e728d9d4c', '8ba3a167de7d5292a2bd4083f29a481b', '2018-06-12 11:49:29', 3600, 2, 1),
(119, 'agora_user_c81e728d9d4c', '6a235fd3c279ba72201d4bc0a2acaadd', '2018-06-12 21:29:44', 3600, 2, 1),
(120, 'agora_user_a87ff679a2f3', '8aed63d1ddebe9f555f6de278d7cdc72', '2018-06-12 21:29:58', 3600, 4, 1),
(121, 'agora_user_c81e728d9d4c', 'f070801c23d01d7636dd4d6ec78f6158', '2018-06-13 20:23:59', 3600, 2, 1),
(122, 'agora_user_a87ff679a2f3', '38e0dcbad800c369b9ec4fc84436984b', '2018-06-13 21:27:50', 3600, 4, 1),
(123, 'agora_user_a87ff679a2f3', '11ac9443c484e9c50ad622a2ab41bed5', '2018-06-13 21:28:56', 3600, 4, 1),
(124, 'agora_user_c81e728d9d4c', '2efb2b10b7c682671fdf7968545c9ef2', '2018-06-13 21:29:09', 3600, 2, 1),
(125, 'agora_user_c81e728d9d4c', '8601aace4d9f7a291fec650258debf1d', '2018-06-14 20:44:52', 3600, 2, 1),
(126, 'agora_user_a87ff679a2f3', '61fe0754ba10543cd350a3d82fe22af3', '2018-06-14 21:05:15', 3600, 4, 1),
(127, 'agora_user_a87ff679a2f3', 'f4b83db53f706c24019891808ef03061', '2018-06-15 00:21:20', 3600, 4, 1),
(128, 'agora_user_a87ff679a2f3', 'ab53da8ec699630a1f68cd28daa63a73', '2018-06-15 02:01:14', 3600, 4, 1),
(129, 'agora_user_c81e728d9d4c', '58da05aa10930506b67a3d2646934889', '2018-06-15 10:58:43', 3600, 2, 1),
(130, 'agora_user_a87ff679a2f3', '9f3533a083a5bddf1a991e8dc34254bc', '2018-06-15 11:09:35', 3600, 4, 1),
(131, 'agora_user_c81e728d9d4c', '74571c19bc7e58b33e5b79bd9fbf4a2b', '2018-06-15 11:20:26', 3600, 2, 1),
(132, 'agora_user_a87ff679a2f3', 'dabc0a99d923c0098f3705e58204ceef', '2018-06-17 02:07:34', 3600, 4, 1),
(133, 'agora_user_a87ff679a2f3', '19181438637023cf4fc645cbeedc9a8d', '2018-06-17 02:46:26', 3600, 4, 1),
(134, 'agora_user_c81e728d9d4c', '3828f68bd105b8f27e371015327909d1', '2018-06-17 02:58:11', 3600, 2, 1),
(135, 'agora_user_c81e728d9d4c', '90608b54eb14e0d82eb4f5440f9af19e', '2018-06-17 02:58:20', 3600, 2, 1),
(136, 'agora_user_c81e728d9d4c', '7f66694b60e0c34929e1475267985e8c', '2018-06-17 02:58:23', 3600, 2, 1),
(137, 'agora_user_c81e728d9d4c', 'fdaaa67fa8d9d7bb7b90a25ce1fe040b', '2018-06-17 02:58:46', 3600, 2, 1),
(138, 'agora_user_c81e728d9d4c', 'dd5c099a1f36d23f1b92b5f45afcc322', '2018-06-17 03:00:08', 3600, 2, 1),
(139, 'agora_user_a87ff679a2f3', 'a40b74eec409a1ca48a89e8cc28e9170', '2018-06-17 03:28:04', 3600, 4, 1),
(140, 'agora_user_a87ff679a2f3', 'bc8475249ef72162501a6646909b0ff8', '2018-06-17 03:33:54', 3600, 4, 1),
(141, 'agora_user_a87ff679a2f3', '76c960d12b42f5cc30f2881f8057eb1c', '2018-06-17 03:37:12', 3600, 4, 1),
(142, 'agora_user_e4da3b7fbbce', '21f06fd8d59445ebeaa1f98b44e031d2', '2018-06-17 03:40:04', 3600, 5, 1),
(143, 'agora_user_a87ff679a2f3', '057ac43634e7f4bf77eef6964ffddc93', '2018-06-17 03:43:59', 3600, 4, 1),
(144, 'agora_user_1679091c5a88', 'f02f3c1c0113ccc28361bf94afdfe395', '2018-06-17 16:00:28', 3600, 6, 1),
(145, 'agora_user_c81e728d9d4c', '4725e1784594aaa41d227eb90676b398', '2018-06-17 16:16:06', 3600, 2, 1),
(146, 'agora_user_a87ff679a2f3', '4312502b27403f676c0f07cca8a9791d', '2018-06-17 16:16:18', 3600, 4, 1),
(147, 'agora_user_c81e728d9d4c', 'e8913a64b96e3788d836358b8eaa557c', '2018-06-17 16:31:34', 3600, 2, 1),
(148, 'agora_user_c81e728d9d4c', 'b214f01d8dcd666f9b37ac820f4464f3', '2018-06-18 20:52:42', 3600, 2, 1),
(149, 'agora_user_c81e728d9d4c', 'd0f336d8306cae03fa0c710a778ec28d', '2018-06-20 20:57:48', 3600, 2, 1),
(150, 'agora_user_c4ca4238a0b9', '83f6cd8612d5ce4f38317b507e8975cb', '2018-06-20 22:52:11', 3600, 1, 1),
(151, 'agora_user_c4ca4238a0b9', 'e79aebe796d9b00011afda0fa23cf182', '2018-06-20 23:00:59', 3600, 1, 1),
(152, 'agora_user_c4ca4238a0b9', '51ce5b14d24730c32051700b44240019', '2018-06-20 23:02:12', 3600, 1, 1),
(153, 'agora_user_c81e728d9d4c', 'd53fc23295e0032738f27f9d21f4b45a', '2018-06-21 00:07:42', 3600, 2, 1),
(154, 'agora_user_c4ca4238a0b9', '5364c450ea2d1556923461a7c4164734', '2018-06-21 00:09:29', 3600, 1, 1),
(155, 'agora_user_c81e728d9d4c', '5eb4e354be4745f3d20cc407f0d06d99', '2018-06-21 09:44:58', 3600, 2, 1),
(156, 'agora_user_c4ca4238a0b9', 'eb98c020d9406bf6501d70ecfe7b28c6', '2018-06-21 09:46:50', 3600, 1, 1),
(157, 'agora_user_c81e728d9d4c', '2e1c34e965c435cc080c2ca55cf8bfbb', '2018-06-21 09:51:12', 3600, 2, 1),
(158, 'agora_user_c81e728d9d4c', '19c35096d6d6564ada50e3407dba3a3e', '2018-06-21 11:02:07', 3600, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscription`
--

CREATE TABLE `tbl_subscription` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_subscription_id` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `subscription_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cancelled_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subscription`
--

INSERT INTO `tbl_subscription` (`subscription_id`, `user_id`, `user_subscription_id`, `status`, `subscription_date`, `cancelled_date`) VALUES
(2, 2, 'ddycww', 1, '2018-05-12 20:37:00', '0000-00-00 00:00:00'),
(3, 2, '8wfv2w', 1, '2018-05-12 21:02:48', '0000-00-00 00:00:00'),
(4, 2, 'c5jgj2', 1, '2018-05-13 00:42:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscription_trials`
--

CREATE TABLE `tbl_subscription_trials` (
  `subscription_trial_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `trial_date_start` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subscription_trials`
--

INSERT INTO `tbl_subscription_trials` (`subscription_trial_id`, `user_id`, `trial_date_start`) VALUES
(3, 2, '2018-05-13 00:34:28'),
(4, 2, '2018-05-13 00:34:38'),
(5, 3, '2018-05-17 21:17:31'),
(6, 5, '2018-06-05 22:34:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `trans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `membership_plan_id` int(11) NOT NULL,
  `subscription_id` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `user_payment_id` varchar(100) NOT NULL,
  `transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`trans_id`, `user_id`, `membership_plan_id`, `subscription_id`, `transaction_id`, `user_payment_id`, `transaction_date`) VALUES
(1, 2, 2, 'ddycww', 'bk61v8xa', '872121981', '2018-05-12 20:37:00'),
(2, 2, 2, '8wfv2w', '8aqetyv4', '518210836', '2018-05-12 21:02:48'),
(3, 2, 2, 'c5jgj2', '2fhjwah4', '552402883', '2018-05-13 00:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_question`
--

CREATE TABLE `tbl_trans_question` (
  `trans_question_id` int(11) NOT NULL,
  `trans_question_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_question_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_country_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_trans_question`
--

INSERT INTO `tbl_trans_question` (`trans_question_id`, `trans_question_title`, `trans_question_description`, `trans_country_id`, `question_id`, `date_added`) VALUES
(12, '数学', 'テストコメント １＋２＋３', 112, 11, '2018-06-22 09:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_question_answers`
--

CREATE TABLE `tbl_trans_question_answers` (
  `trans_question_answers_id` int(11) NOT NULL,
  `trans_question_id` int(11) NOT NULL,
  `question_choice` text COLLATE utf8mb4_unicode_ci,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_trans_question_answers`
--

INSERT INTO `tbl_trans_question_answers` (`trans_question_answers_id`, `trans_question_id`, `question_choice`, `date_added`) VALUES
(9, 12, 'a:4:{i:0;s:1:\"6\";i:1;s:1:\"5\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', '2018-06-22 09:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(45) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_level` int(11) NOT NULL DEFAULT '1',
  `member_payment_id` varchar(100) NOT NULL COMMENT 'ID associated to the user when he/she pays',
  `registration_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_login`, `user_pass`, `user_email`, `user_level`, `member_payment_id`, `registration_date`, `active`) VALUES
(1, 'erukei18', '16d7a4fca7442dda3ad93c9a726597e4', 'johndoe@gmail.com', 0, '', '2018-04-18 14:04:25', 1),
(2, 'keysl183', '16d7a4fca7442dda3ad93c9a726597e4', 'marcjhon18@gmail.com', 2, '552402883', '2018-04-18 21:59:37', 1),
(3, 'keysl183+04', '16d7a4fca7442dda3ad93c9a726597e4', 'marcjhon18+02@gmail.com', 1, '', '2018-05-17 21:05:58', 1),
(4, 'teacher', '16d7a4fca7442dda3ad93c9a726597e4', 'teacher@agoraschool.com', 4, '', '2018-05-27 19:37:11', 1),
(5, 'keysl+001', '16d7a4fca7442dda3ad93c9a726597e4', 'mlawingco.ivp+001@gmail.com', 1, '', '2018-06-05 22:33:27', 1),
(6, 'keysl+002', '16d7a4fca7442dda3ad93c9a726597e4', 'mlawingco.ivp+002@gmail.com', 0, '', '2018-06-17 03:45:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_detail`
--

CREATE TABLE `tbl_users_detail` (
  `user_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` int(1) DEFAULT '1',
  `school` varchar(500) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `education_level` int(11) NOT NULL,
  `profile_image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users_detail`
--

INSERT INTO `tbl_users_detail` (`user_detail_id`, `user_id`, `fname`, `lname`, `mname`, `birthday`, `age`, `gender`, `school`, `country_id`, `zip_code`, `education_level`, `profile_image`) VALUES
(1, 2, 'KL3', 'Lawingco', 'M', '2018-12-18', 22, 1, 'Pamantasan ng Lungsod ng Valenzuela', 5, '1231', 5, 'prof_image-20180502-5ae9155d6d89b.jpeg'),
(4, 31, 'KL', 'Lawingco', 'M', '2018-12-18', 21, 1, NULL, 1, '123', 1, 'prof_image-20180502-5ae9155d6d89b.jpeg'),
(5, 34, 'KL+05', 'Lawingco', NULL, NULL, 22, 1, NULL, 3, '2', 1, NULL),
(6, 33, 'KL+04', 'Lawingco', NULL, NULL, 22, 1, NULL, 93, '1234', 1, NULL),
(7, 3, 'KL', 'Lawingco', NULL, NULL, 22, 1, NULL, 11, '123', 3, NULL),
(8, 4, 'Teacher', 'account', NULL, NULL, 21, 1, NULL, 248, '1234', 5, NULL),
(9, 5, 'KL', 'Lawingco', NULL, NULL, 21, 1, NULL, 1, '13', 5, NULL),
(10, 6, 'KL', 'Lawingco', NULL, NULL, 22, 1, NULL, 174, '1234', 5, NULL),
(11, 1, 'Eru', 'Lawingco', 'M', '2018-12-18', 22, 1, 'PLV', 2, '1231', 1, 'prof_image_20180621-5b2b6af1c867b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_level`
--

CREATE TABLE `tbl_user_level` (
  `user_level_id` int(11) NOT NULL,
  `level_name` varchar(45) NOT NULL,
  `level_description` varchar(255) DEFAULT NULL,
  `level_payment_id` varchar(50) NOT NULL,
  `level_price` decimal(10,0) DEFAULT NULL,
  `level_validity` int(11) DEFAULT NULL COMMENT 'In number of months',
  `level_validity_suffix` varchar(45) DEFAULT NULL COMMENT 'Days\nMonths\nYears'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_level`
--

INSERT INTO `tbl_user_level` (`user_level_id`, `level_name`, `level_description`, `level_payment_id`, `level_price`, `level_validity`, `level_validity_suffix`) VALUES
(1, 'Free Trial', 'Free Trial', 'FR101', '0', 5, 'days'),
(2, 'Monthly', 'Monthly', '2bgb', '15', 1, 'month'),
(3, 'Annual', 'Annual', 'g8tm', '100', 1, 'year');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_activation_keys`
--
ALTER TABLE `tbl_activation_keys`
  ADD PRIMARY KEY (`activation_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `tbl_course_section`
--
ALTER TABLE `tbl_course_section`
  ADD PRIMARY KEY (`course_section_id`);

--
-- Indexes for table `tbl_course_section_item`
--
ALTER TABLE `tbl_course_section_item`
  ADD PRIMARY KEY (`section_item_id`);

--
-- Indexes for table `tbl_course_section_progress`
--
ALTER TABLE `tbl_course_section_progress`
  ADD PRIMARY KEY (`section_progress_id`);

--
-- Indexes for table `tbl_discussion`
--
ALTER TABLE `tbl_discussion`
  ADD PRIMARY KEY (`discussion_id`);

--
-- Indexes for table `tbl_education_level`
--
ALTER TABLE `tbl_education_level`
  ADD PRIMARY KEY (`edu_level_id`);

--
-- Indexes for table `tbl_enroll`
--
ALTER TABLE `tbl_enroll`
  ADD PRIMARY KEY (`enroll_id`);

--
-- Indexes for table `tbl_mail_log`
--
ALTER TABLE `tbl_mail_log`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_question`
--
ALTER TABLE `tbl_question`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `tbl_question_answers`
--
ALTER TABLE `tbl_question_answers`
  ADD PRIMARY KEY (`question_answers_id`);

--
-- Indexes for table `tbl_question_answer_mode`
--
ALTER TABLE `tbl_question_answer_mode`
  ADD PRIMARY KEY (`question_answer_mode_id`);

--
-- Indexes for table `tbl_question_bank`
--
ALTER TABLE `tbl_question_bank`
  ADD PRIMARY KEY (`question_bank_id`);

--
-- Indexes for table `tbl_question_type`
--
ALTER TABLE `tbl_question_type`
  ADD PRIMARY KEY (`question_type_id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tbl_subscription`
--
ALTER TABLE `tbl_subscription`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `tbl_subscription_trials`
--
ALTER TABLE `tbl_subscription_trials`
  ADD PRIMARY KEY (`subscription_trial_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `tbl_trans_question`
--
ALTER TABLE `tbl_trans_question`
  ADD PRIMARY KEY (`trans_question_id`);

--
-- Indexes for table `tbl_trans_question_answers`
--
ALTER TABLE `tbl_trans_question_answers`
  ADD PRIMARY KEY (`trans_question_answers_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_users_detail`
--
ALTER TABLE `tbl_users_detail`
  ADD PRIMARY KEY (`user_detail_id`);

--
-- Indexes for table `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  ADD PRIMARY KEY (`user_level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_activation_keys`
--
ALTER TABLE `tbl_activation_keys`
  MODIFY `activation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_course_section`
--
ALTER TABLE `tbl_course_section`
  MODIFY `course_section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_course_section_item`
--
ALTER TABLE `tbl_course_section_item`
  MODIFY `section_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_course_section_progress`
--
ALTER TABLE `tbl_course_section_progress`
  MODIFY `section_progress_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `tbl_discussion`
--
ALTER TABLE `tbl_discussion`
  MODIFY `discussion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_education_level`
--
ALTER TABLE `tbl_education_level`
  MODIFY `edu_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_enroll`
--
ALTER TABLE `tbl_enroll`
  MODIFY `enroll_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_mail_log`
--
ALTER TABLE `tbl_mail_log`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_question`
--
ALTER TABLE `tbl_question`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_question_answers`
--
ALTER TABLE `tbl_question_answers`
  MODIFY `question_answers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_question_answer_mode`
--
ALTER TABLE `tbl_question_answer_mode`
  MODIFY `question_answer_mode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_question_bank`
--
ALTER TABLE `tbl_question_bank`
  MODIFY `question_bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_question_type`
--
ALTER TABLE `tbl_question_type`
  MODIFY `question_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `tbl_subscription`
--
ALTER TABLE `tbl_subscription`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_subscription_trials`
--
ALTER TABLE `tbl_subscription_trials`
  MODIFY `subscription_trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_trans_question`
--
ALTER TABLE `tbl_trans_question`
  MODIFY `trans_question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_trans_question_answers`
--
ALTER TABLE `tbl_trans_question_answers`
  MODIFY `trans_question_answers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users_detail`
--
ALTER TABLE `tbl_users_detail`
  MODIFY `user_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  MODIFY `user_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
