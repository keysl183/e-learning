-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2018 at 10:28 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agora_dev_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activation_keys`
--

CREATE TABLE `tbl_activation_keys` (
  `activation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(200) NOT NULL,
  `active` int(11) DEFAULT '1',
  `issue_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activation_keys`
--

INSERT INTO `tbl_activation_keys` (`activation_id`, `user_id`, `activation_key`, `active`, `issue_date`) VALUES
(1, 31, '9ae9ec6efe4cac131e032b0f67a60929', 1, '2018-05-01 18:31:49'),
(2, 32, '566ce6b0cc0d6d98e4993e1b151b79b5', 1, '2018-05-03 23:02:03'),
(3, 33, '941992a92e14423fbcd17eda76687825', 1, '2018-05-03 23:07:20'),
(4, 34, '596231889abdabd0fc734f4bff51cd82', 1, '2018-05-03 23:08:57'),
(5, 3, 'a57916875e3f90802c627b1116f7d38a', 1, '2018-05-17 21:05:59'),
(6, 4, '76d715b3c8b116c1ae7f8ef13e16759f', 1, '2018-05-27 19:37:11'),
(7, 5, 'dda2c36a4f63e17b99d112856dfc3c05', 1, '2018-06-05 22:33:27'),
(8, 6, 'a5bd969a5d385fde223d02c0939414d7', 1, '2018-06-17 03:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assignment`
--

CREATE TABLE `tbl_assignment` (
  `assignment_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `section_item_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_from` datetime NOT NULL,
  `available_to` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `points` int(11) DEFAULT NULL COMMENT 'if not mcq',
  `no_of_questions` int(11) DEFAULT NULL,
  `passing_rate` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `question_bank_id` int(11) DEFAULT NULL COMMENT 'If MCQ or type ==20',
  `author_user_id` int(11) NOT NULL,
  `attachment_file` text COLLATE utf8mb4_unicode_ci,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_assignment`
--

INSERT INTO `tbl_assignment` (`assignment_id`, `category_id`, `section_item_id`, `title`, `description`, `available_from`, `available_to`, `due_date`, `points`, `no_of_questions`, `passing_rate`, `type_id`, `question_bank_id`, `author_user_id`, `attachment_file`, `date_added`, `active`) VALUES
(1, 1, 11, '数学', 'テストコメント １＋２＋３', '2018-06-27 00:00:00', '2018-06-30 00:00:00', '2018-07-05 00:00:00', 5, 5, 2, 2, NULL, 2, NULL, '2018-06-27 12:55:28', 1),
(5, 1, 10, 'testassignment3', 'sdadasdasd', '2017-12-12 00:00:00', '2018-06-30 00:00:00', '2018-12-12 00:00:00', 5, NULL, 5, 1, NULL, 4, 'audio_20180629-5b361f9039996.mp3', '2018-06-29 19:59:48', 1),
(7, 2, 2, 'testassign4', 'asdsadasdasdas', '2018-05-01 00:00:00', '2018-06-30 00:00:00', '2017-06-06 00:00:00', 5, NULL, 3, 1, NULL, 4, 'audio_20180629-5b361f9039996.mp3', '2018-06-29 20:03:41', 1),
(8, 2, 1, 'test mcq', 'test description', '2018-12-06 00:00:00', '2019-02-02 00:00:00', '2018-12-30 00:00:00', 5, 3, 3, 2, 31, 4, NULL, '2018-06-29 21:13:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assingment_submission`
--

CREATE TABLE `tbl_assingment_submission` (
  `assign_sumission_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `retake_count` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(50) NOT NULL,
  `category_desc` varchar(500) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_title`, `category_desc`, `active`, `date_added`) VALUES
(1, 'Math', 'MATH CATEGORY', 1, '2018-05-14 18:49:06'),
(2, 'English', 'English', 1, '2018-05-27 22:27:49'),
(3, 'tes', 'tea', 0, '2018-05-30 23:02:08'),
(4, 'asd', 'asd', 0, '2018-06-02 19:10:11'),
(5, 'asd', 'asd', 0, '2018-06-02 19:11:08'),
(6, 'sad', 'asdasd', 0, '2018-06-02 19:11:15'),
(7, 'asd', 'asdasd', 0, '2018-06-02 19:11:22'),
(8, 'Science', '', 1, '2018-06-06 00:11:53'),
(9, 'Literature', 'Literature', 1, '2018-06-10 23:09:48'),
(10, 'Algebra', 'Algebra', 1, '2018-06-10 23:10:12'),
(11, 'Chemistry', 'Chemistry', 1, '2018-06-10 23:10:19'),
(12, 'Physics', 'Physics', 1, '2018-06-10 23:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `country_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `language` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `country_code`, `country_name`, `language`) VALUES
(1, 'AF', 'Afghanistan', 'Pashto'),
(2, 'AX', 'Åland Islands', ''),
(3, 'AL', 'Albania', NULL),
(4, 'DZ', 'Algeria', NULL),
(5, 'AS', 'American Samoa', NULL),
(6, 'AD', 'Andorra', NULL),
(7, 'AO', 'Angola', NULL),
(8, 'AI', 'Anguilla', NULL),
(9, 'AQ', 'Antarctica', NULL),
(10, 'AG', 'Antigua and Barbuda', NULL),
(11, 'AR', 'Argentina', NULL),
(12, 'AM', 'Armenia', 'Armenian'),
(13, 'AW', 'Aruba', NULL),
(14, 'AU', 'Australia', NULL),
(15, 'AT', 'Austria', NULL),
(16, 'AZ', 'Azerbaijan', 'Azerbaijan'),
(17, 'BS', 'Bahamas', NULL),
(18, 'BH', 'Bahrain', 'Arabic'),
(19, 'BD', 'Bangladesh', 'Bengali'),
(20, 'BB', 'Barbados', NULL),
(21, 'BY', 'Belarus', NULL),
(22, 'BE', 'Belgium', NULL),
(23, 'BZ', 'Belize', NULL),
(24, 'BJ', 'Benin', NULL),
(25, 'BM', 'Bermuda', NULL),
(26, 'BT', 'Bhutan', 'Dzongkha'),
(27, 'BO', 'Bolivia, Plurinational St', NULL),
(28, 'BQ', 'Bonaire, Sint Eustatius a', NULL),
(29, 'BA', 'Bosnia and Herzegovina', NULL),
(30, 'BW', 'Botswana', NULL),
(31, 'BV', 'Bouvet Island', NULL),
(32, 'BR', 'Brazil', NULL),
(33, 'IO', 'British Indian Ocean Terr', NULL),
(34, 'BN', 'Brunei Darussalam', 'Malay'),
(35, 'BG', 'Bulgaria', NULL),
(36, 'BF', 'Burkina Faso', NULL),
(37, 'BI', 'Burundi', NULL),
(38, 'KH', 'Cambodia', 'Khmer'),
(39, 'CM', 'Cameroon', NULL),
(40, 'CA', 'Canada', NULL),
(41, 'CV', 'Cape Verde', NULL),
(42, 'KY', 'Cayman Islands', NULL),
(43, 'CF', 'Central African Republic', NULL),
(44, 'TD', 'Chad', NULL),
(45, 'CL', 'Chile', NULL),
(46, 'CN', 'China', 'Mandarin - 普通话'),
(47, 'CX', 'Christmas Island', NULL),
(48, 'CC', 'Cocos (Keeling) Islands', NULL),
(49, 'CO', 'Colombia', NULL),
(50, 'KM', 'Comoros', NULL),
(51, 'CG', 'Congo', NULL),
(52, 'CD', 'Congo, the Democratic Rep', NULL),
(53, 'CK', 'Cook Islands', NULL),
(54, 'CR', 'Costa Rica', NULL),
(55, 'CI', 'Côte d`Ivoire', NULL),
(56, 'HR', 'Croatia', NULL),
(57, 'CU', 'Cuba', NULL),
(58, 'CW', 'Curaçao', NULL),
(59, 'CY', 'Cyprus', 'Greek'),
(60, 'CZ', 'Czech Republic', NULL),
(61, 'DK', 'Denmark', NULL),
(62, 'DJ', 'Djibouti', NULL),
(63, 'DM', 'Dominica', NULL),
(64, 'DO', 'Dominican Republic', NULL),
(65, 'EC', 'Ecuador', NULL),
(66, 'EG', 'Egypt', NULL),
(67, 'SV', 'El Salvado', NULL),
(68, 'GQ', 'Equatorial Guinea', NULL),
(69, 'ER', 'Eritrea', NULL),
(70, 'EE', 'Estonia', NULL),
(71, 'ET', 'Ethiopia', NULL),
(72, 'FK', 'Falkland Islands (Malvina', NULL),
(73, 'FO', 'Faroe Islands', NULL),
(74, 'FJ', 'Fiji', NULL),
(75, 'FI', 'Finland', NULL),
(76, 'FR', 'France', NULL),
(77, 'GF', 'French Guiana', NULL),
(78, 'PF', 'French Polynesia', NULL),
(79, 'TF', 'French Southern Territori', NULL),
(80, 'GA', 'Gabon', NULL),
(81, 'GM', 'Gambia', NULL),
(82, 'GE', 'Georgia', NULL),
(83, 'DE', 'Germany', NULL),
(84, 'GH', 'Ghana', NULL),
(85, 'GI', 'Gibraltar', NULL),
(86, 'GR', 'Greece', NULL),
(87, 'GL', 'Greenland', NULL),
(88, 'GD', 'Grenada', NULL),
(89, 'GP', 'Guadeloupe', NULL),
(90, 'GU', 'Guam', NULL),
(91, 'GT', 'Guatemala', NULL),
(92, 'GG', 'Guernsey', NULL),
(93, 'GN', 'Guinea', NULL),
(94, 'GW', 'Guinea-Bissau', NULL),
(95, 'GY', 'Guyana', NULL),
(96, 'HT', 'Haiti', NULL),
(97, 'HM', 'Heard Island and McDonald', NULL),
(98, 'VA', 'Holy See (Vatican City St', NULL),
(99, 'HN', 'Honduras', NULL),
(100, 'HK', 'Hong Kong', NULL),
(101, 'HU', 'Hungary', NULL),
(102, 'IS', 'Iceland', NULL),
(103, 'IN', 'India', 'Hindi'),
(104, 'ID', 'Indonesia', 'Bahasa Indonesia'),
(105, 'IR', 'Iran, Islamic Republic of', 'Persian'),
(106, 'IQ', 'Iraq', 'Kurdish '),
(107, 'IE', 'Ireland', NULL),
(108, 'IM', 'Isle of Man', NULL),
(109, 'IL', 'Israel', NULL),
(110, 'IT', 'Italy', NULL),
(111, 'JM', 'Jamaica', NULL),
(112, 'JP', 'Japan', '日本語'),
(113, 'JE', 'Jersey', NULL),
(114, 'JO', 'Jordan', NULL),
(115, 'KZ', 'Kazakhstan', 'Қазақша'),
(116, 'KE', 'Kenya', NULL),
(117, 'KI', 'Kiribati', NULL),
(118, 'KP', 'Korea, Democratic People`', NULL),
(119, 'KR', 'Korea, Republic of', '한국어/조선말'),
(120, 'KW', 'Kuwait', NULL),
(121, 'KG', 'Kyrgyzstan', NULL),
(122, 'LA', 'Lao People`s Democratic R', 'ພາສາລາວ'),
(123, 'LV', 'Latvia', NULL),
(124, 'LB', 'Lebanon', NULL),
(125, 'LS', 'Lesotho', NULL),
(126, 'LR', 'Liberia', NULL),
(127, 'LY', 'Libya', NULL),
(128, 'LI', 'Liechtenstein', NULL),
(129, 'LT', 'Lithuania', NULL),
(130, 'LU', 'Luxembourg', NULL),
(131, 'MO', 'Macao', NULL),
(132, 'MK', 'Macedonia, the former Yug', NULL),
(133, 'MG', 'Madagascar', NULL),
(134, 'MW', 'Malawi', NULL),
(135, 'MY', 'Malaysia', 'Bahasa '),
(136, 'MV', 'Maldives', NULL),
(137, 'ML', 'Mali', NULL),
(138, 'MT', 'Malta', NULL),
(139, 'MH', 'Marshall Islands', NULL),
(140, 'MQ', 'Martinique', NULL),
(141, 'MR', 'Mauritania', NULL),
(142, 'MU', 'Mauritius', NULL),
(143, 'YT', 'Mayotte', NULL),
(144, 'FM', 'Micronesia, Federated Sta', NULL),
(145, 'MD', 'Moldova, Republic of', NULL),
(146, 'MC', 'Monaco', NULL),
(147, 'MN', 'Mongolia', NULL),
(148, 'ME', 'Montenegro', NULL),
(149, 'MS', 'Montserrat', NULL),
(150, 'MA', 'Morocco', NULL),
(151, 'MZ', 'Mozambique', NULL),
(152, 'MM', 'Myanmar', NULL),
(153, 'NA', 'Namibia', NULL),
(154, 'NR', 'Nauru', NULL),
(155, 'NP', 'Nepal', NULL),
(156, 'NL', 'Netherlands', NULL),
(157, 'NC', 'New Caledonia', NULL),
(158, 'NZ', 'New Zealand', NULL),
(159, 'NI', 'Nicaragua', NULL),
(160, 'NE', 'Niger', NULL),
(161, 'NG', 'Nigeria', NULL),
(162, 'NU', 'Niue', NULL),
(163, 'NF', 'Norfolk Island', NULL),
(164, 'MP', 'Northern Mariana Islands', NULL),
(165, 'NO', 'Norway', NULL),
(166, 'OM', 'Oman', NULL),
(167, 'PK', 'Pakistan', NULL),
(168, 'PW', 'Palau', NULL),
(169, 'PS', 'Palestinian Territory, Oc', NULL),
(170, 'PA', 'Panama', NULL),
(171, 'PG', 'Papua New Guinea', NULL),
(172, 'PY', 'Paraguay', NULL),
(173, 'PE', 'Peru', NULL),
(174, 'PH', 'Philippines', 'Filipino'),
(175, 'PN', 'Pitcairn', NULL),
(176, 'PL', 'Poland', NULL),
(177, 'PT', 'Portugal', NULL),
(178, 'PR', 'Puerto Rico', NULL),
(179, 'QA', 'Qatar', NULL),
(180, 'RE', 'Réunion', NULL),
(181, 'RO', 'Romania', NULL),
(182, 'RU', 'Russian Federation', NULL),
(183, 'RW', 'Rwanda', NULL),
(184, 'BL', 'Saint Barthélemy', NULL),
(185, 'SH', 'Saint Helena, Ascension a', NULL),
(186, 'KN', 'Saint Kitts and Nevis', NULL),
(187, 'LC', 'Saint Lucia', NULL),
(188, 'MF', 'Saint Martin (French part', NULL),
(189, 'PM', 'Saint Pierre and Miquelon', NULL),
(190, 'VC', 'Saint Vincent and the Gre', NULL),
(191, 'WS', 'Samoa', NULL),
(192, 'SM', 'San Marino', NULL),
(193, 'ST', 'Sao Tome and Principe', NULL),
(194, 'SA', 'Saudi Arabia', NULL),
(195, 'SN', 'Senegal', NULL),
(196, 'RS', 'Serbia', NULL),
(197, 'SC', 'Seychelles', NULL),
(198, 'SL', 'Sierra Leone', NULL),
(199, 'SG', 'Singapore', NULL),
(200, 'SX', 'Sint Maarten (Dutch part)', NULL),
(201, 'SK', 'Slovakia', NULL),
(202, 'SI', 'Slovenia', NULL),
(203, 'SB', 'Solomon Islands', NULL),
(204, 'SO', 'Somalia', NULL),
(205, 'ZA', 'South Africa', NULL),
(206, 'GS', 'South Georgia and the Sou', NULL),
(207, 'SS', 'South Sudan', NULL),
(208, 'ES', 'Spain', NULL),
(209, 'LK', 'Sri Lanka', NULL),
(210, 'SD', 'Sudan', NULL),
(211, 'SR', 'Suriname', NULL),
(212, 'SJ', 'Svalbard and Jan Mayen', NULL),
(213, 'SZ', 'Swaziland', NULL),
(214, 'SE', 'Sweden', NULL),
(215, 'CH', 'Switzerland', NULL),
(216, 'SY', 'Syrian Arab Republic', NULL),
(217, 'TW', 'Taiwan, Province of China', NULL),
(218, 'TJ', 'Tajikistan', NULL),
(219, 'TZ', 'Tanzania, United Republic', NULL),
(220, 'TH', 'Thailand', NULL),
(221, 'TL', 'Timor-Leste', NULL),
(222, 'TG', 'Togo', NULL),
(223, 'TK', 'Tokelau', NULL),
(224, 'TO', 'Tonga', NULL),
(225, 'TT', 'Trinidad and Tobago', NULL),
(226, 'TN', 'Tunisia', NULL),
(227, 'TR', 'Turkey', NULL),
(228, 'TM', 'Turkmenistan', NULL),
(229, 'TC', 'Turks and Caicos Islands', NULL),
(230, 'TV', 'Tuvalu', NULL),
(231, 'UG', 'Uganda', NULL),
(232, 'UA', 'Ukraine', NULL),
(233, 'AE', 'United Arab Emirates', NULL),
(234, 'GB', 'United Kingdom', NULL),
(235, 'US', 'United States', NULL),
(236, 'UM', 'United States Minor Outly', NULL),
(237, 'UY', 'Uruguay', NULL),
(238, 'UZ', 'Uzbekistan', NULL),
(239, 'VU', 'Vanuatu', NULL),
(240, 'VE', 'Venezuela, Bolivarian Rep', NULL),
(241, 'VN', 'Viet Nam', 'Tiếng Việt'),
(242, 'VG', 'Virgin Islands, British', NULL),
(243, 'VI', 'Virgin Islands, U.S.', NULL),
(244, 'WF', 'Wallis and Futuna', NULL),
(245, 'EH', 'Western Sahara', NULL),
(246, 'YE', 'Yemen', NULL),
(247, 'ZM', 'Zambia', NULL),
(248, 'ZW', 'Zimbabwe', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_excerpt` varchar(500) DEFAULT NULL,
  `course_description` text,
  `course_category` int(11) NOT NULL,
  `course_cover_image` text,
  `author_user_id` int(11) NOT NULL,
  `no_of_questions` int(11) DEFAULT NULL,
  `passing_rate` int(11) DEFAULT NULL,
  `retake_allowed` int(11) DEFAULT NULL,
  `time_limit` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_enrolled` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_id`, `course_title`, `course_excerpt`, `course_description`, `course_category`, `course_cover_image`, `author_user_id`, `no_of_questions`, `passing_rate`, `retake_allowed`, `time_limit`, `date_added`, `student_enrolled`, `active`) VALUES
(1, 'KL CrashCourse to PHPUPD', 'This is a comprehensive Course to PHPUPD', 'This is a test description', 1, '', 2, NULL, NULL, NULL, 0, '2018-05-01 13:41:36', 2, 1),
(2, 'KL CrashCourse to PHP', 'This is a comprehensive Course to PHP', 'This is a test description', 1, '', 2, NULL, NULL, NULL, 0, '2018-05-01 14:20:26', 4, 1),
(3, 'TEST', 'TEST', 'TEST', 1, '', 4, NULL, NULL, NULL, 0, '2018-05-27 22:58:31', 1, 0),
(4, 'TEST', 'TEST', 'TEST', 1, '', 4, NULL, NULL, NULL, 0, '2018-05-27 23:01:33', 0, 0),
(5, 'TEST22', 'TEST22', 'TEST', 2, '', 4, NULL, NULL, NULL, 0, '2018-05-27 23:09:38', 0, 0),
(6, 'PENGUIN22', 'TESTPENGUION', 'TEST', 1, 'image_20180528-5b0ba55abd4dc.jpg', 4, NULL, NULL, NULL, 0, '2018-05-27 23:44:50', 0, 0),
(7, 'TEST EXAM 2', 'RWA', 'asdasdasd', 1, 'image_20180530-5b0e2798d4fa6.jpg', 4, NULL, NULL, NULL, 0, '2018-05-29 21:24:58', 0, 0),
(8, 'English Exam For dummies', 'This is a comprehensive Course that enhances your English Skills for better.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2, NULL, 4, NULL, NULL, NULL, 0, '2018-05-29 21:35:01', 0, 1),
(9, 'asd', 'asd', 'sadasd', 1, NULL, 4, NULL, NULL, NULL, 0, '2018-06-02 19:39:29', 0, 0),
(10, 'sad', 'asd', 'asd', 1, NULL, 4, NULL, NULL, NULL, 0, '2018-06-02 19:40:39', 0, 0),
(11, 'Elementary Math', 'A Simple Course Exam for kids', 'Lorem ipsum dolor sit amet, mea maiorum scaevola expetenda ex, nihil impetus sea ne, quo cu ullum senserit.\n\n     Ne putant eripuit suavitate est, ea mea diceret appellantur, vim ea mollis feugait, im ea mollis feugait, im ea mollis feugait, im ea mollis feugait ea mea diceret appellantur, vim ea mollis feugait, im ea mollis feugait, im ea mollis feugait, im ea mollis feugait', 1, 'image_20180606-5b179b40440d5.jpg', 4, 5, 1, 2, 0, '2018-06-06 01:28:55', 5, 1),
(12, 'asda', 'sdasd', 'asdasd', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 19:53:04', 0, 0),
(13, 'asd', 'asdas', 'dasdas', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 19:57:42', 0, 0),
(14, 'asdas', 'asd', 'asdasdasd', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 20:00:24', 0, 0),
(15, 'aasdas', 'dasd', 'asdasdada', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 20:02:29', 0, 0),
(16, 'asdasdas', 'asdasdasd', 'dasdasd', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 20:02:44', 0, 0),
(17, 'asdad', 'asdas', 'dasdad', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 20:05:13', 0, 0),
(18, 'KL22', 'zxcxzcxzc', 'xc', 1, '', 4, NULL, NULL, NULL, 0, '2018-06-10 20:09:08', 0, 0),
(19, 'English Exam Vol. 1', NULL, 'English Exam sample', 2, 'image_20180619-5b28b603c4d3e.jpg', 4, 20, 10, 3, 0, '2018-06-22 21:38:35', 0, 1),
(20, 'asdas', 'toavoid', 'asdasd', 1, '', 4, 1, 1, 3, 0, '2018-06-22 21:45:00', 0, 0),
(21, 'asdasd', 'toavoid', 'asdsada', 9, '', 4, 20, 86, 3, 0, '2018-06-22 21:57:09', 0, 0),
(22, 'asdad', 'toavoid', 'asdasdas', 12, '', 4, 2, 1, 3, 0, '2018-06-22 22:02:12', 0, 0),
(23, 'asdas', 'toavoid', 'asdasdasd', 11, '', 4, 2, 1, 3, 0, '2018-06-22 22:02:27', 0, 0),
(24, 'NEW ADJUSTED TEST', 'toavoid', 'NEW ADJUSTED TEST', 1, 'image_20180630-5b3778979a876.png', 4, 100, 4, 4, 60, '2018-06-30 20:33:31', 0, 1),
(25, 'asdasd', 'toavoid', 'asdasdasdasd', 1, '', 4, 100, 1, 3, 25, '2018-06-30 22:49:42', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section`
--

CREATE TABLE `tbl_course_section` (
  `course_section_id` int(11) NOT NULL,
  `section_title` varchar(255) NOT NULL,
  `section_description` text,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section`
--

INSERT INTO `tbl_course_section` (`course_section_id`, `section_title`, `section_description`, `course_id`) VALUES
(1, 'test', 'test', 6),
(2, 'tes2', 'test2', 7),
(3, 'update2', 'asdasd', 8),
(4, 'test21', 'sdasdas', 6),
(5, 'test3sad', 'sadas', 7),
(6, 'test', 'hj', 8),
(7, 'SECTION 3', 'SECTIO', 6),
(9, 'Basic Addition', 'Contains series of worksheets that can jumpstart your brain. It provides a lot of mathematical thinking involving Addition. Futher more enhancing your kid.', 11),
(10, 'TES', 'TEst', 18),
(11, 'Basic Substraction', 'Contains series of Substraction Questions', 11),
(12, 'TEST SECTION', 'TEST TEST', 21),
(13, 'Adjusted Section I', 'Adjusted Section I', 24),
(14, 'Adjusted Section II', 'Adjusted Section II', 24),
(15, 'Adjusted Section III', 'Adjusted Section III', 24),
(16, 'Adjusted Section IV', 'Adjusted Section IV', 24);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section_item`
--

CREATE TABLE `tbl_course_section_item` (
  `section_item_id` int(11) NOT NULL,
  `item_type` int(11) NOT NULL COMMENT '1 = WorkSheet, 2 = Unit Test',
  `course_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `ws_bank_id` int(11) DEFAULT NULL COMMENT 'applicable only if item is worksheet type',
  `ut_serialize_ws_id` text COMMENT 'applicable only if item is unit test type',
  `answer_type` int(11) DEFAULT NULL COMMENT '1 = multi_choice 2 = essay',
  `no_of_question` int(11) NOT NULL DEFAULT '10',
  `passing_rate` int(11) NOT NULL DEFAULT '5',
  `time_limit` int(11) DEFAULT '0',
  `retake_count` int(11) DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section_item`
--

INSERT INTO `tbl_course_section_item` (`section_item_id`, `item_type`, `course_id`, `section_id`, `title`, `description`, `ws_bank_id`, `ut_serialize_ws_id`, `answer_type`, `no_of_question`, `passing_rate`, `time_limit`, `retake_count`, `date_added`) VALUES
(1, 1, 8, 3, 'New WorkSheet', 'New WorkSheet', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:25:36'),
(2, 1, 8, 3, 'TEST WORKSHEET 2', 'TEST WORKSHEET 2', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:28:30'),
(3, 1, 7, 2, 'asd', 'asdasd', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:29:44'),
(4, 1, 7, 5, 'asd', 'asdsad', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:30:40'),
(5, 1, 8, 3, 'TESTWORKSHEET3', 'TESTWORKSHEET3', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 23:53:28'),
(6, 1, 8, 6, 'TESTWORKSHEET', 'TESTWORKSHEET', 24, NULL, 1, 10, 5, 0, 0, '2018-06-03 00:18:22'),
(8, 1, 8, 8, 'TEST WORKSHEET', 'TEST WORKSHEET', 24, NULL, 1, 10, 5, 0, 0, '2018-06-02 10:28:54'),
(9, 1, 11, 9, 'Basic Addition I', 'Basic Addition I', 29, NULL, 1, 10, 5, 0, 0, '2018-06-06 01:30:32'),
(10, 1, 11, 9, 'Basic Addition II', 'Basic Addition II', 29, NULL, 1, 12, 5, 0, 0, '2018-06-12 11:49:16'),
(11, 1, 8, 3, 'test', 'test', 24, NULL, 1, 10, 8, NULL, NULL, '2018-06-15 02:10:37'),
(14, 1, 11, 11, 'Basic Substraction I', 'Basic Substraction I', 30, NULL, 1, 3, 2, NULL, NULL, '2018-06-15 11:16:01'),
(16, 2, 11, 9, 'Basic Addition Unit Test', 'Basic Addition Unit Test', NULL, NULL, 1, 12, 8, 5, 2, '2018-06-17 02:48:00'),
(17, 1, 24, 13, 'ADJUSTED FLOW WORKSHEET I', 'ADJUSTED FLOW WORKSHEET I', 35, NULL, 1, 10, 5, NULL, 3, '2018-06-30 21:31:32'),
(18, 1, 24, 13, 'ADJUSTED FLOW WORKSHEET II', NULL, 34, NULL, 1, 10, 5, NULL, 3, '2018-06-30 22:14:47'),
(20, 2, 24, 13, 'TEST ADJUSTED UNIT TEST', 'TEST ADJUSTED UNIT TEST', NULL, 'a:1:{i:0;s:2:\"17\";}', 1, 10, 9, 25, 2, '2018-06-30 22:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_section_progress`
--

CREATE TABLE `tbl_course_section_progress` (
  `section_progress_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `section_item_id` int(11) NOT NULL,
  `questions` text NOT NULL COMMENT 'SERIALIZE Question ID''s',
  `score` int(11) DEFAULT '0',
  `answers` text,
  `status` int(11) DEFAULT '1' COMMENT '0 = CANCELLED 1=INPROGRESS 2 = PENDING = 3 COMPLETED',
  `wrong_answer_question_ids` text,
  `pass_failed` int(11) DEFAULT '0' COMMENT '0 = not  completed, 1 = Passed, 2= Failed',
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course_section_progress`
--

INSERT INTO `tbl_course_section_progress` (`section_progress_id`, `user_id`, `section_item_id`, `questions`, `score`, `answers`, `status`, `wrong_answer_question_ids`, `pass_failed`, `active`, `date_added`) VALUES
(13, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-12 21:35:44'),
(14, 2, 5, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"15\";}i:1;a:1:{s:11:\"question_id\";s:2:\"11\";}i:2;a:1:{s:11:\"question_id\";s:2:\"14\";}i:3;a:1:{s:11:\"question_id\";s:2:\"13\";}i:4;a:1:{s:11:\"question_id\";s:2:\"12\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:22:59'),
(15, 2, 10, 'a:11:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"22\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"24\";}i:10;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:27:13'),
(16, 2, 6, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"14\";}i:1;a:1:{s:11:\"question_id\";s:2:\"11\";}i:2;a:1:{s:11:\"question_id\";s:2:\"12\";}i:3;a:1:{s:11:\"question_id\";s:2:\"15\";}i:4;a:1:{s:11:\"question_id\";s:2:\"13\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:27:31'),
(17, 4, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"20\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-13 21:28:42'),
(18, 2, 8, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"15\";}i:1;a:1:{s:11:\"question_id\";s:2:\"12\";}i:2;a:1:{s:11:\"question_id\";s:2:\"11\";}i:3;a:1:{s:11:\"question_id\";s:2:\"13\";}i:4;a:1:{s:11:\"question_id\";s:2:\"14\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-14 22:05:48'),
(19, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-15 11:20:49'),
(20, 2, 16, 'a:11:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"26\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}i:10;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 11, 'a:12:{i:0;i:4;i:1;i:3;i:2;i:2;i:3;i:4;i:4;i:1;i:5;i:3;i:6;i:3;i:7;i:3;i:8;i:2;i:9;i:1;i:10;i:1;i:11;i:1;}', 3, 'a:12:{i:0;i:0;i:1;s:2:\"30\";i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;}', 1, 1, '2018-06-17 02:57:41'),
(21, 2, 1, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"12\";}i:1;a:1:{s:11:\"question_id\";s:2:\"14\";}i:2;a:1:{s:11:\"question_id\";s:2:\"11\";}i:3;a:1:{s:11:\"question_id\";s:2:\"15\";}i:4;a:1:{s:11:\"question_id\";s:2:\"13\";}}', 0, 'a:5:{i:0;i:2;i:1;i:2;i:2;i:2;i:3;i:2;i:4;i:4;}', 3, 'a:5:{i:0;s:2:\"12\";i:1;s:2:\"14\";i:2;s:2:\"11\";i:3;s:2:\"15\";i:4;s:2:\"13\";}', 2, 1, '2018-06-20 22:41:36'),
(22, 1, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"21\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"17\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-20 22:53:22'),
(23, 1, 15, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 1, 'a:3:{i:0;i:2;i:1;i:4;i:2;i:2;}', 3, 'a:3:{i:0;i:0;i:1;s:2:\"29\";i:2;s:2:\"27\";}', 2, 1, '2018-06-20 22:53:43'),
(24, 1, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 1, 'a:3:{i:0;i:2;i:1;i:2;i:2;i:2;}', 3, 'a:3:{i:0;i:0;i:1;s:2:\"27\";i:2;s:2:\"29\";}', 2, 1, '2018-06-20 22:54:25'),
(25, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:10:51'),
(26, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:11:04'),
(27, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"21\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"23\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"22\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:11:09'),
(28, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"19\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"22\";}i:4;a:1:{s:11:\"question_id\";s:2:\"21\";}i:5;a:1:{s:11:\"question_id\";s:2:\"30\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:15:02'),
(29, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"21\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"25\";}i:7;a:1:{s:11:\"question_id\";s:2:\"22\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"24\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:15:04'),
(30, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"24\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:09'),
(31, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"24\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:12'),
(32, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"17\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"19\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"25\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:15'),
(33, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"25\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"20\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:19'),
(34, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"19\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"22\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"20\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:25'),
(35, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"19\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:34'),
(36, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"18\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:20:37'),
(37, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"24\";}i:1;a:1:{s:11:\"question_id\";s:2:\"26\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"16\";}i:6;a:1:{s:11:\"question_id\";s:2:\"25\";}i:7;a:1:{s:11:\"question_id\";s:2:\"19\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:37:32'),
(38, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"25\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"21\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:38:20'),
(39, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"23\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"17\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:38:23'),
(40, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"24\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"18\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:38:54'),
(41, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"16\";}i:2;a:1:{s:11:\"question_id\";s:2:\"26\";}i:3;a:1:{s:11:\"question_id\";s:2:\"30\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"21\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"22\";}i:9;a:1:{s:11:\"question_id\";s:2:\"25\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:41:24'),
(42, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"18\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"26\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:47:13'),
(43, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"24\";}i:1;a:1:{s:11:\"question_id\";s:2:\"16\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"17\";}i:4;a:1:{s:11:\"question_id\";s:2:\"18\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:47:15'),
(44, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"30\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:47:55'),
(45, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"20\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:47:56'),
(46, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"20\";}i:4;a:1:{s:11:\"question_id\";s:2:\"23\";}i:5;a:1:{s:11:\"question_id\";s:2:\"25\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"17\";}i:9;a:1:{s:11:\"question_id\";s:2:\"30\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 11:48:00'),
(47, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"22\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"20\";}i:3;a:1:{s:11:\"question_id\";s:2:\"16\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"30\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"23\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:06'),
(48, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"18\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:08'),
(49, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"24\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"21\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:11'),
(50, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"21\";}i:1;a:1:{s:11:\"question_id\";s:2:\"24\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"26\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"16\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"20\";}i:8;a:1:{s:11:\"question_id\";s:2:\"30\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:14'),
(51, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"30\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"19\";}i:3;a:1:{s:11:\"question_id\";s:2:\"16\";}i:4;a:1:{s:11:\"question_id\";s:2:\"18\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"21\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:17'),
(52, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"30\";}i:1;a:1:{s:11:\"question_id\";s:2:\"25\";}i:2;a:1:{s:11:\"question_id\";s:2:\"22\";}i:3;a:1:{s:11:\"question_id\";s:2:\"21\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"17\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"18\";}i:9;a:1:{s:11:\"question_id\";s:2:\"23\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:19'),
(53, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"16\";}i:1;a:1:{s:11:\"question_id\";s:2:\"18\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"23\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"22\";}i:6;a:1:{s:11:\"question_id\";s:2:\"19\";}i:7;a:1:{s:11:\"question_id\";s:2:\"24\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:04:22'),
(54, 2, 16, 'a:12:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"24\";}i:3;a:1:{s:11:\"question_id\";s:2:\"25\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"17\";}i:6;a:1:{s:11:\"question_id\";s:2:\"18\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"21\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}i:10;a:1:{s:11:\"question_id\";s:2:\"30\";}i:11;a:1:{s:11:\"question_id\";s:2:\"16\";}}', 11, 'a:12:{i:0;i:4;i:1;i:3;i:2;i:2;i:3;i:4;i:4;i:1;i:5;i:3;i:6;i:3;i:7;i:3;i:8;i:2;i:9;i:1;i:10;i:1;i:11;i:1;}', 3, 'a:12:{i:0;i:0;i:1;s:2:\"30\";i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;}', 1, 1, '2018-06-21 12:05:18'),
(55, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"19\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"17\";}i:4;a:1:{s:11:\"question_id\";s:2:\"30\";}i:5;a:1:{s:11:\"question_id\";s:2:\"26\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"18\";}i:8;a:1:{s:11:\"question_id\";s:2:\"25\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:05:23'),
(56, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"18\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"23\";}i:6;a:1:{s:11:\"question_id\";s:2:\"24\";}i:7;a:1:{s:11:\"question_id\";s:2:\"16\";}i:8;a:1:{s:11:\"question_id\";s:2:\"19\";}i:9;a:1:{s:11:\"question_id\";s:2:\"17\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-21 12:05:28'),
(57, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"26\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"16\";}i:4;a:1:{s:11:\"question_id\";s:2:\"17\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"22\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 01:40:51'),
(58, 2, 1, 'a:5:{i:0;a:1:{s:11:\"question_id\";s:2:\"12\";}i:1;a:1:{s:11:\"question_id\";s:2:\"13\";}i:2;a:1:{s:11:\"question_id\";s:2:\"15\";}i:3;a:1:{s:11:\"question_id\";s:2:\"11\";}i:4;a:1:{s:11:\"question_id\";s:2:\"14\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-23 01:41:46'),
(59, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"17\";}i:1;a:1:{s:11:\"question_id\";s:2:\"23\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"26\";}i:4;a:1:{s:11:\"question_id\";s:2:\"20\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"25\";}i:8;a:1:{s:11:\"question_id\";s:2:\"21\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 01:54:21'),
(60, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"30\";}i:1;a:1:{s:11:\"question_id\";s:2:\"23\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"18\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"26\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"16\";}i:9;a:1:{s:11:\"question_id\";s:2:\"20\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 01:54:27'),
(61, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"23\";}i:1;a:1:{s:11:\"question_id\";s:2:\"22\";}i:2;a:1:{s:11:\"question_id\";s:2:\"21\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"30\";}i:6;a:1:{s:11:\"question_id\";s:2:\"20\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 02:58:06'),
(62, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"17\";}i:2;a:1:{s:11:\"question_id\";s:2:\"23\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"24\";}i:5;a:1:{s:11:\"question_id\";s:2:\"30\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"22\";}i:8;a:1:{s:11:\"question_id\";s:2:\"26\";}i:9;a:1:{s:11:\"question_id\";s:2:\"25\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 02:58:09'),
(63, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 02:58:35'),
(64, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 02:59:01'),
(65, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"36\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 02:59:20'),
(66, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"36\";}i:2;a:1:{s:11:\"question_id\";s:2:\"37\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 02:59:25'),
(67, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 02:59:28'),
(68, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"36\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:02:20'),
(69, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:05:34'),
(70, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:14:54'),
(71, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"36\";}i:1;a:1:{s:11:\"question_id\";s:2:\"37\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:15:46'),
(72, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"36\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:16:56'),
(73, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"37\";}i:1;a:1:{s:11:\"question_id\";s:2:\"36\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:21:40'),
(74, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"37\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"36\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:21:48'),
(75, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"37\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:23:25'),
(76, 2, 15, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"37\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"36\";}}', 0, NULL, 1, NULL, 0, 1, '2018-06-23 03:25:38'),
(77, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"36\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"37\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:27:27'),
(78, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"36\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:30:38'),
(79, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"37\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:31:50'),
(80, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"37\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:33:59'),
(81, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"37\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:34:02'),
(82, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"36\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"37\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:34:10'),
(83, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"36\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:34:52'),
(84, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"27\";}i:1;a:1:{s:11:\"question_id\";s:2:\"29\";}i:2;a:1:{s:11:\"question_id\";s:2:\"28\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:35:55'),
(85, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 03:38:01'),
(86, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"25\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"16\";}i:3;a:1:{s:11:\"question_id\";s:2:\"30\";}i:4;a:1:{s:11:\"question_id\";s:2:\"19\";}i:5;a:1:{s:11:\"question_id\";s:2:\"18\";}i:6;a:1:{s:11:\"question_id\";s:2:\"22\";}i:7;a:1:{s:11:\"question_id\";s:2:\"26\";}i:8;a:1:{s:11:\"question_id\";s:2:\"21\";}i:9;a:1:{s:11:\"question_id\";s:2:\"24\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-23 03:39:05'),
(87, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"29\";}i:1;a:1:{s:11:\"question_id\";s:2:\"28\";}i:2;a:1:{s:11:\"question_id\";s:2:\"27\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-23 04:06:43'),
(88, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"18\";}i:1;a:1:{s:11:\"question_id\";s:2:\"20\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"24\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"17\";}i:6;a:1:{s:11:\"question_id\";s:2:\"21\";}i:7;a:1:{s:11:\"question_id\";s:2:\"19\";}i:8;a:1:{s:11:\"question_id\";s:2:\"26\";}i:9;a:1:{s:11:\"question_id\";s:2:\"22\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-29 22:23:32'),
(89, 2, 9, 'a:10:{i:0;a:1:{s:11:\"question_id\";s:2:\"20\";}i:1;a:1:{s:11:\"question_id\";s:2:\"16\";}i:2;a:1:{s:11:\"question_id\";s:2:\"30\";}i:3;a:1:{s:11:\"question_id\";s:2:\"17\";}i:4;a:1:{s:11:\"question_id\";s:2:\"25\";}i:5;a:1:{s:11:\"question_id\";s:2:\"24\";}i:6;a:1:{s:11:\"question_id\";s:2:\"26\";}i:7;a:1:{s:11:\"question_id\";s:2:\"23\";}i:8;a:1:{s:11:\"question_id\";s:2:\"21\";}i:9;a:1:{s:11:\"question_id\";s:2:\"19\";}}', 9, 'a:10:{i:0;i:3;i:1;i:1;i:2;i:3;i:3;i:3;i:4;i:2;i:5;i:2;i:6;i:1;i:7;i:3;i:8;i:4;i:9;i:4;}', 3, 'a:10:{i:0;i:0;i:1;i:0;i:2;s:2:\"30\";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;}', 1, 1, '2018-06-29 23:34:00'),
(90, 2, 14, 'a:3:{i:0;a:1:{s:11:\"question_id\";s:2:\"28\";}i:1;a:1:{s:11:\"question_id\";s:2:\"27\";}i:2;a:1:{s:11:\"question_id\";s:2:\"29\";}}', 2, 'a:3:{i:0;i:4;i:1;i:1;i:2;i:3;}', 3, 'a:3:{i:0;s:2:\"28\";i:1;i:0;i:2;i:0;}', 1, 1, '2018-06-30 00:10:21'),
(91, 2, 16, 'a:12:{i:0;a:1:{s:11:\"question_id\";s:2:\"21\";}i:1;a:1:{s:11:\"question_id\";s:2:\"30\";}i:2;a:1:{s:11:\"question_id\";s:2:\"25\";}i:3;a:1:{s:11:\"question_id\";s:2:\"19\";}i:4;a:1:{s:11:\"question_id\";s:2:\"22\";}i:5;a:1:{s:11:\"question_id\";s:2:\"20\";}i:6;a:1:{s:11:\"question_id\";s:2:\"23\";}i:7;a:1:{s:11:\"question_id\";s:2:\"17\";}i:8;a:1:{s:11:\"question_id\";s:2:\"24\";}i:9;a:1:{s:11:\"question_id\";s:2:\"26\";}i:10;a:1:{s:11:\"question_id\";s:2:\"16\";}i:11;a:1:{s:11:\"question_id\";s:2:\"18\";}}', 11, 'a:12:{i:0;i:4;i:1;i:3;i:2;i:2;i:3;i:4;i:4;i:1;i:5;i:3;i:6;i:3;i:7;i:3;i:8;i:2;i:9;i:1;i:10;i:1;i:11;i:1;}', 3, 'a:12:{i:0;i:0;i:1;s:2:\"30\";i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;}', 1, 1, '2018-06-30 00:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discussion`
--

CREATE TABLE `tbl_discussion` (
  `discussion_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `section_item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `is_reply` int(11) DEFAULT '0',
  `reply_to` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discussion`
--

INSERT INTO `tbl_discussion` (`discussion_id`, `course_id`, `section_item_id`, `user_id`, `comment`, `is_reply`, `reply_to`, `date_added`) VALUES
(1, 11, 14, 2, 'TEST COMMENT', 0, NULL, '2018-06-20 21:29:24'),
(2, 11, 14, 2, 'test12', 0, NULL, '2018-06-20 22:30:47'),
(3, 11, 14, 2, 'test 3', 0, NULL, '2018-06-20 22:35:10'),
(4, 11, 14, 2, 'test 4', 0, NULL, '2018-06-20 22:35:13'),
(5, 11, 14, 2, 'test3', 0, NULL, '2018-06-20 22:36:10'),
(6, 11, 14, 2, 'test3', 0, NULL, '2018-06-20 22:36:37'),
(7, 11, 14, 2, 'test3', 0, NULL, '2018-06-20 22:37:20'),
(8, 8, 1, 2, 'test comment here', 0, NULL, '2018-06-20 22:44:20'),
(9, 8, 1, 2, 'test comment here2', 0, NULL, '2018-06-20 22:44:24'),
(10, 8, 1, 2, 'test comment here3', 0, NULL, '2018-06-20 22:44:27'),
(11, 8, 1, 2, 'test comment here5', 0, NULL, '2018-06-20 22:44:30'),
(12, 8, 1, 2, 'test comment here6', 0, NULL, '2018-06-20 22:44:36'),
(13, 8, 1, 2, 'test comment here7', 0, NULL, '2018-06-20 22:44:54'),
(14, 8, 1, 2, 'test comment her7easd', 0, NULL, '2018-06-20 22:44:58'),
(15, 8, 1, 2, 'test comment her7easdasd', 0, NULL, '2018-06-20 22:44:59'),
(16, 8, 1, 2, 'test comment her7easdasdads', 0, NULL, '2018-06-20 22:45:01'),
(17, 11, 14, 1, 'HEY HEY', 0, NULL, '2018-06-20 22:54:37'),
(18, 11, 14, 1, 'HEY HEY2', 0, NULL, '2018-06-20 22:54:57'),
(19, 11, 14, 1, 'HEY HEY2', 0, NULL, '2018-06-20 22:55:06'),
(20, 11, 14, 1, 'HEY HEY2', 0, NULL, '2018-06-20 22:55:35'),
(21, 11, 14, 1, 'HEY HEY2', 0, NULL, '2018-06-20 22:55:37'),
(22, 11, 15, 1, 'TEST HERE AGAIN', 0, NULL, '2018-06-20 23:57:35'),
(23, 11, 15, 1, 'TEST ', 0, NULL, '2018-06-21 00:15:51'),
(24, 11, 15, 1, 'TEST555', 0, NULL, '2018-06-21 00:17:45'),
(25, 11, 15, 1, 'ZXczxcxzczxczczxcz', 0, NULL, '2018-06-21 00:18:44'),
(27, 11, 14, 2, 'TEST COMMENTzzzzz', 0, NULL, '2018-06-21 09:38:54'),
(28, 11, 14, 2, 'HIdadasdasd', 0, NULL, '2018-06-21 09:45:25'),
(29, 11, 14, 2, 'asdASDASDASD:KA:DA', 0, NULL, '2018-06-21 09:45:31'),
(30, 11, 14, 2, 'axasdasdasdasdas', 0, NULL, '2018-06-21 09:45:54'),
(31, 11, 14, 2, '22222222222', 0, NULL, '2018-06-21 09:46:06'),
(32, 8, 1, 4, 'qddsfsadfsfsdfsdfsdf', 0, NULL, '2018-06-29 20:45:22'),
(33, 11, 14, 2, 'TEST COMMENTzzzzz', 1, 31, '2018-06-29 21:31:58'),
(34, 11, 15, 4, 'asdasdasdasdasd', 1, 22, '2018-06-29 22:04:06'),
(35, 11, 15, 4, 'sadsadas', 1, 22, '2018-06-29 22:07:46'),
(36, 11, 15, 4, 'repkly test', 1, 22, '2018-06-29 22:08:07'),
(37, 11, 9, 2, 'TEST COMMENT 06302018', 0, 0, '2018-06-29 23:36:35'),
(38, 11, 9, 2, 'TEST REPLY TO MYSELF', 1, 37, '2018-06-29 23:37:39'),
(39, 11, 9, 2, 'TEST REPLY2', 1, 37, '2018-06-29 23:37:50'),
(40, 11, 9, 4, 'test 07012018', 1, 37, '2018-07-01 00:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education_level`
--

CREATE TABLE `tbl_education_level` (
  `edu_level_id` int(11) NOT NULL,
  `edu_level_description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_education_level`
--

INSERT INTO `tbl_education_level` (`edu_level_id`, `edu_level_description`) VALUES
(1, 'Elementary'),
(2, 'Secondary'),
(3, 'Undergraduate'),
(4, 'Bachelors'),
(5, 'Post Graduate');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enroll`
--

CREATE TABLE `tbl_enroll` (
  `enroll_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_enroll`
--

INSERT INTO `tbl_enroll` (`enroll_id`, `course_id`, `user_id`, `date_enrolled`) VALUES
(1, 8, 2, '2018-06-07 21:15:55'),
(4, 11, 2, '2018-06-07 21:36:13'),
(5, 11, 6, '2018-06-17 16:01:38'),
(6, 11, 1, '2018-06-20 22:53:01'),
(7, 24, 4, '2018-07-01 01:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_log`
--

CREATE TABLE `tbl_mail_log` (
  `mail_id` int(11) NOT NULL,
  `mail_send_to` varchar(255) DEFAULT NULL,
  `mail_bcc` varchar(255) DEFAULT NULL,
  `mail_subject` text,
  `mail_content` text,
  `mail_send_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urgency` int(11) NOT NULL DEFAULT '1' COMMENT '1=normal, 2= urgent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`message_id`, `sender_id`, `receiver_id`, `title`, `message`, `date_added`, `urgency`) VALUES
(1, 4, 4, 'TEST TITLE', 'TEST MESSAGE', '2018-06-12 22:05:09', 1),
(2, 2, 4, 'TEST TITLE', 'TEST MESSAGE', '2018-06-12 22:11:59', 1),
(3, 2, 4, 'TEST TITLE2', 'TEST MESSAGE', '2018-06-12 22:15:08', 1),
(4, 2, 5, 'TEST TITLE2', 'TEST MESSAGE', '2018-06-12 22:32:40', 1),
(5, 2, 5, 'TEST TITLE2UPD', 'TEST MESSAGE', '2018-06-12 22:33:10', 1),
(6, 4, 1, 'TEST HI TEST', 'TEST HI TEST', '2018-06-27 23:40:28', 1),
(7, 4, 5, 'TEST2asdas', 'dasdasd\nTEST', '2018-06-29 14:03:21', 1),
(8, 4, 1, 'HHHHHHHH', 'HHHHHHHH', '2018-06-29 14:03:54', 1),
(9, 1, 4, 'ASDSADSADSAD', 'SDADASDAS', '2018-06-29 14:13:53', 1),
(10, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 14:33:11', 1),
(11, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 17:50:25', 1),
(12, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 17:50:56', 1),
(13, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 17:51:02', 1),
(14, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 17:51:05', 1),
(15, 4, 1, 'tetetetete', 'tetetette', '2018-06-29 17:51:06', 1),
(16, 4, 5, NULL, 'teadsda', '2018-06-29 19:45:44', 1),
(17, 4, 5, NULL, 'asdsadasdad', '2018-06-29 19:46:30', 1),
(18, 2, 5, NULL, 'teasd', '2018-06-29 22:19:50', 1),
(19, 2, 5, NULL, 'teadas', '2018-06-29 22:20:02', 1),
(20, 2, 5, NULL, 'teadada\n', '2018-06-29 22:20:06', 1),
(21, 4, 1, NULL, 'fgdgfdgdf', '2018-06-30 02:45:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_posts`
--

CREATE TABLE `tbl_posts` (
  `post_id` int(11) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1=Normal.2 = Unlocked, 3 = Achievement',
  `post_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_posts`
--

INSERT INTO `tbl_posts` (`post_id`, `post_type`, `post_content`, `user_id`, `active`, `date_added`) VALUES
(2, 1, 'hjhgjhgjhgj', 4, 1, '2018-06-30 19:44:36'),
(3, 1, 'test posted a status ', 4, 1, '2018-06-30 20:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question`
--

CREATE TABLE `tbl_question` (
  `question_id` int(11) NOT NULL,
  `question_bank_id` int(11) NOT NULL,
  `question_type_id` int(11) NOT NULL,
  `question_title` varchar(255) DEFAULT NULL,
  `question_description` text NOT NULL,
  `question_attachment` text COMMENT 'This is in serialize format',
  `question_answer_mode` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question`
--

INSERT INTO `tbl_question` (`question_id`, `question_bank_id`, `question_type_id`, `question_title`, `question_description`, `question_attachment`, `question_answer_mode`, `date_added`) VALUES
(1, 1, 1, 'Simple Math 101upd2', '1+1 is equal to 2. How about 1+5 Then?', NULL, 1, '2018-05-27 00:14:58'),
(2, 1, 1, 'Simple Math 102', '2+2 is equal to 5. How about 2+5 Then?', NULL, 2, '2018-05-27 00:14:58'),
(3, 1, 1, 'TEST', 'TEST', NULL, 1, '2018-05-27 00:14:58'),
(4, 1, 2, 'zxcz', 'zxczxc', 'image_20180525-5b07b0c9793dc.jpg', 1, '2018-05-27 00:14:58'),
(6, 1, 2, 'test with images', 'test', NULL, 1, '2018-05-27 00:14:58'),
(7, 1, 1, 'test essauy', 'test essay', NULL, 2, '2018-05-27 00:14:58'),
(8, 1, 1, 'asd', 'asdsad', NULL, 2, '2018-05-27 00:14:58'),
(9, 1, 2, 'test with images2', 'test with image with cover photo', 'image_20180527-5b0a2b6f6e602.jpg', 1, '2018-05-27 11:52:56'),
(10, 23, 1, '1', '1', NULL, 1, '2018-05-27 12:15:09'),
(11, 24, 1, 'Your son had promised to call you to USA,____?', ' Choose the most appropriate words to complete the sentence', NULL, 1, '2018-05-27 19:45:53'),
(12, 24, 2, 'How do koala looks like?', 'Choose appropriate Image', 'image_20180528-5b0b6dc0e1d2e.jpg', 1, '2018-05-27 19:47:45'),
(13, 24, 1, 'He has _____________________________ knowledge on the subject.', 'Choose the most appropriate set of words', NULL, 1, '2018-05-27 19:54:02'),
(14, 24, 3, 'Audio Question Test', 'What sounds like Kalimba', 'image_20180528-5b0b70fc38d3d.jpg', 1, '2018-05-27 20:01:47'),
(15, 24, 1, '1', '1', NULL, 1, '2018-06-02 20:50:03'),
(16, 29, 1, 'Simple Addition', '1+2', NULL, 1, '2018-06-06 01:27:06'),
(17, 29, 1, 'Simple Addition', '4+5', NULL, 1, '2018-06-06 01:27:29'),
(18, 29, 1, 'Simple Addition', '1+2', NULL, 1, '2018-06-12 21:30:30'),
(19, 29, 1, 'Simple Addition', '6+1', NULL, 1, '2018-06-12 21:30:49'),
(20, 29, 1, 'Simple Addition', '4+6', NULL, 1, '2018-06-12 21:31:03'),
(21, 29, 1, 'Simple Addition', '2+4', NULL, 1, '2018-06-12 21:31:23'),
(22, 29, 1, 'Simple Addition', '11+4', NULL, 1, '2018-06-12 21:31:37'),
(23, 29, 1, 'Simple Addition', '1+7', NULL, 1, '2018-06-12 21:31:57'),
(24, 29, 1, 'Simple Addition', '0+9', NULL, 1, '2018-06-12 21:32:18'),
(25, 29, 1, 'Simple Addition', '34+1', NULL, 1, '2018-06-12 21:32:39'),
(26, 29, 1, 'Simple Addition', '1+4', NULL, 1, '2018-06-12 21:33:03'),
(27, 30, 1, 'Simple Substraction', '10-6', NULL, 1, '2018-06-15 11:10:52'),
(28, 30, 1, 'Simple Substraction', '1-3', NULL, 1, '2018-06-15 11:11:08'),
(29, 30, 1, 'Simple Substraction', '50-2', NULL, 1, '2018-06-15 11:11:27'),
(30, 29, 1, 'Simple Addition', '1+2+3', NULL, 1, '2018-06-17 03:37:54'),
(31, 24, 1, 'Sample question', 'This is a sample question', NULL, 1, '2018-06-19 02:34:02'),
(32, 31, 1, 'Q1', 'The __________ protects the brain', NULL, 1, '2018-06-20 20:50:16'),
(33, 31, 1, 'Q2', 'The __________ protect our lungs and heart.', NULL, 1, '2018-06-20 20:51:10'),
(34, 31, 1, 'Q3', 'A __________ is formed when two bones are joined together. ', NULL, 1, '2018-06-20 20:51:42'),
(35, 31, 1, 'Q4', 'The __________ and relaxation of muscles causes body movements. ', NULL, 1, '2018-06-20 20:52:40'),
(36, 31, 1, 'Q5', 'Poor eyesight can be __________ by wearing a pair of contact lenses.', NULL, 1, '2018-06-20 20:53:34'),
(37, 31, 2, 'Q6', 'Sounds captured by the ears are sent to the brain via the  __________.', NULL, 1, '2018-06-20 20:59:46'),
(38, 31, 1, 'Q7', 'Blood is __________ by the heart through blood vessels to all parts of the body.', NULL, 1, '2018-06-20 21:04:12'),
(39, 31, 1, 'Q8', 'Some plants and animals are so __________ that they can\'t be seen with the naked eye.', NULL, 1, '2018-06-20 21:04:45'),
(40, 31, 1, 'Q9', 'To travel to space, we must first __________ the Earth\'s gravitational force. ', NULL, 1, '2018-06-20 21:05:12'),
(41, 31, 1, 'Q10', 'The earthquake __________ a series of devastating tsunamis along the coast of Japan. ', NULL, 1, '2018-06-20 21:05:42'),
(42, 31, 1, 'Q11', 'The doe leapt into some thick bushes, leaving the hunters __________.', NULL, 1, '2018-06-20 21:06:15'),
(43, 31, 1, 'Q12', 'This tournament __________ a lot to me and I cannot afford to lose this set. ', NULL, 1, '2018-06-20 21:06:52'),
(44, 31, 1, 'Q13', 'This tournament __________ a lot to me and I cannot afford to lose this set. ', NULL, 1, '2018-06-20 21:07:27'),
(45, 31, 1, 'Q14', 'His decision to invest in Bitcoins was a __________ one. ', NULL, 1, '2018-06-20 21:08:07'),
(46, 31, 1, 'Q15', 'Rob was very __________ of his neighbour\'s help in looking after his cat while he in hospital.', NULL, 1, '2018-06-20 21:08:38'),
(47, 31, 1, 'Q16', 'We are encouraged to display __________ towards the poor, needy and homeless. ', NULL, 1, '2018-06-20 21:09:07'),
(48, 31, 1, 'Q17', 'Bill was so __________ that he lost his temper and shouted loudly. ', NULL, 1, '2018-06-20 21:09:41'),
(49, 31, 1, 'Q18', 'A tour at the Lourve can be an __________ trip for all of us. ', NULL, 1, '2018-06-20 21:10:14'),
(50, 31, 1, 'Q19', 'After hiking for more than 10km, Andrew\'s legs were __________.', NULL, 1, '2018-06-20 21:10:45'),
(51, 31, 1, 'Q20', 'Their decision to adopt a pet is a very exciting idea but could also be a little __________.', NULL, 1, '2018-06-20 21:24:45'),
(52, 31, 1, 'Q21', 'Joseph __________ to be a teacher when he grows up.', NULL, 1, '2018-06-20 21:26:03'),
(53, 31, 1, 'Q22', 'Where do you __________ to apply for college after the exams?', NULL, 1, '2018-06-20 21:34:36'),
(54, 31, 1, 'Q24', 'The work we do here is __________ and no one should know about it, not even your loved ones. ', NULL, 1, '2018-06-20 21:35:15'),
(55, 31, 1, 'Q25', 'You will feel __________ when you try to move around in the dark while blindfolded. ', NULL, 1, '2018-06-20 21:35:51'),
(56, 31, 1, 'Q26', 'She couldn’t make up her mind and was being __________ as she could only afford to buy either a new book or a new dress.', NULL, 1, '2018-06-20 21:39:16'),
(57, 31, 1, 'Q27', 'Singing the national anthem loudly can be seen as a sign of __________.', NULL, 1, '2018-06-20 21:41:22'),
(58, 31, 1, 'Q28', 'The heavy snowfall was __________ as the weather bureau had forecast sunny weather today. ', NULL, 1, '2018-06-20 22:24:05'),
(59, 31, 1, 'Q29', 'Tabloids newspapers seek readership by __________ stories. ', NULL, 1, '2018-06-20 22:27:37'),
(60, 31, 1, 'Q30', 'Alicia __________ when told about how well she performed in the play last night. ', NULL, 1, '2018-06-20 22:28:05'),
(61, 31, 1, 'Q31', 'The __________ athlete commissioned an artist to paint a huge portrait of himself to be hung in his bathroom. ', NULL, 1, '2018-06-20 22:28:34'),
(62, 31, 1, 'Q32', 'Maya was afraid to be seen as practising __________ and decided against hiring any of her family members. ', NULL, 1, '2018-06-20 22:29:26'),
(63, 31, 1, 'Q33', 'Jane emerged out of the water, __________ for air.', NULL, 1, '2018-06-20 22:31:27'),
(64, 31, 1, 'Q34', 'The broken door was an __________ sign that our house had been broken into. ', NULL, 1, '2018-06-20 22:32:48'),
(65, 31, 1, 'Q35', 'After a long and tiring day at sea, the fishermen returned home with __________ appetites. ', NULL, 1, '2018-06-20 22:35:21'),
(66, 31, 1, 'Q36', 'As a __________, he attempts to explain human behaviour without relying on individual factors. ', NULL, 1, '2018-06-20 22:37:10'),
(67, 31, 1, 'Q37', 'As one of several __________ of the company, it is within his right to use his vote to decide on the company\'s fate at this board meeting. ', NULL, 1, '2018-06-20 22:37:37'),
(68, 31, 1, 'Q38', 'President Obama wrote a __________ titled \"Dreams of my Father\", which was voted by Time Magazine as one of the Top 100 non-fiction books. ', NULL, 1, '2018-06-20 22:38:07'),
(69, 31, 1, 'Q39', 'He is a __________ and that talent earned him a position as an interpreter at the UN.', NULL, 1, '2018-06-20 22:40:15'),
(70, 31, 1, 'Q40', 'The dentist sees so many __________ a day that he has to work till late every day.', NULL, 1, '2018-06-20 22:40:47'),
(71, 31, 1, 'Q41', 'The building developer has just hired Tim to be the __________ to prepare plans and drawings for their new buildings. ', NULL, 1, '2018-06-20 22:41:15'),
(72, 31, 1, 'Q42', 'In any professional football game, you will find a __________ officiating it. ', NULL, 1, '2018-06-20 22:41:45'),
(73, 31, 1, 'Q43', 'He has always been interested in the study of space and the universe. He wants to become an __________.', NULL, 1, '2018-06-20 22:42:17'),
(74, 31, 1, 'Q44', 'He has always been interested in the study of space and the universe. He wants to become an __________.', NULL, 1, '2018-06-20 22:43:17'),
(75, 31, 1, 'Q45', 'In that village, you will find a __________ whose embroidery works are exquisite. ', NULL, 1, '2018-06-20 22:44:14'),
(76, 31, 1, 'Q46', 'Not only does he have a fleet of cars, he also hires a __________ to manage his household for him. ', NULL, 1, '2018-06-20 22:44:50'),
(77, 31, 1, 'Q23', 'I took my shoes to the __________ when the heels came off. ', NULL, 1, '2018-06-20 22:45:58'),
(78, 31, 1, 'Q47', 'Please don\'t lose your house keys. The only available __________ is on vacation right now. ', NULL, 1, '2018-06-20 22:46:42'),
(79, 31, 1, 'Q48', 'The case was thrown out by the judge due to the __________ of the defence lawyer. ', NULL, 1, '2018-06-20 22:47:23'),
(80, 31, 1, 'Q49', 'This is a very old house and the water pipes in it are leaky. I would advise you to ask a __________ to check them.', NULL, 1, '2018-06-20 22:48:15'),
(81, 31, 1, 'Q50', 'It is the duty of the class __________ to collect funds from each student and account for how the money is spent. ', NULL, 1, '2018-06-20 22:48:54'),
(82, 31, 1, 'Q51', 'Due to his __________ condition, this particular drug may affect his blood sugar levels.', NULL, 1, '2018-06-20 22:49:34'),
(83, 31, 1, 'Q52', 'If you need a good __________ for your car, tell me and I will introduce you to mine. ', NULL, 1, '2018-06-20 22:50:06'),
(84, 31, 1, 'Q53', 'A __________ must be both strong and weigh as little as possible to ride a horse effectively. ', NULL, 1, '2018-06-20 22:50:52'),
(85, 31, 1, 'Q54', 'You may want to pass some __________ to the porter once he brings your bags to the room. ', NULL, 1, '2018-06-20 22:51:39'),
(86, 31, 1, 'Q55', 'Being a __________ is something he loves since he\'s always had a sweet tooth. ', NULL, 1, '2018-06-20 22:52:13'),
(87, 31, 2, 'Q56', 'She wanted to bring her sick cat to see a __________ but none is available on a Sunday. ', NULL, 1, '2018-06-20 23:00:30'),
(88, 31, 1, 'Q57', 'The owner of this company hired a celebrity to be their brand __________. ', NULL, 1, '2018-06-20 23:01:06'),
(89, 31, 1, 'Q58', 'After many futile attempts to __________ him, the surgeon had no choice but to pronounce him dead. ', NULL, 1, '2018-06-20 23:01:34'),
(90, 31, 1, 'Q59', 'After a long struggle, she finally __________ to her illness and passed away last night. ', NULL, 1, '2018-06-20 23:02:01'),
(91, 31, 1, 'Q60', 'The measles __________ is usually not given until a child is at least a year old.', NULL, 1, '2018-06-20 23:02:28'),
(92, 31, 1, 'Q61', 'She was so __________ that she was wailing all night upon hearing of her father\'s death. ', NULL, 1, '2018-06-20 23:02:58'),
(93, 31, 1, 'Q62', 'Being depressed for a very long time, Janet was __________ taking anti-depressant medication.', NULL, 1, '2018-06-20 23:03:24'),
(94, 31, 1, 'Q63', 'With the advancement of science and technology, we can expect an increase in life __________.', NULL, 1, '2018-06-20 23:03:52'),
(95, 31, 1, 'Q64', 'Pablo Escobar was a __________ Colombian drug lord. ', NULL, 1, '2018-06-20 23:04:42'),
(96, 31, 2, 'Q65', '__________ is a good way to train the mind and the body to be more disciplined. ', NULL, 1, '2018-06-20 23:11:22'),
(97, 31, 1, 'Q66', 'Due to his frail health, he suffered from __________ after the surgery. ', NULL, 1, '2018-06-20 23:26:47'),
(99, 31, 1, 'Q67', 'The current Mexican President will attend the presidential __________ of one of his closest allies in Latin America.', NULL, 1, '2018-06-20 23:28:24'),
(100, 31, 1, 'Q68', 'His __________ was in tatters after he was implicated in a financial scandal. ', NULL, 1, '2018-06-20 23:29:09'),
(101, 31, 1, 'Q69', 'You should drink plenty of plain water to allow your body to __________ from a bout of flu. ', NULL, 1, '2018-06-20 23:29:42'),
(102, 31, 1, 'Q70', 'Unfortunately for him, his medical bills were very high as he didn’t qualify for any health __________. ', NULL, 1, '2018-06-20 23:30:07'),
(103, 31, 2, 'Q71', 'The body will be moved to the __________ for a post mortem to determine the cause of death. ', NULL, 1, '2018-06-20 23:34:19'),
(105, 31, 1, 'Q72', 'The loss of his wife had __________ effects on his physical well-being. ', NULL, 1, '2018-06-20 23:36:20'),
(106, 31, 1, 'Q73', 'Please refrain from visiting him at the hospital until he has fully __________.', NULL, 1, '2018-06-20 23:36:45'),
(107, 31, 1, 'Q74', 'The specialist at the hospital ______________ Allen with having a mild heart condition. ', NULL, 1, '2018-06-20 23:37:48'),
(108, 31, 1, 'Q75', 'She __________ her ankle because she slipped and fell on a wet and slippery floor. ', NULL, 1, '2018-06-20 23:39:36'),
(109, 31, 1, 'Q76', 'Francis was referred to a __________ due to his mental health issues. ', NULL, 1, '2018-06-20 23:41:52'),
(110, 31, 1, 'Q77', 'You should take your child to a __________ if he or she isn\'t feeling well. ', NULL, 1, '2018-06-20 23:42:24'),
(111, 31, 1, 'Q78', 'A __________ is someone who diagnoses and detects physiological ailments using x-rays and other such imaging equipment.', NULL, 1, '2018-06-20 23:43:02'),
(112, 31, 1, 'Q79', 'He was bleeding __________ and soon lost consciousness.', NULL, 1, '2018-06-20 23:43:26'),
(113, 31, 1, 'Q80', 'There were ___________ power cuts because of the severe winter storm.', NULL, 1, '2018-06-20 23:44:12'),
(114, 31, 1, 'Q81', 'You need to know the type of snake that bit you for us to give you the right __________.', NULL, 1, '2018-06-20 23:44:37'),
(115, 31, 1, 'Q82', 'Some __________ of the flu are runny nose, coughing, sore throat, fever and headache. ', NULL, 1, '2018-06-20 23:45:01'),
(116, 31, 1, 'Q83', 'He deserved the __________ showered on him after running a successful re-election campaign.', NULL, 1, '2018-06-20 23:45:28'),
(117, 31, 1, 'Q84', 'Anne is a highly decorated and __________ researcher in the field of genetic engineering.', NULL, 1, '2018-06-20 23:46:00'),
(118, 31, 1, 'Q85', 'She is known to be a morally upright person and will never __________ a situation for her benefit. ', NULL, 1, '2018-06-20 23:46:23'),
(119, 31, 1, 'Q86', 'Many of the world\'s most __________ artwork can be found at the Louvre in Paris.\n', NULL, 1, '2018-06-20 23:46:54'),
(120, 31, 1, 'Q87', 'The storyline of this movie was so __________ that we already knew how it would end. ', NULL, 1, '2018-06-20 23:47:55'),
(121, 31, 1, 'Q88', 'Syntech Technology is __________ over a possible takeover of their direct competitor. ', NULL, 1, '2018-06-20 23:48:43'),
(122, 31, 1, 'Q89', 'A sense of community is pivotal especially among a __________ society.', NULL, 1, '2018-06-20 23:49:28'),
(123, 31, 1, 'Q90', 'A country\'s judicial system plays a __________ role in its legal framework. ', NULL, 1, '2018-06-20 23:49:57'),
(124, 31, 1, 'Q91', 'As a strong leader, he will be able to mould a group of talented individuals into one __________ unit. ', NULL, 1, '2018-06-20 23:54:48'),
(125, 31, 1, 'Q92', 'Living in the Sahara Desert with just the bare necessities can be a __________ experience for anyone. ', NULL, 1, '2018-06-20 23:55:18'),
(126, 31, 1, 'Q93', 'Anna is a fund manager who has built an impressive financial __________ that generates very high returns. ', NULL, 1, '2018-06-21 00:04:16'),
(127, 31, 1, 'Q94', 'For any democracy to thrive, it is __________to have a free press. ', NULL, 1, '2018-06-21 00:05:30'),
(128, 31, 1, 'Q95', 'There are many __________ to this problem. So, you will have to consider all sides thoroughly. ', NULL, 1, '2018-06-21 00:49:24'),
(129, 31, 1, 'Q96', 'Despite his very best effort, it all __________ in failure and disappointment. ', NULL, 1, '2018-06-21 00:49:53'),
(130, 31, 1, 'Q97', 'The financial __________ of most non-profit organisations is highly dependent on donors and sponsors. ', NULL, 1, '2018-06-21 00:50:23'),
(131, 31, 1, 'Q98', 'Despite her relatively young age, Sandra was quickly promoted as regional manager as she showed a lot of __________.', NULL, 1, '2018-06-21 00:50:54'),
(132, 31, 1, 'Q99', 'The challenge of any government is to eradicate a system that is both __________ and bureaucratic. ', NULL, 1, '2018-06-21 00:51:30'),
(133, 31, 1, 'Q100', 'The development of eco-friendly technologies will eventually __________ many forms of pollution. ', NULL, 1, '2018-06-21 00:52:02'),
(134, 31, 1, 'Q101', 'For better flavour __________, it is best you marinate the meat a day before grilling it. ', NULL, 1, '2018-06-21 00:53:09'),
(135, 31, 1, 'Q102', 'Acid rain caused by pollution is the main reason for the severe __________ on the hillside. ', NULL, 1, '2018-06-21 00:53:35'),
(136, 31, 1, 'Q103', 'Many of his friends were rather __________ of his tales as they knew he tended to exaggerate.', NULL, 1, '2018-06-21 00:54:03'),
(137, 31, 1, 'Q104', 'The dark and gloomy skies look a bit __________.', NULL, 1, '2018-06-21 00:54:28'),
(138, 31, 1, 'Q105', 'Brenda had no __________ vouching for the good character of her protégé. ', NULL, 1, '2018-06-21 00:57:07'),
(139, 31, 1, 'Q106', 'Eric used his entire life savings and with __________ agreed to salvage his father\'s failing retail business. ', NULL, 1, '2018-06-21 00:57:38'),
(140, 31, 1, 'Q107', 'Erin and Joseph are still not financially stable, and therefore ____________ about having a child now.', NULL, 1, '2018-06-21 00:58:02'),
(141, 31, 1, 'Q108', 'Amy listened to Raj\'s story about how he scaled the tallest building in the world with an expression of __________.', NULL, 1, '2018-06-21 00:58:37'),
(142, 31, 1, 'Q109', 'The conservatives among us always view societal change and evolution with a degree of ___________', NULL, 1, '2018-06-21 00:59:07'),
(143, 31, 1, 'Q110', 'Any act of terrorism can be seen as barbaric and an act of __________.', NULL, 1, '2018-06-21 00:59:44'),
(144, 31, 1, 'Q111', 'Trial by jury is a legal process that has a certain degree of __________ elements to it. ', NULL, 1, '2018-06-21 01:00:16'),
(145, 31, 1, 'Q112', 'This horror film is one of the most __________ shows ever produced in Japan. ', NULL, 1, '2018-06-21 01:02:35'),
(146, 31, 1, 'Q113', 'I am usually __________ to disagree with Max, but his ideas today made a lot of sense. ', NULL, 1, '2018-06-21 01:03:03'),
(147, 31, 1, 'Q114', 'Jeremy knows that the best way to improve his performance in the ring is to accept any form of __________.', NULL, 1, '2018-06-21 01:03:27'),
(148, 31, 1, 'Q115', 'She __________ when she recalled her encounter with what she believed was a ghost.', NULL, 1, '2018-06-21 01:05:19'),
(149, 31, 1, 'Q116', 'George was rather reserved and often had a __________ look on him. ', NULL, 1, '2018-06-21 01:06:10'),
(150, 31, 1, 'Q117', 'She spent her early years in a refugee camp in a war-torn country and had seen many ________.', NULL, 1, '2018-06-21 01:06:40'),
(151, 31, 1, 'Q118', 'He made a __________ of the justice system by filing countless appeals and causing delays despite being found guilty of the crime. ', NULL, 1, '2018-06-21 01:07:03'),
(152, 31, 1, 'Q119', 'He was so shocked that he was __________ for a brief moment. ', NULL, 1, '2018-06-21 01:08:15'),
(153, 31, 1, 'Q120', 'Being a rather squeamish person, Alison always found snakes and rats to be rather __________.', NULL, 1, '2018-06-21 01:08:43'),
(154, 31, 1, 'Q121', 'After hours of tough and productive negotiation, she is __________ that an agreement can be reached by the end of the day. ', NULL, 1, '2018-06-21 01:09:22'),
(155, 31, 1, 'Q122', 'He finally __________ after his wife threatened to never talk to him again unless she got what she asked for. ', NULL, 1, '2018-06-21 01:09:52'),
(156, 31, 1, 'Q123', 'I am not sure whether her act of __________ is genuine or just another ploy to gain sympathy. ', NULL, 1, '2018-06-21 01:10:17'),
(157, 31, 1, 'Q124', 'Whenever there was a stranger in the house, Joseph would hide and __________ inside the wardrobe. ', NULL, 1, '2018-06-21 01:10:41'),
(158, 31, 1, 'Q125', 'He is __________ if he thinks that his wife, whom he has abused multiple times, will come back to him after the divorce. ', NULL, 1, '2018-06-21 01:11:26'),
(159, 31, 1, 'Q126', 'Many have found that it is difficult to __________ both science and religion. ', NULL, 1, '2018-06-21 01:11:55'),
(160, 31, 1, 'Q127', 'Due to the low number of new admissions, both schools decided to __________ into one institution. ', NULL, 1, '2018-06-21 01:12:49'),
(161, 31, 1, 'Q128', 'It would be counterproductive for all parties if he were to __________ the new measures deemed detrimental to the business. ', NULL, 1, '2018-06-21 01:19:40'),
(162, 31, 1, 'Q129', 'One way to reduce the cost of medicine is to __________ drug patents and make them accessible to other manufacturers.', NULL, 1, '2018-06-21 01:20:29'),
(163, 31, 1, 'Q130', 'Her fiery rhetoric about gender equality managed to __________ and stir passionate support in a largely female crowd.', NULL, 1, '2018-06-21 01:21:17'),
(164, 31, 1, 'Q131', 'The new extension of his villa was found to be illegally __________ on his neighbour\'s land. ', NULL, 1, '2018-06-21 01:21:51'),
(165, 31, 1, 'Q132', 'The __________ evidence that we had collected wasn’t enough for us to charge him with the crime. ', NULL, 1, '2018-06-21 01:22:15'),
(166, 31, 1, 'Q133', 'It is a shame that his immoral actions brought __________ to the honour of his late father.', NULL, 1, '2018-06-21 01:22:57'),
(167, 31, 1, 'Q134', 'We were warned to __________ extreme care when dealing with highly explosive elements. ', NULL, 1, '2018-06-21 01:23:25'),
(168, 31, 1, 'Q135', 'Extreme measures must be taken to avoid a __________ of last night\'s horrifying event. ', NULL, 1, '2018-06-21 01:23:54'),
(169, 31, 1, 'Q136', 'Due to the economic __________, the company had no choice but to file for bankruptcy protection. ', NULL, 1, '2018-06-21 01:24:25'),
(170, 31, 1, 'Q137', 'His cancer has been in __________ for years even though he has never undergone chemotherapy. ', NULL, 1, '2018-06-21 01:24:50'),
(171, 31, 1, 'Q138', 'Some wild animals __________ death when they sense a threat or danger lurking. ', NULL, 1, '2018-06-21 01:25:13'),
(172, 31, 1, 'Q139', 'There is a high __________ between smoking and getting lung cancer. ', NULL, 1, '2018-06-21 01:25:44'),
(173, 31, 1, 'Q140', 'The thieves\' plan was to __________ the security guards and then proceed to the vault.', NULL, 1, '2018-06-21 01:26:51'),
(174, 31, 1, 'Q141', 'As a minor, he will need his parents __________ to apply for a change of school. ', NULL, 1, '2018-06-21 01:27:51'),
(175, 31, 1, 'Q142', 'Emboldened by the unpopular nature of the government, the opposition took their voices of __________ to the masses. ', NULL, 1, '2018-06-21 01:28:25'),
(176, 31, 1, 'Q143', 'He felt as if he had signed his own death ___________ as soon as he cooperated with the police against the triad leader.', NULL, 1, '2018-06-21 01:29:03'),
(177, 31, 1, 'Q144', 'One careless __________ and you may find yourself badly injured at the foot of this hill. ', NULL, 1, '2018-06-21 01:29:35'),
(178, 31, 1, 'Q145', 'Unaware that he was given a __________ programming file, James loaded the programme into the company\'s mainframe. ', NULL, 1, '2018-06-21 01:30:02'),
(179, 31, 1, 'Q146', 'Many conservatives in the East believe that their culture was __________ by Western influences. ', NULL, 1, '2018-06-21 01:30:55'),
(180, 31, 1, 'Q147', 'Knowing her younger brother\'s stubborn nature, she knew wouldn\'t be easily ___________ by anyone.', NULL, 1, '2018-06-21 01:31:25'),
(181, 31, 1, 'Q148', 'The fierce dictator gave his usual __________ to a group of loyalists who dared to disobey him. ', NULL, 1, '2018-06-21 01:32:22'),
(182, 31, 1, 'Q149', 'The immigration officer __________ at a group of refugees to follow him. ', NULL, 1, '2018-06-21 01:32:44'),
(183, 31, 1, 'Q150', 'Julie\'s passion for everything morbid is __________ by her addiction to horror movies.', NULL, 1, '2018-06-21 01:33:14'),
(184, 31, 1, 'Q151', 'Even in the midst of failure and despair, her __________ belief in god is unwavering. ', NULL, 1, '2018-06-21 01:34:08'),
(185, 31, 1, 'Q152', 'Elizabeth is a highly-skilled fencer and she __________ defeated all her competitors. ', NULL, 1, '2018-06-21 01:34:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_answers`
--

CREATE TABLE `tbl_question_answers` (
  `question_answers_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_choice` text,
  `question_choice_attachments` text NOT NULL,
  `correct_answer` int(11) DEFAULT NULL COMMENT 'Index of the correct answer'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_answers`
--

INSERT INTO `tbl_question_answers` (`question_answers_id`, `question_id`, `question_choice`, `question_choice_attachments`, `correct_answer`) VALUES
(1, 1, 'a:4:{i:0;s:8:\"TEST1upd\";i:1;s:8:\"TEST2upd\";i:2;s:8:\"TEST3upd\";i:3;s:8:\"TEST4upd\";}', '', 1),
(2, 2, 'a:4:{i:0;s:1:\"7\";i:1;s:1:\"5\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', '', 1),
(3, 3, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', '', 1),
(4, 4, 'a:4:{i:0;s:8:\"zxczxczx\";i:1;s:11:\"zxczxcxzczx\";i:2;s:6:\"zxczxc\";i:3;s:5:\"zxczx\";}', '', 1),
(5, 5, 'a:4:{i:0;s:2:\"aa\";i:1;s:1:\"a\";i:2;s:2:\"aa\";i:3;s:2:\"aa\";}', '', 1),
(6, 6, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180526-5b0952550cf8a.jpg\";i:1;s:32:\"image_20180526-5b09526b98815.jpg\";i:2;N;i:3;N;}', 1),
(7, 7, 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', NULL),
(8, 8, 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(9, 9, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180527-5b0a2b7ab774f.jpg\";i:1;s:32:\"image_20180527-5b0a2b7e2a1da.jpg\";i:2;N;i:3;N;}', 1),
(10, 10, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(11, 11, 'a:4:{i:0;s:9:\"a little \";i:1;s:6:\"a few \";i:2;s:8:\"another \";i:3;s:12:\"none of this\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(12, 12, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"image_20180528-5b0b6dc470c1d.jpg\";i:1;s:32:\"image_20180528-5b0b6dc72c8e9.jpg\";i:2;s:32:\"image_20180528-5b0b6dcbede20.jpg\";i:3;s:32:\"image_20180528-5b0b6dcf03df1.jpg\";}', 3),
(13, 13, 'a:4:{i:0;s:9:\"a little \";i:1;s:6:\"a few \";i:2;s:8:\"another \";i:3;s:13:\"none of this \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(14, 14, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;s:32:\"audio_20180528-5b0b710a49769.mp3\";i:1;s:32:\"audio_20180528-5b0b710deb64d.mp3\";i:2;s:32:\"audio_20180528-5b0b7110976e5.mp3\";i:3;s:32:\"audio_20180528-5b0b7113484d3.mp3\";}', 1),
(15, 15, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"1\";i:2;s:1:\"1\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(16, 15, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"1\";i:2;s:1:\"1\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(17, 16, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(18, 17, 'a:4:{i:0;s:1:\"2\";i:1;s:2:\"56\";i:2;s:1:\"9\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(19, 18, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"1\";i:2;s:1:\"4\";i:3;s:1:\"5\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(20, 19, 'a:4:{i:0;s:1:\"3\";i:1;s:1:\"6\";i:2;s:2:\"32\";i:3;s:1:\"7\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(21, 20, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:2:\"10\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(22, 21, 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"4\";i:3;s:1:\"6\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(23, 22, 'a:4:{i:0;s:2:\"15\";i:1;s:1:\"2\";i:2;s:1:\"4\";i:3;s:2:\"21\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(24, 23, 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"8\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(25, 24, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"9\";i:2;s:1:\"6\";i:3;s:1:\"3\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(26, 25, 'a:4:{i:0;s:2:\"32\";i:1;s:2:\"35\";i:2;s:2:\"33\";i:3;s:2:\"32\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(27, 26, 'a:4:{i:0;s:1:\"5\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"1\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(28, 27, 'a:4:{i:0;s:1:\"4\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(29, 28, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(30, 29, 'a:4:{i:0;s:2:\"78\";i:1;s:1:\"3\";i:2;s:2:\"48\";i:3;s:1:\"6\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(31, 30, 'a:4:{i:0;s:1:\"5\";i:1;s:1:\"1\";i:2;s:1:\"6\";i:3;s:1:\"2\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(32, 31, 'a:4:{i:0;s:1:\"A\";i:1;s:1:\"B\";i:2;s:1:\"C\";i:3;s:1:\"D\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(33, 32, 'a:4:{i:0;s:5:\"skull\";i:1;s:9:\"membrane \";i:2;s:8:\"forehead\";i:3;s:8:\"ligament\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(34, 33, 'a:4:{i:0;s:4:\"ribs\";i:1;s:5:\"spine\";i:2;s:5:\"cells\";i:3;s:7:\"vessels\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(35, 34, 'a:4:{i:0;s:6:\"muscle\";i:1;s:5:\"organ\";i:2;s:5:\"joint\";i:3;s:5:\"layer\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(36, 35, 'a:4:{i:0;s:8:\"firmness\";i:1;s:11:\"contraction\";i:2;s:6:\"steady\";i:3;s:8:\"mutation\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(37, 36, 'a:4:{i:0;s:10:\"heightened\";i:1;s:5:\"cured\";i:2;s:9:\"corrected\";i:3;s:8:\"pulsated\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(38, 37, 'a:4:{i:0;s:5:\"blood\";i:1;s:8:\"skeleton\";i:2;s:5:\"sight\";i:3;s:6:\"nerves\";}', 'a:4:{i:0;s:32:\"image_20180621-5b2b22a5ef09e.jpg\";i:1;s:32:\"image_20180621-5b2b22a24b327.jpg\";i:2;s:32:\"image_20180621-5b2b229bbd1cf.jpg\";i:3;s:32:\"image_20180621-5b2b2290dc294.jpg\";}', 4),
(39, 38, 'a:4:{i:0;s:12:\"manufactured\";i:1;s:11:\"transferred\";i:2;s:7:\"donated\";i:3;s:6:\"pumped\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(40, 39, 'a:4:{i:0;s:10:\"invisible \";i:1;s:9:\"gigantic \";i:2;s:12:\"microscopic \";i:3;s:7:\"extinct\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(41, 40, 'a:4:{i:0;s:8:\"maintain\";i:1;s:7:\"destroy\";i:2;s:3:\"fix\";i:3;s:8:\"overcome\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(42, 41, 'a:4:{i:0;s:9:\"triggered\";i:1;s:5:\"ended\";i:2;s:9:\"contained\";i:3;s:11:\"restricted \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(43, 42, 'a:4:{i:0;s:7:\"content\";i:1;s:8:\"saddened\";i:2;s:6:\"in awe\";i:3;s:12:\"disappointed\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(44, 43, 'a:4:{i:0;s:5:\"means\";i:1;s:10:\"represents\";i:2;s:11:\"contributes\";i:3;s:5:\"shows\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(45, 44, 'a:4:{i:0;s:5:\"means\";i:1;s:10:\"represents\";i:2;s:11:\"contributes\";i:3;s:5:\"shows\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(46, 45, 'a:4:{i:0;s:6:\"shrewd\";i:1;s:7:\"devious\";i:2;s:8:\"generous\";i:3;s:7:\"cunning\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(47, 46, 'a:4:{i:0;s:10:\"dismissive\";i:1;s:12:\"appreciative\";i:2;s:11:\"descriptive\";i:3;s:9:\"assertive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(48, 47, 'a:4:{i:0;s:10:\"resentment\";i:1;s:6:\"apathy\";i:2;s:11:\"displeasure\";i:3;s:10:\"compassion\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(49, 48, 'a:4:{i:0;s:9:\"concerned\";i:1;s:5:\"stiff\";i:2;s:7:\"annoyed\";i:3;s:8:\"saddened\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(50, 49, 'a:4:{i:0;s:13:\"unimaginative\";i:1;s:9:\"enriching\";i:2;s:8:\"enabling\";i:3;s:9:\"inquiring\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(51, 50, 'a:4:{i:0;s:10:\"shivering \";i:1;s:4:\"soft\";i:2;s:5:\"stiff\";i:3;s:5:\"rigid\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(52, 51, 'a:4:{i:0;s:8:\"daunting\";i:1;s:8:\"fearless\";i:2;s:11:\"disgraceful\";i:3;s:9:\"admirable\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(53, 52, 'a:4:{i:0;s:8:\"acquires\";i:1;s:7:\"aspires\";i:2;s:8:\"dislikes\";i:3;s:7:\"applies\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(54, 53, 'a:4:{i:0;s:6:\"intent\";i:1;s:5:\"imply\";i:2;s:6:\"intend\";i:3;s:7:\"inquire\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(55, 54, 'a:4:{i:0;s:13:\"confidential \";i:1;s:12:\"conditional \";i:2;s:10:\"fulfilling\";i:3;s:12:\"treacherous \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(56, 55, 'a:4:{i:0;s:7:\"assured\";i:1;s:5:\"tired\";i:2;s:9:\"emotional\";i:3;s:8:\"helpless\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(57, 56, 'a:4:{i:0;s:7:\"fearful\";i:1;s:7:\"excited\";i:2;s:10:\"indecisive\";i:3;s:12:\"apprehensive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(58, 57, 'a:4:{i:0;s:8:\"nepotism\";i:1;s:10:\"patriotism\";i:2;s:10:\"compulsion\";i:3;s:12:\"togetherness\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(59, 58, 'a:4:{i:0;s:9:\"predicted\";i:1;s:7:\"assumed\";i:2;s:12:\"contradicted\";i:3;s:10:\"unexpected\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(60, 59, 'a:4:{i:0;s:6:\"faking\";i:1;s:16:\"sensationalising\";i:2;s:10:\"enlarging \";i:3;s:7:\"writing\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(61, 60, 'a:4:{i:0;s:6:\"snored\";i:1;s:7:\"blushed\";i:2;s:7:\"bloomed\";i:3;s:8:\"crumpled\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(62, 61, 'a:4:{i:0;s:8:\"reserved\";i:1;s:6:\"sloppy\";i:2;s:7:\"amazing\";i:3;s:9:\"conceited\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(63, 62, 'a:4:{i:0;s:8:\"nepotism\";i:1;s:10:\"democracy \";i:2;s:12:\"dictatorship\";i:3;s:9:\"communism\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(64, 63, 'a:4:{i:0;s:9:\"breathing\";i:1;s:7:\"gasping\";i:2;s:7:\"weeding\";i:3;s:7:\"feeding\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(65, 64, 'a:4:{i:0;s:7:\"ominous\";i:1;s:11:\"undignified\";i:2;s:9:\"insidious\";i:3;s:11:\"ignominious\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(66, 65, 'a:4:{i:0;s:8:\"ravenous\";i:1;s:12:\"incredulous \";i:2;s:4:\"hard\";i:3;s:11:\"indomitable\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(67, 66, 'a:4:{i:0;s:12:\"psychologist\";i:1;s:9:\"biologist\";i:2;s:9:\"historian\";i:3;s:11:\"sociologist\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(68, 67, 'a:4:{i:0;s:8:\"curators\";i:1;s:9:\"governors\";i:2;s:7:\"workers\";i:3;s:9:\"directors\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(69, 68, 'a:4:{i:0;s:12:\"dissertation\";i:1;s:7:\"episode\";i:2;s:6:\"memoir\";i:3;s:10:\"fairy tale\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(70, 69, 'a:4:{i:0;s:8:\"dictator\";i:1;s:8:\"polyglot\";i:2;s:10:\"ambassador\";i:3;s:6:\"orator\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(71, 70, 'a:4:{i:0;s:9:\"suppliers\";i:1;s:9:\"customers\";i:2;s:8:\"patients\";i:3;s:7:\"clients\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(72, 71, 'a:4:{i:0;s:11:\"draughtsman\";i:1;s:6:\"artist\";i:2;s:4:\"crew\";i:3;s:6:\"drawer\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(73, 72, 'a:4:{i:0;s:7:\"referee\";i:1;s:5:\"judge\";i:2;s:8:\"official\";i:3;s:7:\"manager\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(74, 73, 'a:4:{i:0;s:7:\"referee\";i:1;s:5:\"judge\";i:2;s:8:\"official\";i:3;s:7:\"manager\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(75, 74, 'a:4:{i:0;s:10:\"astrologer\";i:1;s:9:\"geologist\";i:2;s:11:\"taxidermist\";i:3;s:10:\"astronomer\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(76, 75, 'a:4:{i:0;s:6:\"herder\";i:1;s:9:\"spokesman\";i:2;s:6:\"weaver\";i:3;s:8:\"gardener\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(77, 76, 'a:4:{i:0;s:7:\"servant\";i:1;s:6:\"butler\";i:2;s:4:\"maid\";i:3;s:6:\"helper\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(78, 77, 'a:4:{i:0;s:7:\"cobbler\";i:1;s:8:\"salesman\";i:2;s:8:\"mechanic\";i:3;s:7:\"cashier\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(79, 78, 'a:4:{i:0;s:8:\"engineer\";i:1;s:10:\"blacksmith\";i:2;s:9:\"architect\";i:3;s:9:\"locksmith\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(80, 79, 'a:4:{i:0;s:13:\"incompetency \";i:1;s:10:\"astuteness\";i:2;s:7:\"bravado\";i:3;s:9:\"criticism\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(81, 80, 'a:4:{i:0;s:8:\"gardener\";i:1;s:7:\"plumber\";i:2;s:11:\"electrician\";i:3;s:10:\"technician\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(82, 81, 'a:4:{i:0;s:7:\"auditor\";i:1;s:8:\"chairman\";i:2;s:9:\"treasurer\";i:3;s:9:\"secretary\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(83, 82, 'a:4:{i:0;s:9:\"psychotic\";i:1;s:9:\"epileptic\";i:2;s:9:\"traumatic\";i:3;s:8:\"diabetic\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(84, 83, 'a:4:{i:0;s:9:\"secretary\";i:1;s:10:\"technician\";i:2;s:11:\"electrician\";i:3;s:8:\"mechanic\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(85, 84, 'a:4:{i:0;s:10:\"groundsman\";i:1;s:6:\"jockey\";i:2;s:4:\"jock\";i:3;s:9:\"zookeeper\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(86, 85, 'a:4:{i:0;s:4:\"food\";i:1;s:4:\"fees\";i:2;s:4:\"tips\";i:3;s:5:\"coins\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(87, 86, 'a:4:{i:0;s:10:\"patisserie\";i:1;s:4:\"cook\";i:2;s:9:\"concierge\";i:3;s:9:\"sous chef\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(88, 87, 'a:4:{i:0;s:6:\"doctor\";i:1;s:13:\"veterinarian\n\";i:2;s:9:\"caregiver\";i:3;s:5:\"nurse\";}', 'a:4:{i:0;s:32:\"image_20180621-5b2b3eef7e8ab.jpg\";i:1;s:32:\"image_20180621-5b2b3ef3aded3.jpg\";i:2;s:32:\"image_20180621-5b2b3ef7298e2.jpg\";i:3;s:32:\"image_20180621-5b2b3efacd4d7.png\";}', 2),
(89, 88, 'a:4:{i:0;s:7:\"opposer\";i:1;s:7:\"stalker\";i:2;s:10:\"ambassador\";i:3;s:8:\"customer\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(90, 89, 'a:4:{i:0;s:10:\"revitalise\";i:1;s:7:\"relieve\";i:2;s:6:\"rewind\";i:3;s:6:\"revive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(91, 90, 'a:4:{i:0;s:7:\"gave up\";i:1;s:9:\"succumbed\";i:2;s:7:\"subdued\";i:3;s:4:\"fell\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(92, 91, 'a:4:{i:0;s:7:\"vaccine\";i:1;s:9:\"treatment\";i:2;s:5:\"virus\";i:3;s:7:\"disease\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(93, 92, 'a:4:{i:0;s:10:\"distraught\";i:1;s:8:\"repulsed\";i:2;s:9:\"surprised\";i:3;s:6:\"aghast\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(94, 93, 'a:4:{i:0;s:6:\"hating\";i:1;s:8:\"thinking\";i:2;s:13:\"contemplating\";i:3;s:11:\"cultivating\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(95, 94, 'a:4:{i:0;s:11:\"reservation\";i:1;s:13:\"invincibility\";i:2;s:5:\"hopes\";i:3;s:10:\"expectancy\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(96, 95, 'a:4:{i:0;s:3:\"shy\";i:1;s:9:\"notorious\";i:2;s:4:\"mega\";i:3;s:6:\"famous\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(97, 96, 'a:4:{i:0;s:10:\"Meditation\";i:1;s:13:\"Transgression\";i:2;s:9:\"Bicycling\";i:3;s:14:\"Water aerobics\";}', 'a:4:{i:0;s:32:\"image_20180621-5b2b410e5afe7.jpg\";i:1;s:32:\"image_20180621-5b2b41133aab9.jpg\";i:2;s:32:\"image_20180621-5b2b411c13ad0.jpg\";i:3;s:32:\"image_20180621-5b2b4125ec4b4.jpg\";}', 1),
(98, 97, 'a:4:{i:0;s:11:\"convulsions\";i:1;s:11:\"trepidation\";i:2;s:13:\"complications\";i:3;s:8:\"weakness\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(99, 98, 'a:4:{i:0;s:12:\"inauguration\";i:1;s:8:\"festival\";i:2;s:8:\"ceremony\";i:3;s:8:\"uprising\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(100, 99, 'a:4:{i:0;s:12:\"inauguration\";i:1;s:8:\"festival\";i:2;s:8:\"ceremony\";i:3;s:8:\"uprising\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(101, 100, 'a:4:{i:0;s:7:\"fortune\";i:1;s:10:\"reputation\";i:2;s:5:\"creed\";i:3;s:10:\"worthiness\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(102, 101, 'a:4:{i:0;s:10:\"recuperate\";i:1;s:10:\"rejuvenate\";i:2;s:7:\"restore\";i:3;s:6:\"regain\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(103, 102, 'a:4:{i:0;s:4:\"tips\";i:1;s:5:\"bills\";i:2;s:9:\"discounts\";i:3;s:9:\"subsidies\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(104, 103, 'a:4:{i:0;s:8:\"pharmacy\";i:1;s:8:\"mortuary\";i:2;s:4:\"ward\";i:3;s:6:\"clinic\";}', 'a:4:{i:0;s:32:\"image_20180621-5b2b46e4cc00b.jpg\";i:1;s:32:\"image_20180621-5b2b46dec64e2.jpg\";i:2;s:32:\"image_20180621-5b2b46d970d2b.jpg\";i:3;s:32:\"image_20180621-5b2b46d32792b.jpg\";}', 2),
(105, 104, 'a:4:{i:0;s:6:\"normal\";i:1;s:11:\"frustrating\";i:2;s:7:\"adverse\";i:3;s:8:\"positive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(106, 105, 'a:4:{i:0;s:6:\"normal\";i:1;s:11:\"frustrating\";i:2;s:7:\"adverse\";i:3;s:8:\"positive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(107, 106, 'a:4:{i:0;s:13:\"reinvigorated\";i:1;s:8:\"restored\";i:2;s:9:\"recovered\";i:3;s:8:\"reversed\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(108, 107, 'a:4:{i:0;s:5:\"cured\";i:1;s:10:\"discovered\";i:2;s:9:\"presented\";i:3;s:9:\"diagnosed\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(109, 108, 'a:4:{i:0;s:12:\"strengthened\";i:1;s:8:\"sprained\";i:2;s:6:\"abused\";i:3;s:9:\"neglected\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(110, 109, 'a:4:{i:0;s:12:\"cardiologist\";i:1;s:13:\"dermatologist\";i:2;s:13:\"neonatologist\";i:3;s:12:\"psychologist\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(111, 110, 'a:4:{i:0;s:14:\"microbiologist\";i:1;s:12:\"pediatrician\";i:2;s:9:\"patrician\";i:3;s:11:\"radiologist\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(112, 111, 'a:4:{i:0;s:11:\"radiologist\";i:1;s:10:\"geneticist\";i:2;s:10:\"oncologist\";i:3;s:9:\"urologist\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(113, 112, 'a:4:{i:0;s:8:\"urgently\";i:1;s:9:\"profusely\";i:2;s:14:\"intermittently\";i:3;s:7:\"quickly\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(114, 113, 'a:4:{i:0;s:13:\"intermittent \";i:1;s:11:\"exhaustive \";i:2;s:9:\"technical\";i:3;s:6:\"strong\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(115, 114, 'a:4:{i:0;s:8:\"antidote\";i:1;s:10:\"antibiotic\";i:2;s:9:\"antiviral\";i:3;s:10:\"antiseptic\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(116, 115, 'a:4:{i:0;s:8:\"evidence\";i:1;s:5:\"signs\";i:2;s:8:\"symptoms\";i:3;s:10:\"indicators\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(117, 116, 'a:4:{i:0;s:8:\"plaudits\";i:1;s:10:\"resentment\";i:2;s:6:\"awards\";i:3;s:7:\"trauma\n\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(118, 117, 'a:4:{i:0;s:9:\"authentic\";i:1;s:12:\"apprehensive\";i:2;s:7:\"admired\";i:3;s:11:\"disregarded\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(119, 118, 'a:4:{i:0;s:11:\"acknowledge\";i:1;s:7:\"reverse\";i:2;s:7:\"exploit\";i:3;s:4:\"plot\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(120, 119, 'a:4:{i:0;s:10:\"impressive\";i:1;s:10:\"dismissive\";i:2;s:11:\"counterfeit\";i:3;s:6:\"banal \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(121, 120, 'a:4:{i:0;s:8:\"original\";i:1;s:5:\"banal\";i:2;s:7:\"surreal\";i:3;s:5:\"weird\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(122, 121, 'a:4:{i:0;s:8:\"thinking\";i:1;s:7:\"willing\";i:2;s:7:\"mulling\";i:3;s:7:\"pulling\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(123, 122, 'a:4:{i:0;s:10:\"fragmented\";i:1;s:8:\"cohesive\";i:2;s:6:\"absurd\";i:3;s:9:\"redundant\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(124, 123, 'a:4:{i:0;s:13:\"insignificant\";i:1;s:7:\"pivotal\";i:2;s:5:\"basic\";i:3;s:11:\"traditional\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(125, 124, 'a:4:{i:0;s:10:\"disjointed\";i:1;s:6:\"smooth\";i:2;s:8:\"cohesive\";i:3;s:7:\"muddled\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(126, 125, 'a:4:{i:0;s:7:\"fantasy\";i:1;s:8:\"negative\";i:2;s:8:\"positive\";i:3;s:11:\"challenging\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(127, 126, 'a:4:{i:0;s:9:\"portfolio\";i:1;s:8:\"workbook\";i:2;s:7:\"machine\";i:3;s:7:\"produce\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(128, 127, 'a:4:{i:0;s:10:\"fulfilling\";i:1;s:11:\"fundamental\";i:2;s:9:\"forbidden\";i:3;s:9:\"thrilling\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(129, 128, 'a:4:{i:0;s:12:\"vindications\";i:1;s:12:\"sublimations\";i:2;s:10:\"dimensions\";i:3;s:11:\"alterations\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(130, 129, 'a:4:{i:0;s:9:\"responded\";i:1;s:9:\"finalized\";i:2;s:9:\"theorized\";i:3;s:10:\"culminated\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(131, 130, 'a:4:{i:0;s:14:\"sustainability\";i:1;s:6:\"growth\";i:2;s:12:\"adaptability\";i:3;s:7:\"prowess\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(132, 131, 'a:4:{i:0;s:12:\"incompetence\";i:1;s:10:\"initiative\";i:2;s:8:\"idleness\";i:3;s:14:\"transformation\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(133, 132, 'a:4:{i:0;s:10:\"productive\";i:1;s:9:\"efficient\";i:2;s:10:\"cumbersome\";i:3;s:7:\"illegal\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(134, 133, 'a:4:{i:0;s:7:\"promote\";i:1;s:5:\"cause\";i:2;s:4:\"spur\";i:3;s:9:\"eradicate\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(135, 134, 'a:4:{i:0;s:11:\"enhancement\";i:1;s:10:\"detachment\";i:2;s:7:\"erosion\";i:3;s:11:\"procurement\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(136, 135, 'a:4:{i:0;s:6:\"growth\";i:1;s:7:\"erosion\";i:2;s:9:\"explosion\";i:3;s:5:\"state\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(137, 136, 'a:4:{i:0;s:4:\"fond\";i:1;s:10:\"optimistic\";i:2;s:9:\"skeptical\";i:3;s:9:\"convinced\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(138, 137, 'a:4:{i:0;s:10:\"perplexing\";i:1;s:10:\"monotonous\";i:2;s:3:\"sad\";i:3;s:7:\"ominous\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(139, 138, 'a:4:{i:0;s:10:\"hesitation\";i:1;s:10:\"motivation\";i:2;s:11:\"consequence\";i:3;s:8:\"feelings\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(140, 139, 'a:4:{i:0;s:4:\"fear\";i:1;s:11:\"trepidation\";i:2;s:13:\"authorisation\";i:3;s:11:\"ambivalence\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(141, 140, 'a:4:{i:0;s:7:\"leaning\";i:1;s:7:\"curious\";i:2;s:10:\"ambivalent\";i:3;s:5:\"eager\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(142, 141, 'a:4:{i:0;s:7:\"sadness\";i:1;s:7:\"disdain\";i:2;s:10:\"conviction\";i:3;s:11:\"incredulity\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(143, 142, 'a:4:{i:0;s:7:\"disdain\";i:1;s:5:\"pride\";i:2;s:9:\"cowardice\";i:3;s:6:\"honour\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(144, 143, 'a:4:{i:0;s:11:\"superiority\";i:1;s:9:\"cowardice\";i:2;s:7:\"justice\";i:3;s:11:\"consequence\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(145, 144, 'a:4:{i:0;s:8:\"tangible\";i:1;s:6:\"morbid\";i:2;s:10:\"infallible\";i:3;s:5:\"tense\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(146, 145, 'a:4:{i:0;s:10:\"tearjerker\";i:1;s:7:\"sexiest\";i:2;s:11:\"unrealistic\";i:3;s:6:\"morbid\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(147, 146, 'a:4:{i:0;s:8:\"inclined\";i:1;s:8:\"declined\";i:2;s:6:\"unable\";i:3;s:8:\"helpless\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(148, 147, 'a:4:{i:0;s:7:\"mockery\";i:1;s:8:\"feedback\";i:2;s:7:\"disgust\";i:3;s:9:\"brickbats\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(149, 148, 'a:4:{i:0;s:8:\"recoiled\";i:1;s:9:\"shuttered\";i:2;s:9:\"shuddered\";i:3;s:6:\"yelped\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(150, 149, 'a:4:{i:0;s:7:\"bemused\";i:1;s:6:\"jovial\";i:2;s:8:\"delicate\";i:3;s:7:\"pensive\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(151, 150, 'a:4:{i:0;s:11:\"festivities\";i:1;s:12:\"coincidences\";i:2;s:12:\"competitions\";i:3;s:10:\"atrocities\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(152, 151, 'a:4:{i:0;s:7:\"mockery\";i:1;s:7:\"example\";i:2;s:10:\"triviality\";i:3;s:5:\"stand\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(153, 152, 'a:4:{i:0;s:8:\"restless\";i:1;s:8:\"immobile\";i:2;s:8:\"agitated\";i:3;s:7:\"annoyed\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(154, 153, 'a:4:{i:0;s:8:\"adorable\";i:1;s:11:\"mischievous\";i:2;s:9:\"repulsive\";i:3;s:10:\"redundant \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(155, 154, 'a:4:{i:0;s:5:\"happy\";i:1;s:7:\"worried\";i:2;s:11:\"pessimistic\";i:3;s:10:\"optimistic\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(156, 155, 'a:4:{i:0;s:8:\"relented\";i:1;s:7:\"laughed\";i:2;s:7:\"cowered\";i:3;s:8:\"accepted\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(157, 156, 'a:4:{i:0;s:8:\"derision\";i:1;s:7:\"remorse\";i:2;s:10:\"reluctance\";i:3;s:14:\"disappointment\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(158, 157, 'a:4:{i:0;s:6:\"hinder\";i:1;s:6:\"glower\";i:2;s:5:\"cower\";i:3;s:7:\"reflect\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(159, 158, 'a:4:{i:0;s:7:\"comedic\";i:1;s:7:\"hopeful\";i:2;s:7:\"worried\";i:3;s:10:\"delusional\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(160, 159, 'a:4:{i:0;s:9:\"reconcile\";i:1;s:9:\"frustrate\";i:2;s:5:\"cover\";i:3;s:5:\"study\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(161, 160, 'a:4:{i:0;s:7:\"destroy\";i:1;s:5:\"merge\";i:2;s:7:\"restore\";i:3;s:5:\"build\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(162, 161, 'a:4:{i:0;s:8:\"continue\";i:1;s:4:\"find\";i:2;s:9:\"implement\";i:3;s:5:\"relax\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(163, 162, 'a:4:{i:0;s:10:\"monopolise\";i:1;s:7:\"protect\";i:2;s:10:\"decapitate\";i:3;s:7:\"abolish\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(164, 163, 'a:4:{i:0;s:7:\"inflame\";i:1;s:4:\"bore\";i:2;s:5:\"shame\";i:3;s:4:\"lure\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(165, 164, 'a:4:{i:0;s:7:\"pending\";i:1;s:11:\"encroaching\";i:2;s:6:\"bought\";i:3;s:7:\"filling\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(166, 165, 'a:4:{i:0;s:11:\"sufficient \";i:1;s:5:\"false\";i:2;s:5:\"scant\";i:3;s:3:\"raw\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(167, 166, 'a:4:{i:0;s:10:\"ill-repute\";i:1;s:4:\"love\";i:2;s:9:\"disregard\";i:3;s:9:\"disrepute\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(168, 167, 'a:4:{i:0;s:8:\"exercise\";i:1;s:8:\"exorcise\";i:2;s:6:\"handle\";i:3;s:4:\"read\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(169, 168, 'a:4:{i:0;s:12:\"continuation\";i:1;s:10:\"recurrence\";i:2;s:10:\"completion\";i:3;s:6:\"relief\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(170, 169, 'a:4:{i:0;s:6:\"recess\";i:1;s:13:\"transgression\";i:2;s:9:\"recession\";i:3;s:8:\"opinion \";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(171, 170, 'a:4:{i:0;s:6:\"defeat\";i:1;s:9:\"recession\";i:2;s:6:\"hiding\";i:3;s:9:\"remission\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(172, 171, 'a:4:{i:0;s:5:\"feign\";i:1;s:8:\"perceive\";i:2;s:8:\"imitate \";i:3;s:7:\"process\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(173, 172, 'a:4:{i:0;s:9:\"agreement\";i:1;s:11:\"correlation\";i:2;s:12:\"relationship\";i:3;s:15:\"discontinuation\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(174, 173, 'a:4:{i:0;s:6:\"harass\";i:1;s:9:\"instigate\";i:2;s:6:\"subdue\";i:3;s:11:\"interrogate\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(175, 174, 'a:4:{i:0;s:7:\"dissent\";i:1;s:9:\"authority\";i:2;s:7:\"control\";i:3;s:7:\"consent\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(176, 175, 'a:4:{i:0;s:7:\"dissent\";i:1;s:7:\"harmony\";i:2;s:4:\"love\";i:3;s:5:\"power\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(177, 176, 'a:4:{i:0;s:7:\"warning\";i:1;s:7:\"warrant\";i:2;s:8:\"document\";i:3;s:4:\"note\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(178, 177, 'a:4:{i:0;s:4:\"word\";i:1;s:7:\"thought\";i:2;s:4:\"slip\";i:3;s:7:\"emotion\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(179, 178, 'a:4:{i:0;s:8:\"censored\";i:1;s:7:\"corrupt\";i:2;s:13:\"sophisticated\";i:3;s:5:\"clean\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(180, 179, 'a:4:{i:0;s:8:\"dictated\";i:1;s:8:\"enhanced\";i:2;s:9:\"corrupted\";i:3;s:8:\"swindled\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 3),
(181, 180, 'a:4:{i:0;s:10:\"worshipped\";i:1;s:7:\"hassled\";i:2;s:11:\"overwhelmed\";i:3;s:10:\"influenced\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(182, 181, 'a:4:{i:0;s:8:\"harangue\";i:1;s:7:\"chatter\";i:2;s:6:\"speech\";i:3;s:7:\"tribute\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(183, 182, 'a:4:{i:0;s:12:\"gesticulated\";i:1;s:6:\"argued\";i:2;s:9:\"clobbered\";i:3;s:7:\"derided\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1),
(184, 183, 'a:4:{i:0;s:7:\"annoyed\";i:1;s:9:\"augmented\";i:2;s:8:\"troubled\";i:3;s:9:\"decreased\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 2),
(185, 184, 'a:4:{i:0;s:8:\"acquired\";i:1;s:10:\"nonchalant\";i:2;s:9:\"repulsive\";i:3;s:7:\"fervent\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 4),
(186, 185, 'a:4:{i:0;s:11:\"comfortably\";i:1;s:9:\"nervously\";i:2;s:10:\"vigorously\";i:3;s:5:\"badly\";}', 'a:4:{i:0;N;i:1;N;i:2;N;i:3;N;}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_answer_mode`
--

CREATE TABLE `tbl_question_answer_mode` (
  `question_answer_mode_id` int(11) NOT NULL,
  `question_type_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_answer_mode`
--

INSERT INTO `tbl_question_answer_mode` (`question_answer_mode_id`, `question_type_desc`) VALUES
(1, 'multi_choice'),
(2, 'essay');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_bank`
--

CREATE TABLE `tbl_question_bank` (
  `question_bank_id` int(11) NOT NULL,
  `question_bank_title` varchar(255) NOT NULL,
  `question_bank_desc` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `course_section_id` int(11) DEFAULT NULL,
  `author_user_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_bank`
--

INSERT INTO `tbl_question_bank` (`question_bank_id`, `question_bank_title`, `question_bank_desc`, `category_id`, `course_section_id`, `author_user_id`, `active`, `date_added`) VALUES
(1, 'English Vocabulary Bank', 'TEST DESCR2', NULL, NULL, 2, 1, '2018-05-14 12:55:15'),
(2, 'TESTBANK2', 'TEST DESCR2', NULL, NULL, 2, 1, '2018-05-14 12:55:15'),
(3, 'TESTBANK3', 'TEST DESCR3', NULL, NULL, 2, 1, '2018-05-14 12:55:15'),
(4, 'TESTBANK3', 'TEST DESCR3', NULL, NULL, 2, 0, '2018-05-14 12:55:15'),
(14, 'asd', 'asd', NULL, NULL, 2, 0, '2018-05-14 12:55:15'),
(15, 'asd', 'asd', NULL, NULL, 2, 0, '2018-05-14 12:55:15'),
(16, 'asd', 'asdasd', NULL, NULL, 2, 0, '2018-05-14 12:55:15'),
(17, 'TESTBANK1UPD55', 'TEST DESCR2', NULL, NULL, 2, 0, '2018-05-14 12:55:15'),
(18, 'ds', 'fsdf', NULL, NULL, 1, 1, '2018-05-14 12:55:15'),
(19, 'sad', 'asd', NULL, NULL, 2, 0, '2018-05-14 18:47:19'),
(20, 'xzczxc', 'zxczc', NULL, NULL, 2, 0, '2018-05-14 18:47:27'),
(21, 'xz', 'zxzx', NULL, NULL, 2, 0, '2018-05-14 18:47:30'),
(22, 'xzc', 'xzc', NULL, NULL, 2, 0, '2018-05-14 18:58:16'),
(23, 'TEST NEW  BANK', 'TEST', NULL, NULL, 2, 1, '2018-05-27 12:14:46'),
(24, 'ENG Vocab MultiChoice', 'English vocabulary multiple choices question bank.\nContains text, images and audios', NULL, NULL, 4, 1, '2018-05-27 19:40:47'),
(25, 'test2', 'test2', NULL, NULL, 4, 0, '2018-05-27 23:19:38'),
(26, 'wads', 'asda', NULL, NULL, 4, 0, '2018-06-02 19:43:45'),
(27, 'TEST QUESTION BANK', 'TEST QUESTION BANK', NULL, NULL, 4, 1, '2018-06-02 20:48:54'),
(28, 'sad', 'asdasdasz', NULL, NULL, 4, 0, '2018-06-02 23:14:02'),
(29, 'MATH Simple Questions', 'Contains series of elementary level Math questions that can stirr up your mind', NULL, NULL, 4, 1, '2018-06-06 01:26:43'),
(30, 'MATH Simple Substraction', 'Simple Substraction', NULL, NULL, 4, 1, '2018-06-15 11:10:23'),
(31, 'Vocabulary CEFR B2', 'English Vocabulary CEFR Level B2 Questions (MCQ)', NULL, NULL, 4, 1, '2018-06-22 22:12:57'),
(32, 'asdas', 'asdasd', NULL, NULL, 4, 0, '2018-06-22 22:28:53'),
(33, 'test adjusted bank', NULL, 1, 1, 4, 0, '2018-06-30 21:02:58'),
(34, 'New Adjusted Question Bank FOR SECTION I', 'New Adjusted Question Bank FOR SECTION I', 1, 13, 4, 1, '2018-06-30 21:11:28'),
(35, 'New Adjusted Question Bank FOR SECTION I 2', 'New Adjusted Question Bank FOR SECTION II', 1, 13, 4, 1, '2018-06-30 21:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_type`
--

CREATE TABLE `tbl_question_type` (
  `question_type_id` int(11) NOT NULL,
  `question_type_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_type`
--

INSERT INTO `tbl_question_type` (`question_type_id`, `question_type_desc`) VALUES
(1, 'text'),
(2, 'image'),
(3, 'audio');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `session_id` int(11) NOT NULL,
  `session_key` varchar(255) NOT NULL,
  `session_value` text,
  `session_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `session_expiry` bigint(20) DEFAULT NULL,
  `session_user_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`session_id`, `session_key`, `session_value`, `session_created`, `session_expiry`, `session_user_id`, `active`) VALUES
(25, 'agora_user_c81e728d9d4c', '00f392e6ba5d1dc1026717ad23a77ed8', '2018-04-29 23:20:56', 3600, 2, 1),
(26, 'agora_user_c81e728d9d4c', '0560cc66d845f69aef4b7852b9af795d', '2018-05-01 12:45:59', 3600, 2, 1),
(27, 'agora_user_c81e728d9d4c', '8177c70b7d9e50e474fc2ca7753b6bfc', '2018-05-01 19:34:12', 3600, 2, 1),
(28, 'agora_user_c81e728d9d4c', '751b736de8199a452dee72bc6100650d', '2018-05-01 19:34:18', 3600, 2, 1),
(29, 'agora_user_c81e728d9d4c', '2425d264b5defcaaec4d1dc881d2dea3', '2018-05-01 19:34:49', 3600, 2, 1),
(30, 'agora_user_c81e728d9d4c', '50fb6b59422b3bccbfa5285e70849fc7', '2018-05-01 19:34:52', 3600, 2, 1),
(31, 'agora_user_c81e728d9d4c', '7889583427c8f3dc99e6452f5d1ee5e0', '2018-05-02 20:31:29', 3600, 2, 1),
(32, 'agora_user_c81e728d9d4c', '5bcd8b0f8dfeffbd8532de8130420575', '2018-05-02 20:58:02', 3600, 2, 1),
(33, 'agora_user_c81e728d9d4c', 'e04780dfc5a9b9ae51d7374338ec9e87', '2018-05-03 21:22:08', 3600, 2, 1),
(34, 'agora_user_c81e728d9d4c', '6759771f4218f75e50da0a594f07c767', '2018-05-03 21:32:35', 3600, 2, 1),
(35, 'agora_user_c81e728d9d4c', '94d0de620f13b6de12dabdaceabada91', '2018-05-03 21:33:01', 3600, 2, 1),
(36, 'agora_user_c81e728d9d4c', 'baae9fe7f30cd36166ccc076ef347d9d', '2018-05-03 21:33:02', 3600, 2, 1),
(37, 'agora_user_c81e728d9d4c', '00081ba7423edb6069ffbace0c8c9486', '2018-05-03 21:33:05', 3600, 2, 1),
(38, 'agora_user_c81e728d9d4c', 'd5ad857bfdca117b34384a874b116668', '2018-05-03 21:33:06', 3600, 2, 1),
(39, 'agora_user_c81e728d9d4c', '39c6d62b2a5677568eed4c0bbe3dc9e7', '2018-05-03 21:33:07', 3600, 2, 1),
(40, 'agora_user_c81e728d9d4c', '5d6ffc3364f6438d398388dd0518b0b2', '2018-05-03 21:33:10', 3600, 2, 1),
(41, 'agora_user_c81e728d9d4c', 'a7a48bd6a6b25642b87b758fffd5f7f1', '2018-05-03 21:33:11', 3600, 2, 1),
(42, 'agora_user_c81e728d9d4c', 'c23095351c9ede900d72a101bdbe4516', '2018-05-03 21:33:12', 3600, 2, 1),
(43, 'agora_user_c81e728d9d4c', 'a4a8c08e5334420ee1cefb1b180aabcd', '2018-05-03 21:33:26', 3600, 2, 1),
(44, 'agora_user_c81e728d9d4c', 'cdd94992790093119cee73be033106e0', '2018-05-03 21:34:12', 3600, 2, 1),
(45, 'agora_user_c81e728d9d4c', '59198c2314acd369fa367819b2beb6ad', '2018-05-03 23:22:48', 3600, 2, 1),
(46, 'agora_user_c81e728d9d4c', 'b0fae60e85beb0920e55a556ea65d0b4', '2018-05-12 07:48:04', 3600, 2, 1),
(47, 'agora_user_c81e728d9d4c', 'b57e9d03252ce82126404827e7debfe9', '2018-05-14 02:30:03', 3600, 2, 1),
(48, 'agora_user_c81e728d9d4c', '8593524348586c0665378aa9963f1acb', '2018-05-14 02:45:07', 3600, 2, 1),
(49, 'agora_user_c81e728d9d4c', 'a25f77eed600bfd1db0a6f3a1f7052af', '2018-05-14 04:04:38', 3600, 2, 1),
(50, 'agora_user_c81e728d9d4c', '67d3b324ca45e7951c618086ba69b122', '2018-05-14 04:05:11', 3600, 2, 1),
(51, 'agora_user_c81e728d9d4c', 'ebc8712bf858836d8b8e1d9f3f10fdb3', '2018-05-14 18:31:28', 3600, 2, 1),
(52, 'agora_user_c81e728d9d4c', '2f8e617fe36b24e98998dd7da2676ec0', '2018-05-17 20:31:14', 3600, 2, 1),
(53, 'agora_user_c81e728d9d4c', '3b9567c9ea1e7d5c58b40625eab0fbde', '2018-05-17 20:34:19', 3600, 2, 1),
(54, 'agora_user_c81e728d9d4c', 'c3dc7325b3a081478ce3adcb6f33a96a', '2018-05-17 20:59:15', 3600, 2, 1),
(55, 'agora_user_c81e728d9d4c', '91d6c5b8a9ff614f9a7e2bf6fab09ded', '2018-05-17 21:04:20', 3600, 2, 1),
(56, 'agora_user_eccbc87e4b5c', 'bbd243c72cef7e9e45f5ac184fa6f7a1', '2018-05-17 21:17:28', 3600, 3, 1),
(57, 'agora_user_c81e728d9d4c', 'afc8caf9797f50c9f34c23005e6f9b35', '2018-05-24 21:37:57', 3600, 2, 1),
(58, 'agora_user_c81e728d9d4c', '1cdc033fa384b187bcaf57b39b9f492f', '2018-05-24 22:23:41', 3600, 2, 1),
(59, 'agora_user_c81e728d9d4c', 'd46626f342b0f500d9e6cebe94d5990b', '2018-05-24 22:24:44', 3600, 2, 1),
(60, 'agora_user_c81e728d9d4c', '6af00ac3cc3851d5c0c68bf77014fbfc', '2018-05-24 22:28:25', 3600, 2, 1),
(61, 'agora_user_c81e728d9d4c', '93514962a6c1ffca3bb0045843fd0e62', '2018-05-26 19:46:33', 3600, 2, 1),
(62, 'agora_user_a87ff679a2f3', 'c7dc65488606fc16da686b950dc6abe4', '2018-05-27 19:38:32', 3600, 4, 1),
(63, 'agora_user_a87ff679a2f3', '734d19058280dd346c288a07a90fb6be', '2018-05-27 19:39:12', 3600, 4, 1),
(64, 'agora_user_a87ff679a2f3', 'd393fed47c8088a26a93129136d2a9db', '2018-05-29 20:29:24', 3600, 4, 1),
(65, 'agora_user_a87ff679a2f3', 'd393fed47c8088a26a93129136d2a9db', '2018-05-29 20:29:24', 3600, 4, 1),
(66, 'agora_user_a87ff679a2f3', '6d79e192c8620d12641e9b2c0cc28189', '2018-05-29 20:30:01', 3600, 4, 1),
(67, 'agora_user_a87ff679a2f3', '74614bfedeca1a26ae84b458f2d62ef9', '2018-05-30 20:40:53', 3600, 4, 1),
(68, 'agora_user_c81e728d9d4c', '78e25e8ec553452df6327fb9929067c0', '2018-05-30 21:01:05', 3600, 2, 1),
(69, 'agora_user_a87ff679a2f3', '5dd95982d168b3ace7785b8a5bd495ff', '2018-05-30 21:01:09', 3600, 4, 1),
(70, 'agora_user_a87ff679a2f3', '60b57ba966e6551b698eeae33bc4ffd1', '2018-05-31 20:40:12', 3600, 4, 1),
(71, 'agora_user_a87ff679a2f3', '50ab29b2a77b8d4632af09a966f5a89d', '2018-05-31 23:24:40', 3600, 4, 1),
(72, 'agora_user_a87ff679a2f3', '5cd5446408d01a611f16d6fb699e7c3b', '2018-05-31 23:26:44', 3600, 4, 1),
(73, 'agora_user_c81e728d9d4c', '4b517c4233a0972af79ece564005b2ba', '2018-06-02 13:59:28', 3600, 2, 1),
(74, 'agora_user_c81e728d9d4c', 'c051a1cc8ede5651233835b3250612d3', '2018-06-02 14:01:47', 3600, 2, 1),
(75, 'agora_user_c81e728d9d4c', '13cbf0ce3ac4f4a8df6e8ead2cf8b590', '2018-06-02 14:03:08', 3600, 2, 1),
(76, 'agora_user_a87ff679a2f3', '730e53ef9ac5c16ef27c0726b8f10031', '2018-06-02 14:06:10', 3600, 4, 1),
(77, 'agora_user_a87ff679a2f3', '83b1e005671fd5efed558dfd6eecf07c', '2018-06-02 14:06:19', 3600, 4, 1),
(78, 'agora_user_a87ff679a2f3', '833f7112c7b4f631636e40fc31d992ef', '2018-06-02 15:34:28', 3600, 4, 1),
(79, 'agora_user_c81e728d9d4c', 'c49d179f316ad25c4472d8406d97b22a', '2018-06-02 22:41:34', 3600, 2, 1),
(80, 'agora_user_c81e728d9d4c', 'c7edcbed3e16397913735bf1b6951bd2', '2018-06-02 22:43:02', 3600, 2, 1),
(81, 'agora_user_a87ff679a2f3', '7a0d8d3736859274f3887450e3949ec6', '2018-06-02 22:43:20', 3600, 4, 1),
(82, 'agora_user_c81e728d9d4c', 'd0f8fc5d4085cf21bf1445dac18e173b', '2018-06-03 00:48:40', 3600, 2, 1),
(83, 'agora_user_a87ff679a2f3', '156f885a9d511641e6d2ef74e887b0c3', '2018-06-02 10:27:16', 3600, 4, 1),
(84, 'agora_user_c81e728d9d4c', 'eddd6203bf29cbaec293fe301de26b16', '2018-06-02 10:31:32', 3600, 2, 1),
(85, 'agora_user_a87ff679a2f3', 'c10bb695453f81711a40d5502f303531', '2018-06-02 10:46:10', 3600, 4, 1),
(86, 'agora_user_a87ff679a2f3', '868ec29818e6ad2fc5418ba01c8d2260', '2018-06-02 20:30:54', 3600, 4, 1),
(87, 'agora_user_c4ca4238a0b9', 'cf89e3758f5a91d8a03d215cc4e489a5', '2018-06-02 20:36:44', 3600, 1, 1),
(88, 'agora_user_a87ff679a2f3', '0858c918f6609f58d8461aa16b81f6da', '2018-06-03 03:16:05', 3600, 4, 1),
(89, 'agora_user_c81e728d9d4c', '1dceba490b536056e43de928303f0462', '2018-06-03 08:10:47', 3600, 2, 1),
(90, 'agora_user_a87ff679a2f3', 'd7e5d5f6701d04f91116dd83fd146c5e', '2018-06-03 08:12:26', 3600, 4, 1),
(91, 'agora_user_a87ff679a2f3', '7ce267d35aa6e0555e0508d95469b6f9', '2018-06-03 08:38:00', 3600, 4, 1),
(92, 'agora_user_a87ff679a2f3', 'dbca9bc0ef7b2af6a2ad927515a19b7c', '2018-06-03 08:43:29', 3600, 4, 1),
(93, 'agora_user_a87ff679a2f3', '0c2a4bf6f335944f349d1d5d9d6a14e3', '2018-06-04 06:38:22', 3600, 4, 1),
(94, 'agora_user_a87ff679a2f3', '2981ba6fab53d02633ab6498a8d99fd3', '2018-06-05 00:47:18', 3600, 4, 1),
(95, 'agora_user_a87ff679a2f3', '03ed0c5d7dab8b41360aaae065bdaf45', '2018-06-05 00:57:14', 3600, 4, 1),
(96, 'agora_user_a87ff679a2f3', '1902e9366aa826d4dc937b2d587feb84', '2018-06-05 05:31:26', 3600, 4, 1),
(97, 'agora_user_c81e728d9d4c', 'aa9f5e210509de4970783c4ed133806a', '2018-06-05 06:11:38', 3600, 2, 1),
(98, 'agora_user_a87ff679a2f3', 'd37150751cfa30c71f59190ef23bfe22', '2018-06-05 06:58:02', 3600, 4, 1),
(99, 'agora_user_a87ff679a2f3', 'de4cee0c7f50d9e42126b2f3344e4995', '2018-06-05 19:51:45', 3600, 4, 1),
(100, 'agora_user_a87ff679a2f3', '101d0d02e18383cd7b13f49e74ad5c1f', '2018-06-05 20:53:03', 3600, 4, 1),
(101, 'agora_user_e4da3b7fbbce', '2a02f9d72e3ce23518f3c172f14d83d2', '2018-06-05 22:34:48', 3600, 5, 1),
(102, 'agora_user_a87ff679a2f3', 'a27a5941f11a286fca488522b77ed723', '2018-06-06 00:11:44', 3600, 4, 1),
(103, 'agora_user_a87ff679a2f3', '63e6786f349dda55221fb1fc5a64cd40', '2018-06-06 01:25:03', 3600, 4, 1),
(104, 'agora_user_a87ff679a2f3', '4ec8fd2d7b4ec72ddef6aecf61421a71', '2018-06-07 20:52:32', 3600, 4, 1),
(105, 'agora_user_a87ff679a2f3', '8f4d039df860a4d0a34670e6cd84f74a', '2018-06-07 22:04:45', 3600, 4, 1),
(106, 'agora_user_a87ff679a2f3', '78ef751237f7b251fb130ce951c45570', '2018-06-10 19:16:54', 3600, 4, 1),
(107, 'agora_user_a87ff679a2f3', 'e0fb739afbad03813b5335bc86656405', '2018-06-10 19:41:45', 3600, 4, 1),
(108, 'agora_user_a87ff679a2f3', 'e1c7ada2a5beb5d8d7d76d5e34d62f21', '2018-06-10 22:33:00', 3600, 4, 1),
(109, 'agora_user_a87ff679a2f3', 'f22c5ce53b6c06d219aafe5923c4444b', '2018-06-10 23:09:13', 3600, 4, 1),
(110, 'agora_user_c81e728d9d4c', 'ce4719fcf042bcd63f8b9b21162c04e2', '2018-06-11 00:20:35', 3600, 2, 1),
(111, 'agora_user_a87ff679a2f3', '535b4cce2f6f1a12e3be868881bed6ba', '2018-06-11 00:20:59', 3600, 4, 1),
(112, 'agora_user_a87ff679a2f3', '73af8934f3ed5a10d82aa6d52b5a20ca', '2018-06-11 20:59:20', 3600, 4, 1),
(113, 'agora_user_c81e728d9d4c', '01fc0f711ed83640738dc59b7e8d01fd', '2018-06-11 21:00:45', 3600, 2, 1),
(114, 'agora_user_c81e728d9d4c', '98b636c805ddf5d56d3a956c570e674b', '2018-06-12 11:31:02', 3600, 2, 1),
(115, 'agora_user_a87ff679a2f3', 'e9e60a89673ef8ac5d9d83ac5fb62e35', '2018-06-12 11:40:10', 3600, 4, 1),
(116, 'agora_user_c81e728d9d4c', '13e82a23147176042e7111c704e90fe5', '2018-06-12 11:42:28', 3600, 2, 1),
(117, 'agora_user_a87ff679a2f3', '5f45ba235c19957cfa0f66c50ccd72aa', '2018-06-12 11:47:04', 3600, 4, 1),
(118, 'agora_user_c81e728d9d4c', '8ba3a167de7d5292a2bd4083f29a481b', '2018-06-12 11:49:29', 3600, 2, 1),
(119, 'agora_user_c81e728d9d4c', '6a235fd3c279ba72201d4bc0a2acaadd', '2018-06-12 21:29:44', 3600, 2, 1),
(120, 'agora_user_a87ff679a2f3', '8aed63d1ddebe9f555f6de278d7cdc72', '2018-06-12 21:29:58', 3600, 4, 1),
(121, 'agora_user_c81e728d9d4c', 'f070801c23d01d7636dd4d6ec78f6158', '2018-06-13 20:23:59', 3600, 2, 1),
(122, 'agora_user_a87ff679a2f3', '38e0dcbad800c369b9ec4fc84436984b', '2018-06-13 21:27:50', 3600, 4, 1),
(123, 'agora_user_a87ff679a2f3', '11ac9443c484e9c50ad622a2ab41bed5', '2018-06-13 21:28:56', 3600, 4, 1),
(124, 'agora_user_c81e728d9d4c', '2efb2b10b7c682671fdf7968545c9ef2', '2018-06-13 21:29:09', 3600, 2, 1),
(125, 'agora_user_c81e728d9d4c', '8601aace4d9f7a291fec650258debf1d', '2018-06-14 20:44:52', 3600, 2, 1),
(126, 'agora_user_a87ff679a2f3', '61fe0754ba10543cd350a3d82fe22af3', '2018-06-14 21:05:15', 3600, 4, 1),
(127, 'agora_user_a87ff679a2f3', 'f4b83db53f706c24019891808ef03061', '2018-06-15 00:21:20', 3600, 4, 1),
(128, 'agora_user_a87ff679a2f3', 'ab53da8ec699630a1f68cd28daa63a73', '2018-06-15 02:01:14', 3600, 4, 1),
(129, 'agora_user_c81e728d9d4c', '58da05aa10930506b67a3d2646934889', '2018-06-15 10:58:43', 3600, 2, 1),
(130, 'agora_user_a87ff679a2f3', '9f3533a083a5bddf1a991e8dc34254bc', '2018-06-15 11:09:35', 3600, 4, 1),
(131, 'agora_user_c81e728d9d4c', '74571c19bc7e58b33e5b79bd9fbf4a2b', '2018-06-15 11:20:26', 3600, 2, 1),
(132, 'agora_user_a87ff679a2f3', 'dabc0a99d923c0098f3705e58204ceef', '2018-06-17 02:07:34', 3600, 4, 1),
(133, 'agora_user_a87ff679a2f3', '19181438637023cf4fc645cbeedc9a8d', '2018-06-17 02:46:26', 3600, 4, 1),
(134, 'agora_user_c81e728d9d4c', '3828f68bd105b8f27e371015327909d1', '2018-06-17 02:58:11', 3600, 2, 1),
(135, 'agora_user_c81e728d9d4c', '90608b54eb14e0d82eb4f5440f9af19e', '2018-06-17 02:58:20', 3600, 2, 1),
(136, 'agora_user_c81e728d9d4c', '7f66694b60e0c34929e1475267985e8c', '2018-06-17 02:58:23', 3600, 2, 1),
(137, 'agora_user_c81e728d9d4c', 'fdaaa67fa8d9d7bb7b90a25ce1fe040b', '2018-06-17 02:58:46', 3600, 2, 1),
(138, 'agora_user_c81e728d9d4c', 'dd5c099a1f36d23f1b92b5f45afcc322', '2018-06-17 03:00:08', 3600, 2, 1),
(139, 'agora_user_a87ff679a2f3', 'a40b74eec409a1ca48a89e8cc28e9170', '2018-06-17 03:28:04', 3600, 4, 1),
(140, 'agora_user_a87ff679a2f3', 'bc8475249ef72162501a6646909b0ff8', '2018-06-17 03:33:54', 3600, 4, 1),
(141, 'agora_user_a87ff679a2f3', '76c960d12b42f5cc30f2881f8057eb1c', '2018-06-17 03:37:12', 3600, 4, 1),
(142, 'agora_user_e4da3b7fbbce', '21f06fd8d59445ebeaa1f98b44e031d2', '2018-06-17 03:40:04', 3600, 5, 1),
(143, 'agora_user_a87ff679a2f3', '057ac43634e7f4bf77eef6964ffddc93', '2018-06-17 03:43:59', 3600, 4, 1),
(144, 'agora_user_1679091c5a88', 'f02f3c1c0113ccc28361bf94afdfe395', '2018-06-17 16:00:28', 3600, 6, 1),
(145, 'agora_user_c81e728d9d4c', '4725e1784594aaa41d227eb90676b398', '2018-06-17 16:16:06', 3600, 2, 1),
(146, 'agora_user_a87ff679a2f3', '4312502b27403f676c0f07cca8a9791d', '2018-06-17 16:16:18', 3600, 4, 1),
(147, 'agora_user_c81e728d9d4c', 'e8913a64b96e3788d836358b8eaa557c', '2018-06-17 16:31:34', 3600, 2, 1),
(148, 'agora_user_c81e728d9d4c', 'b214f01d8dcd666f9b37ac820f4464f3', '2018-06-18 20:52:42', 3600, 2, 1),
(149, 'agora_user_c81e728d9d4c', 'd0f336d8306cae03fa0c710a778ec28d', '2018-06-20 20:57:48', 3600, 2, 1),
(150, 'agora_user_c4ca4238a0b9', '83f6cd8612d5ce4f38317b507e8975cb', '2018-06-20 22:52:11', 3600, 1, 1),
(151, 'agora_user_c4ca4238a0b9', 'e79aebe796d9b00011afda0fa23cf182', '2018-06-20 23:00:59', 3600, 1, 1),
(152, 'agora_user_c4ca4238a0b9', '51ce5b14d24730c32051700b44240019', '2018-06-20 23:02:12', 3600, 1, 1),
(153, 'agora_user_c81e728d9d4c', 'd53fc23295e0032738f27f9d21f4b45a', '2018-06-21 00:07:42', 3600, 2, 1),
(154, 'agora_user_c4ca4238a0b9', '5364c450ea2d1556923461a7c4164734', '2018-06-21 00:09:29', 3600, 1, 1),
(155, 'agora_user_c81e728d9d4c', '5eb4e354be4745f3d20cc407f0d06d99', '2018-06-21 09:44:58', 3600, 2, 1),
(156, 'agora_user_c4ca4238a0b9', 'eb98c020d9406bf6501d70ecfe7b28c6', '2018-06-21 09:46:50', 3600, 1, 1),
(157, 'agora_user_c81e728d9d4c', '2e1c34e965c435cc080c2ca55cf8bfbb', '2018-06-21 09:51:12', 3600, 2, 1),
(158, 'agora_user_c81e728d9d4c', '19c35096d6d6564ada50e3407dba3a3e', '2018-06-21 11:02:07', 3600, 2, 1),
(159, 'agora_user_a87ff679a2f3', 'e78545dd7107397cecd1c07ae87cd567', '2018-06-22 21:27:03', 3600, 4, 1),
(160, 'agora_user_a87ff679a2f3', 'ef7bdcbd2ec62e35823139ddca97af36', '2018-06-23 01:40:01', 3600, 4, 1),
(161, 'agora_user_c81e728d9d4c', 'e5f141748e98de7a42f569601e0441a3', '2018-06-23 01:40:32', 3600, 2, 1),
(162, 'agora_user_a87ff679a2f3', '8afbda6f337519f615c5fb5d3bf6505c', '2018-06-23 02:07:36', 3600, 4, 1),
(163, 'agora_user_c81e728d9d4c', '1f1915205e81c61ac092bd46ab5eba0b', '2018-06-23 02:41:28', 3600, 2, 1),
(164, 'agora_user_a87ff679a2f3', 'ca73128355d2cf338292dcfb556cf859', '2018-06-23 02:42:16', 3600, 4, 1),
(165, 'agora_user_c81e728d9d4c', 'e785d7a84011246c86ec7dceede15e30', '2018-06-23 02:44:28', 3600, 2, 1),
(166, 'agora_user_a87ff679a2f3', 'e8809e4efb8870b9c39dc7c075b6016b', '2018-06-23 04:03:02', 3600, 4, 1),
(167, 'agora_user_c81e728d9d4c', '5b315e91c15c0b1b66ff631d7d77f331', '2018-06-23 04:06:34', 3600, 2, 1),
(168, 'agora_user_a87ff679a2f3', '365300e16952fc5d6f34ae2415a9040f', '2018-06-25 09:48:14', 3600, 4, 1),
(169, 'agora_user_a87ff679a2f3', '7f5a840fe569214f71f46c3446f54f7c', '2018-06-26 09:25:51', 3600, 4, 1),
(170, 'agora_user_a87ff679a2f3', 'd0d33726ff17e7d5bb0840c4ab9f2fc3', '2018-06-26 09:26:00', 3600, 4, 1),
(171, 'agora_user_a87ff679a2f3', '460519a7764722400e13e05fd973d8ab', '2018-06-27 20:41:09', 3600, 4, 1),
(172, 'agora_user_a87ff679a2f3', '06563ecbd7e1de6d9daeea08e19f5f93', '2018-06-29 13:38:06', 3600, 4, 1),
(173, 'agora_user_a87ff679a2f3', '394015ddafb8606603d39465c9440866', '2018-06-29 13:51:07', 3600, 4, 1),
(174, 'agora_user_c4ca4238a0b9', 'c57fe473263144e59f1b7eff741d4853', '2018-06-29 14:06:27', 3600, 1, 1),
(175, 'agora_user_a87ff679a2f3', '8ad1c58cbfb8eea2f8e45c757f7f2c0f', '2018-06-29 14:21:02', 3600, 4, 1),
(176, 'agora_user_c81e728d9d4c', '18a6e6934fd361103075a0dd5551dd0c', '2018-06-29 22:13:13', 3600, 2, 1),
(177, 'agora_user_a87ff679a2f3', 'adca59d77275d7d8237b76e42b9ef1a3', '2018-06-30 02:34:44', 3600, 4, 1),
(178, 'agora_user_c81e728d9d4c', '09a271363efef15eb5e28d446d6cbedc', '2018-06-30 02:37:45', 3600, 2, 1),
(179, 'agora_user_a87ff679a2f3', 'be11dbeddf865234bb8f9c6dc8abb8c6', '2018-06-30 02:40:03', 3600, 4, 1),
(180, 'agora_user_c81e728d9d4c', '52474a8b2605ddd062c3cb2d8cd10708', '2018-06-30 02:46:53', 3600, 2, 1),
(181, 'agora_user_a87ff679a2f3', '7abe155cc0d2b23b11936a8abca281c4', '2018-06-30 18:59:21', 3600, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscription`
--

CREATE TABLE `tbl_subscription` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_subscription_id` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `subscription_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cancelled_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subscription`
--

INSERT INTO `tbl_subscription` (`subscription_id`, `user_id`, `user_subscription_id`, `status`, `subscription_date`, `cancelled_date`) VALUES
(2, 2, 'ddycww', 1, '2018-05-12 20:37:00', '0000-00-00 00:00:00'),
(3, 2, '8wfv2w', 1, '2018-05-12 21:02:48', '0000-00-00 00:00:00'),
(4, 2, 'c5jgj2', 1, '2018-05-13 00:42:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscription_trials`
--

CREATE TABLE `tbl_subscription_trials` (
  `subscription_trial_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `trial_date_start` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subscription_trials`
--

INSERT INTO `tbl_subscription_trials` (`subscription_trial_id`, `user_id`, `trial_date_start`) VALUES
(3, 2, '2018-05-13 00:34:28'),
(4, 2, '2018-05-13 00:34:38'),
(5, 3, '2018-05-17 21:17:31'),
(6, 5, '2018-06-05 22:34:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `trans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `membership_plan_id` int(11) NOT NULL,
  `subscription_id` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `user_payment_id` varchar(100) NOT NULL,
  `transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`trans_id`, `user_id`, `membership_plan_id`, `subscription_id`, `transaction_id`, `user_payment_id`, `transaction_date`) VALUES
(1, 2, 2, 'ddycww', 'bk61v8xa', '872121981', '2018-05-12 20:37:00'),
(2, 2, 2, '8wfv2w', '8aqetyv4', '518210836', '2018-05-12 21:02:48'),
(3, 2, 2, 'c5jgj2', '2fhjwah4', '552402883', '2018-05-13 00:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_question`
--

CREATE TABLE `tbl_trans_question` (
  `trans_question_id` int(11) NOT NULL,
  `trans_question_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_question_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_country_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_trans_question`
--

INSERT INTO `tbl_trans_question` (`trans_question_id`, `trans_question_title`, `trans_question_description`, `trans_country_id`, `question_id`, `date_added`) VALUES
(12, '数学', 'テストコメント １＋２＋３', 112, 11, '2018-06-22 09:43:30'),
(13, 'test', 'test', 1, 35, '2018-06-23 00:16:28'),
(14, ' 彼女の', '1 + 2 + 3', 112, 35, '2018-06-23 00:43:38'),
(15, ' dsfds', 'cxvxcvcx', 18, 35, '2018-06-23 00:47:34'),
(16, 'xdsf', 'sfsdf', 18, 35, '2018-06-23 00:48:47'),
(19, 'asd', 'asdsad', 1, 35, '2018-06-23 00:50:44'),
(21, 'sad', 'sadad', 103, 35, '2018-06-23 00:52:37'),
(24, 'JP', 'JP 10-6', 112, 27, '2018-06-23 01:35:40'),
(25, 'JP', 'JP 1-3', 112, 28, '2018-06-23 01:36:30'),
(26, 'JP', 'JP - 50-2', 112, 29, '2018-06-23 01:37:40'),
(27, 'Tagalog', 'Sampu bawas anim', 174, 27, '2018-06-23 02:08:41'),
(28, 'FIL', 'Isa bawas tatlo', 174, 28, '2018-06-23 02:43:20'),
(29, 'Fil', 'Limampu bawas lima', 174, 29, '2018-06-23 02:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_question_answers`
--

CREATE TABLE `tbl_trans_question_answers` (
  `trans_question_answers_id` int(11) NOT NULL,
  `trans_question_id` int(11) NOT NULL,
  `question_choice` text COLLATE utf8mb4_unicode_ci,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_trans_question_answers`
--

INSERT INTO `tbl_trans_question_answers` (`trans_question_answers_id`, `trans_question_id`, `question_choice`, `date_added`) VALUES
(9, 12, 'a:4:{i:0;s:1:\"6\";i:1;s:1:\"5\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', '2018-06-22 09:43:30'),
(10, 13, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', '2018-06-23 00:16:28'),
(11, 14, 'a:4:{i:0;s:9:\"1 + 2 + 3\";i:1;s:9:\"1 + 2 + 3\";i:2;s:9:\"1 + 2 + 3\";i:3;s:9:\"1 + 2 + 3\";}', '2018-06-23 00:43:38'),
(12, 15, 'a:4:{i:0;s:2:\"cx\";i:1;s:3:\"dsf\";i:2;s:3:\"cxv\";i:3;s:3:\"cxv\";}', '2018-06-23 00:47:34'),
(13, 16, 'a:4:{i:0;s:3:\"sdf\";i:1;s:4:\"dsfs\";i:2;s:3:\"sdf\";i:3;s:4:\"sdfs\";}', '2018-06-23 00:48:47'),
(14, 17, 'a:4:{i:0;s:4:\"dsad\";i:1;s:5:\"asdas\";i:2;s:4:\"dasd\";i:3;s:3:\"sad\";}', '2018-06-23 00:49:23'),
(15, 18, 'a:4:{i:0;s:4:\"dsad\";i:1;s:5:\"asdas\";i:2;s:4:\"dasd\";i:3;s:3:\"sad\";}', '2018-06-23 00:49:37'),
(16, 19, 'a:4:{i:0;s:3:\"asd\";i:1;s:3:\"asd\";i:2;s:3:\"asd\";i:3;s:5:\"asdsa\";}', '2018-06-23 00:50:44'),
(17, 20, 'a:4:{i:0;s:3:\"ada\";i:1;s:4:\"asda\";i:2;s:4:\"sdad\";i:3;s:3:\"ada\";}', '2018-06-23 00:52:10'),
(18, 21, 'a:4:{i:0;s:4:\"adad\";i:1;s:5:\"asdas\";i:2;s:5:\"dasda\";i:3;s:6:\"asdasd\";}', '2018-06-23 00:52:37'),
(19, 22, 'a:4:{i:0;s:4:\"asda\";i:1;s:3:\"das\";i:2;s:4:\"asda\";i:3;s:3:\"asd\";}', '2018-06-23 00:53:27'),
(20, 23, 'a:4:{i:0;s:4:\"asda\";i:1;s:3:\"das\";i:2;s:4:\"asda\";i:3;s:3:\"asd\";}', '2018-06-23 00:53:36'),
(21, 24, 'a:4:{i:0;s:1:\"4\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}', '2018-06-23 01:35:40'),
(22, 25, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', '2018-06-23 01:36:30'),
(23, 26, 'a:4:{i:0;s:4:\"JP 2\";i:1;s:4:\"JP 3\";i:2;s:5:\"JP 48\";i:3;s:1:\"6\";}', '2018-06-23 01:37:40'),
(24, 27, 'a:4:{i:0;s:4:\"Apat\";i:1;s:3:\"Isa\";i:2;s:6:\"dalawa\";i:3;s:5:\"tatlo\";}', '2018-06-23 02:08:41'),
(25, 28, 'a:4:{i:0;s:3:\"Isa\";i:1;s:6:\"Dalawa\";i:2;s:5:\"Tatlo\";i:3;s:4:\"Apat\";}', '2018-06-23 02:43:20'),
(26, 29, 'a:4:{i:0;s:6:\"Dalawa\";i:1;s:5:\"Tatlo\";i:2;s:16:\"Apat na put walo\";i:3;s:4:\"Anim\";}', '2018-06-23 02:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(45) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_level` int(11) NOT NULL DEFAULT '1',
  `member_payment_id` varchar(100) NOT NULL COMMENT 'ID associated to the user when he/she pays',
  `registration_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_login`, `user_pass`, `user_email`, `user_level`, `member_payment_id`, `registration_date`, `active`) VALUES
(1, 'erukei18', '16d7a4fca7442dda3ad93c9a726597e4', 'johndoe@gmail.com', 0, '', '2018-04-18 14:04:25', 1),
(2, 'keysl183', '16d7a4fca7442dda3ad93c9a726597e4', 'marcjhon18@gmail.com', 2, '552402883', '2018-04-18 21:59:37', 1),
(3, 'keysl183+04', '16d7a4fca7442dda3ad93c9a726597e4', 'marcjhon18+02@gmail.com', 1, '', '2018-05-17 21:05:58', 1),
(4, 'teacher', '16d7a4fca7442dda3ad93c9a726597e4', 'teacher@agoraschool.com', 4, '', '2018-05-27 19:37:11', 1),
(5, 'keysl+001', '16d7a4fca7442dda3ad93c9a726597e4', 'mlawingco.ivp+001@gmail.com', 1, '', '2018-06-05 22:33:27', 1),
(6, 'keysl+002', '16d7a4fca7442dda3ad93c9a726597e4', 'mlawingco.ivp+002@gmail.com', 0, '', '2018-06-17 03:45:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_detail`
--

CREATE TABLE `tbl_users_detail` (
  `user_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` int(1) DEFAULT '1',
  `school` varchar(500) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `education_level` int(11) NOT NULL,
  `profile_image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users_detail`
--

INSERT INTO `tbl_users_detail` (`user_detail_id`, `user_id`, `fname`, `lname`, `mname`, `birthday`, `age`, `gender`, `school`, `country_id`, `zip_code`, `education_level`, `profile_image`) VALUES
(1, 2, 'KL3', 'Lawingco', 'M', '2018-12-18', 22, 1, 'Pamantasan ng Lungsod ng Valenzuela', 5, '1231', 5, 'Student_Avatar5.jpg'),
(4, 31, 'KL', 'Lawingco', 'M', '2018-12-18', 21, 1, NULL, 1, '123', 1, 'prof_image-20180502-5ae9155d6d89b.jpeg'),
(5, 34, 'KL+05', 'Lawingco', NULL, NULL, 22, 1, NULL, 3, '2', 1, NULL),
(6, 33, 'KL+04', 'Lawingco', NULL, NULL, 22, 1, NULL, 93, '1234', 1, NULL),
(7, 3, 'KL', 'Lawingco', NULL, NULL, 22, 1, NULL, 11, '123', 3, NULL),
(8, 4, 'Teacher', 'account', NULL, '1995-12-18', 21, 1, 'TEST SCHOOL', 248, '1234', 5, 'Student_Avatar5.jpg'),
(9, 5, 'KL', 'Lawingco', NULL, NULL, 21, 1, NULL, 1, '13', 5, NULL),
(10, 6, 'KL', 'Lawingco', NULL, NULL, 22, 1, NULL, 174, '1234', 5, NULL),
(11, 1, 'Eru', 'Lawingco', 'M', '2018-12-18', 22, 1, 'PLV', 2, '1231', 1, 'prof_image_20180621-5b2b6af1c867b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_level`
--

CREATE TABLE `tbl_user_level` (
  `user_level_id` int(11) NOT NULL,
  `level_name` varchar(45) NOT NULL,
  `level_description` varchar(255) DEFAULT NULL,
  `level_payment_id` varchar(50) NOT NULL,
  `level_price` decimal(10,0) DEFAULT NULL,
  `level_validity` int(11) DEFAULT NULL COMMENT 'In number of months',
  `level_validity_suffix` varchar(45) DEFAULT NULL COMMENT 'Days\nMonths\nYears'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_level`
--

INSERT INTO `tbl_user_level` (`user_level_id`, `level_name`, `level_description`, `level_payment_id`, `level_price`, `level_validity`, `level_validity_suffix`) VALUES
(1, 'Free Trial', 'Free Trial', 'FR101', '0', 5, 'days'),
(2, 'Monthly', 'Monthly', '2bgb', '15', 1, 'month'),
(3, 'Annual', 'Annual', 'g8tm', '100', 1, 'year');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_activation_keys`
--
ALTER TABLE `tbl_activation_keys`
  ADD PRIMARY KEY (`activation_id`);

--
-- Indexes for table `tbl_assignment`
--
ALTER TABLE `tbl_assignment`
  ADD PRIMARY KEY (`assignment_id`);

--
-- Indexes for table `tbl_assingment_submission`
--
ALTER TABLE `tbl_assingment_submission`
  ADD PRIMARY KEY (`assign_sumission_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `tbl_course_section`
--
ALTER TABLE `tbl_course_section`
  ADD PRIMARY KEY (`course_section_id`);

--
-- Indexes for table `tbl_course_section_item`
--
ALTER TABLE `tbl_course_section_item`
  ADD PRIMARY KEY (`section_item_id`);

--
-- Indexes for table `tbl_course_section_progress`
--
ALTER TABLE `tbl_course_section_progress`
  ADD PRIMARY KEY (`section_progress_id`);

--
-- Indexes for table `tbl_discussion`
--
ALTER TABLE `tbl_discussion`
  ADD PRIMARY KEY (`discussion_id`);

--
-- Indexes for table `tbl_education_level`
--
ALTER TABLE `tbl_education_level`
  ADD PRIMARY KEY (`edu_level_id`);

--
-- Indexes for table `tbl_enroll`
--
ALTER TABLE `tbl_enroll`
  ADD PRIMARY KEY (`enroll_id`);

--
-- Indexes for table `tbl_mail_log`
--
ALTER TABLE `tbl_mail_log`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_posts`
--
ALTER TABLE `tbl_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tbl_question`
--
ALTER TABLE `tbl_question`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `tbl_question_answers`
--
ALTER TABLE `tbl_question_answers`
  ADD PRIMARY KEY (`question_answers_id`);

--
-- Indexes for table `tbl_question_answer_mode`
--
ALTER TABLE `tbl_question_answer_mode`
  ADD PRIMARY KEY (`question_answer_mode_id`);

--
-- Indexes for table `tbl_question_bank`
--
ALTER TABLE `tbl_question_bank`
  ADD PRIMARY KEY (`question_bank_id`);

--
-- Indexes for table `tbl_question_type`
--
ALTER TABLE `tbl_question_type`
  ADD PRIMARY KEY (`question_type_id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tbl_subscription`
--
ALTER TABLE `tbl_subscription`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `tbl_subscription_trials`
--
ALTER TABLE `tbl_subscription_trials`
  ADD PRIMARY KEY (`subscription_trial_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `tbl_trans_question`
--
ALTER TABLE `tbl_trans_question`
  ADD PRIMARY KEY (`trans_question_id`);

--
-- Indexes for table `tbl_trans_question_answers`
--
ALTER TABLE `tbl_trans_question_answers`
  ADD PRIMARY KEY (`trans_question_answers_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_users_detail`
--
ALTER TABLE `tbl_users_detail`
  ADD PRIMARY KEY (`user_detail_id`);

--
-- Indexes for table `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  ADD PRIMARY KEY (`user_level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_activation_keys`
--
ALTER TABLE `tbl_activation_keys`
  MODIFY `activation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_assignment`
--
ALTER TABLE `tbl_assignment`
  MODIFY `assignment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_assingment_submission`
--
ALTER TABLE `tbl_assingment_submission`
  MODIFY `assign_sumission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_course_section`
--
ALTER TABLE `tbl_course_section`
  MODIFY `course_section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_course_section_item`
--
ALTER TABLE `tbl_course_section_item`
  MODIFY `section_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_course_section_progress`
--
ALTER TABLE `tbl_course_section_progress`
  MODIFY `section_progress_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tbl_discussion`
--
ALTER TABLE `tbl_discussion`
  MODIFY `discussion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbl_education_level`
--
ALTER TABLE `tbl_education_level`
  MODIFY `edu_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_enroll`
--
ALTER TABLE `tbl_enroll`
  MODIFY `enroll_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_mail_log`
--
ALTER TABLE `tbl_mail_log`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_posts`
--
ALTER TABLE `tbl_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_question`
--
ALTER TABLE `tbl_question`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `tbl_question_answers`
--
ALTER TABLE `tbl_question_answers`
  MODIFY `question_answers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `tbl_question_answer_mode`
--
ALTER TABLE `tbl_question_answer_mode`
  MODIFY `question_answer_mode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_question_bank`
--
ALTER TABLE `tbl_question_bank`
  MODIFY `question_bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_question_type`
--
ALTER TABLE `tbl_question_type`
  MODIFY `question_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `tbl_subscription`
--
ALTER TABLE `tbl_subscription`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_subscription_trials`
--
ALTER TABLE `tbl_subscription_trials`
  MODIFY `subscription_trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_trans_question`
--
ALTER TABLE `tbl_trans_question`
  MODIFY `trans_question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_trans_question_answers`
--
ALTER TABLE `tbl_trans_question_answers`
  MODIFY `trans_question_answers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users_detail`
--
ALTER TABLE `tbl_users_detail`
  MODIFY `user_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  MODIFY `user_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
