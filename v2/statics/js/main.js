function isEventSupported(eventName) {
    var el = document.createElement('div');
    eventName = 'on' + eventName;
    var isSupported = (eventName in el);
    if (!isSupported) {
        el.setAttribute(eventName, 'return;');
        isSupported = typeof el[eventName] == 'function';
    }
    el = null;
    return isSupported;
}


$(document).ready(function(){

    /*Index Slider */
    var width = $( window ).width();
    $('.bxslider').bxSlider({
        auto: true,
        adaptiveHeight: true,
        adaptiveHeightSpeed: 750,
        autoControls: false,
        stopAutoOnClick: true,
        pager: false,
        responsive:true,
        slideWidth: width
      });
    
    /*Index Course Collection*/
    var itemCount = 13, activeScroll = 0, countScroll = 0,toIncrease=59;
    setInterval(function() {
        if($('#index-course-carousel').is(":hover")) return;
        
        if(countScroll == (itemCount - 10))
        {                  
            activeScroll = 0;
            countScroll = 0;
            $('.course-carousel').animate({scrollTop: 0});
        }
        else
        {
            if(countScroll == 0)
            {
                toIncrease = 45;
            }else{
                toIncrease = 59;
            }
            activeScroll += toIncrease;
            countScroll += 1;
            $('.course-carousel').animate({scrollTop: activeScroll},400,'swing');            
        }
    }, 1000);

    //Index Course Categories Scrollable
    var wheelEvent = isEventSupported('mousewheel') ? 'mousewheel' : 'wheel';
    $('#index-course-carousel').bind(wheelEvent, function(e){
        var oEvent = e.originalEvent,
        delta  = oEvent.deltaY || oEvent.wheelDelta;
        if (delta > 0) {
           
            // $('.course-carousel').animate({scrollTop: toIncrease});
        } else {
            // $('.course-carousel').animate({scrollTop: toIncrease+59});
        }
        e.preventDefault();
    });

    //Index Featured Slider
    var width = $( window ).width();
    var toShowSlides = 0;
    if(width >= 992)
    {
        toShowSlides = 5
    }else if(width >=768)
    {
        toShowSlides = 3
    }else if(width > 576)
    {
        toShowSlides = 2;
    }
    else
    {
        toShowSlides = 1;
    }

    $('.featuredslider').slick({
        slidesToShow: toShowSlides,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        accessibility:false
    });

    //Index Review Sliders
    $('.reviewsliderslick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        autoPlay:false,
        dots:true,
        arrows:false,
        mobileFirst:true,
        focusOnSelect:false,
        accessibility:false
    });
});


